DELIMITER $$
CREATE DEFINER=`aravindan`@`%` PROCEDURE `sp_load_bi_daily_mealplan_detail`()
BEGIN

/* 2019-10-09  Aravindan -  Initial Version*/ 

/*log the starttime*/
update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_daily_mealplan_detail'
;

DROP temporary table IF EXISTS twins.tmp_daily_mealplan;

create temporary table tmp_daily_mealplan
(
	planname varchar(100) not null, 
	mealtype varchar(50) not null,
	measuregroup varchar(50) null, 
	measurecategory varchar(25) null, 
	foodlabel varchar(50) null, 
	E5foodGrading varchar(10) null, 
	food_measure varchar(50) null, 
	quantity double null,
	day varchar(15) null, 
	measurevalue decimal(7,2) null
);

insert into tmp_daily_mealplan
select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity,concat('Day ',a.day) as day, 
convert(sum(a.netcarb_measurevalue), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L1' as measureGroup,'Netcarb' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then typicalconsumedquantity * (carb - fibre) 
			else (typicalconsumedquantity * d.conversionRate * (carb - fibre)) end as netcarb_measureValue,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.calories)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Calories' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * calories) 
													  else  (typicalconsumedquantity * d.conversionRate * calories) end as calories,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.ngc_measureValue)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'NGC' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * (IFNULL((((carb - fibre) * glycemicIndex) / 55), 0)))  
	 else (typicalconsumedquantity * d.conversionrate * (IFNULL((((carb - fibre) * glycemicIndex) / 55), 0))) end as ngc_measureValue,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.fibre)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Fibre' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * fibre) 
													  else (typicalconsumedquantity * d.conversionrate * fibre) end as fibre,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.fat)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Fat' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * fat) 
						else (typicalconsumedquantity * d.conversionrate * fat) end as fat,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.protein)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Protein' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * protein) 
													  else (typicalconsumedquantity * d.conversionRate * protein) end as protein,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.carb)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Total carb' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * carb) 
													  else (typicalconsumedquantity * d.conversionrate * carb) end as carb,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 
-- Next few sections  are for L3 measures i.e. Sodium, potassium, Magnesium, Calcium, Chromium, Omega3, Omega6, ALA, Q10, Biotin, Flavonoids
select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.sodium)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L3' as measureGroup,'Sodium' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * sodium) 
													  else (typicalconsumedquantity * d.conversionrate * sodium) end as sodium,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.potassium)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L3' as measureGroup,'Potassium' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * potassium) 
													  else (typicalconsumedquantity * d.conversionrate * potassium) end as potassium,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.magnesium)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L3' as measureGroup,'Magnesium' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * magnesium) 
													  else (typicalconsumedquantity * d.conversionrate * magnesium) end as magnesium,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.calcium)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L3' as measureGroup,'Calcium' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * calcium) 
													  else (typicalconsumedquantity * d.conversionrate * calcium) end as calcium,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.chromium)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L3' as measureGroup,'Chromium' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * chromium) 
													  else (typicalconsumedquantity * d.conversionrate * chromium) end as chromium,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.omega3)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L3' as measureGroup,'Omega3' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * omega3) 
													  else (typicalconsumedquantity * d.conversionrate * omega3) end as omega3,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.omega6)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L3' as measureGroup,'Omega6' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * omega6) 
													  else (typicalconsumedquantity * d.conversionrate * omega6) end as omega6,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.alphaLipoicAcid)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L3' as measureGroup,'ALA' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * alphaLipoicAcid) 
													  else (typicalconsumedquantity * d.conversionrate * alphaLipoicAcid) end as alphaLipoicAcid,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.q10)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L3' as measureGroup,'q10' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * q10) 
													  else (typicalconsumedquantity * d.conversionrate * q10) end as q10,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.biotin)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L3' as measureGroup,'Biotin' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * biotin) 
													  else (typicalconsumedquantity * d.conversionrate * biotin) end as biotin,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.flavonoids)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L3' as measureGroup,'Flavonoids' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * flavonoids) 
													  else (typicalconsumedquantity * d.conversionrate * flavonoids) end as flavonoids,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

-- Next few sections are for L6 - Vitamins, i.e. Vitamin A, C, D, E, K, B1, B12, B2, B3, B5, B6, Folate
select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.vitaminA)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Vitamins' as measureGroup,'Vitamin A' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminA) 
													  else (typicalconsumedquantity * d.conversionrate * vitaminA) end as vitaminA,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.vitaminC)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Vitamins' as measureGroup,'Vitamin C' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminC) 
													  else (typicalconsumedquantity * d.conversionrate * vitaminC) end as vitaminC,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.vitaminD)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Vitamins' as measureGroup,'Vitamin D' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminD) 
													  else (typicalconsumedquantity * d.conversionrate * vitaminD) end as vitaminD,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.vitaminE)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Vitamins' as measureGroup,'Vitamin E' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminE) 
													  else (typicalconsumedquantity * d.conversionrate * vitaminE) end as vitaminE,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.vitaminK)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Vitamins' as measureGroup,'Vitamin K' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminK) 
													  else (typicalconsumedquantity * d.conversionrate * vitaminK) end as vitaminK,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.vitaminB1)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Vitamins' as measureGroup,'Vitamin B1' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminB1) 
													  else (typicalconsumedquantity * d.conversionrate * vitaminB1) end as vitaminB1,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.vitaminB12)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Vitamins' as measureGroup,'Vitamin B12' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminB12) 
													  else (typicalconsumedquantity * d.conversionrate * vitaminB12) end as vitaminB12,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.vitaminB2)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Vitamins' as measureGroup,'Vitamin B2' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminB2) 
													  else (typicalconsumedquantity * d.conversionrate * vitaminB2) end as vitaminB2,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.vitaminB3)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Vitamins' as measureGroup,'Vitamin B3' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminB3) 
													  else (typicalconsumedquantity * d.conversionrate * vitaminB3) end as vitaminB3,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.vitaminB5)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Vitamins' as measureGroup,'Vitamin B5' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminB5) 
													  else (typicalconsumedquantity * d.conversionrate * vitaminB5) end as vitaminB5,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.vitaminB6)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Vitamins' as measureGroup,'Vitamin B6' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminB6) 
													  else (typicalconsumedquantity * d.conversionrate * vitaminB6) end as vitaminB6,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.folate)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Vitamins' as measureGroup,'Folate' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * folate) 
													  else (typicalconsumedquantity * d.conversionrate * folate) end as folate,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

-- Next few sections are for the L6 Nutraceuticals, Minerals - i.e. Copper, Iron, Zinc, Manganese, Phosporous, Selenium
union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.copper)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Nutraceuticals, Mineral' as measureGroup,'Copper' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * copper) 
													  else (typicalconsumedquantity * d.conversionrate * copper) end as copper,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.iron)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Nutraceuticals, Mineral' as measureGroup,'Iron' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * iron) 
													  else (typicalconsumedquantity * d.conversionrate * iron) end as iron,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.zinc)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Nutraceuticals, Mineral' as measureGroup,'Zinc' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * zinc) 
													  else (typicalconsumedquantity * d.conversionrate * zinc) end as zinc,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.manganese)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Nutraceuticals, Mineral' as measureGroup,'Manganese' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * manganese) 
													  else (typicalconsumedquantity * d.conversionrate * manganese) end as manganese,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.phosphorus)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Nutraceuticals, Mineral' as measureGroup,'Phosphorus' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * phosphorus) 
													  else (typicalconsumedquantity * d.conversionrate * phosphorus) end as phosphorus,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all 

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.selenium)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L6-Nutraceuticals, Mineral' as measureGroup,'Selenium' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * selenium) 
													  else (typicalconsumedquantity * d.conversionrate * selenium) end as selenium,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel
;

truncate table bi_daily_mealplan_detail;

insert into bi_daily_mealplan_detail
select planname, mealtype, measuregroup, measurecategory, foodlabel, E5foodGrading, food_measure, quantity, day, measurevalue
from tmp_daily_mealplan
;

drop temporary table tmp_daily_mealplan
;

/*log the endtime*/
update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_bi_daily_mealplan_detail'
;

END$$
DELIMITER ;
