DELIMITER $$
CREATE DEFINER=`aravindan`@`%` PROCEDURE `sp_load_bi_daily_nutritionplan_patient_detail`()
BEGIN

/*
20190716 - Aravindan - food_measure and quantity columns are replaced to use the measurementType and measurementAmount columns from foods table.
20190806 - Aravindan - measurementType and measurementAmount columns were replaced by the new logic (using typicalconsumedqty, primarycolloquailmeasure). 
20200818 - Aravindan - modified the process for faster run time. It was running for around 10 mins so tweeked the code to run efficiently. 
*/

/*log the starttime*/
update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_daily_nutritionplan_patient_detail'
;

drop temporary table if exists tmp_nutrition_plan_detail; 

-- build a temp table with all the food micro nutrient values for the 3 week period (2 weeks in the past and 1 in future) from current day for all active patients. 
-- This can be used to load the data in the main table, this would also running the process faster. 
create temporary table tmp_nutrition_plan_detail 
as 
select distinct a.clientid, a.mealdate, a.mealtype, case when y.name = 'no meal plan' then 'Not Available' else c.foodLabel end as new_foodlabel,
case when y.name = 'no meal plan' then 'Not Available' else c.measurementType end as food_measure,
case when y.name = 'no meal plan' then 'Not Available' else c.measurementAmount end as quantity,
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.calories)), decimal(7,2)) end as calories, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert(sum(netcarb), decimal(7,2)) end as netcarb,
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.fibre)), decimal(7,2)) end as fibre, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert(sum(netGlycemicIndex), decimal(7,2)) end as netGlycemicIndex,
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.fat)), decimal(7,2)) end as fat,
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.protein)), decimal(7,2)) end as protein, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.carb)), decimal(7,2)) end as totalcarb, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.sodium)), decimal(7,2)) end as sodium, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.Potassium)), decimal(7,2)) end as Potassium, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.Magnesium)), decimal(7,2)) end as Magnesium,
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.Calcium)), decimal(7,2))end as Calcium, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.chromium)), decimal(7,2)) end as chromium,
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.Omega3)), decimal(7,2)) end as Omega3, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.Omega6)), decimal(7,2)) end as Omega6, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.ala)), decimal(7,2)) end as ALA, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.q10)), decimal(7,2)) end as q10,
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.Biotin)), decimal(7,2)) end as Biotin,
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.flavonoids)), decimal(7,2)) end as flavonoids,
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.vitaminA)), decimal(7,2)) end as vitaminA,
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.vitaminC)), decimal(7,2)) end as vitaminC,
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.vitaminD)), decimal(7,2)) end as vitaminD, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.vitaminE)), decimal(7,2)) end as vitaminE, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.vitaminK)), decimal(7,2)) end as vitaminK, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.vitaminB1)), decimal(7,2)) end as vitaminB1, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.vitaminB12)), decimal(7,2)) end as vitaminB12, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.vitaminB2)), decimal(7,2)) end as vitaminB2, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.vitaminB3)), decimal(7,2)) end as vitaminB3,
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.vitaminB5)), decimal(7,2)) end as vitaminB5, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.vitaminB6)), decimal(7,2)) end as vitaminB6,
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.Folate)), decimal(7,2)) end as Folate, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.Copper)), decimal(7,2)) end as Copper, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.Iron)), decimal(7,2)) end as iron, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.Zinc)), decimal(7,2)) end as Zinc, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.Manganese)), decimal(7,2)) end as Manganese, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.Phosphorus)), decimal(7,2)) end as Phosphorus, 
case when y.name = 'no meal plan' then 'no meal plan selected' else convert((sum(c.Selenium)), decimal(7,2)) end as Selenium, 
case when y.name = 'no meal plan' then 'Not Available' else 
(CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END) end as E5foodGrading
from dailynutritionplans a inner join 
	 (
		select c.foodid, c.foodlabel, recommendationRating, 
			primarycolloquialmeasure as measurementType, 
			typicalconsumedquantity as measurementAmount,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * fibre)  
								else (typicalconsumedquantity * d.conversionRate * fibre) end as fibre,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * (carb - fibre))  
								else (typicalconsumedquantity * d.conversionRate * (carb - fibre)) end as netcarb,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * calories)  
								else (typicalconsumedquantity * d.conversionRate * calories) end as calories,		
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * (IFNULL((((carb - fibre) * glycemicIndex) / 55), 0)))  
								else (typicalconsumedquantity * d.conversionRate * (IFNULL((((carb - fibre) * glycemicIndex) / 55), 0))) end as netGlycemicIndex,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * fat)  
								else (typicalconsumedquantity * d.conversionRate * fat) end as fat,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * protein)  
								else (typicalconsumedquantity * d.conversionRate * protein) end as protein,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * carb)  
								else (typicalconsumedquantity * d.conversionRate * carb) end as carb,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * sodium)  
								else (typicalconsumedquantity * d.conversionRate * sodium) end as sodium,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * Potassium)  
								else (typicalconsumedquantity * d.conversionRate * Potassium) end as Potassium,
 			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * Magnesium)  
								else (typicalconsumedquantity * d.conversionRate * Magnesium) end as Magnesium,                               
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * Calcium)  
								else (typicalconsumedquantity * d.conversionRate * Calcium) end as Calcium,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * chromium)  
								else (typicalconsumedquantity * d.conversionRate * chromium) end as chromium,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * Omega3)  
								else (typicalconsumedquantity * d.conversionRate * Omega3) end as Omega3,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * Omega6)  
								else (typicalconsumedquantity * d.conversionRate * Omega6) end as Omega6, 
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * alphaLipoicAcid)  
								else (typicalconsumedquantity * d.conversionRate * alphaLipoicAcid) end as ala,       
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * q10)  
								else (typicalconsumedquantity * d.conversionRate * q10) end as q10,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * Biotin)  
								else (typicalconsumedquantity * d.conversionRate * Biotin) end as Biotin,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * flavonoids)  
								else (typicalconsumedquantity * d.conversionRate * flavonoids) end as flavonoids,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminA)  
								else (typicalconsumedquantity * d.conversionRate * vitaminA) end as vitaminA,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminC)  
								else (typicalconsumedquantity * d.conversionRate * vitaminC) end as vitaminC,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminD)  
								else (typicalconsumedquantity * d.conversionRate * vitaminD) end as vitaminD,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminE)  
								else (typicalconsumedquantity * d.conversionRate * vitaminE) end as vitaminE,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminK)  
								else (typicalconsumedquantity * d.conversionRate * vitaminK) end as vitaminK,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminB1)  
								else (typicalconsumedquantity * d.conversionRate * vitaminB1) end as vitaminB1,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminB12)  
								else (typicalconsumedquantity * d.conversionRate * vitaminB12) end as vitaminB12,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminB2)  
								else (typicalconsumedquantity * d.conversionRate * vitaminB2) end as vitaminB2,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminB3)  
								else (typicalconsumedquantity * d.conversionRate * vitaminB3) end as vitaminB3,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminB5)  
								else (typicalconsumedquantity * d.conversionRate * vitaminB5) end as vitaminB5,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * vitaminB6)  
								else (typicalconsumedquantity * d.conversionRate * vitaminB6) end as vitaminB6,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * Folate)  
								else (typicalconsumedquantity * d.conversionRate * Folate) end as Folate,                                
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * Copper)  
								else (typicalconsumedquantity * d.conversionRate * Copper) end as Copper,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * Iron)  
								else (typicalconsumedquantity * d.conversionRate * Iron) end as Iron,
 			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * Zinc)  
								else (typicalconsumedquantity * d.conversionRate * Zinc) end as Zinc,                               
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * Manganese)  
								else (typicalconsumedquantity * d.conversionRate * Manganese) end as Manganese,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * Phosphorus)  
								else (typicalconsumedquantity * d.conversionRate * Phosphorus) end as Phosphorus,
			case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * Selenium)  
								else (typicalconsumedquantity * d.conversionRate * Selenium) end as Selenium
		from foods c left join foodmeasureconversions d on c.foodid = d.foodid 
        where c.draftstatus = 'published'
	 ) c on a.foodid = c.foodid
	 left outer join 
		(
		select a.clientid, a.mealdate, a.total, b.selected_total, 'no meal plan' as name
		from
		(select clientid, mealdate, count(foodid) as total from dailynutritionplans where mealdate >= date(itz(now())) group by clientid, mealdate) a inner join 
		(select clientid, mealdate, count(foodid) as selected_total from dailynutritionplans where mealdate >= date(itz(now())) and selected = 1 group by clientid, mealdate) b
		where a.clientid = b.clientid
		and a.mealdate = b.mealdate
		and a.total - b.selected_total < 5
		) y on a.clientid = y.clientid and a.mealdate = y.mealdate
where a.selected = 1
and a.mealdate >= date_sub(date(now()), interval 15 day)
group by a.clientid, a.mealdate, a.mealtype, foodlabel
;

create index idx_tmp_nutrition_plan_detail on tmp_nutrition_plan_detail(clientid asc, mealdate asc); 

DROP temporary table IF EXISTS twins.temp_daily_nutritionplan_patient_detail; 

create temporary table temp_daily_nutritionplan_patient_detail
(
clientid int not null, 
mealdate date null, 
measureGroup varchar(50) null, 
measureCategory varchar(25) null, 
mealtype varchar(50) null, 
foodlabel varchar(30) null, 
E5foodGrading varchar(13) null, 
food_measure varchar(50) null, 
quantity varchar(13) null, 
measureValue varchar(21) null
);


/* Section for L1 - NetCarb */
/* The selection has condition like < 5. This is to consider clients who have proper nutritionplan selected. 
Some clients have all their food selected in the complete plan chart. We group such cases as no meal plan*/
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L1' as measureGroup, 
'Netcarb' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.netcarb, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

/* Section for L2 - Calories */
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L2' as measureGroup, 
'Calories' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.calories, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- netGlycemicIndex
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L2' as measureGroup, 
'NGC' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.netGlycemicIndex, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Fibre
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L2' as measureGroup, 
'Fibre' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Fibre, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Fat
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L2' as measureGroup, 
'Fat' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Fat, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Protein
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L2' as measureGroup, 
'Protein' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Protein, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Total carb
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L2' as measureGroup, 
'Total carb' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Totalcarb, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Sodium
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L3' as measureGroup, 
'Sodium' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Sodium, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Potassium
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L3' as measureGroup, 
'Potassium' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Potassium, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Magnesium
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L3' as measureGroup, 
'Magnesium' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Magnesium, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Calcium
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L3' as measureGroup, 
'Calcium' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Calcium, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Chromium
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L3' as measureGroup, 
'Chromium' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Chromium, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Omega3
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L3' as measureGroup, 
'Omega3' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Omega3, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Omega6
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L3' as measureGroup, 
'Omega6' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Omega6, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- ALA
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L3' as measureGroup, 
'ALA' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.ALA, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- q10
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L3' as measureGroup, 
'q10' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.q10, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Biotin
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L3' as measureGroup, 
'Biotin' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Biotin, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Flavonoids
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L3' as measureGroup, 
'Flavonoids' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Flavonoids, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Vitamin A
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Vitamins' as measureGroup, 
'Vitamin A' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.VitaminA, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Vitamin C
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Vitamins' as measureGroup, 
'Vitamin C' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.VitaminC, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Vitamin D
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Vitamins' as measureGroup, 
'Vitamin D' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.VitaminD, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Vitamin E
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Vitamins' as measureGroup, 
'Vitamin E' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.VitaminE, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Vitamin K
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Vitamins' as measureGroup, 
'Vitamin K' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.VitaminK, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Vitamin B1
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Vitamins' as measureGroup, 
'Vitamin B1' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.VitaminB1, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Vitamin B12
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Vitamins' as measureGroup, 
'Vitamin B12' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.VitaminB12, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Vitamin B2
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Vitamins' as measureGroup, 
'Vitamin B2' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.VitaminB2, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Vitamin B3
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Vitamins' as measureGroup, 
'Vitamin B3' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.VitaminB3, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Vitamin B5
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Vitamins' as measureGroup, 
'Vitamin B5' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.VitaminB5, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Vitamin B6
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Vitamins' as measureGroup, 
'Vitamin B6' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.VitaminB6, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Folate
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Vitamins' as measureGroup, 
'Folate' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Folate, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Copper
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Nutraceuticals, Mineral' as measureGroup, 
'Copper' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Copper, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Iron
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Nutraceuticals, Mineral' as measureGroup, 
'Iron' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Iron, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Zinc
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Nutraceuticals, Mineral' as measureGroup, 
'Zinc' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Zinc, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Manganese
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Nutraceuticals, Mineral' as measureGroup, 
'Manganese' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Manganese, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Phosphorus
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Nutraceuticals, Mineral' as measureGroup, 
'Phosphorus' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Phosphorus, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

-- Selenium
insert into temp_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select cd.clientid, cd.date as mealdate, 
'L6-Nutraceuticals, Mineral' as measureGroup, 
'Selenium' as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(new_foodlabel,'Not Available') as foodlabel, 
ifnull(E5foodGrading, 'Not Available') as E5foodGrading,
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.Selenium, 'no meal plan selected') as measureValue
from 
(
	select distinct d.date, b.clientId
	from twins.ddate d
    inner join (
					select clientid, max(status_start_date) as status_start_date 
                    from bi_patient_status 
                    where status = 'active'
                    group by clientid
				) b on d.date > status_Start_date
	where d.date <= date_add(date(now()), interval 7 day) + 7 and d.date >= date_sub(date(now()), interval 14 day)
) cd
left outer join tmp_nutrition_plan_detail x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

/*Truncate and load the main table*/
truncate table bi_daily_nutritionplan_patient_detail;

insert into bi_daily_nutritionplan_patient_detail
(clientid, mealdate, measureGroup, 
measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue)
select 
clientid, mealdate, measureGroup, 
measureCategory, mealtype, foodlabel, E5foodGrading, food_measure, quantity, measureValue
from temp_daily_nutritionplan_patient_detail
;

/*Drop the temporary table after successful load*/
drop temporary table if exists temp_daily_nutritionplan_patient_detail;
drop temporary table if exists tmp_nutrition_plan_detail;

/* update process log */
update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_bi_daily_nutritionplan_patient_detail'
;
END$$
DELIMITER ;
