CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_bi_dim_client`()
BEGIN
/*
2019-11-21  Aravindan - Baseline version created
2020-02-11  Aravindan - Maintain start_medicine_diabetes, is_currently_metformin_only in the dim_client table.
2020-02-12  Aravindan - Added logic to maintain durationYears_diabetes, is_currently_insulin, start_medicine and current medicine drugs. 
2020-02-18  Aravindan - Added logic to maintain the latest_lab_result date as well.
2020-07-13  Aravindan - Added Logic to maintain is_type1 flag.
2020-07-13  Aravindan - Added Logic to maintain start_nondiabetic_conditions.
2020-07-26  Aravindan - Added logic to handle multiple visitdate appointments
2020-08-09  Aravindan - nut_syntax, journey, continuous_reversaldays columns maintenance
2020-08-13  Aravindan - Included code to handle duplicate rows in momentumweekdetails table.
2020-08-22  Aravindan - Included logic to maintain is_bp_patient, is_cholesterol_patient, is_heartdisease_patient columns
2020-09-02  Aravindan - Fixed the bug in updating VisitScheduleDate. The previous version was not handling the NULLs correctly
2020-09-02  Aravindan - Fixed the bug in updating VisitScheduleDate. The previous version was not handling the NULLs correctly
2020-09-15  Aravindan - Using bi_patient_reversal_state_gmi_x_date table to decide the nutrition journey of the table as this table now stores the InReversal by date
					  - Included columns to store start and current medicine drugs for nondiabetic diseases
                      - Included maintenance of Start and Current insulin units
2020-10-29  Aravindan - Updated logic to identify the is_choesterol_patient.
2021-03-06  Aravindan - Added start_conditions, start_allergies, last_Available_Weight, latest_labA1c and motivations maintenance.
2021-03-30  Aravindan - Modified the d90, d180 lab calculation logic
2021-11-10  Aravindan - Modified the start and current non diabetic medicines to use pharmclasses
*/

/*log the starttime*/
update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_dim_client'
;


-- Set the group_concat max value because the server default is only 1024 bytes. We have start medicines of a member sometimes cross the total length hence this setting would help handle such records. 
SET group_concat_max_len=15000;


-- Step 1 : build a temporary table (tmp_dim_client) to stage the complete content from platform tables. 

drop temporary table if exists tmp_dim_client;

create temporary table tmp_dim_client
(
customerkey integer null, 
clientid integer not null, 
doctorName varchar(50) not null,
doctorNameShort varchar(50) not null,
coachName varchar(50) null,
coachNameShort varchar(50) null,
-- address varchar(2500) null, 
-- postalCode varchar(500) null, 
-- userName varchar(2000) null,
-- email varchar(200) null,
-- dateOfBirth datetime null,
-- age integer null,
enrollmentDate datetime null,
status varchar(25) null,
paidType varchar(100) null,
cohortGroup varchar(100) null,
visitDate datetime null,
visitDateAdded datetime null,
visitScheduledDate date null,
renewalVisitScheduledDate datetime null,
dischargeReason varchar(2000) null,
enrolledDays integer null,
renewalCount integer null,
firstDiabetesInReversalTime datetime null, 
latestdiabetesinreversaltime datetime null,
treatmentSetupDate datetime null,
currentTermEndDate datetime null, -- this column is currently in v_active_clients as termEndDate
escalationStage varchar(25) null,
planCode varchar(1000) null,
predictionCgmON tinyInt(1) null,
dateAdded datetime null
); 

insert into tmp_dim_client 
( clientid, doctorName, doctorNameShort, coachName, coachNameShort, enrollmentDate, status, 
paidType, cohortGroup, visitDate, visitDateAdded, visitScheduledDate, renewalVisitScheduledDate, dischargeReason, 
enrolledDays, renewalCount, firstDiabetesInReversalTime, latestdiabetesinreversaltime, treatmentSetupDate, currentTermEndDate, escalationStage, planCode,
predictionCgmON, dateAdded)
SELECT 
        c.ID AS clientId,
        UPPER(CONCAT(d.firstName, ' ', d.lastName)) AS doctorName,
        UPPER(CONCAT(d.firstName, ' ', LEFT(d.lastName, 1))) AS doctorNameShort,
        UPPER(CONCAT(h.firstName, ' ', h.lastName)) AS coachName,
        UPPER(CONCAT(h.firstName, ' ', LEFT(h.lastName, 1))) AS coachNameShort,
        convert_tz(c.enrollmentDate,@@time_zone, c.timezoneId) AS enrollmentDate,
        c.status, 
        c.paidType AS paidType,
        t.name AS cohortGroup,
        v.visitDate AS visitDate,
        v.dateAdded AS visitDateAdded,
        vs.startDate as visitScheduledDate,
        c.renewalVisitScheduled AS renewalVisitScheduledDate,
        c.dischargeReason AS dischargeReason,
		CASE
            WHEN (c.status = 'Active') THEN (TO_DAYS(NOW()) - TO_DAYS(convert_tz(c.enrollmentDate,@@time_zone, c.timezoneId)))
            WHEN (c.status <> 'Active') THEN (TO_DAYS(c.lastStatusChange) - TO_DAYS(convert_tz(c.enrollmentDate,@@time_zone, c.timezoneId)))
        END AS enrolledDays, 
		(FLOOR(((TO_DAYS(convert_tz(c.termEndDate,@@time_zone, c.timezoneId)) - TO_DAYS(convert_tz(c.enrollmentDate,@@time_zone, c.timezoneId))) / 84)) - 1) AS renewalCount,
        convert_tz(c.firstDiabetesInReversalTime, @@time_zone, c.timezoneId) as firstDiabetesInReversalTime, 
        convert_tz(c.latestdiabetesinreversaltime, @@time_zone, c.timezoneId) as latestdiabetesinreversaltime,
        c.treatmentSetupDate as treatmentSetupDate,
        convert_tz(c.termEndDate,@@time_zone, c.timezoneId) AS currentTermEndDate,
        CASE
            WHEN ((TO_DAYS(CURDATE()) - TO_DAYS(convert_tz(c.enrollmentDate,@@time_zone, c.timezoneId))) < 23) THEN 'D2-22'
            WHEN ((TO_DAYS(CURDATE()) - TO_DAYS(convert_tz(c.enrollmentDate,@@time_zone, c.timezoneId))) < 43) THEN 'D23-42'
            WHEN ((TO_DAYS(CURDATE()) - TO_DAYS(convert_tz(c.enrollmentDate,@@time_zone, c.timezoneId))) < 57) THEN 'D43-56'
            WHEN ((TO_DAYS(CURDATE()) - TO_DAYS(convert_tz(c.enrollmentDate,@@time_zone, c.timezoneId))) < 85) THEN 'D57-84'
            WHEN ((TO_DAYS(CURDATE()) - TO_DAYS(convert_tz(c.enrollmentDate,@@time_zone, c.timezoneId))) >= 85) THEN 'D85PLUS'
        END AS escalationStage,
        c.planCode AS planCode,
        c.predictedCGMActivated as predictionCgmON,
        convert_tz(c.dateAdded, @@time_zone, c.timezoneId)  as dateAdded
    FROM
       twins.clients c
        JOIN twins.doctors d ON c.doctorId = d.ID
        LEFT JOIN twinspii.clientspii c1 on c.id = c1.id
        LEFT JOIN twins.coaches h ON c.coachId = h.ID
        LEFT JOIN (
				select a.clientid, a.date_of_visit, a.visitdate, a.dateadded from 
                (
						SELECT 	a.clientId,
								DATE(convert_tz(visitDate,@@time_zone,b.timezoneId)) as date_of_visit,
								MAX(convert_tz(visitDate,@@time_zone,b.timezoneId)) AS visitDate,
								MAX(convert_tz(a.DateAdded,@@time_zone,b.timezoneId)) AS dateAdded
						FROM homevisits a inner join clients b on a.clientid = b.id
						GROUP BY clientId, date(visitdate) -- there are multiple entries for the same home schedule date so pick the one needed (most recent)
                ) a
                inner join (select a.clientid, max(date(convert_tz(visitDate,@@time_zone,b.timezoneId))) as date_of_visit 
							from homevisits a inner join clients b on a.clientid = b.id 
                            group by clientid
                            ) b on a.clientid = b.clientid and a.date_of_visit = b.date_of_visit -- (pick the most recent schedule date)
        ) v ON (c.ID = v.clientId)
        LEFT JOIN cohorts t ON (c.cohortId = t.cohortId)
        LEFT JOIN (select clientid, max(startDate) as startDate from momentumweekdetails group by clientid ) vs on c.id = vs.clientid 
        LEFT JOIN addresses ad on c.addressId = ad.id 
    WHERE
        ((c.status IN ('PROSPECT','PENDING_ACTIVE', 'ACTIVE', 'REGISTRATION', 'INACTIVE', 'DISCHARGED'))
            AND (c.deleted = 0)
            AND (d.test = FALSE)
            AND (NOT ((c1.firstName LIKE '%TEST%')))
            AND (NOT ((c1.lastName LIKE '%TEST%'))))
;


-- Step 2: Update the customerkey (of the recent active record of the patient, usually the max customerkey) in the tmp_dim_client table 
update tmp_dim_client a inner join (select clientid, max(customerkey) as customerkey from dim_client group by clientid) b on a.clientid = b.clientid 
set a.customerkey = b.customerkey; 

-- Step 3: if the customerkey is NULL in tmp_dim_client, then those clients must be new folks. So just insert them. 
insert into dim_client
(clientid, doctorName, doctorNameShort, coachName, coachNameShort, enrollmentDate, status, paidType, cohortGroup, 
visitDate, visitDateAdded, visitScheduledDate, renewalVisitScheduledDate, dischargeReason, 
 enrolledDays, renewalCount, firstDiabetesInReversalTime, latestdiabetesinreversaltime, treatmentSetupDate, currentTermEndDate, escalationStage, planCode,
 predictionCgmON, dateAdded, row_start_date) 
select 
clientid, doctorName, doctorNameShort, coachName, coachNameShort, enrollmentDate,
status, paidType, cohortGroup, visitDate, visitDateAdded, visitScheduledDate, renewalVisitScheduledDate, dischargeReason, 
enrolledDays, renewalCount, firstDiabetesInReversalTime, latestdiabetesinreversaltime, treatmentSetupDate, currentTermEndDate, escalationStage, planCode, 
predictionCgmON, dateAdded, if(dateAdded is null, date(now()), date(dateAdded))
from tmp_dim_client a 
where customerkey is null
; 

-- Step 4: 	Now by comparing the staged content to the main dim_client, Identify the client records changed based on the changeTracking columns. Pull them into 
-- 			temporary table tmp_records_modified
drop temporary table if exists tmp_records_modified; 

create temporary table tmp_records_modified
(
customerkey integer not null,
clientid integer not null, 
doctorName varchar(50) not null,
doctorNameShort varchar(50) not null,
coachName varchar(50) null,
coachNameShort varchar(50) null,
enrollmentDate datetime null,
status varchar(25) null,
paidType varchar(100) null,
cohortGroup varchar(100) null,
visitDate datetime null,
visitDateAdded datetime null,
visitScheduledDate date null,
renewalVisitScheduledDate datetime null,
dischargeReason varchar(2000) null,
enrolledDays integer null,
renewalCount integer null,
firstDiabetesInReversalTime datetime null, 
latestdiabetesinreversaltime datetime null, 
treatmentSetupDate datetime null,
currentTermEndDate datetime null, -- this column is currently in v_active_clients as termEndDate
escalationStage varchar(25) null,
planCode varchar(1000) null,
predictionCgmON tinyint(1) null,
dateAdded datetime null,
changed_columns varchar(5000) null
); 

insert into tmp_records_modified
select a.customerkey, a.clientid, a.doctorName, a.doctorNameShort, a.coachName, a.coachNameShort, 
a.enrollmentDate, a.status, a.paidType, a.cohortGroup, a.visitDate, a.visitDateAdded, 
a.visitScheduledDate, a.renewalVisitScheduledDate, a.dischargeReason, 
a.enrolledDays, a.renewalCount, a.firstDiabetesInReversalTime, a.latestDiabetesInReversalTime, a.treatmentSetupDate,
a.currentTermEndDate, a.escalationStage, a.planCode, a.predictionCgmON, a.dateAdded,
concat(
		 case when a.status <> b.status then concat('status: ''', a.status,''',') else '' end, 
		 case when a.visitScheduledDate <> b.visitScheduledDate then concat('visitScheduledDate: ''', a.visitScheduledDate,''',') else '' end,
		 case when a.predictionCgmON <> b.predictionCgmON then concat('predictionCgmON: ''', a.predictionCgmON,''',') else '' end, 
		 case when a.currentTermEndDate <> b.currentTermEndDate then concat('currentTermEndDate: ''', a.currentTermEndDate,'''') else '' end
     ) as changed_columns 
from tmp_dim_client a inner join dim_client b on (a.clientid = b.clientid and a.customerkey = b.customerkey)
where ( 
	ltrim(rtrim(a.status)) <> ltrim(rtrim(b.status))
	or (ifnull(a.visitScheduledDate,'1900-01-01') <> ifnull(b.visitScheduledDate,'1900-01-01'))
    or (ifnull(a.currentTermEndDate,'1900-01-01') <> ifnull(b.currentTermEndDate,'1900-01-01'))
    or ltrim(rtrim(a.predictionCgmON)) <> ltrim(rtrim(b.predictionCgmON)))
;


-- Step 5: update the columns for the patients for whom the columns might have changed but we are not tracking such columns. 
update dim_client a inner join tmp_dim_client b on a.clientid = b.clientid and a.customerkey = b.customerkey
set a.doctorName = b.doctorName,
    a.doctorNameShort = b.doctorNameShort,
    a.coachName = b.coachName,
    a.coachNameShort = b.coachNameShort,
    a.enrollmentDate = b.enrollmentDate,
    -- a.status = b.status,
    a.paidType = b.paidType,
    a.cohortGroup = b.cohortGroup,
    a.visitDate = b.visitDate,
    a.visitDateAdded = b.visitDateAdded,
    -- a.visitScheduledDate = b.visitScheduledDate,
    a.renewalVisitScheduledDate = b.renewalVisitScheduledDate,
    a.dischargeReason = b.dischargeReason,
    a.enrolledDays = b.enrolledDays,
    a.renewalCount = b.renewalCount,
    a.firstDiabetesInReversalTime = b.firstDiabetesInReversalTime,
    a.latestDiabetesInReversalTime = b.latestDiabetesInReversalTime,
    a.treatmentSetupDate = b.treatmentSetupDate,
    -- a.currentTermEndDate = b.currentTermEndDate,
    a.escalationStage = b.escalationStage,
    a.planCode = b.planCode
    -- a.predictionCgmON = b.predictionCgmON
    where a.clientid not in (select clientid from tmp_records_modified)
;

-- Step 6: Invalidate the already existing active CustomerKey for the patient with the previous day as last day 
update dim_client a inner join tmp_records_modified b on a.customerkey = b.customerkey and a.clientid = b.clientid  
set row_end_date = date_sub(date(now()), interval 1 day),
	is_row_current = 'N' 
;

-- Step 7: Insert the new rows for the patients who were identified as modified with their new values. By default these records will become the active record for the patients.
insert into dim_client 
(clientid, doctorName, doctorNameShort, coachName, coachNameShort, enrollmentDate,
 status, paidType, cohortGroup, visitDate, visitDateAdded, visitScheduledDate, renewalVisitScheduledDate, dischargeReason, 
 enrolledDays, renewalCount, firstDiabetesInReversalTime, latestDiabetesInReversalTime, treatmentSetupDate, currentTermEndDate, escalationStage, planCode, predictionCgmON, 
 dateAdded, row_start_date, changed_columns) 
select 
clientid, doctorName, doctorNameShort, coachName, coachNameShort, enrollmentDate,
status, paidType, cohortGroup, visitDate, visitDateAdded, visitScheduledDate, renewalVisitScheduledDate, dischargeReason, 
enrolledDays, renewalCount, firstDiabetesInReversalTime, latestDiabetesInReversalTime, treatmentSetupDate, currentTermEndDate, escalationStage, planCode, predictionCgmON, 
dateAdded, date(now()) as row_start_date, changed_columns
from tmp_records_modified a 
; 

-- Step 8: 	Now the table is ready, maintain the columns that we want to maintain only for the active record of the patient. The next set of 
-- 			update statements maintain different columns as needed in the reports.

-- maintain d10_date, d35_date, firstBloodWorkDate, d35, d90, etc lab dates
-- For US Clinic OPS, expanded the blood work search dates as +/- 30 days compared to India's  +/- 15 days
update dim_client a inner join 
(
		select s1.clientid, 
		firstbloodworkTime as day0,
		s1.day0 as sensor_day0,
		datediff(status_end_date, day0) as total_treatment_days,
		s1.day10,
		s1.day26,
		s1.day35,
		s1.day81,
		s1.day90,
		s1.day171,
		s1.day180,
		s1.day261, 
		s1.day270,
		s1.day351, 
		s1.day360,
		if(s2.bloodworkdate = s1.day35, s1.day35, max(s2.bloodworkdate)) as lab_day35,
		if(s3.bloodworkdate = s1.day90, s1.day90, max(s3.bloodworkdate)) as lab_day90,
		if(s4.bloodworkdate = s1.day180, s1.day180, max(s4.bloodworkdate)) as lab_day180,
		if(s5.bloodworkdate = s1.day270, s1.day270, max(s5.bloodworkdate)) as lab_day270,
		if(s6.bloodworkdate = s1.day360, s1.day360, max(s6.bloodworkdate)) as lab_day360
		from 
		(
			select distinct b.clientid, status_start_date as day0,
			date_add(status_start_date, interval 10 day) as day10,
			date_add(status_start_date, interval 20 day) as day20,
			date_add(status_start_date, interval 26 day) as day26,
			date_add(status_start_date, interval 35 day) as day35,
			date_add(status_start_date, interval 36 day) as day36,
			date_add(status_start_date, interval 45 day) as day45,
            date_add(status_start_date, interval 61 day) as day61,
			date_add(status_start_date, interval 76 day) as day76,
			date_add(status_start_date, interval 81 day) as day81,
			date_add(status_start_date, interval 90 day) as day90,
			date_add(status_start_date, interval 91 day) as day91,
			date_add(status_start_date, interval 105 day) as day105,
			date_add(status_start_date, interval 120 day) as day120,
			
			date_add(status_start_date, interval 151 day) as day151,
			date_add(status_start_date, interval 166 day) as day166,
			date_add(status_start_date, interval 171 day) as day171,
			date_add(status_start_date, interval 180 day) as day180,
			date_add(status_start_date, interval 181 day) as day181,
			date_add(status_start_date, interval 195 day) as day195,
            date_add(status_start_date, interval 210 day) as day210,

			date_add(status_start_date, interval 241 day) as day241,
			date_add(status_start_date, interval 256 day) as day256,
			date_add(status_start_date, interval 261 day) as day261, 
			date_add(status_start_date, interval 270 day) as day270,
			date_add(status_start_date, interval 271 day) as day271,
			date_add(status_start_date, interval 285 day) as day285,
			date_add(status_start_date, interval 300 day) as day300,
            
            date_add(status_start_date, interval 331 day) as day331,
			date_add(status_start_date, interval 346 day) as day346,
			date_add(status_start_date, interval 351 day) as day351,
			date_add(status_start_date, interval 360 day) as day360,
			date_add(status_start_date, interval 361 day) as day361,
			date_add(status_start_date, interval 375 day) as day375,
            date_add(status_start_date, interval 390 day) as day390,
			firstbloodworkTime, status_end_date
			from
				(
                select clientid, status_start_date, -- if(date(visitdate) > status_start_date or status_start_date is null, date(visitdate), status_start_date) as status_start_date,
                status_end_date from 
                (
					select b.clientid, b.status, 
					max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
					from bi_patient_status b
                    where status = 'active'
					group by clientid, status
				) s 
                ) b 
				left join (select clientid, min(date(ifnull(bloodworktime, eventtime))) as firstbloodworkTime from clinictestresults where deleted = 0 group by clientid) d on b.clientid = d.clientid -- this is because in some investigations bloodworktime is missing and it is one of the bugs due to bloodreport from vendor.
			-- where b.status = 'active' 
		) s1 left join (select distinct a.clientid, date(ifnull(bloodworktime, eventtime)) as bloodworkdate from clinictestresults a inner join v_client_tmp b on a.clientid = b.clientid where deleted = 0) s2 on s1.clientid = s2.clientid and ((s1.day35 = s2.bloodworkdate) or (s2.bloodworkdate between s1.day26 and s1.day35) or (s2.bloodworkdate between s1.day36 and s1.day45))
			 left join (select distinct a.clientid, date(ifnull(bloodworktime, eventtime)) as bloodworkdate from clinictestresults a inner join v_client_tmp b on a.clientid = b.clientid where deleted = 0) s3 on s1.clientid = s3.clientid and ((s1.day90 = s3.bloodworkdate) or (s3.bloodworkdate between s1.day61 and s1.day90) or (s3.bloodworkdate between s1.day91 and s1.day120))
			 left join (select distinct a.clientid, date(ifnull(bloodworktime, eventtime)) as bloodworkdate from clinictestresults a inner join v_client_tmp b on a.clientid = b.clientid where deleted = 0) s4 on s1.clientid = s4.clientid and ((s1.day180 = s4.bloodworkdate) or (s4.bloodworkdate between s1.day151 and s1.day180) or (s4.bloodworkdate between s1.day181 and s1.day210))
			 left join (select distinct a.clientid, date(ifnull(bloodworktime, eventtime)) as bloodworkdate from clinictestresults a inner join v_client_tmp b on a.clientid = b.clientid where deleted = 0) s5 on s1.clientid = s5.clientid and ((s1.day270 = s5.bloodworkdate) or (s5.bloodworkdate between s1.day241 and s1.day270) or (s5.bloodworkdate between s1.day271 and s1.day300))
			 left join (select distinct a.clientid, date(ifnull(bloodworktime, eventtime)) as bloodworkdate from clinictestresults a inner join v_client_tmp b on a.clientid = b.clientid where deleted = 0) s6 on s1.clientid = s6.clientid and ((s1.day360 = s6.bloodworkdate) or (s6.bloodworkdate between s1.day331 and s1.day360) or (s6.bloodworkdate between s1.day361 and s1.day390))
		group by s1.clientid
) b on a.clientid = b.clientid and a.is_row_current = 'Y'
set a.d10_date = b.day10,
	a.d35_date = b.day35,
    a.d90_date = b.day90,
    a.d180_date = b.day180,
    a.d270_date = b.day270,
    a.d360_date = b.day360,
	a.d35_date_lab = b.lab_day35,
    a.d90_date_lab = b.lab_day90,
    a.d180_date_lab = b.lab_day180,
    a.d270_date_lab = b.lab_day270,
    a.d360_date_lab = b.lab_day360,
    a.programStartDate_analytics = b.sensor_day0
; 

-- maintain firstBloodWorkDate, latest_bloodworktime
update dim_client a inner join 
(
		select s.clientid, 
			min(convert_tz(investigationDate, @@time_zone, s1.timezoneId)) as bloodworkTime, 
			max(convert_tz(investigationDate, @@time_zone, s1.timezoneId)) as latest_bloodworktime
		from 
		(    
			select a.id, a.clientid, if(bloodworktime is null, eventtime, bloodworktime) as investigationDate
			from clinictestresults a 
			where a.deleted = 0
		) s inner join clients s1 on s.clientid = s1.id
		group by s.clientid
) c on a.clientid = c.clientid and a.is_row_current = 'Y'
set  firstBloodWorkDate = bloodworkTime,
	 latestBloodWorkDate = if(latest_bloodworktime = bloodworkTime, null, latest_bloodworktime)
;

-- Start Allergies
update dim_client a inner join 
(
    select clientid, group_concat(distinct ifnull(description,name)) as start_allergies
	from clients a left join clinicreportconditions b on a.id = b.clientid
	where type = 'ALLERGY' and hascondition = 1
	-- and date(itz(b.dateadded)) = date(itz(enrollmentDate))
	group by clientid
) b on a.clientid = b.clientid
set a.start_allergies  = b.start_allergies
where  a.is_row_current = 'Y'
;

-- Start Conditions
update dim_client a inner join 
(
    select clientid, group_concat(distinct name) as start_conditions
	from clients a left join clinicreportconditions b on a.id = b.clientid
	where type = 'DISORDER' and hascondition = 1
	-- and date(itz(b.dateadded)) = date(itz(enrollmentDate))
	group by clientid
) b on a.clientid = b.clientid
set a.start_conditions  = b.start_conditions
where  a.is_row_current = 'Y'
;

-- Maintain the start labA1c, height, weight, motivations from 1st lab 
update dim_client a inner join 
(
	select a.id as clientid, convert_tz(ifnull(b.bloodworktime, b.eventtime), @@time_zone, a.timezoneId) as bloodworktime, hotButtonIssues
	from clients a inner join clinictestresults b on a.id = b.clientid
) b on a.clientid = b.clientid and a.firstBloodWorkDate = b.bloodworktime
set a.motivations = b.hotButtonIssues
where a.is_row_current = 'Y'
;

-- first available LabA1c as start LabA1c
update dim_client a inner join 
(
	select s.clientid, first_available_labA1c_date, ifnull(s1.veinhba1c, s1.hba1c) as start_labA1c
	from 
	(
		select clientid, min(bloodworktime) as first_available_labA1c_date
		from 
		(
			select clientid, ifnull(b.bloodworktime, b.eventtime) as bloodworktime
			from clinictestresults b 
			where b.deleted = 0 
			and ifnull(veinHba1c, hba1c) is not null
		) s 
		group by clientid 
	) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.first_available_labA1c_date = s1.bloodworktime or s.first_available_labA1c_date = s1.eventtime)
) b on a.clientid = b.clientid
set a.start_labA1c = b.start_labA1c
where is_row_current = 'Y'
;

-- first available height
update dim_client a inner join 
(
		select s.clientid, first_available_height_date, s1.height as start_height
		from 
		(
			select clientid, min(eventtime) as first_available_height_date
			from 
			(
				select clientid, eventtime
				from clinictestresults b 
				where b.deleted = 0 
				and height is not null
			) s 
			group by clientid 
		) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.first_available_height_date = s1.eventtime)
) b on a.clientid = b.clientid 
set a.start_height = b.start_height
where a.is_row_current = 'Y'
;

-- first available weight
update dim_client a inner join 
(
	select s.clientid, first_available_weight_date, s1.weight as start_weight
	from 
	(
		select clientid, min(eventtime) as first_available_weight_date
		from 
		(
			select clientid, eventtime
			from clinictestresults b 
			where b.deleted = 0 
			and weight is not null
		) s 
		group by clientid 
	) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.first_available_weight_date = s1.eventtime)
) b on a.clientid = b.clientid 
set a.start_weight = b.start_Weight
where a.is_row_current = 'Y'
;


-- maintain latest labA1C
update dim_client a inner join 
(
	select s.clientid, last_available_labA1c_date, ifnull(s1.veinhba1c, s1.hba1c) as latest_labA1c
	from 
	(
		select clientid, last_available_labA1c_date
		from 
		(
			select clientid, min(bloodworktime) as first_available_labA1c_date, max(bloodworktime) as last_available_labA1c_date
			from 
			(
				select clientid, ifnull(b.bloodworktime, b.eventtime) as bloodworktime
				from clinictestresults b 
				where b.deleted = 0 
				and ifnull(veinHba1c, hba1c) is not null
			) s 
			group by clientid 
		) s 
		where first_available_labA1c_date <> last_available_labA1c_date
	) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.last_available_labA1c_date = s1.bloodworktime or s.last_available_labA1c_date = s1.eventtime)
) b on a.clientid = b.clientid
set a.latest_LabA1c  = b.latest_labA1c
where a.is_row_current = 'Y'
;


-- maitain last available weight
update dim_client a inner join 
(
	  select s.clientid, s1.weight as last_weight
	  from 
	  (
		select clientid, max(eventtime) as last_entry from weights where weight is not null group by clientid
	  ) s inner join weights s1 on s.clientid = s1.clientid and s.last_entry = s1.eventtime
) b on a.clientid = b.clientid
set a.last_available_weight = b.last_weight
where is_row_current = 'y'
;

-- maintain treatmentDays (unlike India here there is no concept of Homevisit so taking this out).
update dim_client a inner join 
(
		select clientid, count(distinct date) as treatmentDays 
		from 
		v_date_to_date a inner join (
										select clientid, max(status_start_date) as status_start_date, max(status_end_date) as status_end_date 
										from bi_patient_status 
                                        where status = 'active'
										group by clientid
                                    ) b on a.date between date_add(b.status_start_date, interval 1 day) and b.status_end_Date
		group by clientid 
) b on a.clientid = b.clientid and a.is_row_current = 'Y'
set a.treatmentDays = b.treatmentDays
;


		drop temporary table if exists tmp_first_last_BMI_from_BCM; 

		create temporary table tmp_first_last_BMI_from_BCM as 
		  select s.clientid, 
		  max(case when s.first_entry = s1.eventtime then s1.BMI else null end) as firstBMI,     
		  max(case when s.first_entry <> s.last_entry and s.last_entry = s1.eventtime then s1.BMI else null end) as lastBMI
		  from 
		  (
			select clientid, min(eventtime) as first_entry, max(eventtime) as last_entry from weights where BMI is not null group by clientid
		  ) s inner join weights s1 on s.clientid = s1.clientid and (s.last_entry = s1.eventtime or s.first_entry = s1.eventtime)
		  group by s.clientid
		;

		create index idx_tmp_first_last_BMI_from_BCM on tmp_first_last_BMI_from_BCM(clientid, firstBMI, lastBMI)
		; 

		update dim_client a inner join tmp_first_last_BMI_from_BCM b on a.clientid = b.clientid
		set a.start_BMI_bcm = b.firstBMI
		where a.is_row_current = 'Y'
		;

		update dim_client a inner join tmp_first_last_BMI_from_BCM b on a.clientid = b.clientid
		set a.last_available_BMI = b.lastBMI
		where a.is_row_current = 'Y'
		;

		drop temporary table if exists tmp_first_last_BMI_from_BCM
		; 
        
        
        -- Sometime while collecting Vitals & conditions, the weight is getting missed as well. So for such case bring the first available entry from BCM scale and also bring the last available weight. 
        -- This is because BCM scale not always captures the BMI so this weight can be used to calculate.
		drop temporary table if exists tmp_first_last_weight_from_BCM; 

		create temporary table tmp_first_last_weight_from_BCM as 
		  select s.clientid, 
		  max(case when s.first_entry = s1.eventtime then s1.weight else null end) as first_weight,     
		  max(case when s.first_entry <> s.last_entry and s.last_entry = s1.eventtime then s1.weight else null end) as last_weight
		  from 
		  (
			select clientid, min(eventtime) as first_entry, max(eventtime) as last_entry from weights where weight is not null group by clientid
		  ) s inner join weights s1 on s.clientid = s1.clientid and (s.last_entry = s1.eventtime or s.first_entry = s1.eventtime)
		  group by s.clientid
		;

		create index idx_tmp_first_last_weight_from_BCM on tmp_first_last_weight_from_BCM (clientid, first_weight, last_weight)
		; 

		update dim_client a inner join tmp_first_last_weight_from_BCM b on a.clientid = b.clientid
		set a.start_weight = b.first_weight
		where a.is_row_current = 'Y'
		and a.start_weight is null
		;

		drop temporary table if exists tmp_first_last_weight_from_BCM
		; 
-- maintain the start and latest creatinine, eGFR
	drop temporary table if exists tmp_eGFR_cretinine_start_latest_measures;

	create temporary table tmp_eGFR_cretinine_start_latest_measures as 
		select s.clientid, s.first_bloodworktime, s.latest_bloodworktime, s.start_labA1c, s.latest_labA1c, s.start_creatinine, s.latest_creatinine,
		cast((141 * 
			power(if((start_creatinine/eGFR_Constant_K) < 1, (start_creatinine/eGFR_Constant_K), 1), eGFR_Constant_Alpha) * 
			power(if((start_creatinine/eGFR_Constant_K) > 1, (start_creatinine/eGFR_Constant_K), 1), -1.209) * 
			power(0.993, age) * 
			eGFR_Constant_Gender) as decimal(7,2)) as start_eGFR,
		cast((141 * 
			power(if((latest_creatinine/eGFR_Constant_K) < 1, (latest_creatinine/eGFR_Constant_K), 1), eGFR_Constant_Alpha) * 
			power(if((latest_creatinine/eGFR_Constant_K) > 1, (latest_creatinine/eGFR_Constant_K), 1), -1.209) * 
			power(0.993, age) * 
			eGFR_Constant_Gender) as decimal(7,2)) as latest_eGFR
		from 
		(
				select s1.clientid, s3.gender, s3.age, 
				if(s3.gender = 'FEMALE', 0.7, 0.9) as eGFR_Constant_K, 
				if(s3.gender = 'FEMALE', -0.329, -0.411) as eGFR_Constant_Alpha, 
				if(s3.gender = 'FEMALE', 1.018, 1) as eGFR_Constant_Gender, 
				s1.first_bloodworktime, s1.latest_bloodworktime,
				max(case when s1.first_bloodworkTime = ifnull(s2.bloodworktime, s2.eventtime) then s2.veinHba1c else null end) as start_labA1c, 
				max(case when s1.latest_bloodworktime = ifnull(s2.bloodworktime, s2.eventtime) then s2.veinHba1c else null end) as latest_labA1c, 
				max(case when s1.first_bloodworkTime = ifnull(s2.bloodworktime, s2.eventtime) then s2.creatinine else null end) as start_creatinine, 
				max(case when s1.latest_bloodworktime = ifnull(s2.bloodworktime, s2.eventtime) then s2.creatinine else null end) as latest_creatinine
				from 

				(
					select clientid, min(investigationDate) as first_bloodworkTime, max(investigationDate) as latest_bloodworktime -- this is because Bloodworktime is not coming for some of the investigations. Fix is being implemented in the product end.
					from 
					(
						select id, clientid, eventtime, bloodworktime, creatinine, veinHba1c, height, weight, if(bloodworktime is null, eventtime, bloodworktime) as investigationDate
						from clinictestresults
						where deleted = 0
					) s 
					group by clientid
				) s1 inner join clinictestresults s2 on s1.clientid = s2.clientid and (s1.first_bloodworkTime = ifnull(s2.bloodworktime, s2.eventtime) or s1.latest_bloodworktime = ifnull(s2.bloodworktime, s2.eventtime))
					 inner join v_Client_tmp s3 on s1.clientid = s3.clientid 
				group by s1.clientid
		) s 
		;
        
	create index idx_tmp_eGFR_cretinine_start_latest_measures on tmp_eGFR_cretinine_start_latest_measures(clientid asc, start_creatinine, latest_creatinine, start_eGFR, latest_eGFR); 

	update dim_client a inner join 	tmp_eGFR_cretinine_start_latest_measures b on a.clientid = b.clientid
	set a.start_creatinine = b.start_creatinine,
		a.latest_creatinine = b.latest_creatinine,
		a.start_eGFR = b.start_eGFR,
		a.latest_eGFR = b.latest_eGFR
	where is_row_current = 'Y'
	;

	drop temporary table if exists tmp_eGFR_cretinine_start_latest_measures
	;
    
-- start and last TG/HDL
drop temporary table if exists tmp_tg_hdl_ratio_maintenace; 

create temporary table tmp_tg_hdl_ratio_maintenace as 
select clientid,
max(first_value_available_bloodworkDate) as first_value_available_bloodworkDate,
max(start_tg_hdl_ratio) as start_tg_hdl_ratio,

max(last_value_available_bloodworkDate) as last_value_available_bloodworkDate,
max(last_tg_hdl_ratio) as last_tg_hdl_ratio
from
(
	select s.clientid,
	case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then s.firstbloodwork else null end as first_value_available_bloodworkDate,
    case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then cast(triglycerides/hdlCholesterol as decimal(10,2)) else null end as start_tg_hdl_ratio,

	case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then s.last_value_available_bloodworkDate else null end as last_value_available_bloodworkDate,
	case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then cast(triglycerides/hdlCholesterol as decimal(10,2))  else null end as last_tg_hdl_ratio
	from
	(
			select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork, max(ifnull(bloodworktime, eventtime)) as last_value_available_bloodworkDate  from clinictestresults
			where triglycerides is not null and hdlCholesterol is not null
			and deleted = 0
			group by clientid
	) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) or s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime))
) s
group by clientid
;

create index idx_tmp_tg_hdl_ratio_maintenace on tmp_tg_hdl_ratio_maintenace(clientid, start_tg_hdl_ratio, last_tg_hdl_ratio); 

update dim_client a inner join tmp_tg_hdl_ratio_maintenace b on (a.clientid = b.clientid)
set a.start_tg_hdl_ratio = b.start_tg_hdl_ratio,
	a.last_tg_hdl_ratio = b.last_tg_hdl_ratio
where a.is_row_current = 'Y'
;


		-- BP sometimes is collected during vital and conditions collection. For such patients bring in the values from lab investigation. 
			drop temporary table if exists tmp_bp_5d_from_sensor; 
			
			create temporary table tmp_bp_5d_from_sensor as 
			select s.clientid, 
			max(case when s.first_available_date = s1.measure_event_date then s.first_available_date else null end) as firstBPavailableDate, 
			max(case when s.first_available_date = s1.measure_event_date then round(systolic_5d) else null end) as first_systolicBP,
			max(case when s.first_available_date = s1.measure_event_date then round(diastolic_5d) else null end) as first_diastolicBP,
			
			max(case when s.first_available_date <> s.last_available_bp_date and s.last_available_bp_date = s1.measure_event_date then s.last_available_bp_date else null end) as lastBPavailableDate, 
			max(case when s.first_available_date <> s.last_available_bp_date and s.last_available_bp_date = s1.measure_event_date then round(systolic_5d) else null end) as last_systolicBP,
			max(case when s.first_available_date <> s.last_available_bp_date and s.last_available_bp_date = s1.measure_event_date then round(diastolic_5d) else null end) as last_diastolicBP
			from 
			(
				select clientid, min(measure_event_date) as first_available_date, max(measure_event_date) as last_available_bp_date 
				from bi_patient_measures 
				where systolic_5d is not null 
				group by clientid
			) s inner join bi_patient_measures s1 on s.clientid = s1.clientid and (s.first_available_date = s1.measure_event_date or s.last_available_bp_date = s1.measure_event_date) 
			group by s.clientid
			;
			
			create index idx_tmp_bp_5d_from_sensor on tmp_bp_5d_from_sensor(clientid asc, first_systolicBP, first_diastolicBP, last_systolicBP, last_diastolicBP)
			;
			
			update dim_client a inner join tmp_bp_5d_from_sensor b on (a.clientid = b.clientid)
			set a.start_systolicBP = b.first_systolicBP,
				a.start_diastolicBP = b.first_diastolicBP,
				a.last_systolicBP = b.last_systolicBP,
				a.last_diastolicBP = b.last_diastolicBP
			where a.is_row_current = 'Y'
			;
			
			drop temporary table if exists tmp_bp_5d_from_sensor
			;
			
			commit; 
			
			-- some patients might have data in bloodreport. so if the values are blank after sensor update, bring the values from bloodwork. 
			drop temporary table if exists tmp_bp_maintenace; 

			create temporary table tmp_bp_maintenace as 
			select clientid,
			max(first_value_available_bloodworkDate) as first_value_available_bloodworkDate,
			max(start_sys_BP) as start_sys_BP,
			max(start_Dias_BP) as start_dias_BP,
			max(last_value_available_bloodworkDate) as last_value_available_bloodworkDate,
			max(last_sys_bp) as last_sys_bp,
			max(last_dias_bp) as last_dias_bp
			from
			(
				select s.clientid,
				case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then s.firstbloodwork else null end as first_value_available_bloodworkDate,
				case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then systolicBP else null end as start_sys_BP,
				case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then diastolicBP else null end as start_dias_BP,
				case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then s.last_value_available_bloodworkDate else null end as last_value_available_bloodworkDate,
				case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then systolicBP else null end as last_sys_BP,
				case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then diastolicBP else null end as last_dias_BP
				from
				(
						select clientid, min(ifnull(bloodworktime, eventtime))as firstbloodwork, max(ifnull(bloodworktime, eventtime)) as last_value_available_bloodworkDate 
						from clinictestresults
						where systolicBP is not null and diastolicBP is not null
						and deleted = 0
						group by clientid
				) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) or s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime))
			) s   
			group by clientid
			;

			create index idx_tmp_bp_maintenace on tmp_bp_maintenace(clientid, start_sys_BP, start_Dias_BP, last_sys_BP, last_dias_BP); 

			update dim_client a inner join tmp_bp_maintenace b on (a.clientid = b.clientid)
			set a.start_systolicBP = b.start_sys_BP,
				a.start_diastolicBP = b.start_Dias_BP,
				a.last_systolicBP = b.last_sys_BP,
				a.last_diastolicBP = b.last_dias_BP
			where a.is_row_current = 'Y'
			and (a.start_systolicBP is null and a.start_diastolicBP is null)
			;


			drop temporary table if exists tmp_bp_maintenace
            ;

-- Calculate and store start and latest NAFLD LFS score for Liver Health
		drop temporary table if exists tmp_liverHealth_maintenace; 

		create temporary table tmp_liverHealth_maintenace as 
		select clientid,
		max(first_value_available_bloodworkDate) as first_value_available_bloodworkDate,
		max(start_tg) as start_tg,
		max(start_hdl) as start_hdl,
		max(start_fasting_glucose) as start_fasting_glucose, 
		max(start_tg_hdl_ratio) as start_tg_hdl_ratio,
		max(start_ast) as start_ast,
		max(start_alt) as start_alt, 
		max(start_insulin) as start_insulin,
		max(start_ast_alt_ratio) as start_ast_alt_ratio,

		max(last_value_available_bloodworkDate) as last_value_available_bloodworkDate,
		max(last_tg) as last_tg,
		max(last_hdl) as last_hdl,
		max(last_fasting_glucose) as last_fasting_glucose,
		max(last_tg_hdl_ratio) as last_tg_hdl_ratio,
		max(last_ast) as last_ast,
		max(last_alt) as last_alt,
		max(last_insulin) as last_insulin,
		max(last_ast_alt_ratio) as last_ast_alt_ratio
		from
		(
			select s.clientid,
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then s.firstbloodwork else null end as first_value_available_bloodworkDate,
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then triglycerides else null end as start_tg,
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then hdlCholesterol else null end as start_hdl, 
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then glucose else null end as start_fasting_glucose, 
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then ast else null end as start_ast, 
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then alt else null end as start_alt, 
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then cast(ast/alt as decimal(10,2)) else null end as start_ast_alt_ratio, 
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then insulin else null end as start_insulin, 
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then cast(triglycerides/hdlCholesterol as decimal(10,2)) else null end as start_tg_hdl_ratio,

			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then s.last_value_available_bloodworkDate else null end as last_value_available_bloodworkDate,
			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then triglycerides else null end as last_tg,
			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then hdlCholesterol else null end as last_hdl,
			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then glucose else null end as last_fasting_glucose,
			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then cast(triglycerides/hdlCholesterol as decimal(10,2))  else null end as last_tg_hdl_ratio,
			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then ast else null end as last_ast,
			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then alt else null end as last_alt,
			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then insulin else null end as last_insulin, 
			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then cast(ast/alt as decimal(10,2))  else null end as last_ast_alt_ratio
			from
			(
					select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork, max(ifnull(bloodworktime, eventtime)) as last_value_available_bloodworkDate  from clinictestresults
					where alt is not null and ast is not null and insulin is not null and glucose is not null
					and deleted = 0
					group by clientid
			) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) or s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime))
		) s
		group by clientid
		;

		create index idx_tmp_liverHealth_maintenace on tmp_liverHealth_maintenace (clientid asc, first_value_available_bloodworkDate, last_value_available_bloodworkDate); 

		drop temporary table if exists tmp_nafld_lfs; 

		create temporary table tmp_nafld_lfs as 
		select s.*, if(is_Inreversal = 'Yes' or is_metOnly_by_ops = 'Yes', 0, 2) as last_diabetes_flag,

		if(start_BMI_bcm is null, null, -2.89 + (1.18 * (if(is_StartMetabolicSyndrome = 'Y', 1, 0))) + (0.45 * 2) + (0.15 * start_insulin) + (0.04 * start_ast) - (0.94 * start_ast_alt_ratio)) as start_NAFLD_LFS,
		if(last_available_BMI is null, null, -2.89 + (1.18 * (if(is_LastMetabolicSyndrome = 'Y', 1, 0))) + (0.45 * (if(is_Inreversal = 'Yes' or is_metOnly_by_ops = 'Yes', 0, 2))) + (0.15 * last_insulin) + (0.04 * last_ast) - (0.94 * last_ast_alt_ratio)) as last_NAFLD_LFS

		from 
		(
			select s.*, 
			case when s.start_BMI_bcm >= 25 or s.start_tg >= 150 or s.start_hdl < if(gender = 'MALE', 40, 50) or s.start_fasting_glucose >= 100 or (ifnull(s.start_systolicBP,0) >= 130 or ifnull(s.start_diastolicBP,0) >= 85) then 'Y' else 'N' end as is_StartMetabolicSyndromeOLD,
			case when s.last_available_BMI >= 25 or s.last_tg >= 150 or s.last_hdl < if(gender = 'MALE', 40, 50)  or s.last_fasting_glucose >= 100 or (ifnull(s.last_systolicBP,0) >= 130 or ifnull(s.last_diastolicBP,0) >= 85) then 'Y' else 'N' end as is_LastMetabolicSyndromeOLD,
			
			case when (BMI_flag + TG_flag + HDL_flag + fastingGlucose_flag + BP_flag) >= 3 then 'Y' else 'N' end as is_StartMetabolicSyndrome,
			case when (last_BMI_flag + last_TG_flag + last_HDL_flag + last_fastingGlucose_flag + last_BP_flag) >= 3 then 'Y' else 'N' end as is_LastMetabolicSyndrome
		   
			
			from 
			(
				select a.clientid, a.start_systolicBP, a.start_diastolicBP, a.start_BMI_bcm, b.start_tg, b.start_hdl, b.start_fasting_glucose, start_ast, start_alt, start_ast_alt_ratio, start_insulin, 
				a.last_systolicBP, a.last_diastolicBP, a.last_available_BMI, last_ast, last_alt, last_ast_alt_ratio, last_insulin, last_hdl, last_fasting_glucose, last_tg, c.gender, c.status,
				if(a.start_BMI_bcm is not null and a.start_BMI_bcm >= 25, 1, 0) as BMI_flag, 
				if(b.start_tg is not null and b.start_tg >= 150, 1, 0) as TG_flag, 
				if(b.start_hdl is not null and b.start_hdl < if(gender = 'MALE', 40, 50), 1, 0) as HDL_flag, 
				if(b.start_fasting_glucose is not null and b.start_fasting_glucose >= 100 , 1, 0) as fastingGlucose_flag,
				if((a.start_systolicBP is not null or a.start_diastolicBP is not null) and (ifnull(a.start_systolicBP,0) >= 130 or ifnull(a.start_diastolicBP,0) >= 85), 1, 0) as BP_flag, 
				
				if(a.last_available_BMI is not null and a.last_available_BMI >= 25, 1, 0) as last_BMI_flag, 
				if(b.last_tg is not null and b.last_tg >= 150, 1, 0) as last_TG_flag, 
				if(b.last_hdl is not null and b.last_hdl < if(gender = 'MALE', 40, 50), 1, 0) as last_HDL_flag, 
				if(b.last_fasting_glucose is not null and b.last_fasting_glucose >= 100 , 1, 0) as last_fastingGlucose_flag,
				if((a.last_systolicBP is not null or a.last_diastolicBP is not null) and (ifnull(a.last_systolicBP,0) >= 130 or ifnull(a.last_diastolicBP,0) >= 85), 1, 0) as last_BP_flag
					  
				
				from dim_client a left join tmp_liverHealth_maintenace b on a.clientid = b.clientid
								  left join v_allClient_list c on a.clientid = c.clientid 
				where a.is_row_current = 'Y'
			) s 
			-- where (BMI_flag + TG_flag + HDL_flag + fastingGlucose_flag + BP_flag) >= 3
		) s left join (	
							select s.clientid, is_InReversal, is_metOnly_by_ops
							from 
							(
								select a.clientid, case when status = 'Active' then date_sub(date(now()), interval 1 day) else b.date end as dateChosen
								from v_client_tmp a inner join 
								(
									select clientid, max(measure_event_date) as date 
									from bi_patient_measures
									group by clientid 
								) b on a.clientid = b.clientid 
							) s left join bi_patient_measures s1 on s.clientid = s1.clientid and s1.measure_event_date = dateChosen
						) s1 on s.clientid = s1.clientid
		;

		create index idx_tmp_nafld_lfs on tmp_nafld_lfs (clientid asc, start_NAFLD_LFS, last_NAFLD_LFS); 

		update dim_client a inner join tmp_nafld_lfs b on a.clientid = b.clientid 
		set a.start_NAFLD_LFS = b.start_NAFLD_LFS,
			a.last_NAFLD_LFS = b.last_NAFLD_LFS
		where a.is_row_current = 'Y'
		; 
        
-- update dim_client set start_NAFLD_LFS = NULL, last_NAFLD_LFS = NULL where is_row_current = 'Y'

		drop temporary table if exists tmp_nafld_lfs;

		drop temporary table if exists tmp_liverHealth_maintenace; 

-- Now calculate the start and latest FRS (Framingham Score to calculate the Heart Health). 
drop temporary table if exists tmp_heartHealth_maintenace; 

create temporary table tmp_heartHealth_maintenace as 
select clientid,
max(first_value_available_bloodworkDate) as first_value_available_bloodworkDate,
max(start_hdl) as start_hdl,
max(start_TotalCholesterol) as start_TotalCholesterol, 


max(last_value_available_bloodworkDate) as last_value_available_bloodworkDate,
max(last_hdl) as last_hdl,
max(last_TotalCholesterol) as last_TotalCholesterol,

max(useSmoking) as useSmoking
from
(
	select s.clientid,
	case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then s.firstbloodwork else null end as first_value_available_bloodworkDate,
    case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then hdlCholesterol else null end as start_hdl, 
    case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then cholesterol else null end start_TotalCholesterol, 

	case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then s.last_value_available_bloodworkDate else null end as last_value_available_bloodworkDate,
    case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then hdlCholesterol else null end as last_hdl,
	case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then cholesterol else null end as last_TotalCholesterol, 

	case when s2.clientid is not null then 'Y' else 'N' end as useSmoking
	from
	(
			select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork, max(ifnull(bloodworktime, eventtime)) as last_value_available_bloodworkDate  from clinictestresults
			where Cholesterol is not null and hdlCholesterol is not null
			and deleted = 0
			group by clientid
	) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) or s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime))
		left join 			
            (
				select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork from clinictestresults
				where useSmoking = 1
				and deleted = 0
				group by clientid
            ) s2 on s.clientid = s2.clientid 
) s
group by clientid
;

create index idx_tmp_heartHealth_maintenace on tmp_heartHealth_maintenace(clientid asc, first_value_available_bloodworkDate, last_value_available_bloodworkDate); 


-- Now calculate the start and latest FRS (score to calculate the Heart Health). 
set @M_age := 3.06117, @M_TC := 1.12370, @M_HDL := -0.93263, @M_BP_TRT := 1.99881, @M_BP_NonTRT := 1.93303, @M_SMOKE := 0.65451, @M_DIAB := 0.57367; 
set @F_age := 2.32888, @F_TC := 1.20904, @F_HDL := -0.70833, @F_BP_TRT := 2.82263, @F_BP_NonTRT := 2.76157, @F_SMOKE := 0.52873, @F_DIAB := 0.69154; 

drop temporary table if exists tmp_FRS_score;
create temporary table tmp_FRS_score as 
select clientid, total_start_coeff, total_last_coeff, 
cast(case when gender = 'MALE' then 1 - power(0.88936, exp(total_start_coeff - 23.9802)) 
		  when gender = 'FEMALE' then 1 - power(0.95012, exp(total_start_coeff - 26.1931)) else null end * 100  as decimal(6,2)) as start_FRS,
     
cast(case when gender = 'MALE' then 1 - power(0.88936, exp(total_last_coeff - 23.9802))
		  when gender = 'FEMALE' then 1 - power(0.95012, exp(total_last_coeff - 26.1931)) else null end * 100 as decimal(6,2)) as last_FRS     
from 
(
	select clientid, gender, age, 
	age_coeff + sysBP_coeff + totalCholesterol_coeff + totalHDL_coeff + smoke_coeff + diab_coeff as total_start_coeff, 
	age_coeff + last_sysBP_coeff + last_totalCholesterol_coeff + last_totalHDL_coeff + smoke_coeff + last_diab_coeff as total_last_coeff
	from 
	( 
			select s.clientid, gender, age,
			log_age * if(gender = 'MALE', @M_age, @F_age) as age_coeff, 
			log_start_sysBP * case when gender = 'MALE' and isHTNTreated = 'Y' then @M_BP_TRT
								   when gender = 'MALE' and isHTNTreated = 'N' then @M_BP_NonTRT
								   when gender = 'FEMALE' and isHTNTreated = 'Y' then @F_BP_TRT
								   when gender = 'FEMALE' and isHTNTreated = 'N' then @F_BP_NonTRT else null end as sysBP_coeff, 
			log_startTotalCholesterol * if(gender = 'MALE', @M_TC, @F_TC) as totalCholesterol_coeff, 
			log_start_hdl * if(gender = 'MALE', @M_HDL, @F_HDL) as totalHDL_coeff, 
			if(useSmoking = 'Y', 1, 0) * if(gender = 'MALE', @M_SMOKE, @F_SMOKE)  as smoke_coeff, 
			1 * if(gender = 'MALE', @M_DIAB, @F_DIAB) as diab_coeff,


			log_last_sysBP * case when gender = 'MALE' and isHTNTreated = 'Y' then @M_BP_TRT
								   when gender = 'MALE' and isHTNTreated = 'N' then @M_BP_NonTRT
								   when gender = 'FEMALE' and isHTNTreated = 'Y' then @F_BP_TRT
								   when gender = 'FEMALE' and isHTNTreated = 'N' then @F_BP_NonTRT else null end as last_sysBP_coeff, 
			log_lastTotalCholesterol * if(gender = 'MALE', @M_TC, @F_TC) as last_totalCholesterol_coeff, 
			log_last_hdl * if(gender = 'MALE', @M_HDL, @F_HDL) as last_totalHDL_coeff, 
			if(is_InReversal = 'Yes' or is_metOnly_by_ops= 'Yes', 0, 1) * if(gender = 'MALE', @M_DIAB, @F_DIAB) as last_diab_coeff
			from 
			(
				select a.clientid, b.start_TotalCholesterol, b.last_TotalCholesterol, b.start_hdl, b.last_hdl, 
				b.useSmoking, c.age, a.start_systolicBP, a.last_systolicBP, if(a.start_htn_medicine_drugs is not null, 'Y', 'N') as isHTNTreated, c.gender,
				ln(if(b.start_TotalCholesterol>0, b.start_TotalCholesterol, null)) as log_startTotalCholesterol, 
				ln(if(b.last_TotalCholesterol>0, b.last_TotalCholesterol, null)) as log_lastTotalCholesterol, 
				ln(if(b.start_hdl>0, b.start_hdl, null)) as log_start_hdl,
				ln(if(b.last_hdl>0, b.last_hdl, null)) as log_last_hdl,
				ln(age) as log_age,
				ln(start_systolicBP) as log_start_sysBP,
				ln(last_systolicBP) as log_last_sysBP
				from 
				dim_client a left join tmp_heartHealth_maintenace b on a.clientid = b.clientid
							 left join v_client_tmp c on a.clientid = c.clientid
				where a.is_row_current = 'Y' 
			) s left join (	
								select s.clientid, is_InReversal, is_metOnly_by_ops
								from 
								(
									select a.clientid, case when status = 'Active' then date_sub(date(now()), interval 1 day) else b.date end as dateChosen
									from v_client_tmp a inner join 
									(
										select clientid, max(measure_event_date) as date 
										from bi_patient_measures
										group by clientid 
									) b on a.clientid = b.clientid 
								) s left join bi_patient_measures s1 on s.clientid = s1.clientid and s1.measure_event_date = dateChosen
							) s1 on s.clientid = s1.clientid
	) s 
) s 
; 

create index idx_tmp_FRS_score on tmp_FRS_score (clientid asc, start_FRS, last_FRS); 

update dim_client a inner join tmp_FRS_score b on a.clientid = b.clientid 
set a.start_FRS = b.start_FRS,
	a.last_FRS = b.last_FRS
where a.is_row_current = 'Y'
; 

drop temporary table if exists tmp_FRS_score;

drop temporary table if exists tmp_heartHealth_maintenace; 


-- start and last hsCRP
	drop temporary table if exists tmp_hsCRP_maintenace; 

	create temporary table tmp_hsCRP_maintenace as 
	select clientid,
	max(first_value_available_bloodworkDate) as first_value_available_bloodworkDate,
	max(start_hsCRP) as start_hsCRP,
	max(last_value_available_bloodworkDate) as last_value_available_bloodworkDate,
	max(last_hsCRP) as last_hsCRP
	from
	(
		select s.clientid,
		case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then s.firstbloodwork else null end as first_value_available_bloodworkDate,
		case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then hsCReactive else null end as start_hsCRP,
		case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then s.last_value_available_bloodworkDate else null end as last_value_available_bloodworkDate,
		case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then hsCReactive else null end as last_hsCRP
		from
		(
				select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork, max(ifnull(bloodworktime, eventtime)) as last_value_available_bloodworkDate  from clinictestresults
				where hsCReactive is not null
				and deleted = 0
				group by clientid
		) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) or s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime))
	) s
	group by clientid
	;

	create index idx_tmp_hsCRP_maintenace on tmp_hsCRP_maintenace(clientid, start_hsCRP, last_hsCRP); 

	update dim_client a inner join tmp_hsCRP_maintenace b on (a.clientid = b.clientid)
	set a.start_hsCRP = b.start_hsCRP,
		a.last_hsCRP = b.last_hsCRP
	where a.is_row_current = 'Y'
	;

	drop temporary table if exists tmp_hsCRP_maintenace
	;

-- maintain start and last homa2IR
	drop temporary table if exists tmp_homa2IR_maintenace; 

	create temporary table tmp_homa2IR_maintenace as 
	select clientid,
	max(first_value_available_bloodworkDate) as first_value_available_bloodworkDate,
	max(start_homa2IR) as start_homa2IR,
	max(last_value_available_bloodworkDate) as last_value_available_bloodworkDate,
	max(last_homa2IR) as last_homa2IR
	from
	(
		select s.clientid,
		case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then s.firstbloodwork else null end as first_value_available_bloodworkDate,
		case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then homa2IR else null end as start_homa2IR,
		case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then s.last_value_available_bloodworkDate else null end as last_value_available_bloodworkDate,
		case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then homa2IR else null end as last_homa2IR
		from
		(
				select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork, max(ifnull(bloodworktime, eventtime)) as last_value_available_bloodworkDate  from clinictestresults
				where homa2IR is not null
				and deleted = 0
				group by clientid
		) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) or s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime))
	) s
	group by clientid
	;

	create index idx_tmp_homa2IR_maintenace on tmp_homa2IR_maintenace(clientid, start_homa2IR, last_homa2IR); 

	update dim_client a inner join tmp_homa2IR_maintenace b on (a.clientid = b.clientid)
	set a.start_homa2IR = b.start_homa2IR,
		a.last_homa2IR = b.last_homa2IR
	where a.is_row_current = 'Y'
	;

	drop temporary table if exists tmp_homa2IR_maintenace
	;

-- start and last homa2B
	drop temporary table if exists tmp_homa2b_maintenace; 

	create temporary table tmp_homa2b_maintenace as 
	select clientid,
	max(first_value_available_bloodworkDate) as first_value_available_bloodworkDate,
	max(start_homa2b) as start_homa2b,
	max(last_value_available_bloodworkDate) as last_value_available_bloodworkDate,
	max(last_homa2b) as last_homa2b
	from
	(
		select s.clientid,
		case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then s.firstbloodwork else null end as first_value_available_bloodworkDate,
		case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then homa2b else null end as start_homa2b,
		case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then s.last_value_available_bloodworkDate else null end as last_value_available_bloodworkDate,
		case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then homa2b else null end as last_homa2b
		from
		(
				select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork, max(ifnull(bloodworktime, eventtime)) as last_value_available_bloodworkDate  from clinictestresults
				where homa2b is not null
				and deleted = 0
				group by clientid
		) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) or s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime))
	) s
	group by clientid
	;

	create index idx_tmp_homa2b_maintenace on tmp_homa2b_maintenace(clientid, start_homa2b, last_homa2b); 

	update dim_client a inner join tmp_homa2b_maintenace b on (a.clientid = b.clientid)
	set a.start_homa2b = b.start_homa2b,
		a.last_homa2b = b.last_homa2b
	where a.is_row_current = 'Y'
	;

	drop temporary table if exists tmp_homa2b_maintenace
	;

-- type1Like maintenance
	drop temporary table if exists tmp_type1Like_maintenance; 

	create temporary table tmp_type1Like_maintenance as 
	select clientid, status, is_type1Like, type1Like_category, cPeptide, homa2b, start_insulin_units, durationyears_diabetes, cgm_5d, ketone_5d
	from 
	(
		select a.clientid, a.status,f.cPeptide, f.homa2b, c.start_insulin_units, c.durationyears_diabetes, b1.cgm_5d, b1.ketone_5d, 
		if ((f.cPeptide <= 0.5 and ifnull(f.homa2b,0) <= 51 and c.start_insulin_units > 0) or (f.cPeptide < 1.1 and ifnull(f.homa2b,0) <= 51 and c.start_insulin_units > 0 and c.durationYears_diabetes >= 25) or (b1.cgm_5d > 175 and b1.ketone_5d > 0.4), 'Yes','No') as is_Type1Like,
        case when (f.cPeptide <= 0.5 and ifnull(f.homa2b,0) <= 51 and c.start_insulin_units > 0) then 'C1'
			 when (f.cPeptide < 1.1 and ifnull(f.homa2b,0) <= 51 and c.start_insulin_units > 0 and c.durationYears_diabetes >= 25) then 'C2'
             when (b1.cgm_5d > 175 and b1.ketone_5d > 0.4) then 'C3' else null end as type1Like_category
		from 																
		v_client_tmp a left join ( 
									select s.clientid, s.td8, s1.cgm_5d, s1.ketone_5d
									from 
									(
										select clientid, max(measure_event_date) as td8
										from bi_patient_measures 
										where treatmentdays = 9
										group by clientid 
									) s inner join bi_patient_measures s1 on s.clientid = s1.clientid and s.td8 = s1.measure_event_date
								) b1 on a.clientid = b1.clientid	
						   left join dim_client c on a.clientid = c.clientid and c.is_row_current = 'y'													
						   left join (																
										select distinct a.clientid, LabA1c, BMI, cPeptide, albumin, ast, alt, homa2B, creatinine, gfr 						
										from 								
										(								
											select s.clientid, s.durationYears_diabetes, LabA1c, gfr, albumin, ast, alt, creatinine, GADA, cPeptide, homa2B, BMI							
											from dim_client s inner join (
															select s.clientid, cast(avg(veinhba1c) as decimal(10,2)) as LabA1c, avg(glomerularFiltrationRate) as gfr, avg(albumin) as albumin, 							
															avg(ast) as ast, avg(alt) as alt, avg(Creatinine) as creatinine, 							
															avg(GADA) as GADA, avg(cPeptide) as cPeptide, avg(homa2B) as homa2B,							
															avg(Weight)/(avg(Height)*avg(Height)) as BMI	
															from 
															(
																select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork
																from clinictestresults 
																where deleted = 0
																group by clientid 
															) s inner join clinictestresults s1 on s.clientid = s1.clientid and s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime)
															group by clientid		
                                            ) c on s.clientid = c.clientid 
                                            where s.is_row_current = 'y'
										) a 
									) f on a.clientid = f.clientid 		
	) s 
	;

	create index idx_tmp_type1Like_maintenance on tmp_type1Like_maintenance (clientid asc, is_type1Like, type1Like_category)
	;

	update dim_client a inner join tmp_type1Like_maintenance b on a.clientid = b.clientid 
	set a.is_type1like = b.is_type1like,
		a.type1Like_category = b.type1Like_category
	where is_row_current = 'y'
	;

	drop temporary table if exists tmp_type1Like_maintenance
	;
    
/*Maintain the patient exclusion detail (if they are medically excluded) */
    
	drop temporary table if exists tmp_patient_exclusion; 

    create temporary table tmp_patient_exclusion as 
    select clientid, is_exclusion, exclusion_cohort from v_bi_patient_exclusion_detail_all_patients 
    ;
    
    create index idx_tmp_patient_exclusion on tmp_patient_exclusion(clientid asc, is_exclusion, exclusion_cohort); 
    
    update dim_client a left join tmp_patient_exclusion b on a.clientid = b.clientid
    set a.is_medical_exclusion = b.is_exclusion,
		a.medical_exclusion_cohort = b.exclusion_cohort
	where a.is_row_current = 'y'
    ; 
    
	drop temporary table if exists tmp_patient_exclusion; 
    
-- D10 success
drop temporary table if exists tmp_d10_clients; 

create temporary table tmp_d10_clients as 
select clientid, day10, success_ind from v_bi_patient_d10_detail
;

-- maintain d10
update dim_client a inner join 
tmp_d10_clients b on a.clientid = b.clientid 
set a.d10_success = b.success_ind
where a.is_row_current = 'Y'
;

drop temporary table tmp_d10_clients; 

drop temporary table if exists tmp_d35_clients; 

create temporary table tmp_d35_clients as 
select clientid, day35, success_ind from v_bi_patient_d35_detail
;

update dim_client a inner join 
tmp_d35_clients b on a.clientid = b.clientid 
set a.d35_success = b.success_ind
where a.is_row_current = 'Y'
;

drop temporary table tmp_d35_clients; 


-- maintain patient's starting measures. 
-- Starting Symptoms
update dim_client a left join 
(
	select clientid, group_concat(name) as startSymps 
	from clients a left join clinicreportconditions b on a.id = b.clientid 
	where type = 'symptom' and hascondition = 1
    and name not like '%none%'
	group by clientid
) b on a.clientid = b.clientid and a.is_row_current = 'Y'
set a.start_symptoms  = b.startSymps
;


-- Starting Medicine
update dim_client a inner join 
(
	select clientid, group_concat(name) as startMeds
	from clients a left join clinicreportmedications b on a.id = b.clientid 
	where hasmedicine = 1
	-- and date(b.eventtime) = date(enrollmentdate)
	group by clientid
) b on a.clientid = b.clientid and a.is_row_current = 'Y'
set a.start_medicine  = b.startMeds
;

drop temporary table if exists tmp_glucose_measures;

create temporary table tmp_glucose_measures as 
select clientid, cgm_1d as startGlucose 
from clients a left join bi_patient_measures b on a.id = b.clientid 
where date(enrollmentdate) = b.measure_event_date 
;

-- Starting glucose.
update dim_client a inner join 
tmp_glucose_measures b on a.clientid = b.clientid and a.is_row_current = 'Y'
set a.start_glucose  = b.startGlucose
;

-- Maintain the Start Medicine Diabetes count
update dim_client a inner join 
(
		select a.clientid, group_concat(distinct c.medicineName) as medicinename, group_concat(distinct f.epc, '(',f.category,')') as drugs
		from clinicreportmedications a inner join v_client_tmp b on a.clientid = b.clientid 
		left  join medicines c on a.medicationId = c.medicineID
		left join medicinepharmclasses d on c.medicineid = d.medicineid -- Medicines -> Drugs -> Drugclasses structure is broken now and it is replaced by FDA model - Medicines -> MedicinePharmClasses -> Pharmclassess
		left join pharmclasses f on d.pharmclassid = f.pharmclassid 
		where hasMedicine = 1 
        and category in ('DIABETES','INSULIN')
		group by clientid
) b on a.clientid = b.clientid 
set a.start_medicine_diabetes = b.medicinename,
	a.start_medicine_diabetes_drugs = b.drugs
where a.is_row_current = 'y'
;

-- Maintain the Start Medicine disease categories
-- CHOLES - Cholesterol, HTN - Hypertension, HYPT-THYROD - HYPOTHYROIDISM, HEART-DIS - HEARTDISEASE
update dim_client a inner join 
(
		select a.clientid, group_concat(distinct case when (pc.conditionName like '%CHOLESTEROL%' or pc.conditionName like '%Dyslipidemia%') then 'CHOLES' 
													  when pc.conditionName like '%HYPERTENSION%' then 'HTN'
													  when pc.conditionName like '%HYPOTHYROIDISM%' then 'HYPO-THYROID'
                                                      when pc.conditionName like '%HEARTDISEASE%' then 'HEART-DIS'
                                                      else null end ) as start_nondiabetic_conditions
		from clinicreportmedications a inner join v_client_tmp b on a.clientid = b.clientid 
		left  join medicines c on a.medicationId = c.medicineID
		left join medicinepharmclasses mpc on c.medicineID = mpc.medicineId 
		left join pharmclasses pc on mpc.pharmClassId = pc.pharmClassId 
		where hasMedicine = 1 
		-- and pc.conditionName not in ('DIABETES')
		group by clientid
) b on a.clientid = b.clientid 
set a.start_nondiabetic_conditions = b.start_nondiabetic_conditions
where a.is_row_current = 'y'
;

/*

-- Maintain the flag to show if patient is currently on Metformin(medicine drug name) only, Insulin and their all medicine drugs with the class name (diabetes, Insulin etc).
update dim_client a inner join 
(
		select a.clientid, eventdate, all_medicine, all_med_drug, total_meds, insulin_units, is_currently_metformin_only, is_currently_on_insulin, current_medicine_drugs
		from dim_client a left join 
		(
				select clientid, eventdate, group_concat(medicine) as all_medicine, group_concat(med_drug) as all_med_drug, 
				sum(total_meds) as total_meds, sum(insulin_units) as insulin_units
				from 
				(
					-- This section is to handle the existing records without the new column medicineID. 
					select clientId, eventdate, group_concat(concat(' ',med, '-', unit, 'mgx', amt) order by med) as medicine,
					group_concat(concat(' ', TYPE) order by Type) as med_drug, sum(case when type like '%insulin%' then unit else null end) as insulin_units,
					count(distinct type) as total_meds
					from 
					(
							select a.clientId, date(scheduledTime) as eventDate, Type,
							case when type = 'GLICLAZIDE' then 'Gc'
							  when type = 'GLIMEPIRIDE' then 'Gm'
							  when type = 'JANUVIA' then 'Jv'
							  when type like 'JANUMET%' then 'Jm'
							else left(type,1) end as med, if(targetValue < 1, left(targetValue,3),cast(floor(targetValue) as char(5)))  as unit,   count(type)  as amt  
							from clienttodoitems a inner join v_client_tmp c on a.clientid = c.clientid 
												   left join (select clientid, max(status_start_date) as start_date, max(status_end_date) as end_date from bi_patient_status 
															  where status= 'active' group by clientid) b4 on a.clientid = b4.clientid
							where category = 'medicine' and type not in ('medicine')
							and a.status in ('FINISHED','UNFINISHED')
							and date(a.scheduledTime) = case when c.status = 'active' then date_sub(date(now()), interval 1 day) else b4.end_date end
							group by clientId, date(scheduledTime), type , targetValue
						) s  
					group by clientId, eventDate
					
					union all 

					-- This section for a new medicine tracking change implemented on Dec 21, 2019. After this change implemented, each medicine in the ToDoItems will have the medicineID filled. 
					select clientid, eventdate, group_concat(concat(' ', medicine,'-', qty,unit,'x',amt) order by medicine) as medicine,
					group_concat(' ', left(medicine, 3), '(', med_drug,')x', amt) as med_drug, sum(case when med_drug like '%Insu%' then qty else null end) as insulin_units,
					sum(total_drugs) as total_meds    
					from 
					(
						select a.clientid, eventdate, case when b.medicinename = 'GLICLAZIDE' then 'Gc'
								  when b.medicinename = 'GLIMEPIRIDE' then 'Gm'
								  when b.medicinename = 'JANUVIA' then 'Jv'
								  when b.medicinename like 'JANUMET%' then 'Jm'
								  when b.medicinename = 'METFORMIN' then 'M'
								  else b.medicineName end as medicine, qty, b.unit, group_concat(distinct b2.drugname,'(',b3.category,')') as med_drug, count(distinct medicineName) as amt, count(b2.drugname) as total_drugs
								  -- drugname
						from (
								select a.clientid, date(scheduledtime) as eventdate, medicineid, sum(floor(targetvalue)) as qty 
								from clienttodoitems a inner join v_client_tmp c on a.clientid = c.clientid	
													   left join (select clientid, max(status_start_date) as start_date, max(status_end_date) as end_date from bi_patient_status 
																  where status= 'active' group by clientid) b4 on a.clientid = b4.clientid
								where category = 'medicine' and type='medicine' and a.status in ('FINISHED','UNFINISHED')
								and date(a.scheduledTime) = (case when c.status = 'active' then date_sub(date(now()), interval 1 day) else b4.end_date end)
								group by clientid, date(scheduledtime), medicineid
							  ) a left join medicines b on a.medicineid = b.medicineid 
											   left join medicinedrugs b1 on b.medicineid = b1.medicineid
											   left join drugs b2 on b1.drugid = b2.drugid
											   left join drugclasses b3 on b2.drugclassid = b3.drugclassid
						group by clientid, eventdate, medicinename
					) s group by clientid, eventdate
				) s 
				group by clientid, eventdate
		) s on a.clientid = s.clientid 
		where a.is_row_current = 'y'
) b on a.clientid = b.clientid 
set a.is_currently_metformin_only = (case when ltrim(rtrim(b.all_med_drug)) = 'METFORMIN' or ltrim(rtrim(b.all_med_drug)) = 'METFORMIN (DIAB)' then 'Yes' else null end),
	a.is_currently_on_insulin = (case when ltrim(rtrim(b.all_med_drug)) like '%insu%' then 'Yes' else null end),
    a.current_insulin_units = b.insulin_units,
    a.current_medicine_drugs = ltrim(rtrim(all_med_drug))
where a.is_row_current = 'Y'
;

-- Maintain the Start Insulin units
update dim_client a inner join 
(
		select clientid, eventdate, sum(case when category = 'Insulin' then total_units end) as total
		from                                  
		(
				select clientid, eventdate, category, sum(total_units) as total_units
				from 
				(
					select a.clientid, date(eventtime) as eventdate, category, c.medicineName, group_concat( distinct e.drugname, '(',f.className,')') as drugs, 
                    breakfastDosage+lunchDosage+dinnerDosage as total_units
					from clinicreportmedications a inner join v_client_tmp b on a.clientid = b.clientid 
					inner join medicines c on a.medicationId = c.medicineID
					left join medicinedrugs d on c.medicineid = d.medicineid 
					left join drugs e on d.drugid = e.drugid
					left join drugclasses f on e.drugclassid = f.drugclassid 
					where hasMedicine = 1 
					and f.category in ('Insulin')
					group by clientid, date(eventtime), category, c.medicineName
				) s 
				group by clientid, eventdate, category
			) s1
            group by clientid, eventdate
) b on a.clientid = b.clientid 
set a.start_insulin_units = b.total
where a.is_row_current = 'y'
;
*/

-- Maintain how long the patient is in Diabetes condition 
update dim_client a inner join 
clinicreportconditions b on (a.clientid = b.clientid and b.name like '%diab%' and b.hascondition = 1)
set a.durationYears_diabetes = b.durationYears
where a.is_row_current = 'y'
;

-- Maintain is_type1 flag in the dim_client table to use in health dashboard
update dim_client a inner join 
(
		select distinct clientid from v_client_tmp a 
		where exists 
		(select 1 from clinictestresults b where a.clientid = b.clientid and b.gada > 17)
)  b on a.clientid = b.clientid 
set is_type1 = 'Yes' 
where is_row_current = 'Y'
;


/* the section below maintains the nut_syntax, journey, continuous_reversaldays columns
-- alter table dim_client add column recent_consecutive_reversaldays int null after start_nondiabetic_conditions
-- alter table dim_client add column journey varchar(10) null after recent_consecutive_reversaldays
-- alter table dim_client add column nut_syntax varchar(10) null after journey
-- Lines 563 - 663
*/

drop temporary table if exists tmp_patient_in_reversal_data;
drop temporary table if exists tmp_patient_in_reversal_data1;

create temporary table tmp_patient_in_reversal_data as 
select clientid, measure_event_date as date, is_InReversal as Included_or_not_InREVERSAL 
from bi_patient_measures
where measure_event_date between date_sub(date(now()), interval 51 day) and date_sub(date(now()), interval 1 day)
;

create temporary table tmp_patient_in_reversal_data1 as select * from tmp_patient_in_reversal_data;

drop temporary table if exists tmp_journey_decider;
create temporary table tmp_journey_decider
(
clientid int not null,
recent_consecutive_reversaldays int null,
journey varchar(10) null,
nut_syntax varchar(10) null
); 

insert into tmp_journey_decider (clientid, recent_consecutive_reversaldays, journey, nut_syntax)
select s.clientid, recent_total_consecutivedays_InReversal, 
case when recent_total_consecutivedays_InReversal <= 14 then 'J1'
	 when recent_total_consecutivedays_InReversal between 15 and 21 then 'J2'
	 when recent_total_consecutivedays_InReversal between 22 and 45 then 'J3'
	 when recent_total_consecutivedays_InReversal > 45 then 'J4' end as journey, 
case when recent_total_consecutivedays_InReversal <= 14 then '1-4'
	 when recent_total_consecutivedays_InReversal between 15 and 21 then '1-7'
	 when recent_total_consecutivedays_InReversal between 22 and 45 then '1-10'
	 when recent_total_consecutivedays_InReversal > 45 then '1-14' end as nut_syntax
from 
(
	select curr_record as clientid, count as recent_total_consecutivedays_InReversal
	from 
	(
	select 	@prev_record := @clientid as prev_record, 
			@prev_date := @date as prev_date, 
			@prev_record_reversal := @Included_or_not_InREVERSAL as prev_record_reversal,
			@clientid := clientid as curr_record, @date := date as curr_date, @Included_or_not_InREVERSAL := Included_or_not_InREVERSAL as curr_record_reveral,
			@count := if(@prev_record = @clientid && @prev_record_reversal = @Included_or_not_InREVERSAL, @count + 1, 0) as count
	from tmp_patient_in_reversal_data a cross join (select @count := 0, @clientid := 0, @date := NULL, @Included_or_not_InREVERSAL = NULL, @prev_record = NULL, @prev_date = NULL, @prev_record_reversal = NULL) b
    order by clientid, date
	) s inner join (select clientid, max(date) as recent_date from tmp_patient_in_reversal_data1 group by clientid) s1 on s.curr_record = s1.clientid and s.curr_date = s1.recent_date
) s 
;


create index idx_tmp_journey_decider on tmp_journey_decider(clientid asc); 

update dim_client a inner join tmp_journey_decider b on a.clientid = b.clientid 
set a.recent_consecutive_reversaldays = b.recent_consecutive_reversaldays,
	a.journey = b.journey,
    a.nut_syntax = b.nut_syntax
where is_row_current = 'Y'
and status = 'active'
; 

drop temporary table if exists tmp_journey_decider;
drop temporary table if exists tmp_patient_in_reversal_data;
drop temporary table if exists tmp_patient_in_reversal_data1;

/* END of nut_syntax, journey, continuous_reversaldays columns maintenance */

-- Set up the patient health condition
drop temporary table if exists tmp_patient_health_condition;

create temporary table tmp_patient_health_condition as 
select clientid, if(count(case when is_BP_patient = 'Yes' then date else null end) > 0 , 'Yes', 'No')  as is_HTN_patient, 
if(count(case when is_Cholesterol_patient = 'Yes' then date else null end) > 0 , 'Yes', 'No')  as is_cholesterol_patient, 
if(count(case when is_HD_patient = 'Yes' then date else null end) > 0 , 'Yes', 'No')  as is_heartdisease_patient
from 
(
select distinct clientid, date, isBP_Condition, is_BP_Med, isBP_identified, 
case when (is_BP_Med = 'Yes' or isBP_identified = 'Yes' or isBP_Condition = 'Yes' ) then 'Yes'
	 else 'No' end as is_BP_patient, 
isCholesterol_Condition, is_Cholesterol_Med, is_cholesterol_identified, 
case when (is_Cholesterol_Med = 'Yes' or is_cholesterol_identified = 'Yes' or isCholesterol_Condition = 'Yes' or is_Cholesterol_identified_first_lab = 'Yes') then 'Yes'
     else 'No' end as is_Cholesterol_patient,
is_HD_Med, is_HD_condition, 
case when is_HD_Med ='Yes' or is_HD_condition = 'Yes' then 'Yes' else 'No' end as is_HD_patient
from 
(
		select s.clientid, s.date, s1.isBP_Condition, s2.is_BP_Med, 
		case when (systolic_5d is null or diastolic_5d is null) then null 
			 when (systolic_5d > 130 or diastolic_5d > 80) then 'Yes' else 'No' end as isBP_identified, 
		s11.isCholesterol_Condition, s2.is_Cholesterol_Med, if(s5.clientid is not null, 'Yes', 'No') as is_cholesterol_identified, if(s6.clientid is not null, 'Yes', 'No') as is_Cholesterol_identified_first_lab,
        s2.is_HD_Med
        ,case when dc.start_nondiabetic_conditions like '%HEART-DIS%' then 'Yes' else 'No' end as is_HD_condition
		from 
		(
				select distinct clientid, date
				from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active' 
				-- where a.date >= date_sub(date(itz(now())), interval 30 day) and a.date <= date(itz(now()))
		) s
		left join 
		(
				select distinct clientid, case when medicalConditionId = 6 and hascondition = 1 then 'Yes' else 'No' end as isBP_Condition
				from clinicreportconditions 
				where medicalConditionID in (6)  -- hyperTension, 57 Cholesteol, 38 - Dyslipidemia
				and hascondition = 1
		) s1 on s.clientid = s1.clientid 
		left join 
		(
				select distinct clientid, case when (medicalConditionId = 57 or medicalConditionId = 38) and hascondition = 1 then 'Yes' else 'No' end as isCholesterol_Condition
				from clinicreportconditions 
				where medicalConditionID in (57, 38)  -- hyperTension, 57 Cholesteol, 38 - Dyslipidemia
				and hascondition = 1
		) s11 on s.clientid = s11.clientid 
		left join ( -- This section is for current medicine records
				select distinct a.clientid, da.date, category, if(count(distinct case when subcategory = 'HEARTDISEASE' then a.medicineid end) >0 ,'Yes','No') as is_HD_Med,
                if(count(distinct case when subcategory = 'HYPERTENSION' then a.medicineid end) >0 ,'Yes','No') as is_BP_Med,
                if(count(distinct case when subcategory = 'CHOLESTEROL' then a.medicineid end) >0 ,'Yes','No') as is_Cholesterol_Med
				from v_date_to_date da inner join 
									(
										select a.clientid, date(pm.startdate) as startdate, date(pm.enddate) as enddate, pm.medicineId from prescriptions a 
										inner join prescriptionmedicines pm on a.prescriptionid = pm.prescriptionid	
									)  a on da.date between a.startdate and a.enddate 
									inner join v_client_tmp b on a.clientid = b.clientid 
									inner join medicines c on a.medicineId = c.medicineID
									left join medicinedrugs d on c.medicineid = d.medicineid 
									left join drugs e on d.drugid = e.drugid
									left join drugclasses f on e.drugclassid = f.drugclassid 
                group by a.clientid, da.date
		) s2 on s.clientid = s2.clientid and s.date = s2.date		
        left join bi_patient_measures s3 on s.clientid = s3.clientid and s.date = s3.measure_event_date
		left join (	-- Latest bloodwork essentials
					 select a.clientid, date(b.bloodworktime) as bloodworkdate, b.ldlCholesterol, b.triglycerides
                     from 
						(select clientid, max(bloodworktime) as latest_test from clinictestresults group by clientid ) a inner join clinictestresults b 
                        on a.clientid = b.clientid and a.latest_test = b.bloodworktime
					 where (b.ldlCholesterol > 100 or b.triglycerides > 150)
				   ) s5 on s.clientid = s5.clientid -- and s.date between date_sub(s5.bloodworkdate, interval 15 day) and s5.bloodworkdate
		left join (	-- First bloodwork essentials
					 select a.clientid, date(b.bloodworktime) as bloodworkdate, b.ldlCholesterol, b.triglycerides
                     from 
						(select clientid, min(bloodworktime) as first_test from clinictestresults group by clientid ) a inner join clinictestresults b 
                        on a.clientid = b.clientid and a.first_test = b.bloodworktime
					 where (b.ldlCholesterol > 100 or b.triglycerides > 150)
				   ) s6 on s.clientid = s6.clientid -- and s.date between date_sub(s5.bloodworkdate, interval 15 day) and s5.bloodworkdate
		 left join dim_client dc on s.clientid = dc.clientid and dc.is_row_current = 'y'
 ) s   
 ) s 
 group by clientid 
 ;
 
create index idx_tmp_patient_health_condition on tmp_patient_health_condition(clientid asc);
 
update dim_client a inner join tmp_patient_health_condition b on a.clientid = b.clientid 
set a.is_HTN_patient = b.is_HTN_patient,
	a.is_cholesterol_patient = b.is_cholesterol_patient,
	a.is_heartdisease_patient = b.is_heartdisease_patient
where a.is_row_current= 'y'
;

drop temporary table if exists tmp_patient_health_condition
;


-- START -- Maintain the Start medicine drugs for nondiabetic diseases (Hypertension, Cholesterol, HeartDisease)
drop temporary table if exists tmp_start_medicine_NONDIABETIC;

create temporary table tmp_start_medicine_NONDIABETIC as
select clientid, eventdate, 
count(distinct case when conditionName like '%HYPERTENSION%' then epc else null end) as HT_med_drugs,
count(distinct case when (conditionName like '%CHOLESTEROL%' or conditionName like '%Dyslipidemia%') then epc else null end) as CHOL_med_drugs,
count(distinct case when conditionName like '%HEARTDISEASE%' then epc else null end) as HD_med_drugs,
group_concat(distinct case when conditionName like '%HYPERTENSION%' then epc else null end) as HT_drugs,
group_concat(distinct case when (conditionName like '%CHOLESTEROL%' or conditionName like '%Dyslipidemia%') then epc else null end) as CHOL_drugs,
group_concat(distinct case when conditionName like '%HEARTDISEASE%' then epc else null end) as HD_drugs
from 
(
	select a.clientid, date(eventtime) as eventdate, pc.conditionName, c.medicineName, pc.epc
	from clinicreportmedications a inner join v_client_tmp b on a.clientid = b.clientid 
	inner join medicines c on a.medicationId = c.medicineID
	left join medicinedrugs d on c.medicineid = d.medicineid 
	left join medicinepharmclasses mpc on c.medicineID = mpc.medicineId 
	left join pharmclasses pc on mpc.pharmClassId = pc.pharmClassId 
	-- left join drugs e on d.drugid = e.drugid
	-- left join drugclasses f on e.drugclassid = f.drugclassid 
	where hasMedicine = 1 
	-- and f.subcategory in ('HYPERTENSION','CHOLESTEROL','HEARTDISEASE')
	
) s 
group by clientid, eventdate
;

create index idx_tmp_start_medicine_NONDIABETIC on tmp_start_medicine_NONDIABETIC (clientid asc, HT_med_drugs, CHOL_med_drugs, HD_med_drugs);

update dim_client a inner join tmp_start_medicine_NONDIABETIC b on a.clientid = b.clientid 
set a.start_HTN_medicine_drugs = b.HT_drugs, 
	a.start_CHOL_medicine_drugs = b.CHOL_drugs,
    a.start_HeartDisease_medicine_drugs = b.HD_drugs
where a.is_row_current = 'y'
; 

drop temporary table if exists tmp_start_medicine_NONDIABETIC;

-- END -- Maintain the Start medicine drugs for nondiabetic diseases (Hypertension, Cholesterol, HeartDisease)

-- START -- Maintain the current nondiabetic medicine drugs the patient is on - HyperTension, Cholesterol, HeartDisease
drop temporary table if exists tmp_current_medicine_NONDIABETIC;

create temporary table tmp_current_medicine_NONDIABETIC as 
select clientid, current_status, HT_total_med_drugs, CHOL_total_med_drugs, HD_total_med_drugs, HT_drugs,CHOL_drugs,HD_drugs
from 
(
select s.clientid, c.status as current_status, startdate, enddate, c1.start_date as active_start_date, c1.end_date as active_end_date, 
count(distinct case when conditionName like '%HYPERTENSION%' then epc else null end) as HT_total_med_drugs,
count(distinct case when (conditionName like '%CHOLESTEROL%' or conditionName like '%Dyslipidemia%') then epc else null end) as CHOL_total_med_drugs,
count(distinct case when conditionName like '%HEARTDISEASE%' then epc else null end) as HD_total_med_drugs,
group_concat(distinct case when conditionName like '%HYPERTENSION%' then epc else null end) as HT_drugs,
group_concat(distinct case when (conditionName like '%CHOLESTEROL%' or conditionName like '%Dyslipidemia%') then epc else null end) as CHOL_drugs,
group_concat(distinct case when conditionName like '%HEARTDISEASE%' then epc else null end) as HD_drugs
from 
(
    select distinct clientid, conditionName, epc, min(startdate) as startdate, max(enddate) as enddate -- this is because there are multiple prescriptions for the same medicine so find out the min and max date by drug
    from 
    (
    select distinct P.clientId, pm.medicineid, pc.conditionName, c.medicineName, pc.epc,-- group_concat( distinct e.drugname) as drugs, 
	date(pm.startLocaldate) as startdate, date(pm.endLocaldate) as enddate 
	from prescriptions P join prescriptionmedicines PM on P.prescriptionid = PM.prescriptionid
						left join medicines c on pm.medicineid = c.medicineid
						left join medicinepharmclasses mpc on c.medicineID = mpc.medicineId 
						left join pharmclasses pc on mpc.pharmClassId = pc.pharmClassId 
						-- left join drugs e on d.drugid = e.drugid
						-- left join drugclasses f on e.drugclassid = f.drugclassid 
						inner join v_client_tmp ac on p.clientid = ac.clientid
	-- where f.subcategory in ('HYPERTENSION','CHOLESTEROL','HEARTDISEASE')
    where (pm.startLocaldate is not null and pm.endLocalDate is not null)
    ) s 
	group by clientid, conditionName, epc
) s left join v_client_tmp c on s.clientid = c.clientid 
	left join (select clientid, max(status_Start_Date) as start_date, max(status_end_date) as end_date from bi_patient_status where status = 'active' group by clientid) c1 on s.clientid = c1.clientid
group by clientid, startdate, enddate
) s
where (case when current_status = 'active' then date_sub(date(now()), interval 1 day) else active_end_date end) between startdate and enddate
;

create index idx_tmp_current_medicine_NONDIABETIC on tmp_current_medicine_NONDIABETIC (clientid asc, HT_total_med_drugs, CHOL_total_med_drugs, HD_total_med_drugs);

update dim_client a inner join tmp_current_medicine_NONDIABETIC b on a.clientid = b.clientid 
set a.current_HTN_medicine_drugs = b.HT_drugs, 
	a.current_CHOL_medicine_drugs = b.CHOL_drugs,
    a.current_HeartDisease_medicine_drugs = b.HD_drugs
where a.is_row_current = 'y'
; 

drop temporary table if exists tmp_current_medicine_NONDIABETIC;
-- END -- Maintain the current nondiabetic medicine drugs the patient is on - HyperTension, Cholesterol, HeartDisease


/*log the endtime*/
update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_bi_dim_client'
;


END