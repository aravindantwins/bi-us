DELIMITER $$
CREATE DEFINER=`aravindan`@`%` PROCEDURE `sp_load_bi_doctor_measure_x_scorecard`(  )
BEGIN



update dailyprocesslog 
set startdate = now()
where processname = 'sp_load_bi_doctor_measure_x_scorecard'
;

drop temporary table if exists tmp_bi_doctor_measure_x_scorecard; 

create temporary table tmp_bi_doctor_measure_x_scorecard (
	report_date date not null,
	doctorId varchar(50) not null,
	measure_name varchar(50) not null,
	measure_group varchar(50) not null,
	measure decimal(7,2) null
); 


insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select a.date, c.doctorId, 'Active' as measure_name, '# of PT' as measure_group, count(distinct b.clientid) as measure
from v_date_to_date a left join bi_patient_Status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
					  left join v_Client_tmp c on b.clientid = c.clientid
where a.date >= date_sub(date(itz(now())), interval 1 month) and a.date <= date(itz(now()))
and c.patientname not like '%obsolete%'
and c.doctorId is not null
group by a.date, doctorId
;

insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select a.date, c.doctorId, 'Pending Active' as measure_name, '# of PT' as measure_group, count(distinct b.clientid) as measure
from v_date_to_date a left join bi_patient_Status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  left join v_Client_tmp c on b.clientid = c.clientid
where a.date >= date_sub(date(itz(now())), interval 1 month) and a.date <= date(itz(now()))
and c.patientname not like '%obsolete%'
and c.doctorId is not null
group by a.date, c.doctorId
;

insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select a.date, c.doctorId, 'Manual Cohort' as measure_name, '# of PT' as measure_group, 
count(distinct case when pt.cohort = 'M' and (c.labels not like '%RESEARCH%' and labels not like '%unqualified%') then pt.clientid else null end) as measure
from v_date_to_date a left join bi_patient_Status b on a.date between b.status_start_date and b.status_end_date 
					  left join v_Client_tmp c on b.clientid = c.clientid
                      left join bi_patient_measures pt on a.date = pt.measure_event_date and b.clientid = pt.clientid 
where a.date >= date_sub(date(itz(now())), interval 1 month) and a.date <= date(itz(now()))
and c.patientname not like '%obsolete%'											
and c.doctorId is not null
group by a.date, c.doctorId
;


insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 

select a.measure_event_date, c.doctorId, 'Health' as measure_name, 'pts_with_treatDays_35' as measure_group, count(distinct case when a.treatmentdays >= 36 then a.clientid else null end) as measure 
from bi_patient_measures a left join v_client_tmp c on a.clientid = c.clientid
where a.measure_event_date >= date_sub(date(itz(now())), interval 1 month) and a.measure_event_date <= date(itz(now()))
and c.patientname not like '%obsolete%'
and c.doctorId is not null
group by a.measure_event_date, c.doctorId
;

	
	drop temporary table if exists tmp_7d_caseload;

	create temporary table tmp_7d_caseload as 
	select a.date, doctorId,
    max(case when measure_name = 'Active' and measure_group = '# of PT' then measure else null end) as max_active_cnt_7day,
    max(case when measure_name = 'Pending Active' and measure_group = '# of PT' then measure else null end) as max_PA_cnt_7day,
    max(case when measure_group = 'pts_with_treatDays_35' then measure else null end) as max_active_cnt_35above_7day,
	max(case when measure_name = 'Manual Cohort' and measure_group = '# of PT' then measure else null end) as max_Manual_cnt_7day   
    from v_date_to_date a left join tmp_bi_doctor_measure_x_scorecard b on b.report_date between date_sub(a.date, interval 6 day) and a.date
	where date >= date_sub(date(itz(now())), interval 1 month) and date <= date(itz(now()))
    group by a.date, doctorId
	;

	insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
	select date, doctorId, 'Active' as measure_name, 'active_cnt_7d' as measure_group, max_active_cnt_7day
	from tmp_7d_caseload
	where date >= date_sub(date(itz(now())), interval 1 month) and date <= date(itz(now()))
	;

	insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
	select date, doctorId, 'Pending Active' as measure_name, 'pending_active_cnt_7d' as measure_group, max_PA_cnt_7day
	from tmp_7d_caseload
	where date >= date_sub(date(itz(now())), interval 1 month) and date <= date(itz(now()))
	;
    
	insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
	select date, doctorId, 'Health' as measure_name, 'max_active_cnt_35above_7day' as measure_group, max_active_cnt_35above_7day
	from tmp_7d_caseload
	where date >= date_sub(date(itz(now())), interval 1 month) and date <= date(itz(now()))
	;

	insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
	select date, doctorId, 'Manual Cohort' as measure_name, 'max_manual_cohort_7day' as measure_group, max_Manual_cnt_7day
	from tmp_7d_caseload
	where date >= date_sub(date(itz(now())), interval 1 month) and date <= date(itz(now()))
	;

	drop temporary table if exists tmp_7d_caseload;



drop temporary table if exists tmp_med_recommendation; 

create temporary table tmp_med_recommendation as 
select f1.clientid, f1.doctorId, recommendationDate, status, 
case when f1.recommendationdate <> f2.newpublishingdate then 'Repeated' 
	 when f1.recommendationdate = f2.newpublishingdate then 'New' else null end as publishStatus, actionTime
from
(
		select distinct a.clientid, a.doctorId, 
		date(recommendationdate) as recommendationdate,
		status, actionTime
		from
		(
			select a.clientid, a.doctorId, a.medicationTierId, eventtime as recommendationdate, status, itz(b.dateAdded) as actionedDate, timestampdiff(HOUR, a.eventtime, itz(b.dateAdded)) as actionTime
			from
			(
					select medicationRecommendationId, a.clientid, itz(eventtime) as eventtime, a.status, medicationTierId, type, b.doctorId
					from medicationrecommendations a inner join v_client_tmp b on a.clientid = b.clientid 
					where type = 'DIABETES'
					and (labels not like '%RESEARCH%' and labels not like '%unqualified%')
			) a inner join medicationhistories b on a.clientid = b.clientid and a.medicationRecommendationId = b.medicationRecommendationId
		) a
		where date(recommendationdate) >= date_sub(date(itz(now())), interval 1 month)
) f1 left join (select clientid, min(date(itz(eventtime))) as newpublishingdate from medicationrecommendations where type = 'DIABETES' group by clientid) f2 on f1.clientid = f2.clientid  
;

insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select date, doctorId, 'Med Adoption' as measure_name, 'meds-Automated' as measure_group, count(distinct case when status in ('AUTOMATED') then clientid else null end) as measure
from v_date_to_date a left join tmp_med_recommendation b on b.recommendationdate between date_sub(a.date, interval 6 day) and a.date
where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
group by date, doctorId
;

insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select date, doctorId, 'Med Adoption' as measure_name, 'meds-totalRecommended' as measure_group, count(distinct case when status in ('AUTOMATED', 'OVERRIDDEN', 'SKIPPED_OVERRIDDEN', 'SKIPPED') then clientid else null end) as measure
from v_date_to_date a left join tmp_med_recommendation b on b.recommendationdate between date_sub(a.date, interval 6 day) and a.date
where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
group by date, doctorId
;


insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select date, doctorId, 'Med Adoption' as measure_name, 'meds-automated-within-SLA' as measure_group, 
count(distinct case when publishStatus = 'New' and status = 'AUTOMATED' and actionTime <= 4 then clientid 
					when publishStatus = 'Repeated' and status = 'AUTOMATED' and actionTime <= 24 then clientid else null end) as measure
from v_date_to_date a left join tmp_med_recommendation b on b.recommendationdate between date_sub(a.date, interval 6 day) and a.date
where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
group by date, doctorId
;

insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select date, doctorId, 'Med Adoption' as measure_name, 'NEW-meds-automated-total' as measure_group, 
count(distinct case when publishStatus = 'New' and status = 'AUTOMATED' then clientid else null end) as measure
from v_date_to_date a left join tmp_med_recommendation b on b.recommendationdate between date_sub(a.date, interval 6 day) and a.date
where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
group by date, doctorId
;

insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select date, doctorId, 'Med Adoption' as measure_name, 'NEW-meds-automated-within-SLA' as measure_group, 
count(distinct case when publishStatus = 'New' and status = 'AUTOMATED' and actionTime <= 4 then clientid else null end) as measure
from v_date_to_date a left join tmp_med_recommendation b on b.recommendationdate between date_sub(a.date, interval 6 day) and a.date
where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
group by date, doctorId
;

insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select date, doctorId, 'Med Adoption' as measure_name, 'REPEAT-meds-automated-total' as measure_group, 
count(distinct case when publishStatus = 'Repeated' and status = 'AUTOMATED' then clientid else null end) as measure
from v_date_to_date a left join tmp_med_recommendation b on b.recommendationdate between date_sub(a.date, interval 6 day) and a.date
where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
group by date, doctorId
;

insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select date, doctorId, 'Med Adoption' as measure_name, 'REPEAT-meds-automated-within-SLA' as measure_group, 
count(distinct case when publishStatus = 'Repeated' and status = 'AUTOMATED' and actionTime <= 24 then clientid else null end) as measure
from v_date_to_date a left join tmp_med_recommendation b on b.recommendationdate between date_sub(a.date, interval 6 day) and a.date
where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
group by date, doctorId
;

insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select a.date, doctorId, 'Member Referral' as measure_name, '# of PT' as measure_group, count(distinct clientid ) as measure
from v_date_to_date a left join v_client_tmp b on date(b.enrollmentdate) between date_sub(a.date, interval 6 day) and a.date
where b.source = 'DOCTOR'
and a.date >= date_sub(date(now()), interval 30 day) and a.date <= date(now())
group by a.date, doctorId
;


insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select date, apptDoctId, 'Doctor Appointment' as measure_name, '# of appts completed' as measure_group, count(distinct case when da_status = 'COMPLETED' then clientid else null end) as measure
from v_date_to_date a left join 
(
	select a.clientid, itz(a.eventtime) as DA_scheduled_time, a.status as DA_status, itz(a.dateadded) as DA_schedule_created_date, d.doctorID as apptDoctId, dl.doctornameShort
	from clientappointments a inner join doctorappointments d on a.clientid = d.clientid and a.clientAppointmentId = d.clientAppointmentId
							  left join v_client_tmp c on a.clientid = c.clientid 
                              left join v_alldoctors_list dl on d.doctorid = dl.doctorid
	where date(itz(a.eventtime)) >= date_sub(date(itz(now())), interval 30 day) and date(itz(a.eventtime)) <= date(itz(now()))
) b on date(b.DA_scheduled_time) between date_sub(a.date, interval 6 day) and a.date
where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
group by date, apptDoctId
;

insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select date, apptDoctId, 'Doctor Appointment' as measure_name, '# of appts scheduled' as measure_group, count(distinct case when da_status in ('COMPLETED', 'SCHEDULED') then clientid else null end) as measure
from v_date_to_date a left join 
(
	select a.clientid, itz(a.eventtime) as DA_scheduled_time, a.status as DA_status, itz(a.dateadded) as DA_schedule_created_date, d.doctorID as apptDoctId, dl.doctornameShort
	from clientappointments a inner join doctorappointments d on a.clientid = d.clientid and a.clientAppointmentId = d.clientAppointmentId
							  left join v_client_tmp c on a.clientid = c.clientid 
                              left join v_alldoctors_list dl on d.doctorid = dl.doctorid
	where date(itz(a.eventtime)) >= date_sub(date(itz(now())), interval 30 day) and date(itz(a.eventtime)) <= date(itz(now()))
) b on date(b.DA_scheduled_time) between date_sub(a.date, interval 6 day) and a.date
where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
group by date, apptDoctId
;

	drop temporary table if exists tmp_IMTs_record; 
    
	create temporary table tmp_IMTs_record as 
	select a.incidentId, a.clientid, b.doctornameShort, b.doctorId, category, symptom_severity, a.status, subtype_L1, 
	itz(report_time) as report_time,
    date(itz(report_time)) as report_date,
    if(a.status = 'OPEN', itz(now()), itz(resolved_time)) as resolved_time, 
    itz(owner_entry_date) as owner_entry_date, 
	itz(first_response_time) as first_response_time, 
	address_time_hours, 
	close_duration_hours
	from bi_incident_mgmt a left join v_client_tmp b on a.clientid = b.clientid 
							left join incident c on a.incidentId = c.incidentId
	where category in ('symptoms_handling','patient_treatment') 
    and c.incidentRecipientTypes like '%DOCTOR%'
	and date(report_time) >= date_sub(date(itz(now())), interval 1 month)
	;


    create index idx_tmp_IMTs_record on tmp_IMTs_record(incidentId, clientid, report_time, resolved_time); 

insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select date, doctorId, 'IMT' as measure_name, '# of total imts' as measure_group, count(distinct incidentId) as measure
from v_date_to_date a left join tmp_IMTs_record imt on date(report_time) between date_sub(a.date, interval 6 day) and a.date
where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
group by date, doctorId
;

insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select date, doctorId, 'IMT' as measure_name, '# of total imts resolved <= 1hr' as measure_group, count(distinct case when timestampdiff(hour, report_time, resolved_time) <= 1 then incidentId else null end) as measure
from v_date_to_date a left join tmp_IMTs_record imt on date(report_time) between date_sub(a.date, interval 6 day) and a.date
where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
group by date, doctorId
;


insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select date(itz(now())) as date, doctorId, 'IMT' as measure_name, '# of OPEN imts' as measure_group, count(distinct incidentId) as measure
from tmp_IMTs_record imt 
where status = 'OPEN'
group by date(itz(now())), doctorId
;

insert into tmp_bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure) 
select date(itz(now())) as date, doctorId, 'IMT' as measure_name, '# of OPEN imts open >= 24hr' as measure_group, count(distinct case when timestampdiff(hour, report_time, date(itz(now()))) >= 24 then incidentId else null end) as measure
from tmp_IMTs_record imt 
where status = 'OPEN'
group by date(itz(now())), doctorId
;

	drop temporary table if exists tmp_IMTs_record; 
	drop temporary table if exists tmp_med_recommendation; 


insert into bi_doctor_measure_x_scorecard (report_date, doctorId, measure_name, measure_group, measure)
select report_date, doctorId, measure_name, measure_group, measure
from tmp_bi_doctor_measure_x_scorecard a
where not exists (select 1 from bi_doctor_measure_x_scorecard b where a.report_date = b.report_date and a.doctorId = b.doctorId and a.measure_name = b.measure_name and a.measure_group = b.measure_group)
and a.report_date >= date_sub(date(now()), interval 20 day) 
;


update bi_doctor_measure_x_scorecard a inner join tmp_bi_doctor_measure_x_scorecard b on (a.report_date = b.report_date and a.doctorId = b.doctorId and a.measure_name = b.measure_name and a.measure_group = b.measure_group)
set a.measure = b.measure
where a.report_date >= date_sub(date(now()), interval 20 day) 
;

drop temporary table if exists tmp_bi_doctor_measure_x_scorecard; 

-- 30d calculation maintenance
				drop temporary table if exists tmp_bi_doctor_measure_x_scorecard_30d; 

				create temporary table tmp_bi_doctor_measure_x_scorecard_30d (
					report_date date not null,
					doctorId varchar(50) not null,
					measure_name varchar(50) not null,
					measure_group varchar(50) not null,
					measure decimal(7,2) null
				); 


				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select a.date, c.doctorId, 'Active' as measure_name, '# of PT' as measure_group, count(distinct b.clientid) as measure
				from v_date_to_date a left join bi_patient_Status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
									  left join v_Client_tmp c on b.clientid = c.clientid
				where a.date >= date_sub(date(itz(now())), interval 2 month) and a.date <= date(itz(now()))
				and c.patientname not like '%obsolete%'
				and c.doctorId is not null
				group by a.date, doctorId
				;

				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select a.date, c.doctorId, 'Pending Active' as measure_name, '# of PT' as measure_group, count(distinct b.clientid) as measure
				from v_date_to_date a left join bi_patient_Status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
									  left join v_Client_tmp c on b.clientid = c.clientid
				where a.date >= date_sub(date(itz(now())), interval 2 month) and a.date <= date(itz(now()))
				and c.patientname not like '%obsolete%'
				and c.doctorId is not null
				group by a.date, c.doctorId
				;

				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select a.date, c.doctorId, 'Manual Cohort' as measure_name, '# of PT' as measure_group, 
				count(distinct case when pt.cohort = 'M' and (c.labels not like '%RESEARCH%' and labels not like '%unqualified%') then pt.clientid else null end) as measure
				from v_date_to_date a left join bi_patient_Status b on a.date between b.status_start_date and b.status_end_date 
									  left join v_Client_tmp c on b.clientid = c.clientid
									  left join bi_patient_measures pt on a.date = pt.measure_event_date and b.clientid = pt.clientid 
				where a.date >= date_sub(date(itz(now())), interval 2 month) and a.date <= date(itz(now()))
				and c.patientname not like '%obsolete%'											
				and c.doctorId is not null
				group by a.date, c.doctorId
				;


				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				
				select measure_event_date, c.doctorId, 'Health' as measure_name, 'pts_with_treatDays_35' as measure_group, count(distinct case when a.treatmentdays >= 36 then a.clientid else null end) as measure 
				from bi_patient_measures a left join v_client_tmp c on a.clientid = c.clientid
				where measure_event_date >= date_sub(date(itz(now())), interval 2 month) and measure_event_date <= date(itz(now()))
				and c.patientname not like '%obsolete%'
				and c.doctorId is not null
				group by measure_event_date, c.doctorId
				;

					
					drop temporary table if exists tmp_30d_caseload;

					create temporary table tmp_30d_caseload as 
					select a.date, doctorId,
					max(case when measure_name = 'Active' and measure_group = '# of PT' then measure else null end) as max_active_cnt_7day,
					max(case when measure_name = 'Pending Active' and measure_group = '# of PT' then measure else null end) as max_PA_cnt_7day,
					max(case when measure_group = 'pts_with_treatDays_35' then measure else null end) as max_active_cnt_35above_7day,
					max(case when measure_name = 'Manual Cohort' and measure_group = '# of PT' then measure else null end) as max_Manual_cnt_7day   
					from v_date_to_date a left join tmp_bi_doctor_measure_x_scorecard_30d b on b.report_date between date_sub(a.date, interval 29 day) and a.date
					where date >= date_sub(date(itz(now())), interval 1 month) and date <= date(itz(now()))
					group by a.date, doctorId
					;

					insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
					select date, doctorId, 'Active' as measure_name, 'active_cnt_7d' as measure_group, max_active_cnt_7day
					from tmp_30d_caseload
					where date >= date_sub(date(itz(now())), interval 1 month) and date <= date(itz(now()))
					;

					insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
					select date, doctorId, 'Pending Active' as measure_name, 'pending_active_cnt_7d' as measure_group, max_PA_cnt_7day
					from tmp_30d_caseload
					where date >= date_sub(date(itz(now())), interval 1 month) and date <= date(itz(now()))
					;
					
					insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
					select date, doctorId, 'Health' as measure_name, 'max_active_cnt_35above_7day' as measure_group, max_active_cnt_35above_7day
					from tmp_30d_caseload
					where date >= date_sub(date(itz(now())), interval 1 month) and date <= date(itz(now()))
					;

					insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
					select date, doctorId, 'Manual Cohort' as measure_name, 'max_manual_cohort_7day' as measure_group, max_Manual_cnt_7day
					from tmp_30d_caseload
					where date >= date_sub(date(itz(now())), interval 1 month) and date <= date(itz(now()))
					;

					drop temporary table if exists tmp_30d_caseload;


				
				drop temporary table if exists tmp_med_recommendation_30d; 

				create temporary table tmp_med_recommendation_30d as 
				select f1.clientid, f1.doctorId, recommendationDate, status, 
				case when f1.recommendationdate <> f2.newpublishingdate then 'Repeated' 
					 when f1.recommendationdate = f2.newpublishingdate then 'New' else null end as publishStatus, actionTime
				from
				(
						select distinct a.clientid, a.doctorId, 
						date(recommendationdate) as recommendationdate,
						status, actionTime
						from
						(
							select a.clientid, a.doctorId, a.medicationTierId, eventtime as recommendationdate, status, itz(b.dateAdded) as actionedDate, timestampdiff(HOUR, a.eventtime, itz(b.dateAdded)) as actionTime
							from
							(
									select medicationRecommendationId, a.clientid, itz(eventtime) as eventtime, a.status, medicationTierId, type, b.doctorId
									from medicationrecommendations a inner join v_client_tmp b on a.clientid = b.clientid 
									where type = 'DIABETES'
									and (labels not like '%RESEARCH%' and labels not like '%unqualified%')
							) a inner join medicationhistories b on a.clientid = b.clientid and a.medicationRecommendationId = b.medicationRecommendationId
						) a
						where date(recommendationdate) >= date_sub(date(itz(now())), interval 2 month)
				) f1 left join (select clientid, min(date(itz(eventtime))) as newpublishingdate from medicationrecommendations where type = 'DIABETES' group by clientid) f2 on f1.clientid = f2.clientid  
				;

				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select date, doctorId, 'Med Adoption' as measure_name, 'meds-Automated' as measure_group, count(distinct case when status in ('AUTOMATED') then clientid else null end) as measure
				from v_date_to_date a left join tmp_med_recommendation_30d b on b.recommendationdate between date_sub(a.date, interval 29 day) and a.date
				where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
				group by date, doctorId
				;

				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select date, doctorId, 'Med Adoption' as measure_name, 'meds-totalRecommended' as measure_group, count(distinct case when status in ('AUTOMATED', 'OVERRIDDEN', 'SKIPPED_OVERRIDDEN', 'SKIPPED') then clientid else null end) as measure
				from v_date_to_date a left join tmp_med_recommendation_30d b on b.recommendationdate between date_sub(a.date, interval 29 day) and a.date
				where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
				group by date, doctorId
				;


				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select date, doctorId, 'Med Adoption' as measure_name, 'meds-automated-within-SLA' as measure_group, 
				count(distinct case when publishStatus = 'New' and status = 'AUTOMATED' and actionTime <= 4 then clientid 
									when publishStatus = 'Repeated' and status = 'AUTOMATED' and actionTime <= 24 then clientid else null end) as measure
				from v_date_to_date a left join tmp_med_recommendation_30d b on b.recommendationdate between date_sub(a.date, interval 29 day) and a.date
				where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
				group by date, doctorId
				;

				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select date, doctorId, 'Med Adoption' as measure_name, 'NEW-meds-automated-total' as measure_group, 
				count(distinct case when publishStatus = 'New' and status = 'AUTOMATED' then clientid else null end) as measure
				from v_date_to_date a left join tmp_med_recommendation_30d b on b.recommendationdate between date_sub(a.date, interval 29 day) and a.date
				where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
				group by date, doctorId
				;

				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select date, doctorId, 'Med Adoption' as measure_name, 'NEW-meds-automated-within-SLA' as measure_group, 
				count(distinct case when publishStatus = 'New' and status = 'AUTOMATED' and actionTime <= 4 then clientid else null end) as measure
				from v_date_to_date a left join tmp_med_recommendation_30d b on b.recommendationdate between date_sub(a.date, interval 29 day) and a.date
				where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
				group by date, doctorId
				;

				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select date, doctorId, 'Med Adoption' as measure_name, 'REPEAT-meds-automated-total' as measure_group, 
				count(distinct case when publishStatus = 'Repeated' and status = 'AUTOMATED' then clientid else null end) as measure
				from v_date_to_date a left join tmp_med_recommendation_30d b on b.recommendationdate between date_sub(a.date, interval 29 day) and a.date
				where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
				group by date, doctorId
				;

				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select date, doctorId, 'Med Adoption' as measure_name, 'REPEAT-meds-automated-within-SLA' as measure_group, 
				count(distinct case when publishStatus = 'Repeated' and status = 'AUTOMATED' and actionTime <= 24 then clientid else null end) as measure
				from v_date_to_date a left join tmp_med_recommendation_30d b on b.recommendationdate between date_sub(a.date, interval 29 day) and a.date
				where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
				group by date, doctorId
				;

				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select a.date, doctorId, 'Member Referral' as measure_name, '# of PT' as measure_group, count(distinct clientid ) as measure
				from v_date_to_date a left join v_client_tmp b on date(b.enrollmentdate) between date_sub(a.date, interval 29 day) and a.date
				where b.source = 'DOCTOR'
				and a.date >= date_sub(date(now()), interval 30 day) and a.date <= date(now())
				group by a.date, doctorId
				;

				
				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select date, apptDoctId, 'Doctor Appointment' as measure_name, '# of appts completed' as measure_group, count(distinct case when da_status = 'COMPLETED' then DA_scheduled_time else null end) as measure
				from v_date_to_date a left join 
				(
					select a.clientid, a.eventLocaltime as DA_scheduled_time, a.status as DA_status, d.doctorID as apptDoctId, dl.doctornameShort
					from clientappointments a inner join doctorappointments d on a.clientid = d.clientid and a.clientAppointmentId = d.clientAppointmentId
											  left join v_client_tmp c on a.clientid = c.clientid 
											  left join v_alldoctors_list dl on d.doctorid = dl.doctorid
					where date(a.eventLocaltime) >= date_sub(date(now()), interval 2 month) and date(eventLocaltime) <= date(now())
				) b on date(b.DA_scheduled_time) between date_sub(a.date, interval 29 day) and a.date
				where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
				group by date, apptDoctId
				;

				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select date, apptDoctId, 'Doctor Appointment' as measure_name, '# of appts scheduled' as measure_group, count(distinct case when da_status in ('COMPLETED', 'SCHEDULED') then DA_scheduled_time else null end) as measure 
				from v_date_to_date a left join 
				(
					select a.clientid, a.eventLocaltime as DA_scheduled_time, a.status as DA_status, d.doctorID as apptDoctId, dl.doctornameShort
					from clientappointments a inner join doctorappointments d on a.clientid = d.clientid and a.clientAppointmentId = d.clientAppointmentId
											  left join v_client_tmp c on a.clientid = c.clientid 
											  left join v_alldoctors_list dl on d.doctorid = dl.doctorid
					where date(a.eventLocaltime) >= date_sub(date(now()), interval 2 month) and date(a.eventLocaltime) <= date(now())
				) b on date(b.DA_scheduled_time) between date_sub(a.date, interval 29 day) and a.date
				where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
				group by date, apptDoctId
				;

					drop temporary table if exists tmp_IMTs_record_30d; 
					
					create temporary table tmp_IMTs_record_30d as 
					select a.incidentId, a.clientid, b.doctornameShort, b.doctorId, category, symptom_severity, a.status, subtype_L1, 
					itz(report_time) as report_time,
					date(itz(report_time)) as report_date,
					if(a.status = 'OPEN', itz(now()), itz(resolved_time)) as resolved_time, 
					itz(owner_entry_date) as owner_entry_date, 
					itz(first_response_time) as first_response_time, 
					address_time_hours, 
					close_duration_hours
					from bi_incident_mgmt a left join v_client_tmp b on a.clientid = b.clientid 
											left join incident c on a.incidentId = c.incidentId
					where category in ('symptoms_handling','patient_treatment') 
					and c.incidentRecipientTypes like '%DOCTOR%'
					and date(report_time) >= date_sub(date(itz(now())), interval 2 month)
					;


					create index idx_tmp_IMTs_record_30d on tmp_IMTs_record_30d(incidentId, clientid, report_time, resolved_time); 

				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select date, doctorId, 'IMT' as measure_name, '# of total imts' as measure_group, count(distinct incidentId) as measure
				from v_date_to_date a left join tmp_IMTs_record_30d imt on date(report_time) between date_sub(a.date, interval 29 day) and a.date
				where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
				group by date, doctorId
				;

				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select date, doctorId, 'IMT' as measure_name, '# of total imts resolved <= 1hr' as measure_group, count(distinct case when timestampdiff(hour, report_time, resolved_time) <= 1 then incidentId else null end) as measure
				from v_date_to_date a left join tmp_IMTs_record_30d imt on date(report_time) between date_sub(a.date, interval 29 day) and a.date
				where date >= date_sub(date(now()), interval 30 day) and date <= date(now())
				group by date, doctorId
				;


				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select date(itz(now())) as date, doctorId, 'IMT' as measure_name, '# of OPEN imts' as measure_group, count(distinct incidentId) as measure
				from tmp_IMTs_record_30d imt 
				where status = 'OPEN'
				group by date(itz(now())), doctorId
				;

				insert into tmp_bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure) 
				select date(itz(now())) as date, doctorId, 'IMT' as measure_name, '# of OPEN imts open >= 24hr' as measure_group, count(distinct case when timestampdiff(hour, report_time, date(itz(now()))) >= 24 then incidentId else null end) as measure
				from tmp_IMTs_record_30d imt 
				where status = 'OPEN'
				group by date(itz(now())), doctorId
				;

					drop temporary table if exists tmp_IMTs_record_30d; 
					drop temporary table if exists tmp_med_recommendation_30d; 


				insert into bi_doctor_measure_x_scorecard_30d (report_date, doctorId, measure_name, measure_group, measure)
				select report_date, doctorId, measure_name, measure_group, measure
				from tmp_bi_doctor_measure_x_scorecard_30d a
				where not exists (select 1 from bi_doctor_measure_x_scorecard_30d b where a.report_date = b.report_date and a.doctorId = b.doctorId and a.measure_name = b.measure_name and a.measure_group = b.measure_group)
				and a.report_date >= date_sub(date(now()), interval 20 day) 
				;


				update bi_doctor_measure_x_scorecard_30d a inner join tmp_bi_doctor_measure_x_scorecard_30d b on (a.report_date = b.report_date and a.doctorId = b.doctorId and a.measure_name = b.measure_name and a.measure_group = b.measure_group)
				set a.measure = b.measure
				where a.report_date >= date_sub(date(now()), interval 20 day) 
				;

				drop temporary table if exists tmp_bi_doctor_measure_x_scorecard_30d; 


update dailyprocesslog 
set updatedate = now()
where processname = 'sp_load_bi_doctor_measure_x_scorecard'
;

END$$
DELIMITER ;
