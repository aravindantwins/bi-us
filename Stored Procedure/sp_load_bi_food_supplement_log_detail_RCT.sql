CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_bi_food_supplement_log_detail_RCT`()
begin

/* 2021-10-22  Initial Version - To load the food supplement log and the total target summary on daily basis for the RCT members. 
 * To change the list of the RCT members, only need to touch the v_allclients_rct view as it controls the data to be loaded by this process.
 */	
	
update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_food_supplement_log_detail_RCT'
;

drop temporary table if exists tmp_all_RCT_patients; 
 
create temporary table tmp_all_RCT_patients as 
select clientid,labels,status,enrollmentdate from v_allclients_rct;

 
create index idx_tmp_all_RCT_patients on tmp_all_RCT_patients (clientid asc, enrollmentdate); 
 
drop temporary table if exists tmp_foodlog_section_RCT; 
create temporary table tmp_foodlog_section_RCT
(
		category varchar(20), 
		clientid int not null, 
        eventdate date, 
		eventTime datetime,
        categorytype varchar(30),
		itemId int, 
		itemName varchar(200),
		quantity decimal(10,5),
		E5grading varchar(25),
		measurementType varchar(100),
		measurementAmount decimal(10,5),
		net_carb decimal(10,5),
		calories decimal(10,5), 
		fat decimal(10,5), 
		fibre decimal(10,5),
		protein decimal(10,5),
		total_Carb decimal(10,5),
		netGICarb decimal(10,5),
        foodRating decimal(10,2),
        creator varchar(50) null,
		chosenRecommendationRating decimal(8,5) null
);

insert into tmp_foodlog_section_RCT 
select distinct 
 'foodlog' as category, 
		fl.clientid, 
        fl.mealdate as eventdate, 
		-- convert_tz(fl.mealtime, @@time_zone, ct.timezoneId)  as eventTime, -- Convert the mealtime as per patient's timezone
        concat(fl.mealdate,' ',fl1.mealDateTimeOnly) as eventTime,
		fl.mealtype as categorytype,
		if(cf.customfoodid is not null, cf.customfoodid, b.foodid) as itemId, 
		if(cf.foodName is not null, cf.foodName, b.foodlabel) as itemName,
		fl.quantity as quantity,
		CASE WHEN ISNULL(ifnull(p.neRecommendationRating, b.recommendationRating)) THEN 'Purple' -- take the recommendationRating from personalizedfoods table if the food is personalized for a patient.
		 WHEN (ifnull(p.neRecommendationRating, b.recommendationRating) <= 1) THEN 'Red'
		 WHEN ((ifnull(p.neRecommendationRating, b.recommendationRating) > 1) AND (ifnull(p.neRecommendationRating, b.recommendationRating)  <= 2)) THEN 'Orange'
		 WHEN ((ifnull(p.neRecommendationRating, b.recommendationRating) > 2) AND (ifnull(p.neRecommendationRating, b.recommendationRating)  <= 3)) THEN 'Green'
		 WHEN (ifnull(p.neRecommendationRating, b.recommendationRating) > 3) THEN 'Green*'
		END AS E5Grading,
		fl.measure as measurementType,
		fl.basequantity as measurementAmount,
		cast((b.net_carb * fl.baseQuantity) as decimal(10,5)) as net_carb,
		cast((b.calories * fl.baseQuantity) as decimal(10,5)) as calories, 
		cast((b.fat * fl.baseQuantity) as decimal(10,5)) as fat, 
		cast((b.fibre * fl.baseQuantity) as decimal(10,5)) as fibre,
		cast((b.protein * fl.baseQuantity) as decimal(10,5)) as protein,
		cast((b.total_Carb *fl.baseQuantity) as decimal(10,5)) as total_carb,
		b.netGICarb,
        foodRating as foodRating,
		concat(fl1.creatorRole,' - ', fl1.creatorType) as creator,
		ifnull(p.neRecommendationRating, b.recommendationRating) as chosenRecommendationRating
from foodlogs_view fl inner join tmp_all_RCT_patients rct on fl.clientid = rct.clientid and fl.mealdate >= rct.enrollmentdate
				 left join (
									select
									foodid, foodlabel,
                                    recommendationRating,
									CASE WHEN ISNULL(recommendationRating) THEN 'Purple'
										 WHEN (recommendationRating <= 1) THEN 'Red'
										 WHEN ((recommendationRating > 1) AND (recommendationRating <= 2)) THEN 'Orange'
										 WHEN ((recommendationRating > 2) AND (recommendationRating <= 3)) THEN 'Green'
										 WHEN (recommendationRating > 3) THEN 'Green*'
									END AS E5grading,
									measure,
									measurementType,
									measurementAmount,
									carb - fibre as net_carb -- level1
									,calories, carb as total_Carb, protein, fat, fibre, -- level 2,
                                    cast(IFNULL((((carb - fibre) * glycemicIndex) / 55),0) as decimal(10,5)) AS netGICarb
									from foods
						) b on fl.foodid = b.foodid
				left join personalizedfoods p on fl.foodid = p.foodid and fl.clientid = p.clientid          
                left join customfoods cf on fl.foodlogid = cf.foodlogid and fl.clientid = cf.clientid -- and deleted = 0
				left join foodlogs fl1 on fl.foodlogid = fl1.foodlogid
-- where fl.mealdate >= date_sub(date(now()), interval 6 month) and fl.mealdate <= date(now())
-- group by category, fl.clientid, mealdate, mealtype, fl.foodid
;


-- truncate and load is needed because RCT coaches can modify any older data. 

truncate table bi_food_supplement_log_detail_RCT
;

insert into bi_food_supplement_log_detail_RCT
(category, clientid, eventdate, eventTime, categorytype, itemId, itemName, quantity, E5grading, measurementType, measurementAmount,
net_carb, calories, fat, fibre, protein, total_Carb, netGICarb, foodRating)

select category, clientid, eventdate, eventTime, categorytype, itemId, itemName, quantity, E5grading, 
measurementType, measurementAmount,net_carb,calories, fat, fibre, protein, total_Carb, netGICarb, foodRating
from tmp_foodlog_section_RCT a 
;


drop temporary table if exists tmp_supplementlog_section_RCT;

create temporary table tmp_supplementlog_section_RCT
(
		category varchar(20), 
		clientid int not null, 
        eventdate date, 
		eventTime datetime,
        categorytype varchar(30),
		itemId int, 
		itemName varchar(200),
		quantity decimal(10,5),
		E5grading varchar(25),
		measurementType varchar(100),
		measurementAmount decimal(10,5),
		net_carb decimal(10,5),
		calories decimal(10,5), 
		fat decimal(10,5), 
		fibre decimal(10,5),
		protein decimal(10,5),
		total_Carb decimal(10,5),
		netGICarb decimal(10,5)
);
                        
insert into tmp_supplementlog_section_RCT                       
SELECT 'supplementlog' as category, 
        s.clientId AS clientId,
        s.eventDate,
        s.eventTime AS eventTime,
        timeslot AS categorytype,
        s.supplementId AS itemId,
        t.supplementName AS itemName,
        s.quantity AS quantity,
        '' as E5grading,
        t.measurementType,
        t.measurementAmount,
        (((t.carb - t.fibre)/t.measurementAmount) * s.quantity) AS netCarb,
        ((t.calories/t.measurementAmount) * s.quantity) AS calories,
        ((t.fat/t.measurementAmount) * s.quantity) AS fat,
        ((t.fibre/t.measurementAmount) * s.quantity) AS fiber,
        ((t.protein/t.measurementAmount) * s.quantity) AS protein,
        ((t.carb/t.measurementAmount) * s.quantity) AS totalCarb,
        cast(IFNULL((((t.carb - t.fibre) * t.glycemicIndex) / 55),0) as decimal(10,5)) AS netGICarb
FROM
        (
			SELECT DISTINCT 
					t.clientId AS clientId,
                    date(t.scheduledLocalTime) AS eventDate,
					max(t.scheduledLocalTime) AS eventTime, -- Convert the intaketime as per patient's timezone
					ifnull(s1.supplementId, s2.supplementId) AS supplementId,
					t.type AS supplement,
                    t.timeslot AS timeslot,
					sum(t.targetvalue) AS quantity
			FROM
					twins.clienttodoitems t
			inner join tmp_all_RCT_patients ct on t.clientid = ct.clientid and date(t.scheduledLocalTime) >= ct.enrollmentdate
            left join supplements s1 on t.supplementId = s1.supplementId
			left join (
					-- Handle the Supplements in 2 sections if the SupplementId is not captured in ClientToDoItems
					-- Because there are certain supplements have the same patientActionType but we have to choose the 1 to bring the nutrition values.
					-- 1. If there is a supplementId in the todo item, use the specific supplement
					-- 2. If there is no supplement, find all supplements mapped by the patientActionType attribute
					-- 3. If the the mapped supplements only have 1, then use the one
					-- 4. if there are multiple, use the one with “recommended” attribute set as true
					-- 5. If there are multiple with recommended set as True, use the 1st one from the results. So using min(supplementId)
								select s1.patientActionType, min(s2.supplementId) as supplementId
								from 
								(
									select patientActionType, count(distinct supplementId) as total 
									from supplements 
									where patientActionType is not null and draftstatus = 'published'
									group by patientActionType 
									having count(distinct supplementId) > 1
								) s1 inner join supplements s2 on s1.patientActionType = s2.patientActionType and s2.recommended = 1
								group by s1.patientActionType

								union all 

								select s1.patientActionType, s2.supplementId
								from 
								(
									select patientActionType, count(distinct supplementId) as total 
									from supplements 
									where patientActionType is not null and draftstatus = 'published'
									group by patientActionType 
									having count(distinct supplementId) = 1
								) s1 inner join supplements s2 on s1.patientActionType = s2.patientActionType and s2.draftStatus = 'published'
					  ) s2 on t.type = s2.patientActionType
			WHERE t.category = 'SUPPLEMENT' AND t.status = 'FINISHED'
            group by clientid, date(t.scheduledLocalTime), ifnull(s1.supplementId, s2.supplementId), t.type, timeslot
        ) s
LEFT JOIN twins.supplements t ON (s.supplementId = t.SupplementId)
;

create index tmp_idx_supplementlog_RCT on tmp_supplementlog_section_RCT (category asc, clientid asc, eventdate asc, eventtime asc, categorytype asc, itemid asc, itemname asc);

insert into bi_food_supplement_log_detail_RCT
(category, clientid, eventdate, eventTime, categorytype, itemId, itemName, quantity, E5grading, measurementType, measurementAmount,
net_carb,calories, fat, fibre, protein, total_Carb, netGICarb)

select category, clientid, eventdate, eventTime, categorytype, itemId, itemName, quantity, E5grading, 
measurementType, measurementAmount,net_carb,calories, fat, fibre, protein, total_Carb, netGICarb
from tmp_supplementlog_section_RCT a 
;

-- Cleanup
drop temporary table if exists tmp_all_RCT_patients; 
drop temporary table if exists tmp_foodlog_section_RCT; 
drop temporary table if exists tmp_supplementlog_section_RCT;
 

			drop temporary table if exists tmp_1d_foodlog_nutrition_target_RCT; 
            
            create temporary table tmp_1d_foodlog_nutrition_target_RCT
            (
				clientid	int	,
				mealdate	date,	
				category	varchar(11)	,
				net_carb	decimal(10,5)	,
				calories	decimal(10,5)	,
				netGlycemicCarb	decimal(10,5)	,
				fibre	decimal(10,5)	,
				fat	decimal(10,5)	,
				protein	decimal(10,5)	,
				Total_Carb	decimal(10,5)	,
				sodium	decimal(10,5)	,
				potassium	decimal(10,5)	,
				magnesium	decimal(10,5)	,
				calcium	decimal(10,5)	,
				chromium	decimal(10,5)	,
				omega3	decimal(10,5)	,
				omega6	decimal(10,5)	,
				alphaLipoicAcid	decimal(10,5)	,
				q10	decimal(10,5)	,
				biotin	decimal(10,5)	,
				flavonoids	decimal(10,5)	,
				improveInsulinSensitivity	decimal(10,5),	
				inhibitGluconeogenis	decimal(10,5)	,
				inhibitCarbAbsorption	decimal(10,5)	,
				improveInsulinSecretion	decimal(10,5)	,
				improveBetaCellRegeneration	decimal(10,5)	,
				inhibitHunger	decimal(10,5)	,
				inhibitGlucoseKidneyReabsorption	decimal(10,5)	,
				lactococcus	decimal(10,5)	,
				lactobacillus	decimal(10,5)	,
				leuconostoc	decimal(10,5)	,
				streptococcus	decimal(10,5)	,
				bifidobacterium	decimal(10,5)	,
				saccharomyces	decimal(10,5)	,
				bacillus	decimal(10,5)	,
				fructose	decimal(10,5)	,
				glycemicIndex	decimal(10,5)	,
				saturatedFat	decimal(10,5)	,
				monounsaturatedFat	decimal(10,5)	,
				polyunsaturatedFat	decimal(10,5)	,
				transFat	decimal(10,5)	,
				cholesterol	decimal(10,5)	,
				histidine	decimal(10,5)	,
				isolecuine	decimal(10,5)	,
				lysine	decimal(10,5)	,
				methionineAndCysteine	decimal(10,5)	,
				phenylananineAndTyrosine	decimal(10,5)	,
				tryptophan	decimal(10,5)	,
				threonine	decimal(10,5)	,
				valine	decimal(10,5)	,
				vitaminA	decimal(10,5)	,
				vitaminC	decimal(10,5)	,
				vitaminD	decimal(10,5)	,
				vitaminE	decimal(10,5)	,
				vitaminK	decimal(10,5)	,
				vitaminB1	decimal(10,5)	,
				vitaminB12	decimal(10,5)	,
				vitaminB2	decimal(10,5)	,
				vitaminB3	decimal(10,5)	,
				vitaminB5	decimal(10,5)	,
				vitaminB6	decimal(10,5)	,
				folate	decimal(10,5)	,
				copper	decimal(10,5)	,
				iron	decimal(10,5)	,
				zinc	decimal(10,5)	,
				manganese	decimal(10,5)	,
				phosphorus	decimal(10,5)	,
				selenium	decimal(10,5)	,
				omega6_3_ratio	decimal(10,5)	,
				zinc_copper_ratio	decimal(10,5)	,
				pot_sod_ratio	decimal(10,5)	,
				cal_mag_ratio	decimal(10,5)	,
				pralAlkalinity	decimal(10,5)	,
				improveBloodPressure	decimal(10,5)	,
				improveCholesterol	decimal(10,5)	,
				reduceWeight	decimal(10,5)	,
				improveRenalFunction	decimal(10,5)	,
				improveLiverFunction	decimal(10,5)	,
				improveThyroidFunction	decimal(10,5)	,
				improveArthritis	decimal(10,5)	,
				reduceUricAcid	decimal(10,5)	,
				veg	decimal(10,5)	,
				nonVeg	decimal(10,5)	,
				fruit	decimal(10,5)	,
				oil	decimal(10,5)	,
				spice	decimal(10,5)	,
				grain	decimal(10,5)	,
				legume	decimal(10,5)	,
				nuts	decimal(10,5)	,
				seeds	decimal(10,5)	,
				inflammatoryIndex	decimal(10,5)	,
				oxidativeStressIndex	decimal(10,5)	,
				gluten	decimal(10,5)	,
				allergicIndex	decimal(10,5)	,
				water	decimal(10,5)	
            );
            
            Insert into tmp_1d_foodlog_nutrition_target_RCT
            (
				clientid,
				mealdate,
				category,
				net_carb,
				calories,
				netGlycemicCarb,
				fibre,
				fat,
				protein,
				Total_Carb,
				sodium,
				potassium,
				magnesium,
				calcium,
				chromium,
				omega3,
				omega6,
				alphaLipoicAcid,
				q10,
				biotin,
				flavonoids,
				improveInsulinSensitivity,
				inhibitGluconeogenis,
				inhibitCarbAbsorption,
				improveInsulinSecretion,
				improveBetaCellRegeneration,
				inhibitHunger,
				inhibitGlucoseKidneyReabsorption,
				lactococcus,
				lactobacillus,
				leuconostoc,
				streptococcus,
				bifidobacterium,
				saccharomyces,
				bacillus,
				fructose,
				glycemicIndex,
				saturatedFat,
				monounsaturatedFat,
				polyunsaturatedFat,
				transFat,
				cholesterol,
				histidine,
				isolecuine,
				lysine,
				methionineAndCysteine,
				phenylananineAndTyrosine,
				tryptophan,
				threonine,
				valine,
				vitaminA,
				vitaminC,
				vitaminD,
				vitaminE,
				vitaminK,
				vitaminB1,
				vitaminB12,
				vitaminB2,
				vitaminB3,
				vitaminB5,
				vitaminB6,
				folate,
				copper,
				iron,
				zinc,
				manganese,
				phosphorus,
				selenium,
				omega6_3_ratio,
				zinc_copper_ratio,
				pot_sod_ratio,
				cal_mag_ratio,
				pralAlkalinity,
				improveBloodPressure,
				improveCholesterol,
				reduceWeight,
				improveRenalFunction,
				improveLiverFunction,
				improveThyroidFunction,
				improveArthritis,
				reduceUricAcid,
				veg,
				nonVeg,
				fruit,
				oil,
				spice,
				grain,
				legume,
				nuts,
				seeds,
				inflammatoryIndex,
				oxidativeStressIndex,
				gluten,
				allergicIndex,
				water
            )
    
			select clientid, mealdate, '1d-Nutrient' as category, 
			cast(sum(net_carb) as decimal(10,5))as net_carb,
			cast(sum(calories) as decimal(10,5)) as calories,
			cast(sum(netGlycemicCarb) as decimal(10,5)) as netGlycemicCarb,
			cast(sum(fibre) as decimal(10,5)) as fibre,
			cast(sum(fat) as decimal(10,5)) as fat,
			cast(sum(protein) as decimal(10,5)) as protein,
			cast(sum(Total_Carb) as decimal(10,5)) as Total_Carb,
			cast(sum(sodium) as decimal(10,5)) as sodium,
            
            cast(sum(potassium) as decimal(10,5)) as potassium,
			cast(sum(magnesium) as decimal(10,5)) as magnesium,
			cast(sum(calcium) as decimal(10,5)) as calcium,
			cast(sum(chromium) as decimal(10,5)) as chromium,
			cast(sum(omega3) as decimal(10,5)) as omega3,
			cast(sum(omega6) as decimal(10,5)) as omega6,
			cast(sum(alphaLipoicAcid) as decimal(10,5)) as alphaLipoicAcid,
			cast(sum(q10) as decimal(10,5)) as q10,
			cast(sum(biotin) as decimal(10,5)) as biotin,
			cast(sum(flavonoids) as decimal(10,5)) as flavonoids,
			cast(sum(improveInsulinSensitivity) as decimal(10,5)) as improveInsulinSensitivity,
			cast(sum(inhibitGluconeogenis) as decimal(10,5)) as inhibitGluconeogenis,
			cast(sum(inhibitCarbAbsorption) as decimal(10,5)) as inhibitCarbAbsorption,
			cast(sum(improveInsulinSecretion) as decimal(10,5))  as improveInsulinSecretion,
			cast(sum(improveBetaCellRegeneration) as decimal(10,5)) as improveBetaCellRegeneration,
			cast(sum(inhibitHunger) as decimal(10,5)) as inhibitHunger,
			cast(sum(inhibitGlucoseKidneyReabsorption) as decimal(10,5)) as inhibitGlucoseKidneyReabsorption,
			cast(sum(lactococcus) as decimal(10,5)) as lactococcus,
			cast(sum(lactobacillus) as decimal(10,5)) as lactobacillus,
			cast(sum(leuconostoc) as decimal(10,5)) as leuconostoc,
			cast(sum(streptococcus) as decimal(10,5)) as streptococcus,
			cast(sum(bifidobacterium) as decimal(10,5))  as bifidobacterium,
			cast(sum(saccharomyces) as decimal(10,5)) as saccharomyces,
			cast(sum(bacillus) as decimal(10,5)) as bacillus,
			cast(sum(fructose) as decimal(10,5)) as fructose,

			cast(sum(glycemicIndex) as decimal(10,5)) as glycemicIndex,
			cast(sum(saturatedFat) as decimal(10,5)) as saturatedFat,
			cast(sum(monounsaturatedFat) as decimal(10,5)) as monounsaturatedFat,
			cast(sum(polyunsaturatedFat) as decimal(10,5)) as polyunsaturatedFat,
			cast(sum(transFat) as decimal(10,5)) as transFat,
			cast(sum(cholesterol) as decimal(10,5)) as cholesterol,
			cast(sum(histidine) as decimal(10,5)) as histidine,
			cast(sum(isolecuine) as decimal(10,5)) as isolecuine,
			cast(sum(lysine)as decimal(10,5)) as lysine,
			cast(sum(methionineAndCysteine) as decimal(10,5)) as methionineAndCysteine,
			cast(sum(phenylananineAndTyrosine)as decimal(10,5)) as phenylananineAndTyrosine,
			cast(sum(tryptophan) as decimal(10,5)) as tryptophan,
			cast(sum(threonine) as decimal(10,5)) as threonine,
			cast(sum(valine) as decimal(10,5)) as valine,
			cast(sum(vitaminA) as decimal(10,5)) as vitaminA,
			cast(sum(vitaminC) as decimal(10,5)) as vitaminC,
			cast(sum(vitaminD) as decimal(10,5)) as vitaminD,
			cast(sum(vitaminE) as decimal(10,5)) as vitaminE,
			cast(sum(vitaminK) as decimal(10,5)) as vitaminK,
			cast(sum(vitaminB1) as decimal(10,5)) as vitaminB1,
			cast(sum(vitaminB12) as decimal(10,5)) as vitaminB12,
			cast(sum(vitaminB2) as decimal(10,5)) as vitaminB2,
			cast(sum(vitaminB3) as decimal(10,5)) as vitaminB3,
			cast(sum(vitaminB5) as decimal(10,5)) as vitaminB5,
			cast(sum(vitaminB6) as decimal(10,5)) as vitaminB6,
			cast(sum(folate) as decimal(10,5)) as folate,
			cast(sum(copper) as decimal(10,5)) as copper,
			cast(sum(iron) as decimal(10,5)) as iron,
			cast(sum(zinc) as decimal(10,5))  as zinc,
			cast(sum(manganese) as decimal(10,5)) as manganese,
			cast(sum(phosphorus) as decimal(10,5)) as phosphorus,
			cast(sum(selenium) as decimal(10,5)) as selenium,

			cast(sum(omega6)/sum(omega3) as decimal(10,5)) as omega6_3_ratio,
			cast(sum(zinc)/sum(copper) as decimal(10,5)) as zinc_copper_ratio,

			cast(sum(potassium)/sum(sodium) as decimal) as pot_sod_ratio,

			cast(sum(calcium)/sum(magnesium) as decimal(10,5)) as cal_mag_ratio,


			cast(sum((0.49 * protein) + (0.037 * phosphorus) - (0.021 * potassium) - (0.026 * magnesium) - (0.013 * calcium)) as decimal(10,5)) as pralAlkalinity,
			cast(sum(improveBloodPressure) as decimal(10,5)) as improveBloodPressure,
			cast(sum(improveCholesterol) as decimal(10,5)) as improveCholesterol,
			cast(sum(reduceWeight) as decimal(10,5)) as reduceWeight,
			cast(sum(improveRenalFunction) as decimal(10,5)) as improveRenalFunction,
			cast(sum(improveLiverFunction) as decimal(10,5)) as improveLiverFunction,
			cast(sum(improveThyroidFunction) as decimal(10,5)) as improveThyroidFunction,
			cast(sum(improveArthritis) as decimal(10,5)) as improveArthritis,
			cast(sum(reduceUricAcid) as decimal(10,5)) as reduceUricAcid,
			cast(sum(veg) as decimal(10,5)) as veg,
			cast(sum(nonVeg) as decimal(10,5))as nonVeg,
			cast(sum(fruit) as decimal(10,5))as fruit,
			cast(sum(oil)as decimal(10,5)) as oil,
			cast(sum(spice) as decimal(10,5))as spice,
			cast(sum(grain)as decimal(10,5)) as grain,
			cast(sum(legume)as decimal(10,5)) as legume,
			cast(sum(nuts) as decimal(10,5))as nuts,
			cast(sum(seeds) as decimal(10,5))as seeds,
			cast(sum(inflammatoryIndex) as decimal(10,5)) as inflammatoryIndex,
			cast(sum(oxidativeStressIndex) as decimal(10,5)) as oxidativeStressIndex,
			cast(sum(gluten)as decimal(10,5)) as gluten,

			cast(sum(allergicIndex) as decimal(10,5)) as allergicIndex,
			cast(sum(water) as decimal(10,5)) as water
			from 
			(
					select distinct a.clientid, eventdate as mealdate, 
					sum(b.net_carb  * fl.measurementAmount) as net_carb,
					sum(b.calories * fl.measurementAmount) as calories,
					sum(b.netGlycemicCarb * fl.measurementAmount) as netGlycemicCarb,
					sum(b.fibre * fl.measurementAmount) as fibre,
					sum(b.fat * fl.measurementAmount) as fat,
					sum(b.protein * fl.measurementAmount) as protein,
					sum(b.Total_Carb * fl.measurementAmount) as Total_Carb,
					sum(sodium * fl.measurementAmount) as sodium,
                    
                    sum(potassium * fl.measurementAmount) as potassium,
					sum(magnesium * fl.measurementAmount) as magnesium,
					sum(calcium * fl.measurementAmount) as calcium,
					sum(chromium * fl.measurementAmount) as chromium,
					sum(omega3 * fl.measurementAmount) as omega3,
					sum(omega6 * fl.measurementAmount) as omega6,
					sum(alphaLipoicAcid * fl.measurementAmount) as alphaLipoicAcid,
					sum(q10 * fl.measurementAmount) as q10,
					sum(biotin * fl.measurementAmount) as biotin,
					sum(flavonoids * fl.measurementAmount) as flavonoids,
					sum(improveInsulinSensitivity * fl.measurementAmount) as improveInsulinSensitivity,
					sum(inhibitGluconeogenis * fl.measurementAmount) as inhibitGluconeogenis,
					sum(inhibitCarbAbsorption * fl.measurementAmount) as inhibitCarbAbsorption,
					sum(improveInsulinSecretion * fl.measurementAmount) as improveInsulinSecretion,
					sum(improveBetaCellRegeneration * fl.measurementAmount) as improveBetaCellRegeneration,
					sum(inhibitHunger * fl.measurementAmount) as inhibitHunger,
					sum(inhibitGlucoseKidneyReabsorption * fl.measurementAmount) as inhibitGlucoseKidneyReabsorption,
					sum(lactococcus * fl.measurementAmount) as lactococcus,
					sum(lactobacillus * fl.measurementAmount) as lactobacillus,
					sum(leuconostoc * fl.measurementAmount) as leuconostoc,
					sum(streptococcus * fl.measurementAmount) as streptococcus,
					sum(bifidobacterium * fl.measurementAmount) as bifidobacterium,
					sum(saccharomyces * fl.measurementAmount) as saccharomyces,
					sum(bacillus * fl.measurementAmount) as bacillus,
					sum(fructose * fl.measurementAmount) as fructose,

					sum(glycemicIndex * fl.measurementAmount) as glycemicIndex,
					sum(saturatedFat * fl.measurementAmount) as saturatedFat,
					sum(monounsaturatedFat * fl.measurementAmount) as monounsaturatedFat,
					sum(polyunsaturatedFat * fl.measurementAmount) as polyunsaturatedFat,
					sum(transFat * fl.measurementAmount) as transFat,
					sum(cholesterol * fl.measurementAmount) as cholesterol,
					sum(histidine * fl.measurementAmount)  as histidine,
					sum(isolecuine * fl.measurementAmount)  as isolecuine,
					sum(lysine * fl.measurementAmount) as lysine,
					sum(methionineAndCysteine * fl.measurementAmount) as methionineAndCysteine,
					sum(phenylananineAndTyrosine * fl.measurementAmount) as phenylananineAndTyrosine,
					sum(tryptophan * fl.measurementAmount) as tryptophan,
					sum(threonine * fl.measurementAmount)  as threonine,
					sum(valine * fl.measurementAmount) as valine,
					sum(vitaminA * fl.measurementAmount) as vitaminA,
					sum(vitaminC * fl.measurementAmount) as vitaminC,
					sum(vitaminD * fl.measurementAmount) as vitaminD,
					sum(vitaminE * fl.measurementAmount) as vitaminE,
					sum(vitaminK * fl.measurementAmount) as vitaminK,
					sum(vitaminB1 * fl.measurementAmount) as vitaminB1,
					sum(vitaminB12 * fl.measurementAmount) as vitaminB12,
					sum(vitaminB2 * fl.measurementAmount) as vitaminB2,
					sum(vitaminB3 * fl.measurementAmount) as vitaminB3,
					sum(vitaminB5 * fl.measurementAmount) as vitaminB5,
					sum(vitaminB6 * fl.measurementAmount) as vitaminB6,
					sum(folate * fl.measurementAmount) as folate,
					sum(copper * fl.measurementAmount) as copper,
					sum(iron * fl.measurementAmount) as iron,
					sum(zinc * fl.measurementAmount) as zinc,
					sum(manganese * fl.measurementAmount) as manganese,
					sum(phosphorus * fl.measurementAmount) as phosphorus,
					sum(selenium * fl.measurementAmount) as selenium,
					sum(omega6_3_ratio * fl.measurementAmount) as omega6_3_ratio,
					sum(zinc_copper_ratio * fl.measurementAmount) as zinc_copper_ratio,
					sum(pot_sod_ratio * fl.measurementAmount) as pot_sod_ratio,
					sum(cal_mag_ratio * fl.measurementAmount) as cal_mag_ratio,
					sum(pralAlkalinity * fl.measurementAmount) as pralAlkalinity,
					sum(improveBloodPressure * fl.measurementAmount) as improveBloodPressure,
					sum(improveCholesterol * fl.measurementAmount) as improveCholesterol,
					sum(reduceWeight * fl.measurementAmount) as reduceWeight,
					sum(improveRenalFunction * fl.measurementAmount) as improveRenalFunction,
					sum(improveLiverFunction * fl.measurementAmount) as improveLiverFunction,
					sum(improveThyroidFunction * fl.measurementAmount)  as improveThyroidFunction,
					sum(improveArthritis * fl.measurementAmount) as improveArthritis,
					sum(reduceUricAcid * fl.measurementAmount)  as reduceUricAcid,
					sum(veg * fl.measurementAmount) as veg,
					sum(nonVeg * fl.measurementAmount) as nonVeg,
					sum(fruit * fl.measurementAmount) as fruit,
					sum(oil * fl.measurementAmount) as oil,
					sum(spice * fl.measurementAmount) as spice,
					sum(grain * fl.measurementAmount) as grain,
					sum(legume * fl.measurementAmount) as legume,
					sum(nuts * fl.measurementAmount) as nuts,
					sum(seeds * fl.measurementAmount) as seeds,
					sum(inflammatoryIndex * fl.measurementAmount) as inflammatoryIndex,
					sum(oxidativeStressIndex * fl.measurementAmount) as oxidativeStressIndex,
					sum(gluten * fl.measurementAmount) as gluten,

					sum(allergicIndex * fl.measurementAmount) as allergicIndex,
					sum(water * fl.measurementAmount) as water
					from bi_food_supplement_log_detail_RCT fl inner join v_allclients_rct a on (fl.clientid = a.clientid and fl.eventdate >= enrollmentdate)
									 left join (
														select
														foodid, foodlabel,
														CASE WHEN ISNULL(recommendationRating) THEN 'Purple'
															 WHEN (recommendationRating <= 1) THEN 'Red'
															 WHEN ((recommendationRating > 1) AND (recommendationRating <= 2)) THEN 'Orange'
															 WHEN ((recommendationRating > 2) AND (recommendationRating <= 3)) THEN 'Green'
															 WHEN (recommendationRating > 3) THEN 'Green*'
														END AS E5foodgrading,
														measure,
														measurementType,
														measurementAmount,
														carb - fibre as net_carb 
														,calories, cast(IFNULL((((carb - fibre) * glycemicIndex) / 55),0) as decimal(10,5)) as netGlycemicCarb, carb as Total_Carb, protein, fat, fibre 
														,sodium
                                                        
                                                        ,potassium, magnesium, calcium, chromium, omega3, omega6, alphaLipoicAcid, q10, biotin, flavonoids 
									,improveInsulinSensitivity, improveInsulinSecretion, improveBetaCellRegeneration, inhibitGluconeogenis, inhibitCarbAbsorption, inhibitHunger, inhibitGlucoseKidneyReabsorption  
									,lactococcus, lactobacillus, leuconostoc, streptococcus, bifidobacterium, saccharomyces, bacillus 
									,fructose, lactose, glycemicIndex  
									,saturatedFat, monounsaturatedFat, polyunsaturatedFat, transFat, cholesterol 
									,histidine, isolecuine, lysine, methionineAndCysteine, phenylananineAndTyrosine, tryptophan, threonine, valine 
									,vitaminA, vitaminC, vitaminD, vitaminE, vitaminK, vitaminB1, vitaminB12, vitaminB2, vitaminB3, vitaminB5, vitaminB6, folate  
									,  copper, iron, zinc, manganese, phosphorus, selenium  
									, omega6/omega3 as omega6_3_ratio, zinc/copper as zinc_copper_ratio, potassium/sodium as pot_sod_ratio, calcium/magnesium as cal_mag_ratio, pralAlkalinity  
									,improveBloodPressure, improveCholesterol, improveRenalFunction, improveLiverFunction, improveThyroidFunction, improveArthritis, reduceUricAcid, reduceWeight  , vegetarian as veg, case when vegetarian = 0 then 1 end as nonVeg,  fruit, oil, spice, grain, legume,nuts, seeds  
									, inflammatoryIndex, oxidativeStressIndex, gluten, allergicIndex  
									, water 
									from foods a
														
											) b on fl.itemid = b.foodid
					where -- eventdate between date_sub(date(itz(now())), interval 45 day) and date(itz(now())) and 
					category = 'foodlog'
					group by a.clientid, eventdate
     
					union all 

					select distinct a.clientid, eventdate as mealdate, 
					sum((b.net_carb/b.measurementAmount) * fl.quantity) as net_carb,
					sum((b.calories/b.measurementAmount) * fl.quantity) as calories,
					sum((b.netGlycemicCarb/b.measurementAmount) * fl.quantity) as netGlycemicCarb,
					sum((b.fibre/b.measurementAmount) * fl.quantity) as fibre,
					sum((b.fat/b.measurementAmount) * fl.quantity) as fat,
					sum((b.protein/b.measurementAmount) * fl.quantity) as protein,
					sum((b.Total_Carb/b.measurementAmount) * fl.quantity) as Total_Carb,
					sum((sodium/b.measurementAmount) * fl.quantity) as sodium,
                    
                    sum((potassium/b.measurementAmount) * fl.quantity) as potassium,
					sum((magnesium/b.measurementAmount) * fl.quantity) as magnesium,
					sum((calcium/b.measurementAmount) * fl.quantity) as calcium,
					sum((chromium/b.measurementAmount) * fl.quantity) as chromium,
					sum((omega3/b.measurementAmount) * fl.quantity) as omega3,
					sum((omega6/b.measurementAmount) * fl.quantity) as omega6,
					sum((alphaLipoicAcid/b.measurementAmount) * fl.quantity) as alphaLipoicAcid,
					sum((q10/b.measurementAmount) * fl.quantity) as q10,
					sum((biotin/b.measurementAmount) * fl.quantity) as biotin,
					sum((flavonoids/b.measurementAmount) * fl.quantity) as flavonoids,
					sum((improveInsulinSensitivity/b.measurementAmount) * fl.quantity) as improveInsulinSensitivity,
					sum((inhibitGluconeogenis/b.measurementAmount) * fl.quantity) as inhibitGluconeogenis,
					sum((inhibitCarbAbsorption/b.measurementAmount) * fl.quantity) as inhibitCarbAbsorption,
					sum((improveInsulinSecretion/b.measurementAmount) * fl.quantity) as improveInsulinSecretion,
					sum((improveBetaCellRegeneration/b.measurementAmount) * fl.quantity) as improveBetaCellRegeneration,
					sum((inhibitHunger/b.measurementAmount) * fl.quantity) as inhibitHunger,
					sum((inhibitGlucoseKidneyReabsorption/b.measurementAmount) * fl.quantity) as inhibitGlucoseKidneyReabsorption,
					sum((lactococcus/b.measurementAmount) * fl.quantity) as lactococcus,
					sum((lactobacillus/b.measurementAmount) * fl.quantity) as lactobacillus,
					sum((leuconostoc/b.measurementAmount) * fl.quantity) as leuconostoc,
					sum((streptococcus/b.measurementAmount) * fl.quantity) as streptococcus,
					sum((bifidobacterium/b.measurementAmount) * fl.quantity) as bifidobacterium,
					sum((saccharomyces/b.measurementAmount) * fl.quantity) as saccharomyces,
					sum((bacillus/b.measurementAmount) * fl.quantity) as bacillus,
					sum((fructose/b.measurementAmount) * fl.quantity) as fructose,

					sum((glycemicIndex/b.measurementAmount) * fl.quantity) as glycemicIndex,
					sum((saturatedFat/b.measurementAmount) * fl.quantity) as saturatedFat,
					sum((monounsaturatedFat/b.measurementAmount) * fl.quantity) as monounsaturatedFat,
					sum((polyunsaturatedFat/b.measurementAmount) * fl.quantity) as polyunsaturatedFat,
					sum((transFat/b.measurementAmount) * fl.quantity) as transFat,
					sum((cholesterol/b.measurementAmount) * fl.quantity) as cholesterol,
					sum((histidine/b.measurementAmount) * fl.quantity) as histidine,
					sum((isolecuine/b.measurementAmount) * fl.quantity) as isolecuine,
					sum((lysine/b.measurementAmount) * fl.quantity) as lysine,
					sum((methionineAndCysteine/b.measurementAmount) * fl.quantity) as methionineAndCysteine,
					sum((phenylananineAndTyrosine/b.measurementAmount) * fl.quantity) as phenylananineAndTyrosine,
					sum((tryptophan/b.measurementAmount) * fl.quantity) as tryptophan,
					sum((threonine/b.measurementAmount) * fl.quantity) as threonine,
					sum((valine/b.measurementAmount) * fl.quantity) as valine,
					sum((vitaminA/b.measurementAmount) * fl.quantity) as vitaminA,
					sum((vitaminC/b.measurementAmount) * fl.quantity) as vitaminC,
					sum((vitaminD/b.measurementAmount) * fl.quantity) as vitaminD,
					sum((vitaminE/b.measurementAmount) * fl.quantity) as vitaminE,
					sum((vitaminK/b.measurementAmount) * fl.quantity) as vitaminK,
					sum((vitaminB1/b.measurementAmount) * fl.quantity) as vitaminB1,
					sum((vitaminB12/b.measurementAmount) * fl.quantity) as vitaminB12,
					sum((vitaminB2/b.measurementAmount) * fl.quantity) as vitaminB2,
					sum((vitaminB3/b.measurementAmount) * fl.quantity) as vitaminB3,
					sum((vitaminB5/b.measurementAmount) * fl.quantity) as vitaminB5,
					sum((vitaminB6/b.measurementAmount) * fl.quantity) as vitaminB6,
					sum((folate/b.measurementAmount) * fl.quantity) as folate,
					sum((copper/b.measurementAmount) * fl.quantity) as copper,
					sum((iron/b.measurementAmount) * fl.quantity) as iron,
					sum((zinc/b.measurementAmount) * fl.quantity) as zinc,
					sum((manganese/b.measurementAmount) * fl.quantity) as manganese,
					sum((phosphorus/b.measurementAmount) * fl.quantity) as phosphorus,
					sum((selenium/b.measurementAmount) * fl.quantity) as selenium,
					sum(((omega6/b.measurementAmount) / (omega3/b.measurementAmount)) * fl.quantity) as omega6_3_ratio,
					sum(((zinc/b.measurementAmount) / (copper/b.measurementAmount)) * fl.quantity) as zinc_copper_ratio,
					sum(((potassium/b.measurementAmount) / (sodium/b.measurementAmount)) * fl.quantity) as pot_sod_ratio,
					sum(((calcium/b.measurementAmount) / (magnesium/b.measurementAmount)) * fl.quantity) as cal_mag_ratio,
					sum((pralAlkalinity/b.measurementAmount) * fl.quantity) as pralAlkalinity,
					sum((improveBloodPressure/b.measurementAmount) * fl.quantity) as improveBloodPressure,
					sum((improveCholesterol/b.measurementAmount) * fl.quantity) as improveCholesterol,
					sum((reduceWeight/b.measurementAmount) * fl.quantity) as reduceWeight,
					sum((improveRenalFunction/b.measurementAmount) * fl.quantity) as improveRenalFunction,
					sum((improveLiverFunction/b.measurementAmount) * fl.quantity) as improveLiverFunction,
					sum((improveThyroidFunction/b.measurementAmount) * fl.quantity) as improveThyroidFunction,
					sum((improveArthritis/b.measurementAmount) * fl.quantity) as improveArthritis,
					sum((reduceUricAcid/b.measurementAmount) * fl.quantity) as reduceUricAcid,
					sum((veg/b.measurementAmount) * fl.quantity) as veg,
					sum((nonVeg/b.measurementAmount) * fl.quantity) as nonVeg,
					sum((fruit/b.measurementAmount) * fl.quantity) as fruit,
					sum((oil/b.measurementAmount) * fl.quantity) as oil,
					sum((spice/b.measurementAmount) * fl.quantity) as spice,
					sum((grain/b.measurementAmount) * fl.quantity) as grain,
					sum((legume/b.measurementAmount) * fl.quantity) as legume,
					sum((nuts/b.measurementAmount) * fl.quantity) as nuts,
					sum((seeds/b.measurementAmount) * fl.quantity) as seeds,
					sum((inflammatoryIndex/b.measurementAmount) * fl.quantity) as inflammatoryIndex,
					sum((oxidativeStressIndex/b.measurementAmount) * fl.quantity) as oxidativeStressIndex,
					sum((gluten/b.measurementAmount) * fl.quantity) as gluten,

					sum((allergicIndex/b.measurementAmount) * fl.quantity) as allergicIndex,
					sum((water/b.measurementAmount) * fl.quantity) as water
					from bi_food_supplement_log_detail_RCT fl inner join v_allclients_rct a on (fl.clientid = a.clientid and fl.eventdate >= enrollmentdate )
									 left join (
														select
														supplementId, supplementName,
														'' as E5foodgrading,
														'' as measure,
														measurementType,
														measurementAmount,
														carb - fibre as net_carb 
														,calories, cast(IFNULL((((carb - fibre) * glycemicIndex) / 55),0) as decimal(10,5)) as netGlycemicCarb, carb as Total_Carb, protein, fat, fibre 
														,sodium
                                                        ,potassium, magnesium, calcium, chromium, omega3, omega6, alphaLipoicAcid, q10, biotin, flavonoids 
									,improveInsulinSensitivity, improveInsulinSecretion, improveBetaCellRegeneration, inhibitGluconeogenis, inhibitCarbAbsorption, inhibitHunger, inhibitGlucoseKidneyReabsorption  
									,lactococcus, lactobacillus, leuconostoc, streptococcus, bifidobacterium, saccharomyces, bacillus 
									,fructose, lactose, glycemicIndex  
									,saturatedFat, monounsaturatedFat, polyunsaturatedFat, transFat, cholesterol 
									,histidine, isolecuine, lysine, methionineAndCysteine, phenylananineAndTyrosine, tryptophan, threonine, valine 
									,vitaminA, vitaminC, vitaminD, vitaminE, vitaminK, vitaminB1, vitaminB12, vitaminB2, vitaminB3, vitaminB5, vitaminB6, folate  
									,  copper, iron, zinc, manganese, phosphorus, selenium  
									, omega6/omega3 as omega6_3_ratio, zinc/copper as zinc_copper_ratio, potassium/sodium as pot_sod_ratio, calcium/magnesium as cal_mag_ratio, pralAlkalinity  
									,improveBloodPressure, improveCholesterol, improveRenalFunction, improveLiverFunction, improveThyroidFunction, improveArthritis, reduceUricAcid, reduceWeight  
									, '' as veg, '' as nonVeg,  fruit, oil, spice, grain, legume,nuts, seeds  
									, inflammatoryIndex, oxidativeStressIndex, gluten, allergicIndex  
									, water 
									from supplements a
														
											) b on fl.itemid = b.supplementId
					where  -- eventdate between date_sub(date(itz(now())), interval 45 day) and date(itz(now())) 
					category = 'supplementlog'
					group by a.clientid, eventdate
                   
			) s
			group by clientid, mealdate
			;
			create index idx_tmp_1d_RCT on tmp_1d_foodlog_nutrition_target_RCT (clientid asc, mealdate asc, category);

			truncate table bi_foodlog_nutrition_target_summary_RCT; 
            
			insert into bi_foodlog_nutrition_target_summary_RCT
			(
			clientid, mealdate, category, net_carb,calories, netGlycemicCarb, fibre, fat, protein, Total_Carb, sodium,
            potassium,
			magnesium,
			calcium,
			chromium,
			omega3,
			omega6,
			alphaLipoicAcid,
			q10,
			biotin,
			flavonoids,
			improveInsulinSensitivity,
			inhibitGluconeogenis,
			inhibitCarbAbsorption,
			improveInsulinSecretion,
			improveBetaCellRegeneration,
			inhibitHunger,
			inhibitGlucoseKidneyReabsorption,
			lactococcus,
			lactobacillus,
			leuconostoc,
			streptococcus,
			bifidobacterium,
			saccharomyces,
			bacillus,
			fructose,
			glycemicIndex,
			saturatedFat,
			monounsaturatedFat,
			polyunsaturatedFat,
			transFat,
			cholesterol,
			histidine,
			isolecuine,
			lysine,
			methionineAndCysteine,
			phenylananineAndTyrosine,
			tryptophan,
			threonine,
			valine,
			vitaminA,
			vitaminC,
			vitaminD,
			vitaminE,
			vitaminK,
			vitaminB1,
			vitaminB12,
			vitaminB2,
			vitaminB3,
			vitaminB5,
			vitaminB6,
			folate,
			copper,
			iron,
			zinc,
			manganese,
			phosphorus,
			selenium,
			omega6_3_ratio,
			zinc_copper_ratio,
			pot_sod_ratio,
			cal_mag_ratio,
			pralAlkalinity,
			improveBloodPressure,
			improveCholesterol,
			reduceWeight,
			improveRenalFunction,
			improveLiverFunction,
			improveThyroidFunction,
			improveArthritis,
			reduceUricAcid,
			veg,
			nonVeg,
			fruit,
			oil,
			spice,
			grain,
			legume,
			nuts,
			seeds,
			inflammatoryIndex,
			oxidativeStressIndex,
			gluten,
			allergicIndex,
			water
			)
			select clientid, mealdate, category, net_carb, calories, netGlycemicCarb, fibre, fat, protein, Total_Carb, sodium,
            potassium,
			magnesium,
			calcium,
			chromium,
			omega3,
			omega6,
			alphaLipoicAcid,
			q10,
			biotin,
			flavonoids,
			improveInsulinSensitivity,
			inhibitGluconeogenis,
			inhibitCarbAbsorption,
			improveInsulinSecretion,
			improveBetaCellRegeneration,
			inhibitHunger,
			inhibitGlucoseKidneyReabsorption,
			lactococcus,
			lactobacillus,
			leuconostoc,
			streptococcus,
			bifidobacterium,
			saccharomyces,
			bacillus,
			fructose,
			glycemicIndex,
			saturatedFat,
			monounsaturatedFat,
			polyunsaturatedFat,
			transFat,
			cholesterol,
			histidine,
			isolecuine,
			lysine,
			methionineAndCysteine,
			phenylananineAndTyrosine,
			tryptophan,
			threonine,
			valine,
			vitaminA,
			vitaminC,
			vitaminD,
			vitaminE,
			vitaminK,
			vitaminB1,
			vitaminB12,
			vitaminB2,
			vitaminB3,
			vitaminB5,
			vitaminB6,
			folate,
			copper,
			iron,
			zinc,
			manganese,
			phosphorus,
			selenium,
			omega6_3_ratio,
			zinc_copper_ratio,
			pot_sod_ratio,
			cal_mag_ratio,
			pralAlkalinity,
			improveBloodPressure,
			improveCholesterol,
			reduceWeight,
			improveRenalFunction,
			improveLiverFunction,
			improveThyroidFunction,
			improveArthritis,
			reduceUricAcid,
			veg,
			nonVeg,
			fruit,
			oil,
			spice,
			grain,
			legume,
			nuts,
			seeds,
			inflammatoryIndex,
			oxidativeStressIndex,
			gluten,
			allergicIndex,
			water
			from tmp_1d_foodlog_nutrition_target_RCT a 
			;
            
		
            drop temporary table if exists tmp_7d_foodlog_nutrition_target_RCT;

			create temporary table tmp_7d_foodlog_nutrition_target_RCT
			as 
			select a.clientid, a.mealdate, '7d-Nutrient' as category, 
			avg(cast(b.net_carb as decimal(10,5))) as net_carb,
			avg(cast(b.calories as decimal(10,5))) as calories,
			avg(cast(b.netGlycemicCarb as decimal(10,5))) as netGlycemicCarb,
			avg(cast(b.fibre as decimal(10,5))) as fibre,
			avg(cast(b.fat as decimal(10,5))) as fat,
			avg(cast(b.protein as decimal(10,5))) as protein,
			avg(cast(b.Total_Carb as decimal(10,5))) as Total_Carb,
			avg(cast(b.sodium as decimal(10,5))) as sodium,
            
            avg(cast(b.potassium as decimal(10,5))) as potassium,
			avg(cast(b.magnesium as decimal(10,5))) as magnesium,
			avg(cast(b.calcium as decimal(10,5))) as calcium,
			avg(cast(b.chromium as decimal(10,5))) as chromium,
			avg(cast(b.omega3 as decimal(10,5))) as omega3,
			avg(cast(b.omega6 as decimal(10,5))) as omega6,
			avg(cast(b.alphaLipoicAcid as decimal(10,5))) as alphaLipoicAcid,
			avg(cast(b.q10 as decimal(10,5))) as q10,
			avg(cast(b.biotin as decimal(10,5))) as biotin,
			avg(cast(b.flavonoids as decimal(10,5))) as flavonoids,
			avg(cast(b.improveInsulinSensitivity as decimal(10,5))) as improveInsulinSensitivity,
			avg(cast(b.inhibitGluconeogenis as decimal(10,5))) as inhibitGluconeogenis,
			avg(cast(b.inhibitCarbAbsorption as decimal(10,5))) as inhibitCarbAbsorption,
			avg(cast(b.improveInsulinSecretion as decimal(10,5))) as improveInsulinSecretion,
			avg(cast(b.improveBetaCellRegeneration as decimal(10,5))) as improveBetaCellRegeneration,
			avg(cast(b.inhibitHunger as decimal(10,5))) as inhibitHunger,
			avg(cast(b.inhibitGlucoseKidneyReabsorption as decimal(10,5))) as inhibitGlucoseKidneyReabsorption,
			avg(cast(b.lactococcus as decimal(10,5))) as lactococcus,
			avg(cast(b.lactobacillus as decimal(10,5))) as lactobacillus,
			avg(cast(b.leuconostoc as decimal(10,5))) as leuconostoc,
			avg(cast(b.streptococcus as decimal(10,5))) as streptococcus,
			avg(cast(b.bifidobacterium as decimal(10,5))) as bifidobacterium,
			avg(cast(b.saccharomyces as decimal(10,5))) as saccharomyces,
			avg(cast(b.bacillus as decimal(10,5))) as bacillus,
			avg(cast(b.fructose as decimal(10,5))) as fructose,

			avg(cast(b.glycemicIndex as decimal(10,5))) as glycemicIndex,
			avg(cast(b.saturatedFat as decimal(10,5))) as saturatedFat,
			avg(cast(b.monounsaturatedFat as decimal(10,5))) as monounsaturatedFat,
			avg(cast(b.polyunsaturatedFat as decimal(10,5))) as polyunsaturatedFat,
			avg(cast(b.transFat as decimal(10,5))) as transFat,
			avg(cast(b.cholesterol as decimal(10,5))) as cholesterol,
			avg(cast(b.histidine as decimal(10,5))) as histidine,
			avg(cast(b.isolecuine as decimal(10,5))) as isolecuine,
			avg(cast(b.lysine as decimal(10,5))) as lysine,
			avg(cast(b.methionineAndCysteine as decimal(10,5))) as methionineAndCysteine,
			avg(cast(b.phenylananineAndTyrosine as decimal(10,5))) as phenylananineAndTyrosine,
			avg(cast(b.tryptophan as decimal(10,5))) as tryptophan,
			avg(cast(b.threonine as decimal(10,5))) as threonine,
			avg(cast(b.valine as decimal(10,5))) as valine,
			avg(cast(b.vitaminA as decimal(10,5))) as vitaminA,
			avg(cast(b.vitaminC as decimal(10,5))) as vitaminC,
			avg(cast(b.vitaminD as decimal(10,5))) as vitaminD,
			avg(cast(b.vitaminE as decimal(10,5))) as vitaminE,
			avg(cast(b.vitaminK as decimal(10,5))) as vitaminK,
			avg(cast(b.vitaminB1 as decimal(10,5))) as vitaminB1,
			avg(cast(b.vitaminB12 as decimal(10,5))) as vitaminB12,
			avg(cast(b.vitaminB2 as decimal(10,5))) as vitaminB2,
			avg(cast(b.vitaminB3 as decimal(10,5))) as vitaminB3,
			avg(cast(b.vitaminB5 as decimal(10,5))) as vitaminB5,
			avg(cast(b.vitaminB6 as decimal(10,5))) as vitaminB6,
			avg(cast(b.folate as decimal(10,5))) as folate,
			avg(cast(b.copper as decimal(10,5))) as copper,
			avg(cast(b.iron as decimal(10,5))) as iron,
			avg(cast(b.zinc as decimal(10,5))) as zinc,
			avg(cast(b.manganese as decimal(10,5))) as manganese,
			avg(cast(b.phosphorus as decimal(10,5))) as phosphorus,
			avg(cast(b.selenium as decimal(10,5))) as selenium,
			avg(cast(b.omega6_3_ratio as decimal(10,5))) as omega6_3_ratio,
			avg(cast(b.zinc_copper_ratio as decimal(10,5))) as zinc_copper_ratio,
			avg(cast(b.pot_sod_ratio as decimal(10,5))) as pot_sod_ratio,
			avg(cast(b.cal_mag_ratio as decimal(10,5))) as cal_mag_ratio,
			avg(cast(b.pralAlkalinity as decimal(10,5))) as pralAlkalinity,
			avg(cast(b.improveBloodPressure as decimal(10,5))) as improveBloodPressure,
			avg(cast(b.improveCholesterol as decimal(10,5))) as improveCholesterol,
			avg(cast(b.reduceWeight as decimal(10,5))) as reduceWeight,
			avg(cast(b.improveRenalFunction as decimal(10,5))) as improveRenalFunction,
			avg(cast(b.improveLiverFunction as decimal(10,5))) as improveLiverFunction,
			avg(cast(b.improveThyroidFunction as decimal(10,5))) as improveThyroidFunction,
			avg(cast(b.improveArthritis as decimal(10,5))) as improveArthritis,
			avg(cast(b.reduceUricAcid as decimal(10,5))) as reduceUricAcid,
			avg(cast(b.veg as decimal(10,5))) as veg,
			avg(cast(b.nonVeg as decimal(10,5))) as nonVeg,
			avg(cast(b.fruit as decimal(10,5))) as fruit,
			avg(cast(b.oil as decimal(10,5))) as oil,
			avg(cast(b.spice as decimal(10,5))) as spice,
			avg(cast(b.grain as decimal(10,5))) as grain,
			avg(cast(b.legume as decimal(10,5))) as legume,
			avg(cast(b.nuts as decimal(10,5))) as nuts,
			avg(cast(b.seeds as decimal(10,5))) as seeds,
			avg(cast(b.inflammatoryIndex as decimal(10,5))) as inflammatoryIndex,
			avg(cast(b.oxidativeStressIndex as decimal(10,5))) as oxidativeStressIndex,
			avg(cast(b.gluten as decimal(10,5))) as gluten,

			avg(cast(b.allergicIndex as decimal(10,5))) as allergicIndex,
			avg(cast(b.water as decimal(10,5))) as water
						
			from bi_foodlog_nutrition_target_summary_RCT a inner join tmp_1d_foodlog_nutrition_target_RCT b 
			on (a.clientid = b.clientid and a.category = b.category and a.category = '1d-nutrient' and b.mealdate between date_sub(a.mealdate, interval 6 day) and a.mealdate)
			-- where a.mealdate between date_sub(date(itz(now())), interval 39 day) and date(itz(now()))
			group by a.clientid, a.mealdate
			;

			insert into bi_foodlog_nutrition_target_summary_RCT
			select * from tmp_7d_foodlog_nutrition_target_RCT
			;

			drop temporary table tmp_1d_foodlog_nutrition_target_RCT
			;

			drop temporary table tmp_7d_foodlog_nutrition_target_RCT
			;
		

update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_bi_food_supplement_log_detail_RCT'
;

	
END