DELIMITER $$
CREATE DEFINER=`aravindan`@`%` PROCEDURE `sp_load_bi_incident_mgmt`()
BEGIN
/* 
2019-11-14  Aravindan -- Initial version
2020-02-09  Aravindan - Have taken out the logic of insert/update. It will be a truncate and load table. Because
						the status of the incident will be changing gradually and we dont need to track that change. 
2020-04-20  Aravindan - Modified to consider the resolved time using the incident comment entered with incidentStatus = 'RESOLVED'
2020-06-22  Aravindan - Included new columns related incidents (like incidentclassification, subtype L1, L2, custom_subtype
*/

-- log the process start time
update dailyprocesslog 
set startdate = now()
where processname = 'sp_load_bi_incident_mgmt'
;

drop temporary table if exists tmp_bi_incident_mgmt;

create temporary table tmp_bi_incident_mgmt
(
incidentid int not null,
clientid int not null,
status varchar(50) null,
category varchar(50) null,
symptom varchar(50) null,
symptom_severity varchar(50) null,
symptom_classification varchar(100) null,
incident_classification varchar(100) null, 
subtype_L1 varchar(50) null,
subtype_L2 varchar(50) null,
custom_subtype varchar(50) null,
owner varchar(50) null,
responder varchar(50) not null,
owner_description varchar(5000) null,
responder_notes varchar(10000) null,
report_time datetime not null,
datemodified datetime null,
owner_entry_date datetime null,
first_response_time datetime null,
address_time_hours int null,
close_duration_hours int null,
resolved_time datetime null
)
;

insert into tmp_bi_incident_mgmt (incidentid, clientid, status, category, symptom, symptom_severity, symptom_classification, incident_classification, 
subtype_L1, subtype_L2, custom_subtype, owner, responder, owner_description, responder_notes, report_time, dateModified, owner_entry_date, 
first_response_time, address_time_hours, close_duration_hours, resolved_time)
select 	a.incidentId as incidentId,
		a.clientid,
		a.incidentstatus as status, 
		a.incidentType as category, 
        b.symptomType as title,
        case when b1.value = 3 then 'HIGH' 
			 when b1.value = 2 then 'MEDIUM'
             when b1.value = 1 then 'LOW' end as severity,
		b.symptomClassification, 
		a.incidentClassification as incident_classification,
        a.subTypeL1 as subtype_L1,
        a.subTypeL2 as subtype_L2,
        a.customSubType as custom_subtype,
		concat(a.userfirstName,' ',a.userlastName) as owner,
        concat(e.firstName,' ',e.lastName) as responder,
        h.incidentcomment as incident_owner_description,
        group_concat(c.comment) as all_comments_by_responder,
        a.eventtime as report_time,        
        a.dateModified,
		f.owner_entry_date,
        f.first_response_time,
        timestampdiff(hour, f.owner_entry_date, f.first_response_time) as addres_time_HOURS, 
        if(a.incidentstatus = 'Resolved', timestampdiff(hour, a.dateadded, ic.resolved_time), null) as close_duration_HOURS,
        ic.resolved_time
from incident a left join incidentsymptom b on a.incidentId = b.incidentId
				left join selfreportanswers b1 on b.selfReportAnswerId = b1.ID
			    left join (select distinct incidentid, userId from incidentrecipient) d on a.incidentId = d.incidentID 
                left join twinspii.userspii e on d.userId = e.Id                
               	left join 
				(	
					select incidentid, userId, concat(firstName,' ',lastname) as userName, 
					group_concat(incidentStatus,'-',incidentComment order by dateadded separator '-**-') as comment 
					from incidentcomment group by incidentId, userID -- order by dateadded
                ) c on a.incidentId = c.incidentID and d.userId = c.userId
                left join (
								select incidentID, min(dateadded) as first_response_time, owner_entry_date
								from 
								(
									select a.incidentId, b.owner_entry_date, a.dateadded from incidentcomment a, 
									(select incidentId, min(dateadded) as owner_entry_date from incidentcomment group by incidentId) b 
									where a.incidentId = b.incidentId and a.dateadded > b.owner_entry_date
								) s group by incidentid
						  ) f on a.incidentid = f.incidentid
				left join incidentcomment h on f.incidentId = h.incidentID and h.dateadded = f.owner_entry_date
				left join (
								select incidentId, min(date(datemodified)) as resolved_date, min(datemodified) as resolved_time
								from incidentcomment 
								where incidentStatus = 'RESOLVED' 
								group by incidentid
                          ) ic on a.incidentid = ic.incidentid
				inner join v_client_tmp g on a.clientid = g.clientid
group by incidentid, clientid, a.incidentstatus, category, responder
;


truncate table bi_incident_mgmt;

insert into bi_incident_mgmt (incidentid, clientid, status, category, symptom, symptom_severity, symptom_classification, incident_classification, subtype_L1, subtype_L2, custom_subtype, 
owner, responder, owner_description, responder_notes, report_time, dateModified, owner_entry_date, first_response_time, address_time_hours, close_duration_hours, resolved_time)
select incidentid, clientid, status, category, symptom, symptom_severity, symptom_classification, incident_classification, subtype_L1, subtype_L2, custom_subtype, owner, responder, owner_description, responder_notes, 
report_time, dateModified, owner_entry_date, first_response_time, address_time_hours, close_duration_hours, resolved_time
from tmp_bi_incident_mgmt a
;

drop temporary table tmp_bi_incident_mgmt
;

-- update end time
update dailyprocesslog 
set updatedate = now()
where processname = 'sp_load_bi_incident_mgmt'
;

END$$
DELIMITER ;
