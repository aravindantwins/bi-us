CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_bi_member_milestones`()
begin
	
	
/*
 * This procedure maintains the columns needed for Milestones related billing using the below conditions. Since we need to count the days continously for all the members, this process needs all rows in the bi_patient_measures table of all members.
 * M1a - startLaba1c - current eA1c60d > 0.4
 * M1b - current eA1c60d < 6.5
 * M3 - current eA1c60d < 6.5 and on no medicine or only metformin
 */	


  DECLARE done INT DEFAULT FALSE;
  DECLARE cId, prevcId, m1b_ind, m1a_ind, m3_ind int; 
  declare count_eA1C_noMed, count_baselineA1c, count_nominaleA1c int default 0; 
  declare eventdate date;
  declare eA1c60d, eA1cdiff double precision;  
  declare medDrugs varchar(5000);
 
  DECLARE cur1 CURSOR FOR select s.clientid, lag(clientid) over memRecords as prevClientID, s.measure_event_date, eA1c, eA1cDifference, medicine_drugs, M1b_indicator, M1a_indicator, M3_indicator from 
(
	select clientid, start_laba1c, measure_event_date, eA1c, eA1cDifference, medicine_drugs, 
	if(eA1c < 6.5, 1, null) as M1b_indicator, 
	if(eA1cDifference > 0.4, 1, null) as M1a_indicator, 
	if((eA1c < 6.5 and (medicine_drugs is null or medicine_drugs in ('Biguanide (DIABETES)', 'METFORMIN', 'METFORMIN (DIAB)'))), 1, null) as M3_indicator
	from (
					select s1.clientid, s1.start_labA1c, s2.status_start_date, s2.status_end_date, s3.measure_event_date, s3.eA1C_60d as eA1c, s1.start_labA1c - s3.eA1C_60d as eA1cDifference, s3.medicine_drugs
					from 
					(
						select clientid, start_labA1c from v_client_tmp where status in ('active', 'discharged', 'inactive')
					) s1 inner join  
					(
						select clientid, max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
						from bi_patient_status 
						where status = 'Active'
						group by clientid 
					) s2 on s1.clientid = s2.clientid 
					left join bi_patient_measures s3 on s2.clientid = s3.clientid and s3.measure_event_date between s2.status_start_date and s2.status_end_date
		  ) a 
	 where a.measure_event_date is not null
) s
window memRecords as (partition by s.clientid order by s.clientid, s.measure_event_date)
;


DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;


/*log the starttime*/
update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_member_milestones'
;

drop temporary table if exists tmp_milestone_targets; 

create temporary table tmp_milestone_targets 
(
 clientid int not null,
 eventdate date not null, 
 eA1c60d double precision,
 eA1cdiff double precision,
 medicine_drugs varchar(5000),
 m1a_ind int null,
 m1a_counter int null,
 m1b_ind int null,
 m1b_counter int null,
 m3_ind int null,
 m3_counter int null
); 
	
 
  OPEN cur1;

  read_loop: LOOP
    FETCH cur1 INTO cId, prevcId, eventdate, eA1c60d, eA1cdiff, medDrugs, m1b_ind, m1a_ind, m3_ind;
    IF done THEN
      LEAVE read_loop;
    END IF;
   
   -- insert into tmp_milestone_targets (clientid, eventdate, eA1c60d, eA1cdiff, medicine_drugs, m1b_ind, m1a_ind, m3_ind) values (cId, eventdate, eA1c60d, eA1cdiff, medDrugs, m1b_ind, m1a_ind, m3_ind);
   
   
    IF cId = prevcId and m1a_ind = 1 THEN
      set count_baselineA1c = count_baselineA1c + 1;
    else 
   	  set count_baselineA1c = 0;
    END IF;
 
    IF cId = prevcId and m1b_ind = 1 THEN
      set count_nominaleA1c = count_nominaleA1c + 1;
    else 
   	  set count_nominaleA1c = 0;
    END IF;
 
    IF cId = prevcId and m3_ind = 1 THEN
      set count_eA1C_noMed = count_eA1C_noMed + 1;
    else 
   	  set count_eA1C_noMed = 0;
    END IF;
   
   insert into tmp_milestone_targets values (cid, eventdate, eA1c60d, eA1cdiff, medicine_drugs, m1a_ind, count_baselineA1c, m1b_ind, count_nominaleA1c, m3_ind, count_eA1C_noMed);
  
 	END LOOP;

  CLOSE cur1;
 

create index idx_tmp_milestone_targets on tmp_milestone_targets(clientid asc, eventdate asc, ea1c60d);

-- select * from tmp_milestone_targets; 

update bi_patient_measures a inner join tmp_milestone_targets b on (a.clientid = b.clientid and a.measure_event_date = b.eventdate)
set a.eA1c60d_difference = b.eA1cdiff,
	a.m1a_counter = b.m1a_counter,
	a.m1b_counter = b.m1b_counter,
	a.m3_counter = b.m3_counter
;

drop temporary table if exists tmp_milestone_targets
;

/*log the starttime*/
update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_bi_member_milestones'
;

END