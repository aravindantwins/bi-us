DELIMITER $$
CREATE DEFINER=`aravindan`@`%` PROCEDURE `sp_load_bi_nutrition_happiness`()
BEGIN

/*log the starttime*/
update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_nutrition_happiness'
;

set @mdate = date(date_sub(now(), interval 15 day));

drop temporary table if exists tmp_food_rating;

create temporary table tmp_food_rating as 
select a.clientid, a.mealdate, a.foodid, a.mealtype, b.foodrating 
from foodlogs a
 join personalizedfoods b
on a. clientid=b.clientid and a.foodid=b.foodid 
where a.mealdate >=@mdate
;

create index idx_tmp_food_rating on tmp_food_rating (clientid asc, mealdate asc, foodId, foodRating); 

drop temporary table if exists tmp_meal_rating;

create temporary table tmp_meal_rating as
select distinct a.clientid, a.mealdate, b.mealid, a.mealtype,  b.rating as mealRating
from foodlogs a
 join mealratings b
on a. clientid=b.clientid and a.mealid=b.mealid 
where a.mealdate >= @mdate
;

create index idx_tmp_meal_rating on tmp_meal_rating (clientid asc, mealdate asc, mealId, mealRating); 

drop temporary table if exists tmp_nutrition_friction;
create temporary table tmp_nutrition_friction as
select distinct clientid, incidentid,category,report_time, resolved_time from   bi_incident_mgmt where  category = 'Nutrition' 
;


create index idx_tmp_nutrition_friction on tmp_nutrition_friction(clientid asc, incidentId, report_time, resolved_time);

drop temporary table if exists tmp_bi_nutrition_happiness; 

create temporary table tmp_bi_nutrition_happiness as 
Select clientid, date, food_count, foodrating, foodRating_flag,
		meal_count, mealrating, mealRating_goodOkay_percentage, mealRating_flag, Nutrition_Friction_Count, nutritionFriction_flag, 
        Case when foodRating_flag='Yes' and mealRating_goodOkay_percentage >= 0.75 and nutritionFriction_flag='No' Then 'Yes' Else 'No' End as nut_happy
FROM 
(
	select a.clientid,a.date, count(distinct foodid) as Food_count,avg(foodrating) as foodrating,Case when avg(foodrating)>=4 or avg(foodrating) is null then 'Yes' else 'No' End  as foodRating_flag,
	count(distinct mealid) as Meal_count ,avg(Mealrating) as Mealrating, 
    Case when avg(Mealrating)>=4 or avg(Mealrating) is null then 'Yes' else 'No' End  as mealRating_flag,
	count(case when mealRating >= 2 then mealrating else null end)/count(mealRating) as mealRating_goodOkay_percentage,
    count(d.incidentid) as Nutrition_Friction_Count, if(count(d.incidentid)>=1, 'Yes','No') as nutritionFriction_flag
	from 
	(
		select a.date, b.clientid
		FROM v_date_to_date a
		inner join bi_patient_status b
		on a.date between b.status_start_date and b.status_end_date and status='Active' 
        where a.date >=@mdate
	) a
	left join tmp_food_rating b on a.clientid=b.clientid and a.date=b.mealdate
	left join tmp_meal_rating c on a.clientid=c.clientid and a.date=c.mealdate
	left join tmp_Nutrition_Friction d on a.clientid=d.clientid and a.date between date(d.report_time) and ifnull(date(d.resolved_time), date(now()))
	group by a.clientid, a.date
) a
;


insert into bi_nutrition_happiness (clientid, date, foodRating_avg, foodRating_flag, mealRating_avg, mealRating_goodOkay_percentage, mealRating_flag, nutritionFriction_flag, nut_happy)
select clientid, date, foodRating, foodRating_flag, mealRating, mealRating_goodOkay_percentage, mealRating_flag, nutritionFriction_flag, nut_happy
from tmp_bi_nutrition_happiness a
where not exists (select 1 from bi_nutrition_happiness b where a.clientid = b.clientid and a.date = b.date)
;

update bi_nutrition_happiness a inner join tmp_bi_nutrition_happiness b on a.clientid = b.clientid and a.date = b.date
set a.foodRating_avg = b.foodRating, 
	a.foodRating_flag = b.foodRating_flag, 
    a.mealRating_avg = b.mealRating, 
    a.mealRating_goodOkay_percentage = b.mealRating_goodOkay_percentage,
    a.mealRating_flag = b.mealRating_flag, 
    a.nutritionFriction_flag = b.nutritionFriction_flag, 
    a.nut_happy = b.nut_happy
;


drop temporary table if exists tmp_bi_nutrition_happiness; 
drop temporary table if exists tmp_nutrition_friction;
drop temporary table if exists tmp_food_rating;
drop temporary table if exists tmp_meal_rating;

		-- To calculate mealRating_avg_5d and nut_happy_5d %
		drop temporary table if exists tmp_nutrition_1d_1;
		create temporary table tmp_nutrition_1d_1 as 
				select clientid, date, mealRating_avg, nut_happy from bi_nutrition_happiness
				where date between date_sub(date(now()) , interval 16 day) and date(now()) 
				;

		create index idx_tmp_nutrition_1d_1 on tmp_nutrition_1d_1(clientid asc, date asc, mealRating_avg, nut_happy); 
			   
		drop temporary table if exists tmp_nutrition_1d_2;
		create temporary table tmp_nutrition_1d_2 as select * from tmp_nutrition_1d_1     
		;

		create index idx_tmp_nutrition_1d_2 on tmp_nutrition_1d_2(clientid asc, date asc, mealRating_avg, nut_happy); 

		drop temporary table if exists tmp_nut_happiness_5d; 

		create temporary table tmp_nut_happiness_5d as     
		select a.clientid,a.date, 
		cast(avg(b.mealRating_avg) as decimal(10,2)) as mealRating_avg_5d,
		cast((count(case when b.nut_happy='yes' then 1 end)/5) as decimal(6,2)) as nut_happy_5d
		FROM tmp_nutrition_1d_1 a
		INNER JOIN tmp_nutrition_1d_2 b 
			ON (b.date between date_sub(a.date, INTERVAL 4 DAY) and a.date)
		AND a.clientId = b.clientId 
		GROUP BY a.clientId, a.date
		;

		create index idx_tmp_nut_happiness_5d on tmp_nut_happiness_5d(clientid asc, date asc); 

		update bi_nutrition_happiness a inner join tmp_nut_happiness_5d b on (a.clientid = b.clientid and a.date = b.date)
		set a.mealRating_avg_5d = b.mealRating_avg_5d,
			a.nut_happy_5d = b.nut_happy_5d
		where a.date >= date_sub(date(now()) , interval 10 day)
		;

		drop temporary table if exists tmp_nutrition_1d_1;
		drop temporary table if exists tmp_nutrition_1d_2;

		drop temporary table if exists tmp_nut_happiness_5d; 

/*log the endtime*/
update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_bi_nutrition_happiness'
;

END$$
DELIMITER ;
