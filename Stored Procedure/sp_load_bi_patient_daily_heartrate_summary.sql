CREATE DEFINER=`aravindan`@`%` PROCEDURE `twins`.`sp_load_bi_patient_daily_heartrate_summary`()
BEGIN
/* 
2020-11-14  Aravindan -- Initial version
2021-05-29  Aravindan - included 5d avg_rest_heartrate maintenance
2021-06-09  Aravindan - Updating Resting HR using the heartRateSummaries table directly from product
*/

-- Step 0 : Capture the process start time in the dailyprocesslog. 
update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_patient_daily_heartrate_summary'
;

set @start_date = concat(date(date_sub(now(), interval 10 day)),' 00:00:00');

drop temporary table if exists tmp_HR_last_10_days; 

create temporary table tmp_HR_last_10_days as 
select id, a.clientid, date(convert_tz(eventtime, @@time_zone, timezoneId)) as eventdate, convert_tz(eventtime, @@time_zone, timezoneId) as eventtime, rate 
from heartrates a inner join v_client_tmp b on a.clientid = b.clientid
where eventtime >= @start_date
;

create index idx_tmp_HR_last_10_days on tmp_HR_last_10_days (clientid asc, eventdate asc, eventtime, rate); 

-- HeartRate
drop temporary table if exists tmp_hr_summary; 

create temporary table tmp_hr_summary as 
select b.clientid, b.date as eventdate, min(eventtime) as first_sync_time, max(eventtime) as last_sync_time, 
floor(avg(rate)) as avg_hr, 
-- floor(avg(case when z.exerciseZone = 'Rest' then rate else null end)) as avg_rest_hr,
floor(avg(case when z.exerciseZone = 'Cardio' then rate else null end)) as avg_cardio_hr,
floor(avg(case when z.exerciseZone = 'Fitness' then rate else null end)) as avg_fitness_hr,
count(distinct id) as total_syncs,
cast((count(distinct case when z.exerciseZone = 'Rest' then id else null end)/count(distinct id)) * 100 as decimal(5,2)) as rest_hr_percentage, 
cast((count(distinct case when z.exerciseZone = 'Cardio' then id else null end)/count(distinct id)) * 100 as decimal(5,2)) as cardio_hr_percentage, 
cast((count(distinct case when z.exerciseZone = 'Fitness' then id else null end)/count(distinct id)) * 100 as decimal(5,2)) as fitness_hr_percentage
from 
(
	select b.clientid, a.date, age
    from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
						  left join v_client_tmp c on b.clientid = c.clientid
	where a.date >= date(date_sub(now(), interval 7 day))
) b left join tmp_HR_last_10_days a on a.clientid = b.clientid and b.date = a.eventdate
	inner join lookupAgeGroup la on b.age >= la.ageGroupStart and b.age <= la.ageGroupEnd 
	inner join lookupHBEZ z on la.ageGroupKey = z.ageGroupKey and a.rate between HBStart and HBEnd 
group by b.clientid, b.date
;

create index idx_tmp_hr_summary on tmp_hr_summary(clientid asc, eventdate asc); 

insert into bi_patient_daily_heartrate_summary (clientid, eventdate, first_sync_time, last_sync_time, avg_hr, avg_cardio_hr, avg_fitness_hr, total_syncs, rest_hr_percentage, cardio_hr_percentage, fitness_hr_percentage)
select clientid, eventdate, first_sync_time, last_sync_time, avg_hr, avg_cardio_hr, avg_fitness_hr, total_syncs, rest_hr_percentage, cardio_hr_percentage, fitness_hr_percentage
from tmp_hr_summary a 
where not exists (select 1 from bi_patient_daily_heartrate_summary b where a.clientid = b.clientid and a.eventdate = b.eventdate)
;

update bi_patient_daily_heartrate_summary a 
inner join tmp_hr_summary b on (a.clientid = b.clientid and a.eventdate = b.eventdate)
set a.first_sync_time = b.first_sync_time,
	a.last_sync_time = b.last_sync_time,
    a.avg_hr = b.avg_hr,
    -- a.avg_rest_hr = b.avg_rest_hr,
    a.avg_cardio_hr = b.avg_cardio_hr,
    a.avg_fitness_hr = b.avg_fitness_hr,
    a.total_syncs = b.total_syncs,
    a.rest_hr_percentage = b.rest_hr_percentage,
    a.cardio_hr_percentage = b.cardio_hr_percentage,
    a.fitness_hr_percentage = b.fitness_hr_percentage
;

-- updating Resting HR directly from heartRateSummaries table from product itself instead of calculating it using the exerciseZones. 
update bi_patient_daily_heartrate_summary a inner join 
(
	select a.clientid, date(convert_tz(eventtime, @@time_zone, timezoneId)) as eventDate, avg(restingHeartRate) as restingHeartRate
	from heartratesummaries a inner join v_client_tmp b on a.clientid = b.clientid 
    where date(convert_tz(eventtime, @@time_zone, timezoneId)) >= date_sub(date(now()), interval 7 day)
	group by a.clientid, date(convert_tz(eventtime, @@time_zone, timezoneId))
) b on a.clientid = b.clientid and a.eventdate = b.eventdate
set a.avg_rest_hr = b.restingHeartRate
;

drop temporary table if exists tmp_hr_summary;
drop temporary table if exists tmp_HR_last_10_days; 

-- 5d Values calulation
	drop temporary table if exists tmp_5d_measures_hr; 

	create temporary table tmp_5d_measures_hr as 
	SELECT 
		c.clientId
		,c.eventdate
        ,cast(avg(p.avg_rest_hr) as decimal(6,2)) as avg_rest_hr_5d
	FROM bi_patient_daily_heartrate_summary c 
	INNER JOIN bi_patient_daily_heartrate_summary p 
		ON (p.eventdate between date_sub(c.eventdate, INTERVAL 4 DAY) and c.eventdate)
		AND c.clientId = p.clientId 
	GROUP BY c.clientId, c.eventdate
	;

	update bi_patient_daily_heartrate_summary a inner join tmp_5d_measures_hr b on a.clientid = b.clientid and a.eventdate = b.eventdate 
	set a.avg_rest_hr_5d = b.avg_rest_hr_5d
	; 

	drop temporary table if exists tmp_5d_measures_hr
	;


-- Log the end time
update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_bi_patient_daily_heartrate_summary'
;

END