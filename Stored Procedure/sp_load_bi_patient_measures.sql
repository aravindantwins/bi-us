CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_bi_patient_measures`()
BEGIN
/* 
2020-11-14  Aravindan -- Initial version
2021-01-12  Aravindan - Included a date condition in where clause (to limit the run time).
2021-03-20  Aravindan - Modified EA1C to get loaded from the product table.
						modified the inReversal and inReversalWMet logic to follow the template in India clinic
                        included maintenance for removal of records if a patient profile was set as Active for multiple times. 
2021-05-29  Aravindan - Included weight_5d, sleep_duration_minutes_5d, cgm_60d maintenance
2021-06-18  Aravindan - Included maintenance for predicted cgm 1d
2021-07-08  Aravindan - Included maintenance for continuous_SleepDuration_Minutes 
2021-08-31  Aravindan - Included maintenance for insulin units
2021-10-28  Aravindan - Included maintenance for NAFLD, FRS scores and recentlabDate, recetLabA1c as per calendar date
2021-11-03  Aravindan - Included maintenance for eA1C_60d and reversal columns using the same. last available values for BMI, sysBP_5d, diasBP_5d per member as of a calendar date
*/


-- log the process start time
update dailyprocesslog 
set startdate = now()
where processname = 'sp_load_bi_patient_measures'
;

drop temporary table if exists tmp_1d_measures; 

create temporary table tmp_1d_measures 
(
clientid int not null,
measure_event_date date not null,
cgm_1d decimal(6,2) null,
ketone_1d decimal(6,2) null,
weight decimal(6,2) null,
bmi decimal(6,2) null,
sleep_duration_minutes int null,
continuous_SleepDuration_Minutes int null,
deepSleep_mins int null, 
remSleep_mins int null,
lightSleep_mins int null,
awakeSleep_mins int null,
steps int null, 
systolic_1d int null,
diastolic_1d int null,
actionScore_1d decimal(6,2) null, 
energy decimal(5,2) null, 
mood decimal(5,2) null,
nut_sat decimal(5,2) null,
starRating_1d decimal(5,2) null,
symptoms int null,
medicine_drugs varchar(500) null,
medicine varchar(500) null,
medicine_excl_metformin varchar(500) null,
insulin_units int null,
tac_1d decimal(5,2) null,
sleepScore decimal(6,2) null
);

insert into tmp_1d_measures
(clientid, measure_event_date, cgm_1d, ketone_1d, weight, bmi, systolic_1d, diastolic_1d, 
actionScore_1d, energy, mood, nut_sat, starRating_1d, symptoms, tac_1d) 
select s.clientid, s.date as measure_event_date, 
s1.cgm_1d as cgm_1d, 
cast(avg(s5.ketone) as decimal(6,2)) as ketone_1d,
cast(avg(s2.weight) as decimal(6,2)) as weight, 
cast(avg(s2.bmi) as decimal(6,2)) as bmi, 
-- sum(s3.count) as steps, 
-- max(s4.totalMinutes) as sleep_duration_minutes,
floor(avg(s6.systolic)) as systolic_1d,
floor(avg(s6.diastolic)) as diastolic_1d,
cast(avg(s7.score) as decimal(6,2)) as actionScore_1d,
cast(avg(case when s8.type = 'ENERGY' then s8.value else null end) as decimal(5,2)) as energy, 
cast(avg(case when s8.type = 'MOOD' then s8.value else null end) as decimal(5,2)) as mood, 
cast(avg(case when s8.type = 'NUTRITION_SATISFACTION' then s8.value else null end) as decimal(5,2)) as nut_sat, 
cast(avg(case when s8.type = 'COACH' then s8.value else null end) as decimal(5,2)) as starRating_1d,
count(distinct case when s8.answerType = 'hml' then s8.type else null end) as symptoms,
cast(avg(s9.tac_score) as decimal(5,2)) as tac_1d
from 
(
	select b.clientid, a.date, c.timezoneid from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
																inner join v_client_tmp c on b.clientid = c.clientid
) s left join (
		select a.clientid, date(convert_tz(a.eventtime,@@time_zone,c.timezoneid)) as eventtdate, cast(avg(a.value) as decimal(6,2)) as cgm_1d
		from stagingglucosevalues a inner join v_client_tmp c on a.clientid = c.clientid
		group by a.clientid, date(convert_tz(a.eventtime,@@time_zone,c.timezoneid)) 
        ) s1 on s.clientid = s1.clientid and s.date = s1.eventtdate
	left join weights s2 on s.clientid = s2.clientid and s.date = date(convert_tz(s2.eventtime,@@time_zone,s.timezoneid)) 
	left join glucoseketones s5 on s.clientid = s5.clientid and s.date = date(convert_tz(s5.eventtime, @@time_zone,s.timezoneid))
    left join bloodpressurevalues s6 on s.clientid = s6.clientid and s.date = date(convert_tz(s6.eventtime, @@time_zone,s.timezoneid))
    left join (	
				select s.clientid, date_sub(date(convert_tz(eventtime, @@time_zone,b.timezoneid)), interval 1 day) as eventdate, scoreType, period, score -- this is because action score is always for the previous day (eventtime column). 
				from twins.scores s inner join v_client_tmp b on s.clientid = b.clientid 
				where scoreType = 'DAILY_SCORE' and period = 'DAILY'
			  ) s7 on s.clientid = s7.clientid and s.date = s7.eventdate
    left join (
					select a.clientid, type, date(convert_tz(eventtime, @@time_zone,c.timezoneid)) as eventdate, value, answerType 
                    from selfreportanswers a inner join selfreportquestions b on a.selfReportQuestionId = b.id
											 inner join v_client_tmp c on a.clientid = c.clientid
			  ) s8 on s.clientid = s8.clientid and s.date = s8.eventdate
	left join bi_tac_measure s9 on s.clientid = s9.clientid and s.date = s9.measure_date
group by s.clientid, s.date
;

create index idx_tmp_1d_measures on tmp_1d_measures (clientid asc, measure_event_date asc, cgm_1d, ketone_1d, weight, systolic_1d, diastolic_1d, tac_1d); 

/*
-- Medicine Drugs
drop temporary table if exists tmp_med_drugs; 

create temporary table tmp_med_drugs as 
select clientid, eventdate, group_concat(distinct med_drug) as medicine_drugs
from 
(
	-- This section for a new medicine tracking change implemented on Dec 21, 2019. After this change implemented, each medicine in the ToDoItems will have the medicineID filled. 
	select clientid, eventdate, group_concat(distinct med_drug) as med_drug
    from 
    (
		select a.clientid, date(scheduledLocalTime) as eventdate, case when b.medicinename = 'GLICLAZIDE' then 'Gc'
				  when b.medicinename = 'GLIMEPIRIDE' then 'Gm'
				  when b.medicinename = 'JANUVIA' then 'Jv'
				  when b.medicinename like 'JANUMET%' then 'Jm'
				  when b.medicinename = 'METFORMIN' then 'M'
				  else b.medicineName end as medicine, group_concat(distinct b5.epc,' ','(',b5.category ,')') as med_drug -- group_concat(distinct b2.drugname,' ','(',left(b3.category,4) ,')') as med_drug
		from clienttodoitems a left join medicines b on a.medicineid = b.medicineid 
							   -- left join medicinedrugs b1 on b.medicineid = b1.medicineid
                               -- left join drugs b2 on b1.drugid = b2.drugid
                               -- left join drugclasses b3 on b2.drugClassId = b3.drugClassId
							   left join medicinepharmclasses b4 on b.medicineId = b4.medicineID
                               left join pharmclasses b5 on b4.pharmClassId = b5.pharmClassId
							   inner join v_client_tmp c on a.clientid = c.clientid	
		where a.category = 'medicine' and a.type='medicine' and a.status in ('FINISHED','UNFINISHED')
        -- and scheduledLocalTime >= date_sub(now(), interval 12 day) and scheduledLocalTime <= now()
		group by clientid, date(scheduledLocalTime), medicinename
    ) s group by clientid, eventdate

	union all 

	-- This section is to handle the existing records without the new column medicineID. 
    select clientId, eventDate, group_concat(distinct med_drug) as med_drug
	from 
	(
			select a.clientId, date(scheduledLocalTime) as eventdate, 
			case when ifnull(b.medicinename, type) = 'GLIMEPIRIDE' then 'Gm'
				  when ifnull(b.medicinename, type) = 'JANUVIA' then 'Jv'
				  when ifnull(b.medicinename, type) like 'JANUMET%' then 'Jm'
				  when ifnull(b.medicinename, type) = 'METFORMIN' then 'M'
				  else ifnull(b.medicinename, type) end as med,  ifnull(group_concat(distinct b5.epc,' ','(',b5.category ,')'), type) as med_drug
			from clienttodoitems a left join medicines b on a.medicineid = b.medicineid 
								   left join medicinepharmclasses b4 on b.medicineId = b4.medicineID
								   left join pharmclasses b5 on b4.pharmClassId = b5.pharmClassId
            inner join v_client_tmp c on a.clientid = c.clientid 
			where a.category = 'medicine' and type not in ('medicine') and a.status in ('FINISHED','UNFINISHED')
            -- and scheduledLocalTime >= date_sub(now(), interval 12 day) and scheduledLocalTime <= now()
			group by clientId, date(scheduledLocalTime), type
		) s  
	group by clientId, eventDate
) s 
group by clientid, eventdate
;

update tmp_1d_measures a inner join tmp_med_drugs b on a.clientid = b.clientid and a.measure_event_date = b.eventdate
set a.medicine_drugs = b.medicine_drugs
;

drop temporary table if exists tmp_med_drugs; 


	OLD medicine maintenance
	create temporary table tmp_medicine_detail as 
	select clientid, eventdate, group_concat(medicine) as medicine
	from 
	(
		-- This section for a new medicine tracking change implemented on Dec 21, 2019. After this change implemented, each medicine in the ToDoItems will have the medicineID filled. 
		select clientid, eventdate, group_concat(concat(' ', medicine, '-', qty,unit,'x',amt) order by medicine) as medicine
		-- ,group_concat(concat(' ', medicine, '(', med_drug,')', 'X',amt) order by medicine) as medicine_drug
		from 
		(
			select a.clientid, date(scheduledLocalTime) as eventdate, case when b.medicinename = 'GLICLAZIDE' then 'Gc'
					  when b.medicinename = 'GLIMEPIRIDE' then 'Gm'
					  when b.medicinename = 'JANUVIA' then 'Jv'
					  when b.medicinename like 'JANUMET%' then 'Jm'
					  when b.medicinename = 'METFORMIN' then 'M'
					  else b.medicineName end as medicine, floor(targetvalue) as qty, b.unit, b2.drugname as med_drug, count(medicineName) as amt
			from clienttodoitems a left join medicines b on a.medicineid = b.medicineid 
								   left join medicinedrugs b1 on b.medicineid = b1.medicineid
								   left join drugs b2 on b1.drugid = b2.drugid
								   inner join v_client_tmp c on a.clientid = c.clientid	
			where category = 'medicine' and type='medicine' and a.status in ('FINISHED','UNFINISHED')
			-- and scheduledLocalTime >= date_sub(now(), interval 12 day) and scheduledLocalTime <= now()
			group by clientid, date(scheduledLocalTime), medicinename
		) s group by clientid, eventdate

		union all 

		-- This section is to handle the existing records without the new column medicineID. 
		select clientId, eventDate, group_concat(concat(' ',med,'-', unit, 'mgx', amt) order by med) as medicine
		from 
		(
				select a.clientId, date(scheduledLocalTime) as eventDate, 
				case when type = 'GLICLAZIDE' then 'Gc'
				  when type = 'GLIMEPIRIDE' then 'Gm'
				  when type = 'JANUVIA' then 'Jv'
				  when type like 'JANUMET%' then 'Jm'
				else left(type,1) end as med, if(targetValue < 1, left(targetValue,3),cast(floor(targetValue) as char(5)))  as unit,   count(type)  as amt  
				from clienttodoitems a inner join v_client_tmp c on a.clientid = c.clientid 
				where category = 'medicine' and type not in ('medicine') and a.status in ('FINISHED','UNFINISHED')
				-- and scheduledLocalTime >= date_sub(now(), interval 12 day) and scheduledLocalTime <= now()
				group by clientId, date(scheduledLocalTime), type , targetValue
			) s  
		group by clientId, eventDate
	) s 
	group by clientid, eventdate
	;

-- maintenance of insulin units
		drop temporary table if exists tmp_medicine_records; 

		create temporary table tmp_medicine_records as 
		select cd.clientid, cd.date as eventdate, group_concat(med_drug) as all_med_drug, sum(insulin_units) as insulin_units, sum(total_meds) as total_meds
		from 
		(
				select distinct d.date, c.clientId
				from twins.v_date_to_date d
				inner join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		) cd left join
		(   
			select clientid, startdate, enddate, ifnull(group_concat(distinct epc,' ','(',pharmclassCategory ,')'), group_concat(distinct type)) as med_drug, 
			sum(case when ifnull(epc, type) like '%insulin%' then (targetvalue * amt) else null end) as insulin_units, 
			group_concat(concat(' ', medicine, '-', floor(targetvalue),unit,'x',amt) order by medicine) as medicine,
			count(distinct ifnull(epc, type)) as total_meds
			from 
			(
					select distinct clientid, a.category, targetvalue, type, startdate, enddate, a.medicineId, status, b.medicinename, b5.epc, b5.category as pharmclassCategory, ifnull(b.unit, 'mg') as unit,
						case  when ifnull(b.medicinename, type) = 'GLICLAZIDE' then 'Gc'
							  when ifnull(b.medicinename, type) = 'GLIMEPIRIDE' then 'Gm'
							  when ifnull(b.medicinename, type) = 'JANUVIA' then 'Jv'
							  when ifnull(b.medicinename, type) like 'JANUMET%' then 'Jm'
							  when ifnull(b.medicinename, type) = 'METFORMIN' then 'M'
							  else ifnull(b.medicineName, left(type,1)) end as medicine, length(a.timeslots) - length(replace(a.timeslots,',','')) + 1 as amt
					from clienttodoschedules a  left join medicines b on a.medicineid = b.medicineid 
												left join medicinepharmclasses b4 on b.medicineId = b4.medicineID
												left join pharmclasses b5 on b4.pharmClassId = b5.pharmClassId 
					where a.category = 'Medicine' and status not in ('DELETED')
			) s 
			group by clientid, startdate, enddate
		) s on cd.clientid = s.clientid and cd.date between s.startdate and s.enddate
		group by cd.clientid, cd.date
		;


		create index idx_tmp_medicine_records on tmp_medicine_records(clientid asc, eventdate asc, insulin_units, total_meds); 

		update bi_patient_measures a inner join tmp_medicine_records b on a.clientid = b.clientid and a.measure_event_date = b.eventdate
		set a.insulin_units = b.insulin_units
		;

		drop temporary table if exists tmp_medicine_records; 

*/

-- Medicine (in US clinic, take all medicines related from clientodoschedules table instead of clienttodoitems. 2 reasons - we dont want whether the member finished/unfinished the medicine or not here and faster retrieval of data due to the size of source table. 
-- If the formatting is good, slowly this will be updated in India clinic also. 
drop temporary table if exists tmp_medicine_detail; 

create temporary table tmp_medicine_detail as 
select date, clientid, 
medicine, 
medicine_drugs,
medicine_excl_metformin,
insulin_units
from 
( 
	select x.date, x.clientid, 
	ltrim(rtrim(group_concat(distinct concat('  ', x.allMeds,' ', x.allMedsDosage) order by x.allMeds))) as medicine, 
	ltrim(rtrim(group_concat(distinct concat('  ', x.medsWoMet,' ',x.medsWoMetDosage) order by x.medsWoMet))) as medicine_excl_metformin, 
    group_concat(distinct x.med_drug) as medicine_drugs,
    sum(case when x.category = 'INSULIN' then x.totalDayDosage else null end) as insulin_units
    from 
    (
			select distinct a.date, a.clientid, b.medName as allMeds, cast(b.dayDosage as char(30)) as allMedsDosage, c.medName as medsWoMet, cast(c.dayDosage as char(30)) as medsWoMetDosage, b.med_drug, b.category, totalDayDosage
			from 
			(											
						select date, clientid 
						from v_date_to_date a inner join 
						(
								select b.clientid, 'active' as status, max(status_start_date) as status_start_date, max(status_end_date) as status_end_date 						
								from bi_patient_status B				
								where b.status = 'active'						
								and not exists (select 1 from bi_patient_Status d where b.clientid = d.clientid and d.status = 'pending_active' and d.status_end_date > b.status_end_date)		
								group by b.clientid						
						) b on a.date between b.status_start_date and b.status_end_date

			) a left join 	
			( -- Get all medicines
								select s.clientid, s.startdate, s.enddate, medicineId, medicineName, unit, s.type, s.timeslot, s.frequency, s.status, medClass,	medName, med_drug, category, 
								s.bDosage, s.lDosage, s.dDosage, concat('(',s.bDosage,'-',s.lDosage,'-',s.dDosage,')') as dayDosage, (s.bDosage + s.lDosage + s.dDosage) as totalDayDosage,
								cast(case when s.timeslot like '%BEFORE%' then 'Before Food'						
										  when s.timeslot like '%AFTER%' then 'After Food' end as char(15)) as Timing					
								from 						
								(						
									 select clientid, startdate, enddate, d.medicineId, m.medicineName, m.unit, type, d.targetValue, group_concat(distinct d.timeslots) as timeslot, d.frequency, d.status, pc.epc as medClass, pc.category,
									 case when pc.category like '%Insulin%' then concat(m.medicineName,' ')						
										  when pc.category not like '%Insulin%' then concat(m.medicineName,' ',case (mod(targetvalue, 1) > 0) when true then round(targetvalue, 1) else floor(targetvalue) end, ' ', unit) end medName, 
									 group_concat(distinct pc.epc,' ','(',pc.category ,')') as med_drug,
									 
									 sum(case when d.timeslots like '%BREAKFAST%' and pc.category like '%insulin%' then floor(d.targetValue) 					
											  when d.timeslots like '%BREAKFAST%' and pc.category not like '%insulin%' then 1 else 0 end) as bDosage,      				
									 sum(case when d.timeslots like '%LUNCH%' and pc.category like '%insulin%'  then floor(d.targetValue)					
											  when d.timeslots like '%LUNCH%' and pc.category not like '%insulin%'  then 1 else 0 end) as lDosage,				
									 sum( case when d.timeslots like '%DINNER%' and pc.category like '%insulin%'  then floor(d.targetValue)					
											   when d.timeslots like '%DINNER%' and pc.category not like '%insulin%'  then 1 else 0 end) as dDosage
											   
											   
									from clienttodoschedules d left join medicines m on d.medicineID = m.medicineID		
																left join medicinepharmclasses mp on m.medicineId = mp.medicineId
																left join pharmclasses pc on mp.pharmclassId = pc.pharmclassId
									where d.category = 'medicine'	
									group by clientid, startdate, enddate, d.medicineId					
								) s 
				) b on a.clientid = b.clientid and a.date between b.startdate and b.enddate
				left join 
				(-- Get all medicines excluding Metformin
								select s.clientid, s.startdate, s.enddate, medicineId, medicineName, unit, s.type, s.timeslot, s.frequency, s.status, medClass,	medName, med_drug, 
								s.bDosage, s.lDosage, s.dDosage, concat('(',s.bDosage,'-',s.lDosage,'-',s.dDosage,')') as dayDosage, 						
								cast(case when s.timeslot like '%BEFORE%' then 'Before Food'						
										  when s.timeslot like '%AFTER%' then 'After Food' end as char(15)) as Timing					
								from 						
								(						
									 select clientid, startdate, enddate, d.medicineId, m.medicineName, m.unit, type, d.targetValue, group_concat(distinct d.timeslots) as timeslot, d.frequency, d.status, pc.epc as medClass, 
									 case when pc.category like '%Insulin%' then concat(m.medicineName,' ')						
										  when pc.category not like '%Insulin%' then concat(m.medicineName,' ',case (mod(targetvalue, 1) > 0) when true then round(targetvalue, 1) else floor(targetvalue) end, ' ', unit) end medName, 
									 group_concat(distinct pc.epc,' ','(',pc.category ,')') as med_drug,
									 
									 sum(case when d.timeslots like '%BREAKFAST%' and pc.category like '%insulin%' then floor(d.targetValue) 					
											  when d.timeslots like '%BREAKFAST%' and pc.category not like '%insulin%' then 1 else 0 end) as bDosage,      				
									 sum(case when d.timeslots like '%LUNCH%' and pc.category like '%insulin%'  then floor(d.targetValue)					
											  when d.timeslots like '%LUNCH%' and pc.category not like '%insulin%'  then 1 else 0 end) as lDosage,				
									 sum( case when d.timeslots like '%DINNER%' and pc.category like '%insulin%'  then floor(d.targetValue)					
											   when d.timeslots like '%DINNER%' and pc.category not like '%insulin%'  then 1 else 0 end) as dDosage
											   
											   
									from clienttodoschedules d left join medicines m on d.medicineID = m.medicineID		
																left join medicinepharmclasses mp on m.medicineId = mp.medicineId
																left join pharmclasses pc on mp.pharmclassId = pc.pharmclassId
									where d.category = 'medicine'	
									and pc.epc not like '%Biguanide%'
									group by clientid, startdate, enddate, d.medicineId					
								) s 
				) c on a.clientid = c.clientid and a.date between c.startdate and c.enddate and b.medicineID = c.medicineId
				order by a.clientid, a.date, b.medName, c.medName
		) x
		group by x.clientid, x.date
) s 
;

update tmp_1d_measures a inner join tmp_medicine_detail b on a.clientid = b.clientid and a.measure_event_date = b.date
set a.medicine = b.medicine,
	a.medicine_excl_metformin = b.medicine_excl_metformin,
    a.medicine_drugs = b.medicine_drugs,
	a.insulin_units = b.insulin_units
;

drop temporary table if exists tmp_medicine_detail; 



-- All level sleep
drop temporary table if exists tmp_sleep_details_measure;

create temporary table tmp_sleep_details_measure 
as 
select s1.clientid, s1.date, s2.deepMinutes, remMinutes, lightMinutes, awakeMinutes, asleepMinutes, otherLevelMinutes, totalMinutes, sleepStartTime, sleepEndTime, continuous_SleepDuration_Minutes
from 
(
	select b.clientid, a.date from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
	-- where a.date >= date_sub(date(now()), interval 10 day)
) s1 left join 
(
			select 
			clientid, 
			sleepStartTime,
			sleepEndTime,
			eventDate,
			deepMinutes, 
			remMinutes, 
			lightMinutes, 
			awakeMinutes,
			asleepMinutes,
			otherLevelMinutes, 
			totalMinutes - ifnull(awakeMinutes,0) as totalMinutes,
			continuous_SleepDuration_Minutes
			from 
			(
				select a.clientid, 
				convert_tz(a.eventtime, @@time_zone, c.timezoneId) as sleepStartTime, 
			    convert_tz(date_add(a.eventtime, interval duration/1000 second), @@time_zone, c.timeZoneID) as sleepEndTime, 
			    date(convert_tz(date_add(a.eventtime, interval duration/1000 second), @@time_zone, c.timeZoneID)) as eventdate, -- add the sleep duration to the eventtime and get that date because sleep usually starts the previous day and ends the next day
				deepMinutes, 
				remMinutes, 
				lightMinutes, 
				awakeMinutes,
				asleepMinutes,
				otherLevelMinutes, 
				sum((a.duration/1000/60)) as totalMinutes,
			    -- READ THE BELOW 2 LINES for continuous_SleepDuration_Minutes
			    -- fucntion maxContinuousSleepSeconds takes an input as Allowed Interruption Seconds. That is during the sleep, if a patient is awake for those seconds then that interval gets ignored from the total sleep and remaining is calculated as continuous sleep. 
				-- In future if the allowed interruption changes, modify this line (2nd parameter)
			    sum(maxContinuousSleepSeconds(a.id, 60)/60) as continuous_SleepDuration_Minutes
				from 
				sleeps a left join (
													select sleepid, clientid, 
													sum(case when level = 'deep' then (durationSeconds/60) end) as deepMinutes, 
													sum(case when level = 'rem' then (durationSeconds/60) end) as remMinutes, 
													sum(case when level = 'light' then (durationSeconds/60) end) as lightMinutes, 
													sum(case when level in ('wake','awake') then (durationSeconds/60) end) as awakeMinutes,
													sum(case when level = 'asleep' then (durationSeconds/60) end) as asleepMinutes,
													sum(case when level not in ('deep','rem','light','awake','wake','asleep') then (durationSeconds/60) end) as otherLevelMinutes
													from sleepdetails
													group by sleepId, clientid
									) b on a.id = b.sleepId and a.clientid = b.clientid 
						 inner join v_client_tmp c on a.clientid = c.clientid 
				group by a.clientid, date(convert_tz(date_add(a.eventtime, interval duration/1000 second), @@time_zone, c.timeZoneID))
			) s 
) s2 on s1.clientid = s2.clientid and s1.date = s2.eventdate
;


create index idx_tmp_sleep_details_measure on tmp_sleep_details_measure (clientid asc, date asc, deepMinutes, remMinutes, lightMinutes, awakeMinutes, totalMinutes, continuous_SleepDuration_Minutes)
;

update tmp_1d_measures a inner join tmp_sleep_details_measure b on (a.clientid = b.clientid and a.measure_event_date = b.date)
set a.deepSleep_mins = b.deepMinutes, 
	a.remSleep_mins = b.remMinutes, 
	a.lightSleep_mins = b.lightMinutes,
    a.awakeSleep_mins = b.awakeMinutes,
    a.sleep_duration_minutes = b.totalMinutes,
    a.continuous_SleepDuration_Minutes = b.continuous_SleepDuration_Minutes
;

drop temporary table if exists tmp_sleep_details_measure;

-- maintain the sleep score
drop temporary table if exists tmp_sleepscore_detail
;

create temporary table tmp_sleepscore_detail
(
	clientid int not null,
	measure_event_date date not null,
	sleepScore decimal(6,2) null
); 


insert into tmp_sleepscore_detail (clientid, measure_event_date, sleepScore) 
select a.clientid, a.date, b.score 
from 
(select b.clientid, a.date from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active') a 
left join 
(
	select clientid, date, score from clienthealthscores s join healthscoreversions v on s.versionid=v.healthscoreversionid
	where v.type='SLEEP_V1' and v.latest=1
) b on a.clientid = b.clientid and a.date = b.date 
;


update tmp_1d_measures a inner join tmp_sleepscore_detail b on (a.clientid = b.clientid and a.measure_event_date = b.measure_event_date)
set a.sleepScore = b.sleepScore
;

drop temporary table if exists tmp_sleepscore_detail
;

-- Maintain measures table
insert into bi_patient_measures 
(clientid, measure_event_date, cgm_1d, ketone_1d, weight, bmi, sleep_duration_minutes, continuous_SleepDuration_Minutes, deepSleep_mins, remSleep_mins, lightSleep_mins, awakeSleep_mins, steps, systolic_1d, diastolic_1d, actionScore_1d, energy, mood, nut_sat, starRating_1d, symptoms, medicine_drugs, medicine, medicine_excl_metformin, insulin_units, tac_1d, sleepScore)
select clientid, measure_event_date, cgm_1d, ketone_1d, weight, bmi, sleep_duration_minutes, continuous_SleepDuration_Minutes, deepSleep_mins, remSleep_mins, lightSleep_mins, awakeSleep_mins, steps, systolic_1d, diastolic_1d, actionScore_1d, energy, mood, nut_sat, starRating_1d, symptoms, medicine_drugs, medicine, medicine_excl_metformin, insulin_units, tac_1d,sleepScore
from tmp_1d_measures a
where not exists 
(select 1 from bi_patient_measures b where a.clientid = b.clientid and a.measure_event_date = b.measure_event_date)
;

update bi_patient_measures a inner join tmp_1d_measures b on a.clientid = b.clientid and a.measure_event_date = b.measure_event_date 
set a.cgm_1d = b.cgm_1d,
	a.ketone_1d = b.ketone_1d,
	a.weight = b.weight,
    a.bmi = b.bmi,
    a.sleep_duration_minutes = b.sleep_duration_minutes,
    a.continuous_SleepDuration_Minutes = b.continuous_SleepDuration_Minutes,
	a.deepSleep_mins = b.deepSleep_mins, 
	a.remSleep_mins = b.remSleep_mins, 
	a.lightSleep_mins = b.lightSleep_mins,
    a.awakeSleep_mins = b.awakeSleep_mins,
    a.systolic_1d = b.systolic_1d,
    a.diastolic_1d = b.diastolic_1d,
    a.actionScore_1d = b.actionScore_1d,
    a.energy = b.energy,
    a.mood = b.mood,
    a.nut_sat = b.nut_sat, 
    a.starRating_1d = b.starRating_1d,
    a.symptoms = b.symptoms,
    a.medicine_drugs = b.medicine_drugs, 
    a.medicine = b.medicine,
    a.medicine_excl_metformin = b.medicine_excl_metformin, 
    a.insulin_units = b.insulin_units, 
    a.tac_1d = b.tac_1d,
    a.sleepScore = b.sleepScore
    -- a.eA1C_1d = cast((b.cgm_1d+ 46.7)/28.7 as decimal(6,2))
; 

-- bring eA1C from product table
update bi_patient_measures a inner join 
(select clientid, date, value from clienthealthscoredetails where parameterId in (Select healthScoreParameterId from healthscoreparameters where name = 'eA1c')) b 
on (a.clientid = b.clientid and a.measure_event_date = b.date)
set a.eA1C = b.value
;



drop temporary table if exists tmp_steps; 

create temporary table tmp_steps as 
select a.clientid, a.date as eventdate, b.total_steps, c.steps as actSummarySteps,
if(b.total_steps is null and c.steps is null, null, if(ifnull(b.total_steps,0) > ifnull(c.steps,0), b.total_steps,c.steps)) as Total_steps_chosen
from 
(
	select clientid, date 
	from 
	v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
	where date >= date_sub(date(now()), interval 20 day)
) a left join 
(
	select a.clientid, date(convert_tz(eventtime,@@time_zone, b.timezoneId)) as eventdate, sum(count) as total_steps from steps a inner join v_client_tmp b on a.clientid = b.clientid 
	where eventtime >= date_sub(now(), interval 25 day) 
	group by a.clientid, date(convert_tz(eventtime,@@time_zone, b.timezoneId))
) b on a.clientid = b.clientid and a.date = b.eventdate 
left join 
(
	select a.clientid, date(convert_tz(eventtime,@@time_zone, b.timezoneId)) as eventdate, sum(steps) as steps
    from activitysummaries a inner join v_client_tmp b on a.clientid = b.clientid 
    where eventtime >= date_sub(now(), interval 25 day) 
    group by a.clientid, date(convert_tz(eventtime,@@time_zone, b.timezoneId))
) c on a.clientid = c.clientid and a.date = c.eventdate
;


create index idx_tmp_steps on tmp_steps (clientid asc, eventdate asc, Total_steps_chosen); 

update bi_patient_measures a 
inner join tmp_steps b on a.clientid = b.clientid and a.measure_event_date = b.eventdate 
set a.steps = b.Total_steps_chosen
;

drop temporary table if exists tmp_steps
;

-- Let us maintain some of the other important 1d measures like Cohort, Med Adh, etc
-- Cohort
	drop temporary table if exists tmp_pts_daily_cohort; 

	create temporary table tmp_pts_daily_cohort
	(
	clientid int not null,
	measure_event_date date not null,
	cohort varchar(30) null
	); 

	insert into tmp_pts_daily_cohort
	select a.clientid, a.date as measure_event_date, ifnull(c.name,'Default') as cohort
	from 
	(
		select cd.clientid, cd.timeZoneId, cd.date, max(convert_tz(Starttime, @@time_zone, cd.timezoneId)) as change_time
		from 
		(	
			select d.date, c.clientId, ct.timezoneId
			from twins.v_date_to_date d
			inner join bi_patient_status c on d.date >= c.status_Start_Date and d.date <= c.status_end_date and c.status = 'active'
            inner join v_client_tmp ct on c.clientid = ct.clientid
		) cd 
		left join (select a.*, b.name from cohorthistories a inner join medicalconditions b on a.medicalConditionID = b.medicalConditionId where b.name = 'Diabetes') a on cd.clientid = a.clientid and date(convert_tz(Starttime, @@time_zone, cd.timezoneId)) <= cd.date
		group by cd.clientid, cd.date
		) a left join (select a.*, b.name from cohorthistories a inner join medicalconditions b on a.medicalConditionID = b.medicalConditionId where b.name = 'Diabetes') b on a.clientid = b.clientid and a.change_time = convert_tz(b.Starttime, @@time_zone, a.timezoneId)
			left join cohorts c on b.cohortid = c.cohortid
	;

	create index idx_tmp_pts_cohort on tmp_pts_daily_cohort (clientid asc, measure_event_date asc, cohort);

	update bi_patient_measures a inner join tmp_pts_daily_cohort b on a.clientid = b.clientid and a.measure_event_date = b.measure_event_date 
	set a.cohort = b.cohort
	;

	drop temporary table if exists tmp_pts_daily_cohort; 

-- Medicine Adherence 
	drop temporary table if exists tmp_pts_med_adh; 

	create temporary table tmp_pts_med_adh
	(
	clientid int not null,
	measure_event_date date not null,
	med_adh varchar(10) null
	); 

	insert into tmp_pts_med_adh
	select a.clientid, a.date as measure_event_date, b.med_adh
	from 
	(    
			select date, clientid
			from v_date_to_date a inner join bi_patient_status b on ((a.date between b.status_start_date and b.status_end_date) and b.status = 'active')
			-- where date >= date_sub(date(itz(now())), interval 15 day)
	) a left join
	(
		select clientid, date(scheduledLocalTime) as eventDate, count(medicineId) as total_to_consume, count(case when status = 'FINISHED' then medicineID else null end) as total_finished, 
		if(count(medicineId) = count(case when status = 'FINISHED' then medicineID else null end), 'Yes', 'No') as med_adh
		from clienttodoitems 
		where category = 'medicine'
		and scheduledLocalTime <= now()
		group by clientid, date(scheduledLocalTime)
	) b on a.clientid = b.clientid and a.date = b.eventDate
	;
	/*
	(
	   select a.clientid, a.calllogfieldid, report_date, value
	   from
		(
			select a.clientid, a.calllogfieldid, c.calllogid, date(convert_tz(c.eventtime,@@time_zone, ct.timezoneId)) as report_date, max(c.calllogid) as recentCallLogID 
			from calllogdetails a, calllogfields b, calllogs c, v_client_tmp ct 
			where a.calllogfieldid = b.calllogfieldid
			and a.calllogid = c.calllogid
            and a.clientid = ct.clientid
			and b.label = 'Ensure MED ADH'
			group by a.clientid, a.calllogfieldid, date(convert_tz(c.eventtime,@@time_zone, ct.timezoneId))
		) a, calllogdetails b
		where a.clientid = b.clientid and a.calllogfieldid = b.calllogfieldid and a.recentCallLogID = b.calllogid
	) b on a.clientid = b.clientid and a.date = b.report_date
	;
	*/

	create index idx_tmp_pts_med_adh on tmp_pts_med_adh (clientid asc, measure_event_date asc, med_adh);

	update bi_patient_measures a inner join tmp_pts_med_adh b on a.clientid = b.clientid and a.measure_event_date = b.measure_event_date 
	set a.med_adh = b.med_adh
	;

	drop temporary table if exists tmp_pts_med_adh; 


	drop temporary table if exists tmp_recent_LabA1C_value; 

	create temporary table tmp_recent_LabA1C_value as 
		select s.clientid, s.treatmentdays, s.measure_event_date, s.recentLabDate, s1.labA1C as recentLabA1C
		from 
		(
				select s1.clientid, s1.treatmentdays, s1.measure_event_date, max(s2.bloodworkdate) as recentLabDate
				from bi_patient_measures s1 left join 
				(
					select distinct clientid, date(ifnull(bloodworktime, eventtime)) as bloodworkdate
					from clinictestresults 
					where ifnull(bloodworktime, eventtime) is not null
				) s2 on s1.clientid = s2.clientid and s2.bloodworkdate <= s1.measure_event_date
				group by s1.clientid, s1.measure_event_date
		) s left join 
		(
					select distinct clientid, date(ifnull(bloodworktime, eventtime)) as bloodworkdate, cast(avg(veinhba1c) as decimal(10,2)) as LabA1c 
					from clinictestresults 
					where ifnull(bloodworktime, eventtime) is not null
					group by clientid, date(bloodworktime)
		) s1 on s.clientid = s1.clientid and s.recentLabDate = s1.bloodworkdate
		where s.measure_event_date >= date_sub(date(now()), interval 15 day) and s.measure_event_date <= date(now())
        ;

		create index idx_tmp_recent_LabA1C_value on tmp_recent_LabA1C_value(clientid asc, measure_event_date asc, recentLabDate, recentLabA1C);
     
    
		update bi_patient_measures a inner join tmp_recent_LabA1C_value b on (a.clientid = b.clientid and a.measure_event_date = b.measure_event_date)
		set a.recentLabDate = b.recentLabDate,
			a.recentLabA1c = b.recentLabA1C
		; 
	
    drop temporary table if exists tmp_recent_LabA1C_value; 

-- Store the recent lab on a calandar date with triglycerides and hdlCholesterol values present but the lab shouldn't be the first lab. So for a date, in the GMI table if column last_available_LabTg_Hdl_date is blank that means the member never had lab or only had 1st lab.
		drop temporary table if exists tmp_recent_lab_with_tg_hdl_value; 
	
		create temporary table tmp_recent_lab_with_tg_hdl_value as 
		select s.clientid, s.date, if(s.recentLabDate = s2.firstBloodWork, null, s.recentLabDate) as recentLabDate -- , s1.LabTg as recentLabTg,s1.LabHdl as recentLabHdl
		from 
		(
				select s1.clientid, s1.measure_event_date as date, max(s2.bloodworkdate) as recentLabDate
				from bi_patient_measures s1 left join 
				(
					select distinct clientid, date(ifnull(bloodworktime, eventtime)) as bloodworkdate
					from clinictestresults 
					where triglycerides is not null and hdlCholesterol is not null
					AND deleted = 0
				) s2 on s1.clientid = s2.clientid and s2.bloodworkdate <= s1.measure_event_date 
				group by s1.clientid, s1.measure_event_date
		) s left join 
		(
					select distinct clientid, date(ifnull(bloodworktime, eventtime)) as bloodworkdate
					from clinictestresults 
					where bloodworktime is not null and triglycerides is not null and hdlCholesterol is not NULL
					AND deleted = 0
		) s1 on s.clientid = s1.clientid and s.recentLabDate = s1.bloodworkdate
		left join 
		(
					select distinct a.clientid, min(date(ifnull(bloodworktime, eventtime))) as firstBloodWork
					from clinictestresults a 
					where bloodworktime is not null and triglycerides is not null and hdlCholesterol is not NULL
					AND deleted = 0
					group by a.clientid
		) s2 on s.clientid = s2.clientid
		-- where s.date >= date_sub(date(now()), interval 15 day) and s.date <= date(now())		
		;

		create index idx_tmp_recent_lab_with_tg_hdl_value on tmp_recent_lab_with_tg_hdl_value(clientid asc, date asc, recentLabDate);


        update bi_patient_measures a inner join tmp_recent_lab_with_tg_hdl_value b on a.clientid=b.clientid and a.measure_event_date=b.date  
        set a.last_available_LabTg_Hdl_date = b.recentLabDate
	    ;
     
       
        drop temporary table if exists tmp_recent_lab_with_tg_hdl_value; 
       
        
 -- Store the recent lab on a calandar date with alt and ast values present but the lab shouldn't be the first lab. So for a date, in the GMI table if column last_available_LabAst_Alt_date is blank that means the member never had lab or only had 1st lab.
        drop temporary table if exists tmp_recent_lab_with_alt_ast_value; 
       
		create temporary table tmp_recent_lab_with_alt_ast_value as 
		select s.clientid, s.date, if(s.recentLabDate = s2.firstBloodWork, null, s.recentLabDate) as recentLabDate
		from 
		(
				select s1.clientid, s1.measure_event_date as date, max(s2.bloodworkdate) as recentLabDate
				from bi_patient_measures s1 left join 
				(
					select distinct clientid, date(ifnull(bloodworktime, eventtime)) as bloodworkdate
					from clinictestresults 
					where alt is not null and ast is not null and insulin is not null and glucose is not null
					and deleted = 0
				) s2 on s1.clientid = s2.clientid and s2.bloodworkdate <= s1.measure_event_date 
				group by s1.clientid, s1.measure_event_date
		) s left join 
		(
					select distinct clientid, date(ifnull(bloodworktime, eventtime)) as bloodworkdate
					from clinictestresults 
					where alt is not null and ast is not null and insulin is not null and glucose is not null
					and deleted = 0
		) s1 on s.clientid = s1.clientid and s.recentLabDate = s1.bloodworkdate
		left join 
		(
					select distinct a.clientid, min(date(ifnull(bloodworktime, eventtime))) as firstBloodWork
					from clinictestresults a 
					where alt is not null and ast is not null and insulin is not null and glucose is not null
					and deleted = 0
					group by a.clientid
		) s2 on s.clientid = s2.clientid
		-- where s.date >= date_sub(date(now()), interval 15 day) and s.date <= date(now())
        ;

		create index idx_tmp_recent_lab_with_alt_ast_value on tmp_recent_lab_with_alt_ast_value(clientid asc, date asc, recentLabDate);

	    update bi_patient_measures a inner join tmp_recent_lab_with_alt_ast_value b on a.clientid=b.clientid and a.measure_event_date=b.date  
	    set a.last_available_LabAst_Alt_date=b.recentLabDate
		;

		drop temporary table if exists tmp_recent_lab_with_alt_ast_value; 
	
	
-- 5d Values calulation
	drop temporary table if exists tmp_1d_measures_1; 

	create temporary table tmp_1d_measures_1 as 
	select clientid, measure_event_date, cgm_1d, systolic_1d, diastolic_1d, ketone_1d, actionScore_1d, starRating_1d, tac_1d, sleep_duration_minutes, weight, steps,energy,mood
	from tmp_1d_measures
	;

	create index idx_tmp_1d_measures_1 on tmp_1d_measures_1 (clientid asc, measure_event_date asc); 

	drop temporary table if exists tmp_5d_measures; 

	create temporary table tmp_5d_measures as 
	SELECT 
		c.clientId
		,c.measure_event_date
		,if(count(distinct case when p.cgm_1d is not null then p.measure_event_date else null end) >= 3, cast(avg(p.cgm_1d) as decimal(6,2)), null) as cgm_5d
		,cast(AVG(p.ketone_1d) as decimal(6,2)) as ketone_5d
		,floor(cast(AVG(p.systolic_1d) as decimal(6,2))) as systolic_5d
		,floor(cast(AVG(p.diastolic_1d) as decimal(6,2))) as diastolic_5d
		,cast(AVG(p.actionScore_1d) as decimal(6,2)) as actionScore_5d
		,cast(AVG(p.starRating_1d) as decimal(6,2)) as starRating_5d
		,cast(AVG(p.tac_1d) as decimal(6,2)) as tac_5d
        ,cast(avg(p.sleep_duration_minutes) as decimal(5,0)) as sleep_duration_minutes_5d
        ,cast(avg(p.weight) as decimal(5,2)) as weight_5d
        ,cast(avg(p.steps) as decimal(5,0)) as steps_5d
        ,cast(avg(p.energy) as decimal(5,2)) as energy_5d
        ,cast(avg(p.mood) as decimal(5,2)) as mood_5d
	FROM tmp_1d_measures c 
	INNER JOIN tmp_1d_measures_1 p 
		ON (p.measure_event_date between date_sub(c.measure_event_date, INTERVAL 4 DAY) and c.measure_event_date)
		AND c.clientId = p.clientId 
	GROUP BY c.clientId, c.measure_event_date
	;

	update bi_patient_measures a inner join tmp_5d_measures b on a.clientid = b.clientid and a.measure_event_date = b.measure_event_date 
	set a.cgm_5d = b.cgm_5d,
		a.ketone_5d = b.ketone_5d,
		a.systolic_5d = b.systolic_5d,
		a.diastolic_5d = b.diastolic_5d,
		a.actionScore_5d = b.actionScore_5d,
		a.starRating_5d = b.starRating_5d,
		a.tac_5d = b.tac_5d,
        a.sleep_duration_minutes_5d = b.sleep_duration_minutes_5d,
        a.weight_5d = b.weight_5d,
        a.steps_5d = b.steps_5d,
        a.energy_5d = b.energy_5d,
        a.mood_5d = b.mood_5d
	; 

	drop temporary table if exists tmp_5d_measures
	;

-- 14 day numbes to calculate the GMI, eA1C
	drop temporary table if exists tmp_14d_measures; 

	create temporary table tmp_14d_measures as 
	SELECT 
		c.clientId
		,c.measure_event_date
		,cast(AVG(p.cgm_1d) as decimal(6,2)) as cgm_14d
		,cast(3.31 + (0.02392*cast(avg(p.cgm_1d) as decimal(6,2))) as decimal(5,1)) as GMI
		-- ,cast((cast(AVG(p.cgm_1d) as decimal(6,2)) + 46.7)/28.7 as decimal(6,2)) eA1C_14d
	FROM tmp_1d_measures c 
	INNER JOIN tmp_1d_measures_1 p 
		ON (p.measure_event_date between date_sub(c.measure_event_date, INTERVAL 13 DAY) and c.measure_event_date)
		AND c.clientId = p.clientId 
	GROUP BY c.clientId, c.measure_event_date
	;

	update bi_patient_measures a inner join tmp_14d_measures b on a.clientid = b.clientid and a.measure_event_date = b.measure_event_date 
	set a.cgm_14d = b.cgm_14d,
		a.GMI = b.GMI
		-- a.eA1C_14d = b.eA1C_14d
	; 

	drop temporary table if exists tmp_14d_measures; 


-- 60d CGM
	drop temporary table if exists tmp_60d_measures; 

	create temporary table tmp_60d_measures as 
	SELECT 
		c.clientId
		,c.measure_event_date
		,cast(AVG(p.cgm_1d) as decimal(6,2)) as cgm_60d
	FROM tmp_1d_measures c 
	INNER JOIN tmp_1d_measures_1 p 
		ON (p.measure_event_date between date_sub(c.measure_event_date, INTERVAL 59 DAY) and c.measure_event_date)
		AND c.clientId = p.clientId 
	GROUP BY c.clientId, c.measure_event_date
	;

	update bi_patient_measures a inner join tmp_60d_measures b on a.clientid = b.clientid and a.measure_event_date = b.measure_event_date 
	set a.cgm_60d = b.cgm_60d
	; 

	drop temporary table if exists tmp_60d_measures; 
    
drop temporary table if exists tmp_1d_measures; 
drop temporary table if exists tmp_1d_measures_1; 


-- This section is to maitain 10d med adh NO to use in InReversal calculation
		drop temporary table if exists tmp_med_adh1;
		drop temporary table if exists tmp_med_adh2;

		create temporary table tmp_med_adh1 as 
		select s1.clientid, s1.date, med_adh as value from 
		(
			select clientid, date
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status ='active' 
			where date >= date_sub(date(now()), interval 20 day) 
		) s1 left join 
		(
			select clientid, measure_Event_date, case when med_adh = 'No' then 1 else 0 end as med_adh from bi_patient_measures 
			where measure_Event_date >= date_sub(date(now()), interval 30 day) 
		) s2
		on s1.clientid = s2.clientid and s2.measure_Event_date = s1.date 
		;
		create index idx_tmp_med_adh1 on tmp_med_adh1 (clientid asc, date asc, value); 

		create temporary table tmp_med_adh2 as select * From tmp_med_adh1; 

		create index idx_tmp_med_adh2 on tmp_med_adh2 (clientid asc, date asc, value); 

		drop temporary table if exists tmp_med_adh_NO_10d;

		create temporary table tmp_med_adh_NO_10d as 
		select s1.clientid, s1.date, cast(sum(s2.value)/count(s2.value) as decimal(10,2)) as med_adh_no_10d
		from 
		tmp_med_adh1 s1 left join  
		tmp_med_adh2 s2 on s1.clientid = s2.clientid and s2.date between date_sub(s1.date, interval 9 day) and s1.date 
		group by s1.clientid, s1.date    
		;

		create index idx_tmp_med_adh_NO_10d on tmp_med_adh_NO_10d (clientid asc, date asc, med_adh_no_10d); 

		update bi_patient_measures a inner join 
		tmp_med_adh_NO_10d b on (a.clientid = b.clientid and a.measure_event_date = b.date)
		set a.med_adh_NO_10d = b.med_adh_NO_10d
		;

drop temporary table if exists tmp_med_adh1;
drop temporary table if exists tmp_med_adh2;
drop temporary table if exists tmp_med_adh_NO_10d;

-- maitain the last_available_cgm_5d to a calendar date
drop temporary table if exists tmp_last_available_cgm_5d;

		create temporary table tmp_last_available_cgm_5d as 
		select a.clientid, a.measure_event_date, b.measure_event_date as last_available_cgm_5d_date, b.cgm_5d as last_available_cgm_5d
		from 
		(
			select a.clientid, a.measure_event_date, max(case when b.cgm_5d is not null then b.measure_event_date else null end) as last_cgm_5d_date 
			from (select clientid, measure_event_date from bi_patient_measures where measure_event_date >= date_sub(date(now()), interval 20 day)) a inner join bi_patient_measures b on a.clientid = b.clientid and b.measure_event_date <= a.measure_event_date
			group by clientid, a.measure_event_date
		) a inner join bi_patient_measures b on a.clientid = b.clientid and a.last_cgm_5d_date = b.measure_event_date 
		;

		create index idx_tmp_last_available_cgm_5d on tmp_last_available_cgm_5d (clientid asc, measure_event_date asc, last_available_cgm_5d_date, last_available_cgm_5d); 
       
		update bi_patient_measures a inner join tmp_last_available_cgm_5d b on a.clientid = b.clientid and a.measure_event_date = b.measure_event_Date
		set a.last_available_cgm_5d = b.last_available_cgm_5d,
			a.last_available_cgm_5d_date = b.last_available_cgm_5d_date
		where a.measure_event_date >= date_sub(date(now()), interval 15 day)	
		;

drop temporary table if exists tmp_last_available_cgm_5d;

-- maintain last available cgm_60d
	drop temporary table if exists tmp_last_available_cgm_60d; 
			
	create temporary table tmp_last_available_cgm_60d as 
	select a.clientid, a.measure_event_date, b.measure_event_date as last_available_cgm_60d_date, b.cgm_60d as last_available_cgm_60d
	from 
	(
		select a.clientid, a.measure_event_date, max(case when b.cgm_60d is not null then b.measure_event_date else null end) as last_cgm_60d_date 
		from (select clientid, measure_event_date from bi_patient_measures where measure_event_date >= date_sub(date(now()), interval 20 day)) a inner join bi_patient_measures b on a.clientid = b.clientid and b.measure_event_date <= a.measure_event_date
		group by clientid, a.measure_event_date
	) a inner join bi_patient_measures b on a.clientid = b.clientid and a.last_cgm_60d_date = b.measure_event_date 
	;

	create index idx_tmp_last_available_cgm_60d on tmp_last_available_cgm_60d(clientid asc, measure_event_date asc, last_available_cgm_60d_date, last_available_cgm_60d); 

	update bi_patient_measures a inner join tmp_last_available_cgm_60d b on a.clientid = b.clientid and a.measure_event_date = b.measure_event_date 
	set a.last_available_cgm_60d = b.last_available_cgm_60d,
		a.last_available_cgm_60d_date = b.last_available_cgm_60d_date
	where a.measure_event_date >= date_sub(date(now()), interval 15 day)
	; 

	 drop temporary table if exists tmp_last_available_cgm_60d; 
 
-- this table will store the last available weight_1d for each patient on a calendar date.
				drop temporary table if exists tmp_last_available_weight_1d;

				create temporary table tmp_last_available_weight_1d as 
				select a.clientid, a.measure_event_date, b.measure_event_date as last_available_weight_1d_date, b.weight as last_available_weight_1d
				from 
				(
					select a.clientid, a.measure_event_date, max(case when b.weight is not null then b.measure_event_date else null end) as last_weight_1d_date 
					from (select clientid, measure_event_date from bi_patient_measures  where measure_event_date >= date_sub(date(now()), interval 20 day)) a inner join bi_patient_measures b on a.clientid = b.clientid and b.measure_event_date <= a.measure_event_date
					group by clientid, a.measure_event_date
				) a inner join bi_patient_measures b on a.clientid = b.clientid and a.last_weight_1d_date = b.measure_event_date 
				;

				create index idx_tmp_last_available_weight_1d on tmp_last_available_weight_1d (clientid asc, measure_event_date asc, last_available_weight_1d_date, last_available_weight_1d); 
				
				update bi_patient_measures a inner join tmp_last_available_weight_1d b on a.clientid = b.clientid and a.measure_event_date = b.measure_event_date 
				set a.last_available_1d_weight_date = b.last_available_weight_1d_date
				where a.measure_event_date >= date_sub(date(now()), interval 15 day)
				;
				
				drop temporary table if exists tmp_last_available_weight_1d
				;

    --  maitain the last_available_BMI to a calendar date
    
    drop temporary table if exists tmp_last_available_BMI;
	create temporary table tmp_last_available_BMI as 
    select a.clientid, a.measure_event_date,b.measure_event_date as last_available_BMI_date,b.BMI as last_available_BMI 
    from
	(
			select a.clientid, a.measure_event_date, max(case when b.BMI is not null then b.measure_event_date else null end) as last_BMI_date 
			from (select clientid, measure_event_date from bi_patient_measures where measure_event_date >= date_sub(date(now()), interval 20 day)) a inner join bi_patient_measures b on a.clientid = b.clientid and b.measure_event_date <= a.measure_event_date
			group by a.clientid, a.measure_event_date
	) a inner join bi_patient_measures b on a.clientid=b.clientid and a.last_BMI_date=b.measure_event_date
     ;       
     
	create index idx_tmp_last_available_BMI on tmp_last_available_BMI (clientid asc, measure_event_date asc, last_available_BMI_date, last_available_BMI); 
	      
		update bi_patient_measures a inner join tmp_last_available_BMI b on a.clientid = b.clientid and a.measure_event_date = b.measure_event_Date
		set a.last_available_BMI = b.last_available_BMI,
			a.last_available_BMI_date = b.last_available_BMI_date
        where a.measure_event_date >= date_sub(date(now()), interval 15 day)  
      ;
      
	    drop temporary table if exists tmp_last_available_BMI;

    
    -- maitain the last_available_systolic_5d to a calendar date
    drop temporary table if exists tmp_last_available_systolic_5d;
	create temporary table tmp_last_available_systolic_5d as 
    select a.clientid, a.measure_event_date,b.measure_event_date as last_available_systolic_5d_date,b.systolic_5d as last_available_systolic_5d 
    from
	(
			select a.clientid, a.measure_event_date, max(case when b.systolic_5d is not null then b.measure_event_date else null end) as last_systolic_5d_date 
			from (select clientid, measure_event_date from bi_patient_measures where measure_event_date >= date_sub(date(now()), interval 20 day)) a inner join bi_patient_measures b on a.clientid = b.clientid and b.measure_event_date <= a.measure_event_date
			group by clientid, a.measure_event_date
	) a inner join bi_patient_measures b on a.clientid=b.clientid and a.last_systolic_5d_date=b.measure_event_date
     ;       
     
	create index idx_tmp_last_available_systolic_5d on tmp_last_available_systolic_5d (clientid asc, measure_event_date asc, last_available_systolic_5d_date, last_available_systolic_5d); 
	   
		update bi_patient_measures a inner join tmp_last_available_systolic_5d b on a.clientid = b.clientid and a.measure_event_date = b.measure_event_Date
		set a.last_available_systolic_5d = b.last_available_systolic_5d,
			a.last_available_systolic_5d_date = b.last_available_systolic_5d_date
		where a.measure_event_date >= date_sub(date(now()), interval 15 day)  
		;
        
	    drop temporary table if exists tmp_last_available_systolic_5d;

    
     -- maitain the last_available_diastolic_5d to a calendar date
    drop temporary table if exists tmp_last_available_diastolic_5d;
	create temporary table tmp_last_available_diastolic_5d as 
    select a.clientid, a.measure_event_date,b.measure_event_date as last_available_diastolic_5d_date,b.diastolic_5d as last_available_diastolic_5d
    from
	(
			select a.clientid, a.measure_event_date, max(case when b.diastolic_5d is not null then b.measure_event_date else null end) as last_diastolic_5d_date 
			from (select clientid, measure_event_date from bi_patient_measures where measure_event_date >= date_sub(date(now()), interval 20 day)) a inner join bi_patient_measures b on a.clientid = b.clientid and b.measure_event_date <= a.measure_event_date
			group by clientid, a.measure_event_date
	) a inner join bi_patient_measures b on a.clientid=b.clientid and a.last_diastolic_5d_date=b.measure_event_date
     ;
    
	create index idx_tmp_last_available_diastolic_5d on tmp_last_available_diastolic_5d (clientid asc, measure_event_date asc, last_available_diastolic_5d_date, last_available_diastolic_5d); 
	      
		update bi_patient_measures a inner join tmp_last_available_diastolic_5d b on a.clientid = b.clientid and a.measure_event_date = b.measure_event_Date
		set a.last_available_diastolic_5d = b.last_available_diastolic_5d,
			a.last_available_diastolic_5d_date = b.last_available_diastolic_5d_date
		where a.measure_event_date >= date_sub(date(now()), interval 15 day)  
		;
   
	drop temporary table if exists tmp_last_available_diastolic_5d;


-- Maintenance of NonAdh_Brittle patients
		drop temporary table if exists tmp_na_brittle_maintenance; 
		create temporary table tmp_na_brittle_maintenance as 
			select clientid, measure_event_date, treatmentdays, last_available_cgm_5d_date, nut_adh_syntax_5d, medicine_drugs, total_200above_days, if(medicine_drugs like '%insu%', 'Yes', 'No') is_on_Insulin,
			case 	 
				 when treatmentdays >= 35
						and last_available_cgm_5d > 175
						and medicine_drugs like '%insu%'
						and nut_adh_syntax_5d < 0.8 then 'NA-Brittle1'

				 when treatmentdays >= 7
						and total_200above_days = 7 
						and nut_adh_syntax_5d < 0.8
						and if((medicine_drugs like '%alpha Glucosidase Inhibitors%' or medicine_drugs like '%GLP-1 Receptor Agonist%' or
								medicine_drugs like '%Glinide%' or medicine_drugs like '%Sodium-Glucose Cotransporter 2 Inhibitor%' or medicine_drugs like '%Sulfonylurea%' or
								medicine_drugs like '%Thiazolidinedione%' or medicine_drugs like '%Diabetes Others%' or 
								(medicine_drugs like '%Biguanide%' and medicine_drugs like '%Dipeptidyl Peptidase 4 Inhibitor%')), 'Yes', 'No') = 'Yes' then 'NA-Brittle2'
							
				 when if(medicine_drugs like '%alpha Glucosidase Inhibitors%' or medicine_drugs like '%GLP-1 Receptor Agonist%' or
						medicine_drugs like '%Glinide%' or medicine_drugs like '%Sodium-Glucose Cotransporter 2 Inhibitor%' or medicine_drugs like '%Sulfonylurea%' or
						medicine_drugs like '%Thiazolidinedione%' or  medicine_drugs like '%Diabetes Others%', 'Yes', 'No') = 'Yes'
						and nut_adh_syntax_5d < 0.8
						and treatmentdays > 90 then 'NA-Brittle3' else null end as NABrittle_Category
					  
			from 
			(
				select s1.clientid, s1.measure_event_date, s1.treatmentdays, s1.cgm_5d, s1.last_available_cgm_5d_date, s1.last_available_cgm_5d, s1.nut_adh_syntax_5d, s1.medicine_drugs, 
                count(distinct case when s2.cgm_5d > 200 then s2.measure_event_date else null end) as total_200above_days
				from 
				(select clientid, measure_event_date, treatmentdays, cgm_5d, cgm_1d, last_available_cgm_5d_date,last_available_cgm_5d, nut_adh_syntax_5d, medicine_drugs from bi_patient_measures where measure_event_date >= date_sub(date(itz(now())), interval 7 day)) s1
				inner join bi_patient_measures s2 on s1.clientid = s2.clientid and s2.measure_event_date between date_sub(s1.last_available_cgm_5d_date, interval 6 day) and s1.last_available_cgm_5d_date
				group by s1.clientid, s1.measure_event_date
			) s
		;

		drop temporary table if exists tmp_na_brittle_maintenance_1; 

		create temporary table tmp_na_brittle_maintenance_1 as 
		select * from tmp_na_brittle_maintenance where NABrittle_Category is not null
		;

		create index idx_tmp_na_brittle_maintenance on tmp_na_brittle_maintenance_1 (clientid asc, measure_event_date asc, NABrittle_Category);

	
		update bi_patient_measures a inner join tmp_na_brittle_maintenance_1 b on a.clientid = b.clientid and a.measure_event_date = b.measure_event_date 
		set a.nonAdh_brittle_category = b.NABrittle_Category
		;

		drop temporary table if exists tmp_na_brittle_maintenance;
		drop temporary table if exists tmp_na_brittle_maintenance_1; 
        
-- find out for each calendar, what is the last 14 days CGM_5d available days for each patients
-- because if a patient is not having any 5d in the last 14 days on a calendar date then that patient will be ignored in the calculation. 
drop temporary table if exists tmp_last_14days_CGM_avail;

		create temporary table tmp_last_14days_CGM_avail
		select a.clientid, a.measure_event_date, 
		count(case when (b.cgm_1d is null and b.isOnCGMPrediction = 'No') then b.measure_event_date else null end) as total_noCGM_14_days            
		from 
		bi_patient_measures a inner join 
		 (
			select a.clientid, a.measure_event_date, a.cgm_1d, if(pg.clientid is not null, 'Yes', 'No') as isOnCGMPrediction
			from bi_patient_measures a left join predictedcgmcycles pg on a.clientid = pg.clientid and a.measure_event_date between pg.startdate and pg.enddate
			-- where a.date >= date_sub(date(itz(now())), interval 30 day)
		) b on a.clientid = b.clientid and b.measure_event_date >= date_sub(a.measure_event_date, interval 13 day) and b.measure_event_date <= a.measure_event_date
		-- where a.date >= date_sub(date(itz(now())), interval 15 day)
		group by clientid, a.measure_event_date
		;

		create index idx_tmp_last_14days_CGM_avail on tmp_last_14days_CGM_avail (clientid asc, measure_event_date asc, total_noCGM_14_days); 
  
  		update bi_patient_measures a inner join tmp_last_14days_CGM_avail b on a.clientid = b.clientid and a.measure_event_date = b.measure_event_Date
		set a.isNoCGM_14days = if(b.total_noCGM_14_days = 14, 'Yes', 'No')
		;

drop temporary table if exists tmp_last_14days_CGM_avail
;

-- Is InReversal related calculations
-- is_InReversal, Is_MetOnlybyOPS, Is_moreThanMET columns are being maintained. 
		drop temporary table if exists tmp_med_recommends;
		create temporary table tmp_med_recommends as 
							select a.clientid, date(convert_tz(eventtime,@@time_zone,b.timezoneId)) as notificationDate 
							from medicationrecommendations a inner join v_client_tmp b on a.clientid = b.clientid 
                            where medicationtierid in (
								select tiers.medicationtierid 
								from medicationtiers tiers join medicationtables ts on tiers.medicationtableid=ts.medicationtableid where tiers.tiernumber=0 -- tiernumber = 0 means no medicine recommendation
							)
		;

		create index idx_tmp_med_recommends on tmp_med_recommends (clientid asc, notificationdate asc);

		drop temporary table if exists tmp_pts_detail_reversal_medicine;

		create temporary table tmp_pts_detail_reversal_medicine as 
		select distinct a.clientid, a.treatmentDays, date, b.cohort, b.med_adh, b.CGM_5d,  b.medicine_drugs as medicine_drugs, -- s.notificationdate,
		case when b.medicine_drugs is null and (b.last_available_cgm_5d < 140 or pgc.clientid is not null) then 'Yes'
			 else null end as is_InReversal,

		case when (b.medicine_drugs = 'METFORMIN' or b.medicine_drugs = 'METFORMIN (DIAB)' or b.medicine_drugs = 'Biguanide (DIABETES)') then 'Yes' else null end as is_MetOnly,

		case when ((b.last_available_cgm_5d < 140 or pgc.clientid is not null) and (b.medicine_drugs = 'METFORMIN' or b.medicine_drugs = 'METFORMIN (DIAB)' or b.medicine_drugs = 'Biguanide (DIABETES)')) then 'Yes' else null end as is_MetOnly_by_OPS,
			
		case when b.medicine_drugs is not null and (b.medicine_drugs <> 'METFORMIN' and b.medicine_drugs <> 'METFORMIN (DIAB)' and b.medicine_drugs <> 'Biguanide (DIABETES)') then 'Yes' else null end as is_MoreThanMet
		from 
		(
			select a.date, b.clientId, b.startdate, (datediff(a.date, b.startDate)) as treatmentDays
			from twins.v_date_to_date a
			inner join
			(
				select clientid, startDate, endDate
				from
				(
					select clientid, max(status_start_date) as startDate, max(Status_end_date) as endDate
					from bi_patient_status
					where status = 'active'
					group by clientid
				) s
			) b on a.date between b.startDate and b.endDate
			where date >= date_sub(date(now()), interval 28 day)
		) a left join bi_patient_measures b on a.clientid = b.clientid and a.date = b.measure_event_date
			-- left join tmp_med_recommends s on a.clientid = s.clientid and a.date = s.notificationdate
			left join predictedcgmcycles pgc on a.clientid = pgc.clientid and a.date between pgc.startdate and pgc.enddate
	;

		create index idx_tmp_pt_reversal on tmp_pts_detail_reversal_medicine (clientid asc, date asc, is_InReversal, is_MetOnly, is_MoreThanMet); 
        
		update bi_patient_measures a inner join tmp_pts_detail_reversal_medicine b on (a.clientid = b.clientid and a.measure_event_date = b.date)
		set a.treatmentDays = b.treatmentDays,
			a.is_InReversal = b.is_InReversal,
			a.is_MetOnly = b.is_MetOnly,
			a.is_MetOnly_by_OPS = if(b.is_InReversal = 'Yes', 'No', b.is_MetOnly_by_OPS),
			a.is_moreThanMet = b.is_moreThanMet
		;
		
        drop temporary table if exists tmp_med_recommends;
        drop temporary table if exists tmp_pts_detail_reversal_medicine;        

       
       
		--  maintain reversal flags using 60d eA1C  60d eA1C < 6.5 on no meds OR only with metformin 
		drop temporary table if exists tmp_eA1C60d_flag_measures;
		create temporary table tmp_eA1C60d_flag_measures as 
		select 
				clientId
				,measure_event_date
		        ,cast((cgm_60d+ 46.7)/28.7 as decimal(6,2)) as eA1C_60d
		         ,case when (cast((cgm_60d+ 46.7)/28.7 as decimal(6,2)) < 6.5) and ( medicine_drugs is null) then 'Yes' end as is_reversal_eA1C60d_with_noMed
				,case when (cast((cgm_60d+ 46.7)/28.7 as decimal(6,2)) < 6.5) and ( medicine_drugs = 'METFORMIN' or medicine_drugs = 'METFORMIN (DIAB)' or medicine_drugs = 'Biguanide (DIABETES)') then 'Yes' end as is_reversal_eA1C60d_with_only_met
		from  bi_patient_measures
		-- where measure_event_date >= date_sub(date(now()), interval 1 month)
		;
		
		
		
		create index idx_tmp_eA1C60d_flag_measures on tmp_eA1C60d_flag_measures(clientId asc,measure_event_date asc,eA1C_60d,is_reversal_eA1C60d_with_noMed,is_reversal_eA1C60d_with_only_met);
		
		update bi_patient_measures a inner join tmp_eA1C60d_flag_measures b on a.clientid = b.clientid and a.measure_event_date = b.measure_event_date 
		set a.eA1C_60d=b.eA1C_60d,
		    a.is_reversal_eA1C60d_with_noMed=b.is_reversal_eA1C60d_with_noMed,
		    a.is_reversal_eA1C60d_with_only_met=b.is_reversal_eA1C60d_with_only_met
		;
		
		drop temporary table if exists tmp_eA1C60d_flag_measures
		;

/*
-- Nut Syntax Adherence - Identify the current nut syntax based on the foodlog and assign the color for patient

-- Step 1 : Set up nutritionprofiles audit
	drop temporary table if exists tmp_nutrition_profiles_aud; 

    create temporary table tmp_nutrition_profiles_aud as 
    select s1.clientid, s1.syntaxname, s1.greenfoods as greenfoods, s1.pstarFoods as pstarFoods, s1.portionControlledRedFoods as portionControlledRedFoods, 
				s1.breakfast as breakfast, s1.lunch as lunch, s1.dinner as dinner, s1.timerestrictedEating as timerestrictedEating,  s1.currentManualSyntax, 
				s1.date as start_date, 
				ifnull(date_sub(s2.date, interval 1 day), date(now())) as end_date 
				from 
				(
					select @rnum := case when s1.clientid = @clientid then @rnum + 1 else 1 end as rownum, 
					@clientid := s1.clientid as clientid, s1.syntaxname, s1.greenfoods, s1.pstarFoods, s1.portionControlledRedFoods, s1.breakfast, s1.lunch, s1.dinner, s1.timerestrictedEating, s1.currentManualSyntax, date
					from 
					(	
							select a.clientid,ns.syntaxname, b.greenfoods, b.pstarFoods, b.portionControlledRedFoods, b.breakfast, b.lunch, b.dinner, b.timerestrictedEating, b.currentManualSyntax, date(a.recentModified) as date from 
							(
								select clientid, date(datemodified) as date, max(datemodified) as recentModified from nutritionprofiles_aud group by clientid, date(datemodified) 
							) a inner join nutritionprofiles_aud b on a.clientid = b.clientid and a.recentModified = b.datemodified
								inner join nutritionsyntax ns on b.nutritionSyntaxId = ns.nutritionSyntaxId
								inner join v_client_tmp c on a.clientid = c.clientid 
							
							union 
							   
							select a.clientid, ns.syntaxname, a.greenfoods, a.pstarFoods, a.portionControlledRedFoods, a.breakfast, a.lunch, a.dinner, a.timerestrictedEating, a.currentManualSyntax, date(a.datemodified) as date from 
							nutritionprofiles a inner join nutritionsyntax ns on a.nutritionSyntaxId = ns.nutritionSyntaxId
												inner join v_client_tmp b on a.clientid = b.clientid 
					 ) s1 cross join (select @rnum := 0, @clientid := '') s2 -- limit 10
					 order by s1.clientid, s1.date
				) s1 left join 
				(
					select @snum := case when s1.clientid = @clientid then @snum + 1 else 1 end as rownum, 
					@clientid := s1.clientid as clientid, s1.syntaxname, s1.greenfoods, s1.pstarFoods, s1.portionControlledRedFoods, s1.breakfast, s1.lunch, s1.dinner, s1.timerestrictedEating, s1.currentManualSyntax, date
					from 
					(	
							select a.clientid, ns.syntaxname, b.greenfoods, b.pstarFoods, b.portionControlledRedFoods, b.breakfast, b.lunch, b.dinner, b.timerestrictedEating, b.currentManualSyntax, date(a.recentModified) as date from 
							(
								select clientid, date(datemodified) as date, max(datemodified) as recentModified from nutritionprofiles_aud group by clientid, date(datemodified) 
							) a inner join nutritionprofiles_aud b on a.clientid = b.clientid and a.recentModified = b.datemodified
								inner join nutritionsyntax ns on b.nutritionSyntaxId = ns.nutritionSyntaxId
								inner join v_client_tmp c on a.clientid = c.clientid 
							
							union 
							   
							select a.clientid, ns.syntaxname, a.greenfoods, a.pstarFoods, a.portionControlledRedFoods, a.breakfast, a.lunch, a.dinner, a.timerestrictedEating, a.currentManualSyntax, date(a.datemodified) as date from 
							nutritionprofiles a inner join nutritionsyntax ns on a.nutritionSyntaxId = ns.nutritionSyntaxId
												inner join v_client_tmp b on a.clientid = b.clientid 
					 ) s1 cross join (select @snum := 0, @clientid := '') s2 -- limit 10
					 order by s1.clientid, s1.date
				) s2 on s1.clientid = s2.clientid and s2.rownum = s1.rownum + 1 
		; 

		create index idx_tmp_nutrition_profiles_aud on tmp_nutrition_profiles_aud(clientid asc, start_date, end_date, timerestrictedEating);

-- Step 2 : Determine current nutrition syntax
		drop temporary table if exists tmp_nutrition_syntax_prediction;

		create temporary table tmp_nutrition_syntax_prediction as 
				select s.clientid, date  as mealdate, current_nut_syntax, 
				case when journey = 'J1' and current_nut_syntax in ('1','2','3','4') THEN 'Green'
					 when journey = 'J1' and current_nut_syntax in ('5','6','7') THEN 'Orange'
					 when journey = 'J1' and current_nut_syntax in ('8','9','10','11','12','13','14') THEN 'Red'
					 
					 when journey = 'J2' and current_nut_syntax in ('1','2','3','4','5','6','7') then 'Green'
					 when journey = 'J2' and current_nut_syntax in ('8','9','10') then 'Orange'
					 when journey = 'J2' and current_nut_syntax in ('11','12','13','14') THEN 'Red'
					 
					 when journey = 'J3' and current_nut_syntax in ('1','2','3','4','5','6','7','8','9','10') then 'Green'
					 when journey = 'J3' and current_nut_syntax in ('11','12') then 'Orange'
					 when journey = 'J3' and current_nut_syntax in ('13','14') THEN 'Red'
					 
					 when journey = 'J4' and current_nut_syntax in ('1','2','3','4','5','6','7','8','9','10','11','12','13','14') then 'Green' end as color
				from 
				(
							select s1.clientid, s1.date, -- s1.patientname, s1.age, s1.vegetarian, s1.enrollmentdateITZ, s1.enrolleddays, s1.doctorname, s1.coachname, s1.cohortgroup,
							ifnull(s1.journey,'J1') as journey, 
							ifnull(s1.green_syntax,'1-4') as green_syntax, ifnull(s1.recent_consecutive_reversaldays,'') as recent_consecutive_reversaldays, 
							ifnull(s1.orange_syntax,'5-7') as orange_syntax, 
							ifnull(s1.red_syntax,'8-14') as red_syntax, 

							ifnull(s2.current_nut_syntax,'NA') as current_nut_syntax, 
							ifnull(currentManualSyntax,'') as currentManualSyntax -- ,s2.*
							from 
							(
								select a.clientid, 	a.date, journey, nut_syntax as green_syntax, recent_consecutive_reversaldays,
								case when journey = 'J1' then '5-7' 
									 when journey = 'J2' then '8-10' 
									 when journey = 'J3' then '11-12' 
											else null end as orange_syntax,  
								case when journey = 'J1' then '8-14' 
									 when journey = 'J2' then '11-14' 
									 when journey = 'J3' then '13-14' 
											else null end as red_syntax
								from (	
											select date, b.clientid from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
											where date >= date_sub(date(itz(now())), interval 45 day) 
									 ) a left join dim_client b on a.clientid = b.clientid and b.is_row_current = 'Y'
							) s1 left join 
							( 
							select s.clientid, s.mealdate, greenfood, redFood, pstarFood, B_eggFood, B_totalFood, 
							B_pS1food, L_pS1food, D_pS1food, S_pS1food, B_pS2food, L_pS2food, D_pS2food, S_pS2food, B_PCRfood, L_PCRfood, D_PCRfood, S_PCRfood, 
							is_on_mealSkip, is_on_timeRestrictionEating, currentManualSyntax, 
							case when greenfood > 0 and is_on_mealskip = 1 and redFood = 0 and pstarFood = 0 then 1 
								 when greenfood > 0 and is_on_timeRestrictionEating = 1 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 0 then 2
								 when greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 0 and (B_eggFood = B_totalFood = 1) then 3
								 when greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 0 then 4
								 
								 when ((greenfood > 0 and is_on_mealskip = 1 and redFood = 0 and pstarFood = 1) and (B_pS1food+L_pS1food+D_pS1food+S_pS1food) = 1) then 5
								 when ((greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 1 and (B_eggFood = B_totalFood = 1)) and (B_pS1food+L_pS1food+D_pS1food+S_pS1food) = 1) then 6
								 when ((greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 1 ) and (B_pS1food+L_pS1food+D_pS1food+S_pS1food) = 1) then 7

								 when ((greenfood > 0 and is_on_mealskip = 1 and redFood = 0 and pstarFood = 1) and (B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1) then 8
								 when ((greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 1 and (B_eggFood = B_totalFood = 1)) and (B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1) then 9
								 when ((greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 1) and (B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1) then 10
								 
								 when ((greenfood > 0 and is_on_mealskip = 1 and (redFood = 1 or pstarFood = 1)) and ((B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1 or (B_PCRfood+L_PCRfood+D_PCRfood+S_PCRfood) = 1)) then 11
								 when ((greenfood > 0 and is_on_mealskip = 0 and (redFood = 1 or pstarFood = 1) and (B_eggFood = B_totalFood = 1)) and ((B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1 or (B_PCRfood+L_PCRfood+D_PCRfood+S_PCRfood) = 1)) then 12
								 when ((greenfood > 0 and is_on_mealskip = 0 and (redFood = 1 or pstarFood = 1)) and ((B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1 or (B_PCRfood+L_PCRfood+D_PCRfood+S_PCRfood) = 1)) then 13
								 when ((greenfood > 0 and is_on_mealskip = 0 and (redFood > 1 or pstarFood > 1)) and ((B_pS2food+L_pS2food+D_pS2food+S_pS2food) >= 1 or (B_PCRfood+L_PCRfood+D_PCRfood+S_PCRfood) >= 1)) then 14

								 else 'UK'
								 end as current_nut_syntax
							from 
							(
								select s1.clientid, s1.eventdate as mealdate, 
								ifnull(B_pS1food,0) as B_pS1food, ifnull(L_pS1food,0) as L_pS1food, ifnull(D_pS1food,0) as D_pS1food, ifnull(S_pS1food,0) as S_pS1food, 
								ifnull(B_pS2food,0) as B_pS2food, ifnull(L_pS2food,0) as L_pS2food, ifnull(D_pS2food,0) as D_pS2food, ifnull(S_pS2food,0) as S_pS2food, 
								ifnull(B_PCRfood,0) as B_PCRfood, ifnull(L_PCRfood,0) as L_PCRfood, ifnull(D_PCRfood,0) as D_PCRfood, ifnull(S_PCRfood,0) as S_PCRfood,
								if(s2.breakfast = 0 or s2.dinner = 0, 1, 0) as is_on_mealSkip, 
								if(s2.timeRestrictedEating = 1, 1, 0) as is_on_timeRestrictionEating,
								ifnull(s2.currentManualSyntax,'') as currentManualSyntax,
								ifnull(greenFood,0) as greenFood, ifnull(redfood_adjusted,0) as redFood, ifnull(pstarFood,0) as pstarFood, ifnull(B_eggFood,0) as B_eggFood, ifnull(B_totalFood,0) as B_totalFood
								from 
								(
									select clientid, eventdate, 
									max(case when categoryType = 'BREAKFAST' then pS1food else null end) as B_pS1food, 
									max(case when categoryType = 'LUNCH' then pS1food else null end) as L_pS1food, 
									max(case when categoryType = 'DINNER' then pS1food else null end) as D_pS1food, 
									sum(case when categoryType like '%SNACK%' then pS1food else null end) as S_pS1food,
									
									max(case when categoryType = 'BREAKFAST' then pS2food else null end) as B_pS2food, 
									max(case when categoryType = 'LUNCH' then pS2food else null end) as L_pS2food, 
									max(case when categoryType = 'DINNER' then pS2food else null end) as D_pS2food,
									sum(case when categoryType like '%SNACK%' then pS2food else null end) as S_pS2food,
								   
									
									max(case when categoryType = 'BREAKFAST' then PCRfood else null end) as B_PCRfood, 
									max(case when categoryType = 'LUNCH' then PCRfood else null end) as L_PCRfood, 
									max(case when categoryType = 'DINNER' then PCRfood else null end) as D_PCRfood,
									sum(case when categoryType like '%SNACK%' then PCRfood else null end) as S_PCRfood,

									
									sum(redfood) as redFood, sum(greenfood) as greenFood, sum(pstar_food) as pstarFood, sum(redfood) - sum(pstar_food) as redfood_adjusted,
									sum(case when categoryType = 'BREAKFAST' then egg_related_food else 0 end) as B_eggFood,
									max(case when categoryType = 'BREAKFAST' then totalFood else null end) as B_totalFood
									from 
									(
										select a.clientid, eventdate, categoryType, 
										count(distinct case when foodGroups like '%P_STAR%' then b.foodid else null end) as pS1food, 
										count(distinct case when foodGroups like '%P_STAR%' then b.foodid else null end) as pS2food,
										count(distinct case when foodGroups like '%PCR%' then b.foodid else null end) as PCRfood,
										count(distinct case when b.recommendationRating <= 1 then b.foodid else null end) as redfood, 
										count(distinct case when b.recommendationRating > 2 then b.foodid else null end) as greenfood,
										count(distinct case when e.foodid is not null and b.recommendationRating <= 1 then a.itemid else null end) as pstar_food,
										count(distinct case when d.name like '%egg%' then b.foodid else null end) as egg_related_food,
										count(distinct b.foodid) as totalFood
										from bi_food_supplement_log_detail a inner join foods b on a.itemid = b.foodid
																			 inner join foodingredientmappings c on b.foodid = c.foodid
																			 inner join ingredients d on c.ingredientid = d.ingredientId
																			 left join personalizedfoods e on a.itemid = e.foodid and a.clientid = e.clientid and e.recommendationrating is not null 
										where eventdate >= date_sub(date(now()), interval 50 day) 
										and a.category = 'foodlog'
										group by clientid, eventdate, categoryType
									) s 
									group by clientid, eventdate
								) s1 left join tmp_nutrition_profiles_aud s2 on s1.clientid = s2.clientid and s1.eventdate between s2.start_date and s2.end_date
							) s 
							) s2 on s1.clientid = s2.clientid and s1.date = s2.mealdate
				) s 
		;

		create index idx_tmp_nutrition_syntax_prediction on tmp_nutrition_syntax_prediction (clientid asc, mealdate asc, current_nut_syntax, color); 

		update bi_patient_measures a inner join tmp_nutrition_syntax_prediction b on (a.clientid = b.clientid and a.measure_event_date = b.mealdate)
		set a.nut_syntax = b.current_nut_syntax, 
			a.nut_syntax_color = b.color
		;
        
-- Maintain Nut_adh_syntax_1d
        update bi_patient_measures a, 
		(
			select a.clientid, a.measure_event_date, a.nut_syntax_color, a.tac_1d, round(c.macro) as macro, round(c.micro) as micro, round(c.biota) as biota
			from 
			bi_patient_measures a left join nutritionscores c on a.clientid = c.clientid and a.measure_event_date = c.date
			where a.measure_event_date >= date_sub(date(now()), interval 45 day)
		) b 
		set a.nut_adh_syntax = if(b.tac_1d = 3 and macro >= 70 and micro >= 70 and biota >= 70 and b.nut_syntax_color = 'Green', 'Yes', 'No')
		where (a.clientid = b.clientid and a.measure_event_date = b.measure_event_date) 
		;
        
-- Maintain Nut_adh_syntax_5d
		update bi_patient_measures a, 
		(
				select s1.clientid, s1.measure_event_date, cast(avg(s2.nut_adh) as decimal(5,2)) as nut_adh_5d															
				from 															
									(															
										select clientid, measure_event_date, case when nut_adh_syntax = 'yes' then 1 when nut_adh_syntax = 'no' then 0 end as nut_adh														
										from bi_patient_measures
										where measure_event_date >=  date_sub(date(now()), interval 30 day)
									) s1 inner join 															
									(															
										select clientid, measure_event_date, case when nut_adh_syntax = 'yes' then 1 when nut_adh_syntax = 'no' then 0 end as nut_adh														
										from bi_patient_measures	
										 where measure_event_date >= date_sub(date(now()), interval 40 day)
									) s2 on s1.clientid = s2.clientid and s2.measure_event_date between date_sub(s1.measure_event_date, interval 4 day) and s1.measure_event_date															
				group by s1.clientid, s1.measure_event_date
		) b 
		set a.nut_adh_syntax_5d = b.nut_adh_5d
		where (a.clientid = b.clientid and a.measure_event_date = b.measure_event_date) 
		;
*/
-- maintain the predicted 1d CGM
		drop temporary table if exists tmp_1d_predicted_cgm;

		create temporary table tmp_1d_predicted_cgm	as 
		select e.clientid, cast(score as decimal(7,2)) as predicted_1d_cgm, date(convert_tz(eventtime, @@time_zone, c.timezoneId)) as eventdate from 
		scores e inner join predictedcgmcycles pc on e.clientid = pc.clientid and date(eventtime) between pc.startdate and pc.enddate
				 left join clients c on e.clientid = c.id
		where e.scoreType = 'CGM' and e.period = 'Daily' and e.predicted = 1
		;

		create index idx_tmp_1d_predicted_cgm on tmp_1d_predicted_cgm(clientid asc, eventdate asc, predicted_1d_cgm)
		;

		update bi_patient_measures a inner join tmp_1d_predicted_cgm b on a.clientid = b.clientid and a.measure_event_date = b.eventdate
		set a.predicted_1d_cgm = b.predicted_1d_cgm
        ;

		drop temporary table if exists tmp_1d_predicted_cgm;


 -- maintenance of starRating 28d calculations
			drop temporary table if exists tmp_starRating_28d;
		  
			create temporary table tmp_starRating_28d
			select a.clientid, a.measure_event_date, 
			cast(avg(b.starRating_1d) as decimal(5,2)) as starRating_28d,
			count(distinct Case when b.starRating_1d is not null then b.measure_event_date else null end) as starRating_28d_cnt
			from bi_patient_measures a inner join bi_patient_measures b on a.clientid = b.clientid and b.measure_event_date between date_sub(a.measure_event_date, interval 27 day) and a.measure_event_date
			where a.measure_event_date >= date_sub(date(now()), interval 50 day)
			group by a.clientid, a.measure_event_date
			;
			
			create index idx_tmp_starRating_28d on tmp_starRating_28d(clientid asc, measure_event_date asc);


			update bi_patient_measures a inner join tmp_starRating_28d b on 
			(a.clientid = b.clientid and a.measure_event_date = b.measure_event_date)
			set a.starRating_28d = b.starRating_28d,
				a.starRating_28d_cnt=b.starRating_28d_cnt
			where a.measure_event_date >=date_sub(date(now()), interval 10 day)
			;

			drop temporary table if exists tmp_starRating_28d;
	
-- maintenance of Medication card completion detail

				drop temporary table if exists tmp_medication_cards;

				create temporary table tmp_medication_cards as
				select clientid, date(scheduledLocalTime) as date, 
				count(timeslot) as Total_cards,  
				count(case when status='FINISHED' then timeslot else null end) as Total_cards_finished,
				cast(count(case when status='FINISHED' then timeslot else null end)/count(timeslot) as decimal(5,2)) as Total_cards_completion_per
				from clienttodoitems 
				where category = 'medicine'
				and status not in ('CANCELED')
				and scheduledLocalTime >= date_sub(now(), interval 50 day) and scheduledLocalTime <= now()
				group by clientid, date(scheduledLocalTime)
				;
				
				create index idx_tmp_medication_cards on tmp_medication_cards(clientid asc, date asc);
				
				drop temporary table if exists tmp_medication_cards_1;
				create temporary table tmp_medication_cards_1 as
				select * from tmp_medication_cards ;
				
				create index idx_tmp_medication_cards_1 on tmp_medication_cards_1(clientid asc, date asc);
				
				drop temporary table if exists tmp_medication_cards_28d;
				
				create temporary table tmp_medication_cards_28d as
				select a.clientid, a.date, 
				cast(avg(b.Total_cards_completion_per) as decimal(6,2)) as totalMedicationCards_Completion_28d,
				cast(avg(b.Total_cards) as decimal(6,2)) as totalMedicationCards_28d
				from tmp_medication_cards a inner join tmp_medication_cards_1 b on a.clientid=b.clientid and b.date between date_sub(a.date, interval 27 day) and a.date
				where a.date >=date_sub(date(now()), interval 10 day)
				group by a.clientid, a.date
				;
				
				create index idx_tmp_medication_cards_28d on tmp_medication_cards_28d (clientid asc, date asc); 
				
				
			   
				update bi_patient_measures a inner join tmp_medication_cards_28d b on  (a.clientid = b.clientid and a.measure_event_date = b.date)
				set a.totalMedicationCards_Completion_28d = b.totalMedicationCards_Completion_28d,
					a.totalMedicationCards_28d=b.totalMedicationCards_28d
				;
			   
				
				drop temporary table if exists tmp_medication_cards;
				drop temporary table if exists tmp_medication_cards_1;
				drop temporary table if exists tmp_medication_cards_28d;

			
-- maintain NAFLS_LiverFat Score
		drop temporary table if exists tmp_nafld_maintenace; 

		create temporary table tmp_nafld_maintenace as 
		select clientid, measure_event_date, systolic_5d, diastolic_5d, s.bmi, is_Inreversal, is_metOnly_by_ops, 
		max(latest_value_available_bloodworkDate) as latest_value_available_bloodworkDate,
		max(latest_tg) as latest_tg,
		max(latest_hdl) as latest_hdl,
		max(latest_fasting_glucose) as latest_fasting_glucose,
		max(latest_tg_hdl_ratio) as latest_tg_hdl_ratio,
		max(latest_ast) as latest_ast,
		max(latest_alt) as latest_alt,
		max(latest_insulin) as latest_insulin,
		max(latest_ast_alt_ratio) as latest_ast_alt_ratio
		from
		(
			select s.clientid, s.measure_event_date, systolic_5d, diastolic_5d, s.bmi, is_Inreversal, is_metOnly_by_ops, 
			s1.bloodworkTime as latest_value_available_bloodworkDate,
			triglycerides as latest_tg,
			hdlCholesterol as latest_hdl,
			glucose as latest_fasting_glucose,
			cast(triglycerides/hdlCholesterol as decimal(10,2))  as latest_tg_hdl_ratio,
			ast as latest_ast,
			alt as latest_alt,
			insulin as latest_insulin, 
			cast(ast/alt as decimal(10,2)) as latest_ast_alt_ratio
			from
			(
					-- select a.clientid, measure_event_date, recentLabDate, systolic_5d, diastolic_5d, weight, is_Inreversal,  is_MetOnly_by_OPS, ifnull(cast(weight/(b.start_height * b.start_height) as decimal(10,2)), bmi) as BMI -- take BMI from weight computation if null, check BMI received - to maximize the values
                    
					select a.clientid, measure_event_date,recentLabDate, last_available_LabAst_Alt_date, systolic_5d, diastolic_5d, weight, is_Inreversal, is_metOnly_by_ops, last_available_bmi as BMI 
					from bi_patient_measures a left join (select distinct clientid, start_height from dim_client where is_Row_current = 'y') b on a.clientid = b.clientid 
					where measure_event_date >= date_sub(date(now()), interval 14 day)
			) s inner join clinictestresults s1 on s.clientid = s1.clientid and s.last_available_LabAst_Alt_date = date(ifnull(s1.bloodworktime, s1.eventtime))
		) s
		group by clientid, measure_event_date
		;

		create index idx_tmp_nafld_maintenace on tmp_nafld_maintenace (clientid asc, latest_value_available_bloodworkDate); 

		drop temporary table if exists tmp_nafld_liverfat_score; 

		create temporary table tmp_nafld_liverfat_score as 
		select s.*, if(is_Inreversal = 'Yes' or is_metOnly_by_ops = 'Yes', 0, 2) as latest_diabetes_flag,
		if(BMI is null, null, -2.89 + (1.18 * (if(is_MetabolicSyndrome = 'Y', 1, 0))) + (0.45 * (if(is_Inreversal = 'Yes' or is_metOnly_by_ops = 'Yes', 0, 2))) + (0.15 * latest_insulin) + (0.04 * latest_ast) - (0.94 * latest_ast_alt_ratio)) as NAFLD_LFS
		from 
		(
			select s.*, 
			case when (latest_BMI_flag + latest_TG_flag + latest_HDL_flag + latest_fastingGlucose_flag + latest_BP_flag) >= 3 then 'Y' else 'N' end as is_MetabolicSyndrome
			from 
			(
				select b.clientid, b.measure_event_date, is_InReversal, is_metOnly_by_ops, bmi, latest_insulin, latest_ast, latest_ast_alt_ratio, latest_value_available_bloodworkDate,
				if(b.bmi is not null and b.bmi >= 25, 1, 0) as latest_BMI_flag, 
				if(b.latest_tg is not null and b.latest_tg >= 150, 1, 0) as latest_TG_flag, 
				if(b.latest_hdl is not null and b.latest_hdl < if(gender = 'MALE', 40, 50), 1, 0) as latest_HDL_flag, 
				if(b.latest_fasting_glucose is not null and b.latest_fasting_glucose >= 100 , 1, 0) as latest_fastingGlucose_flag,
				if((b.systolic_5d is not null or b.diastolic_5d is not null) and (ifnull(b.systolic_5d,0) >= 130 or ifnull(b.diastolic_5d,0) >= 85), 1, 0) as latest_BP_flag
				from tmp_nafld_maintenace b  left join v_allClient_list c on b.clientid = c.clientid 
			) s 
		) s
		;

		create index idx_tmp_nafld_liverfat_score on tmp_nafld_liverfat_score (clientid asc, measure_event_date asc, NAFLD_LFS); 


		update bi_patient_measures a inner join tmp_nafld_liverfat_score b on (a.clientid = b.clientid and a.measure_event_date = b.measure_event_date)
		set a.NAFLD_LFS = b.NAFLD_LFS,
			a.is_MetabolicSyndrome = b.is_MetabolicSyndrome
		; 

		drop temporary table if exists tmp_nafld_liverfat_score;

		drop temporary table if exists tmp_nafld_maintenace; 			
			

-- Hearthealth FRS score maintenance

		drop temporary table if exists tmp_heartHealth; 

		create temporary table tmp_heartHealth as 
		select clientid,measure_event_date, is_Inreversal, is_metOnly_by_ops,systolic_5d,
		max(latest_bloodworkDate) as latest_bloodworkDate,
		max(latest_hdl) as latest_hdl,
		max(latest_TotalCholesterol) as latest_TotalCholesterol,
		max(useSmoking) as useSmoking
		from
		(
			select s.clientid, s.measure_event_date, is_Inreversal, is_metOnly_by_ops, systolic_5d,
			s1.bloodworktime as latest_bloodworkDate,
			hdlCholesterol as latest_hdl,
			cholesterol as latest_TotalCholesterol, 
			case when s2.clientid is not null then 'Y' else 'N' end as useSmoking
			from
			(
				select a.clientid, measure_event_date, recentLabDate, systolic_5d, is_Inreversal, is_metOnly_by_ops
				from bi_patient_measures a
			    where measure_event_date >= date_sub(date(now()), interval 14 day)
			) s inner join clinictestresults s1 on s.clientid = s1.clientid and s.recentLabDate = date(ifnull(s1.bloodworktime, s1.eventtime))
				left join 			
					(
						select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork from clinictestresults
						where useSmoking = 1
						and deleted = 0
						group by clientid
					) s2 on s.clientid = s2.clientid 
		) s
		group by clientid, measure_event_date
		;


		create index idx_tmp_heartHealth on tmp_heartHealth(clientid asc, latest_bloodworkDate); 


		set @M_age := 3.06117, @M_TC := 1.12370, @M_HDL := -0.93263, @M_BP_TRT := 1.99881, @M_BP_NonTRT := 1.93303, @M_SMOKE := 0.65451, @M_DIAB := 0.57367; 
		set @F_age := 2.32888, @F_TC := 1.20904, @F_HDL := -0.70833, @F_BP_TRT := 2.82263, @F_BP_NonTRT := 2.76157, @F_SMOKE := 0.52873, @F_DIAB := 0.69154; 

		drop temporary table if exists tmp_FRS;

		create temporary table tmp_FRS as 
		select clientid, measure_event_date, total_lastest_coeff, 
		cast(case when gender = 'MALE' then 1 - power(0.88936, exp(total_lastest_coeff - 23.9802))
				  when gender = 'FEMALE' then 1 - power(0.95012, exp(total_lastest_coeff - 26.1931)) else null end * 100 as decimal(6,2)) as latest_FRS     
		from 
		(
			select clientid, measure_event_date, gender, age, 
			age_coeff + latest_sysBP_coeff + latest_totalCholesterol_coeff + latest_totalHDL_coeff + smoke_coeff + latest_diab_coeff as total_lastest_coeff
			from 
			( 
					select s.clientid, measure_event_date, gender, age,
					log_age * if(gender = 'MALE', @M_age, @F_age) as age_coeff, 
					if(useSmoking = 'Y', 1, 0) * if(gender = 'MALE', @M_SMOKE, @F_SMOKE)  as smoke_coeff, 

					log_latest_sysBP * case when gender = 'MALE' and isHTNTreated = 'Y' then @M_BP_TRT
										   when gender = 'MALE' and isHTNTreated = 'N' then @M_BP_NonTRT
										   when gender = 'FEMALE' and isHTNTreated = 'Y' then @F_BP_TRT
										   when gender = 'FEMALE' and isHTNTreated = 'N' then @F_BP_NonTRT else null end as latest_sysBP_coeff, 
					log_latestTotalCholesterol * if(gender = 'MALE', @M_TC, @F_TC) as latest_totalCholesterol_coeff, 
					log_latest_hdl * if(gender = 'MALE', @M_HDL, @F_HDL) as latest_totalHDL_coeff, 
					if(is_InReversal = 'Yes' or is_metOnly_by_ops= 'Yes', 0, 1) * if(gender = 'MALE', @M_DIAB, @F_DIAB) as latest_diab_coeff
					from 
					(
						select b.clientid, b.measure_event_date, b.latest_TotalCholesterol, b.latest_hdl, is_InReversal, is_metOnly_by_ops, 
						b.useSmoking, c.age, b.systolic_5d, if(dc.start_htn_medicine_drugs is not null, 'Y', 'N') as isHTNTreated, c.gender,
						if(b.latest_TotalCholesterol <= 0, null, ln(b.latest_TotalCholesterol)) as log_latestTotalCholesterol, 
						if(b.latest_hdl <= 0, null, ln(b.latest_hdl)) as log_latest_hdl,
						if(c.age <= 0, null, ln(c.age)) as log_age,
						if(systolic_5d <= 0, null, ln(systolic_5d)) as log_latest_sysBP
						from 
						tmp_heartHealth b left join v_client_tmp c on b.clientid = c.clientid
										  left join dim_client dc on b.clientid = dc.clientid and dc.is_row_current = 'y'
					) s 
			) s 
		) s 
		; 

		create index idx_tmp_FRS on tmp_FRS (clientid asc, measure_event_date asc, latest_FRS); 

		update bi_patient_measures a inner join tmp_FRS b on (a.clientid = b.clientid and a.measure_event_date = b.measure_event_date)
		set a.FRS_HeartHealth = b.latest_FRS
		; 

		drop temporary table if exists tmp_FRS;

		drop temporary table if exists tmp_heartHealth; 	
	

-- Maintain the non diabetic drugs
			drop temporary table if exists tmp_nondiabetic_drugs; 

			create temporary table tmp_nondiabetic_drugs  
			(
			clientid int not null,
			date date not null, 
			HTN_drugs varchar(500) null, 
			CHOL_drugs varchar(500) null,
			HD_drugs varchar(500) null
			); 

			insert into tmp_nondiabetic_drugs (clientid, date, HTN_drugs, CHOL_drugs, HD_drugs) 
			select s1.clientid, s1.date, 
			group_concat(distinct case when conditionName like '%HYPERTENSION%' then epc else null end) as HTN_drugs,   
			group_concat(distinct case when (conditionName like '%CHOLESTEROL%' or conditionName like '%Dyslipidemia%')  then epc else null end) as CHOL_drugs,
			group_concat(distinct case when conditionName like '%HEARTDISEASE%' then epc else null end) as HD_drugs
			from 
			(
						select a.date, b.clientId
						from twins.v_date_to_date a
						inner join
						(
							select clientid, startDate, endDate
							from
							(
								select clientid, max(status_start_date) as startDate, max(Status_end_date) as endDate
								from bi_patient_status
								where status = 'active'
								group by clientid
							) s
						) b on a.date between b.startDate and b.endDate
                        where a.date >= date_sub(date(now()), interval 25 day)
			) s1 left join 
			(
			   select distinct clientid, conditionName, epc, min(startdate) as startdate, max(enddate) as enddate 
				from 
				(
				select distinct P.clientId, pm.medicineid, c.medicineName, pc.epc, pc.conditionName, 
				(date(pm.startLocaldate)) as startdate, (date(pm.endLocaldate)) as enddate 
				from prescriptions P join prescriptionmedicines PM on P.prescriptionid = PM.prescriptionid
									left join medicines c on pm.medicineid = c.medicineid
									left join medicinepharmclasses mpc on c.medicineID = mpc.medicineId 
									left join pharmclasses pc on mpc.pharmClassId = pc.pharmClassId 
									inner join v_client_tmp ac on p.clientid = ac.clientid
				where 
				(pm.startLocaldate is not null and pm.endLocalDate is not null)
				) s 
				group by clientid, conditionName, epc
			) s2 on s1.clientid = s2.clientid and (s1.date between s2.startdate and s2.enddate)
			group by s1.clientid, s1.date
		;

			create index idx_tmp_nondiabetic_drugs on tmp_nondiabetic_drugs (clientid asc, date asc, HTN_drugs, CHOL_drugs, HD_drugs); 
            

			update bi_patient_measures a inner join tmp_nondiabetic_drugs b on (a.clientid = b.clientid and a.measure_event_date = b.date) 
			set 
			a.nonDiabetic_medicine_drugs_HTN = b.HTN_drugs, 
			a.nonDiabetic_medicine_drugs_CHOL = b.CHOL_drugs,
			a.nonDiabetic_medicine_drugs_HD = b.HD_drugs
			;

			drop temporary table if exists tmp_nondiabetic_drugs; 	
	
-- due to multiple changes happening to the patient's profile status, the treatmentdays keeps getting reset. 
-- so doing a clean up of those old dates for each patient. 
drop temporary table if exists tmp_records_to_remove;

create temporary table tmp_records_to_remove as 
select clientid, treatmentdays, max(measure_event_date) as latest_start_date
from bi_patient_measures 
where treatmentdays = 0
group by clientid, treatmentdays
;

delete bi_patient_measures
from bi_patient_measures inner join tmp_records_to_remove on bi_patient_measures.clientid = tmp_records_to_remove.clientid and bi_patient_measures.measure_event_date < tmp_records_to_remove.latest_start_date
;

drop temporary table if exists tmp_records_to_remove;

-- update end time
update dailyprocesslog 
set updatedate = now()
where processname = 'sp_load_bi_patient_measures'
;
END