CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_bi_patient_monitor_measures`(  )
BEGIN



update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_patient_monitor_measures'
;

drop temporary table if exists tmp_bi_patient_monitor_measures; 

create temporary table tmp_bi_patient_monitor_measures
(
clientid int(11) null, 
measure_event_date date null, 
measure_source varchar(25) null, 
measure_name varchar(25) null, 
measure varchar(500) null
);


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'Medicine' as measure_source, 'medicine' as measure_name, measurevalue as measure
from 
(
        select distinct d.date, c.clientId, c.enrollmentDateITZ
		from twins.v_date_to_date d
		inner join bi_patient_status c 
        where d.date between c.status_start_date and c.status_end_date and c.status = 'active'
        and d.date >= date_sub(date(itz(now())), interval 6 day)
) cd left join
(    
select clientid, eventdate, group_concat(medicine) as measurevalue
from 
(
	
	select clientid, eventdate, group_concat(concat(' ', medicine, '-', qty,unit,'x',amt) order by medicine) as medicine
    
    from 
    (
		select a.clientid, eventdate, 
			case  when b.medicinename = 'GLICLAZIDE' then 'Gc'
				  when b.medicinename = 'GLIMEPIRIDE' then 'Gm'
				  when b.medicinename = 'JANUVIA' then 'Jv'
				  when b.medicinename like 'JANUMET%' then 'Jm'
				  when b.medicinename = 'METFORMIN' then 'M'
				  else b.medicineName end as medicine, floor(targetvalue) as qty, b.unit, b4.epc as med_drug, count(medicineName) as amt
		from (
				select clientid, date(scheduledLocalTime) as eventdate, status, category, type, targetvalue, medicineId
				from clienttodoitems a
				where a.category = 'medicine' and type = 'medicine' and a.status in ('FINISHED','UNFINISHED')
				and date(scheduledLocalTime) >= date(date_sub(itz(now()), interval 7 day)) and date(scheduledLocalTime) <= date(itz(now()))
			) a left join medicines b on a.medicineid = b.medicineid 
							   
                               
                               left join medicinepharmclasses b3 on b.medicineid = b3.medicineid
                               left join pharmclasses b4 on b3.pharmClassId = b4.pharmClassId
		group by clientid, eventdate, medicinename
    ) s group by clientid, eventdate

	union all 

	
    select clientId, eventdate, group_concat(concat(' ',med,'-', unit, 'mgx', amt) order by med) as medicine
	from 
	(
 			select a.clientId, eventdate, 
			case when ifnull(b.medicinename, type) = 'GLIMEPIRIDE' then 'Gm'
				  when ifnull(b.medicinename, type) = 'JANUVIA' then 'Jv'
				  when ifnull(b.medicinename, type) like 'JANUMET%' then 'Jm'
				  when ifnull(b.medicinename, type) = 'METFORMIN' then 'M'
				  else ifnull(b.medicinename, type) end as med, if(targetValue < 1, left(targetValue,3),cast(floor(targetValue) as char(5)))  as unit,   count(type)  as amt  
			from (
						select clientid, date(scheduledLocalTime) as eventdate, status, category, type, targetvalue, medicineId
						from clienttodoitems a
						where a.category = 'medicine' and type not in ('medicine') and a.status in ('FINISHED','UNFINISHED')
						and date(scheduledLocalTime) >= date(date_sub(itz(now()), interval 7 day)) and date(scheduledLocalTime) <= date(itz(now()))
				 ) a left join medicines b on a.medicineid = b.medicineid 
								   left join medicinepharmclasses b4 on b.medicineId = b4.medicineID
								   left join pharmclasses b5 on b4.pharmClassId = b5.pharmClassId
			group by clientId, eventdate, type , targetValue
		) s  
	group by clientId, eventdate
) s 
group by clientid, eventdate
) s on cd.clientid = s.clientid and cd.date = s.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'Medicine' as measure_source, 'medicine_drug' as measure_name, 
ltrim(rtrim(measurevalue)) as measure
from 
(
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		cross join bi_patient_status c 
        where d.date between c.status_start_date and c.status_end_date and c.status = 'active'
        and d.date >= date_sub(date(itz(now())), interval 6 day)
) cd left join
(   
select clientid, eventdate, group_concat(distinct med_drug) as measurevalue
from 
(
	
	select clientid, eventdate, group_concat(distinct med_drug) as med_drug
    from 
    (
		select a.clientid, eventdate, 
			case when b.medicinename = 'GLICLAZIDE' then 'Gc'
				  when b.medicinename = 'GLIMEPIRIDE' then 'Gm'
				  when b.medicinename = 'JANUVIA' then 'Jv'
				  when b.medicinename like 'JANUMET%' then 'Jm'
				  when b.medicinename = 'METFORMIN' then 'M'
				  else b.medicineName end as medicine, group_concat(distinct b5.epc,' ','(',b5.category ,')') as med_drug
		from  (
				select clientid, date(scheduledLocalTime) as eventdate, status, category, type, targetvalue, medicineId
				from clienttodoitems a
				where a.category = 'medicine' and type = 'medicine' and a.status in ('FINISHED','UNFINISHED')
				and date(scheduledLocalTime) >= date(date_sub(itz(now()), interval 7 day)) and date(scheduledLocalTime) <= date(itz(now()))
				) a  left join medicines b on a.medicineid = b.medicineid 
							   
                               
                               
                               left join medicinepharmclasses b4 on b.medicineId = b4.medicineID
                               left join pharmclasses b5 on b4.pharmClassId = b5.pharmClassId
		group by clientid, eventdate, medicinename
    ) s group by clientid, eventdate

	union all 

	
    select clientId, eventdate, group_concat(distinct med_drug) as med_drug
	from 
	(
			select a.clientId, eventdate, 
			case when ifnull(b.medicinename, type) = 'GLIMEPIRIDE' then 'Gm'
				  when ifnull(b.medicinename, type) = 'JANUVIA' then 'Jv'
				  when ifnull(b.medicinename, type) like 'JANUMET%' then 'Jm'
				  when ifnull(b.medicinename, type) = 'METFORMIN' then 'M'
				  else ifnull(b.medicinename, type) end as med,  ifnull(group_concat(distinct b5.epc,' ','(',b5.category ,')'), type) as med_drug
			from  (
				select clientid, date(scheduledLocalTime) as eventdate, status, category, type, targetvalue, medicineId
				from clienttodoitems a
				where a.category = 'medicine' and type not in ('medicine') and a.status in ('FINISHED','UNFINISHED')
				and date(scheduledLocalTime) >= date(date_sub(itz(now()), interval 7 day)) and date(scheduledLocalTime) <= date(itz(now()))
				) a  left join medicines b on a.medicineid = b.medicineid 
								   left join medicinepharmclasses b4 on b.medicineId = b4.medicineID
								   left join pharmclasses b5 on b4.pharmClassId = b5.pharmClassId
			group by clientId, eventdate, type
		) s  
	group by clientId, eventdate
) s 
group by clientid, eventdate
) s on cd.clientid = s.clientid and cd.date = s.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)  
select a.clientid, a.date as measure_event_date, 'Calllog' as measure_source, 'MED ADH' as measureName, b.value as measureValue
from 
(    
        select date, clientid
        from v_date_to_date a left join bi_patient_status b on ((a.date between b.status_start_date and b.status_end_date) and b.status = 'active')
        where date >= '2019-06-10' 
        and date >= date_sub(date(itz(now())), interval 7 day)
) a left join
(
   select a.clientid, a.calllogfieldid, report_date, value
   from
    (
        select a.clientid, a.calllogfieldid, c.calllogid, date(itz(c.eventtime)) as report_date, max(c.calllogid) as recentCallLogID 
        from calllogdetails a, calllogfields b, calllogs c
        where a.calllogfieldid = b.calllogfieldid
        and a.calllogid = c.calllogid
        and b.label = 'Ensure MED ADH'
        and c.eventtime >= date_sub(now(), interval 9 day)
        group by a.clientid, a.calllogfieldid, date(itz(c.eventtime))
    ) a, calllogdetails b
    where a.clientid = b.clientid and a.calllogfieldid = b.calllogfieldid and a.recentCallLogID = b.calllogid
) b on a.clientid = b.clientid and a.date = b.report_date
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)  
select a.clientid, a.date as measure_event_date, 'Calllog' as measure_source, 'NUT COM' as measure_name, b.value as measureValue
from 
(    
        select date, clientid
        from v_date_to_date a left join bi_patient_status b on ((a.date between b.status_start_date and b.status_end_date) and b.status = 'active')
        where date >= '2019-06-10' 
		and date >= date_sub(date(itz(now())), interval 7 day)
) a left join
(
   select a.clientid, a.calllogfieldid, report_date, value
   from
    (
        select a.clientid, a.calllogfieldid, c.calllogid, date(itz(c.eventtime)) as report_date, max(c.calllogid) as recentCallLogID 
        from calllogdetails a, calllogfields b, calllogs c
        where a.calllogfieldid = b.calllogfieldid
        and a.calllogid = c.calllogid
        and b.label = 'Ensure NUT SAT'
		and c.eventtime >= date_sub(now(), interval 9 day)
        group by a.clientid, a.calllogfieldid, date(itz(c.eventtime))
    ) a, calllogdetails b
    where a.clientid = b.clientid and a.calllogfieldid = b.calllogfieldid and a.recentCallLogID = b.calllogid
) b on a.clientid = b.clientid and a.date = b.report_date
;







insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select a.clientid, a.date as measure_event_date, 'Cohort' as measure_source, 'Cohort' as measure_name, ifnull(c.name,'Default') as measure
from 
(
    select cd.clientid, cd.date, max(itz(starttime)) as change_timeITZ
	from 
    (	
		select d.date, c.clientId
		from twins.v_date_to_date d
		left join bi_patient_status c on d.date >= c.status_Start_Date and d.date <= c.status_end_date and c.status = 'active'
        where date >=  date_sub(date(itz(now())), interval 35 day)
	) cd 
	left join (select a.*, b.name from cohorthistories a inner join medicalconditions b on a.medicalConditionID = b.medicalConditionId where b.name = 'Diabetes') a on cd.clientid = a.clientid and date(itz(Starttime)) <= cd.date
	group by cd.clientid, cd.date
    ) a left join (select a.*, b.name from cohorthistories a inner join medicalconditions b on a.medicalConditionID = b.medicalConditionId where b.name = 'Diabetes') b on a.clientid = b.clientid and a.change_timeITZ = itz(b.starttime)
        left join cohorts c on b.cohortid = c.cohortid
;


set time_zone = 'Asia/Calcutta'; 
drop temporary table if exists tmp_sleep_details_measure;

create temporary table tmp_sleep_details_measure 
as 
    select a.clientid, a.eventtime as sleepStartTime, 
    date_add(a.eventtime, interval duration/1000 second) as sleepEndTime, 
    date(date_add(a.eventtime, interval duration/1000 second)) as eventdate, 
	sum(case when level = 'deep' then (durationSeconds/60) end) as deepMinutes, 
	sum(case when level = 'rem' then (durationSeconds/60) end) as remMinutes, 
	sum(case when level = 'light' then (durationSeconds/60) end) as lightMinutes, 
	sum(case when level in ('wake','awake') then (durationSeconds/60) end) as awakeMinutes,
	sum(case when level = 'asleep' then (durationSeconds/60) end) as asleepMinutes,
	sum(case when level not in ('deep','rem','light','awake','wake','asleep') then (durationSeconds/60) end) as otherLevelMinutes, 
	a.duration/1000/60 as totalMinutes
	from 
	sleeps a left join sleepdetails b on a.id = b.sleepId 
			 
	where date(a.eventtime) >= date_sub(date(now()), interval 9 day)
	group by a.clientid, date(date_add(a.eventtime, interval duration/1000 second))
    ;
    
    create index idx_tmp_sleep_details on tmp_sleep_details_measure (clientid asc, eventdate asc, totalminutes, awakeMinutes, remMinutes, deepMinutes, lightMinutes, otherLevelMinutes)
;

set time_zone = 'UTC';






insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select a.clientid, a.date as measure_event_date, 'System' as measure_source, 'NUT ADH' as measureType, b.nut_adh_syntax as measureValue
from 
(    
        select date, clientid
        from v_date_to_date a left join bi_patient_status b on ((a.date between b.status_start_date and b.status_end_date) and b.status = 'active')
        where date >= '2020-10-01' and date >= date_sub(date(itz(now())), interval 10 day) 
) a left join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and a.date = b.date 
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'Sleep' as measure_source, 'TotalMinutes' as measure_name, 
totalMinutes as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
        where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_sleep_details_measure b on cd.clientid = b.clientid and cd.date = b.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'Sleep' as measure_source, 'DeepMinutes' as measure_name, 
deepMinutes as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_sleep_details_measure b on cd.clientid = b.clientid and cd.date = b.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'Sleep' as measure_source, 'awakeMinutes' as measure_name, 
awakeMinutes as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_sleep_details_measure b on cd.clientid = b.clientid and cd.date = b.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'Sleep' as measure_source, 'lightMinutes' as measure_name, 
lightMinutes as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_sleep_details_measure b on cd.clientid = b.clientid and cd.date = b.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'Sleep' as measure_source, 'RemMinutes' as measure_name, 
RemMinutes as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_sleep_details_measure b on cd.clientid = b.clientid and cd.date = b.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'Sleep' as measure_source, 'otherLevelMinutes' as measure_name, 
otherLevelMinutes as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_sleep_details_measure b on cd.clientid = b.clientid and cd.date = b.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'Sleep' as measure_source, 'asleepMinutes' as measure_name, 
asleepMinutes as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_sleep_details_measure b on cd.clientid = b.clientid and cd.date = b.eventdate
;



drop temporary table if exists tmp_bcm_measures; 

create temporary table tmp_bcm_measures as 
select a.clientid, date(itz(eventtime)) as eventdate, cast(avg(fat) as decimal(8,2)) as fat, cast(avg(bmr) as decimal(8,2)) as bmr, cast(avg(bodywaterrate) as decimal(8,2)) as bodywaterrate,
cast(avg(bonemass) as decimal(8,2)) as bonemass, cast(avg(leanbodyweight) as decimal(8,2)) as leanbodyweight, cast(avg(metabolicage) as decimal(8,2)) as metabolicage, cast(avg(musclemass) as decimal(8,2)) as musclemass, 
cast(avg(musclerate) as decimal(8,2)) as musclerate, cast(avg(protein) as decimal(8,2)) as protein, cast(avg(subcutaneousfat) as decimal(8,2)) as subcutaneousfat, cast(avg(visceralFat) as decimal(8,2)) as visceralFat,
cast(avg(weight) as decimal(8,2)) as weight, cast(avg(bmi) as decimal(8,2)) as bmi
from weights a inner join v_client_tmp b on a.clientid = b.clientid 
where date(eventtime) >= date_sub(date(now()), interval 10 day)
group by a.clientid, date(itz(eventtime))
;

create index idx_tmp_bcm_measures on tmp_bcm_measures (clientid asc, eventdate asc, fat, bmr, bodywaterrate, 
bonemass, leanbodyweight, metabolicage, musclemass, musclerate, protein, subcutaneousfat, visceralfat, weight, bmi); 


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'BCM' as measure_source, 'bodyFat' as measure_name, 
fat as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_bcm_measures b on cd.clientid = b.clientid and cd.date = b.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'BCM' as measure_source, 'BMR' as measure_name, 
bmr as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_bcm_measures b on cd.clientid = b.clientid and cd.date = b.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'BCM' as measure_source, 'bodywaterrate' as measure_name, 
bodywaterrate as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_bcm_measures b on cd.clientid = b.clientid and cd.date = b.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'BCM' as measure_source, 'bonemass' as measure_name, 
bonemass as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_bcm_measures b on cd.clientid = b.clientid and cd.date = b.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'BCM' as measure_source, 'leanbodyweight' as measure_name, 
leanbodyweight as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_bcm_measures b on cd.clientid = b.clientid and cd.date = b.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'BCM' as measure_source, 'metabolicage' as measure_name, 
metabolicage as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_bcm_measures b on cd.clientid = b.clientid and cd.date = b.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'BCM' as measure_source, 'musclemass' as measure_name, 
musclemass as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_bcm_measures b on cd.clientid = b.clientid and cd.date = b.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'BCM' as measure_source, 'protein' as measure_name, 
protein as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_bcm_measures b on cd.clientid = b.clientid and cd.date = b.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'BCM' as measure_source, 'subcutaneousfat' as measure_name, 
subcutaneousfat as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_bcm_measures b on cd.clientid = b.clientid and cd.date = b.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'BCM' as measure_source, 'visceralFat' as measure_name, 
visceralFat as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_bcm_measures b on cd.clientid = b.clientid and cd.date = b.eventdate
;


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'BCM' as measure_source, 'BMI' as measure_name, 
bmi as measure
from 
    (
        select distinct d.date, c.clientId
		from twins.v_date_to_date d
		join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status = 'active'
		where date >= date_sub(date(itz(now())), interval 7 day)
) cd left join tmp_bcm_measures b on cd.clientid = b.clientid and cd.date = b.eventdate
;



	
	drop temporary table if exists tmp_all_MH_symptoms; 

	create temporary table tmp_all_MH_symptoms as 
		select a.clientid, date(ifnull(a.eventLocalTime, itz(a.eventTime))) as eventDate, b.type
		from selfreportanswers a inner join selfreportquestions b on a.selfreportquestionId = b.id
		where b.answerType = 'HML'
		and a.value > 1 
		and date(ifnull(a.eventLocalTime, itz(a.eventTime))) >= date_sub(date(now()), interval 12 day)
	;

	create index idx_tmp_all_MH_symptoms on tmp_all_MH_symptoms(clientid asc, eventdate asc, type); 


insert into tmp_bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select cd.clientid, cd.date as measure_event_date, 'Selfreport' as measure_source, 'Symptoms-M/H' as measure_name, count(distinct type) as average_measure 
from 
	(
		select d.date, c.clientId 
		from twins.ddate d
		inner join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status= 'active'
	) cd left join tmp_all_MH_symptoms b on (cd.clientid = b.clientid and cd.date = b.eventdate)
where cd.date >= date_sub(date(itz(now())), interval 10 day)
group by clientid, cd.date 
;

drop temporary table if exists tmp_all_MH_symptoms; 



create index test_idx_monitor_measures on tmp_bi_patient_monitor_measures (clientid asc, measure_event_date asc, measure_name asc);

insert into bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select clientid, measure_event_date, measure_source, measure_name, measure
from tmp_bi_patient_monitor_measures a
where not exists 
(select 1 from bi_patient_monitor_measures b 
where a.clientid = b.clientid and a.measure_event_date = b.measure_event_date and a.measure_name = b.measure_name)
;

update bi_patient_monitor_measures a inner join 
tmp_bi_patient_monitor_measures b on (a.clientid = b.clientid and a.measure_event_date = b.measure_event_date and a.measure_name = b.measure_name)
set a.measure = b.measure
;


drop temporary table tmp_bi_patient_monitor_measures;
drop temporary table tmp_sleep_details_measure;
drop temporary table tmp_bcm_measures; 



drop temporary table if exists tmp_bi_patient_monitor_measures_5d; 

create temporary table tmp_bi_patient_monitor_measures_5d
(
clientid int(11) null, 
measure_event_date date null, 
measure_source varchar(25) null, 
measure_name varchar(25) null, 
measure varchar(500) null
);


insert into tmp_bi_patient_monitor_measures_5d
select s1.clientid, s1.date, 'System' as measure_source, 'NUT_ADH_5d' as measure_name, cast(avg(s2.nut_adh) as decimal(5,2)) as nut_adh_5d															
from 															
					(															
						select s1.clientid, date, case when s2.measure = 'yes' then 1 when s2.measure = 'no' then 0 end as nut_adh														
						from 														
						(														
								select clientid, date 												
								from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active' 												
								where date >= date_sub(date(itz(now())), interval 14 day)												
						) s1 left join bi_patient_monitor_measures s2 on s1.clientid = s2.clientid and s1.date = s2.measure_Event_date and s2.measure_name = 'nut adh'														
					) s1 inner join 															
					(															
						select s1.clientid, date, case when s2.measure = 'yes' then 1 when s2.measure = 'no' then 0 end as nut_adh														
						from 														
						(														
								select clientid, date 												
								from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active' 												
								where date >= date_sub(date(itz(now())), interval 20 day)												
						) s1 left join bi_patient_monitor_measures s2 on s1.clientid = s2.clientid and s1.date = s2.measure_Event_date and s2.measure_name = 'nut adh'														
					) s2 on s1.clientid = s2.clientid and s2.date between date_sub(s1.date, interval 4 day) and s1.date															
where s1.date >= date_sub(date(itz(now())), interval 12 day)
group by s1.clientid, s1.date			
;

create index test_idx_monitor_measures_5d on tmp_bi_patient_monitor_measures_5d (clientid asc, measure_event_date asc, measure_name asc);

insert into bi_patient_monitor_measures(clientid, measure_event_date, measure_source, measure_name, measure)
select clientid, measure_event_date, measure_source, measure_name, measure
from tmp_bi_patient_monitor_measures_5d a
where not exists 
(select 1 from bi_patient_monitor_measures b 
where a.clientid = b.clientid and a.measure_event_date = b.measure_event_date and a.measure_name = b.measure_name)
;

update bi_patient_monitor_measures a inner join 
tmp_bi_patient_monitor_measures_5d b on (a.clientid = b.clientid and a.measure_event_date = b.measure_event_date and a.measure_name = b.measure_name)
set a.measure = b.measure
;

drop temporary table tmp_bi_patient_monitor_measures_5d
;




drop temporary table if exists tmp_selfreport_nut_sat1; 

create temporary table tmp_selfreport_nut_sat1 as 
select clientid, date(itz(eventtime)) as eventdate, value  from selfreportanswers where selfReportQuestionId = 101
and date(eventtime) >= date_sub(date(now()), interval 20 day) 
;
  
create index idx_tmp_selfreport_nut_sat1 on tmp_selfreport_nut_sat1 (clientid asc, eventdate asc, value);

drop temporary table if exists tmp_selfreport_nut_sat2; 
create temporary table tmp_selfreport_nut_sat2 as 
select clientid, date(itz(eventtime)) as eventdate, value  from selfreportanswers where selfReportQuestionId = 101
and date(eventtime) >= date_sub(date(now()), interval 24 day) 
;

create index idx_tmp_selfreport_nut_sat2 on tmp_selfreport_nut_sat2 (clientid asc, eventdate asc, value);

drop temporary table if exists tmp_nut_sat_5d; 

create temporary table tmp_nut_sat_5d as 
select s1.clientid, s1.date as measure_event_date, 'SelfReport' as measure_source, 'nut_sat_5d' as measure_name, cast(avg(s2.value) as decimal(10,2)) as nut_sat_5d
	from 
	(
		select s1.clientid, s1.date, avg(value) as value from 
		(
			select clientid, date
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status ='active' 
			where date >= date_sub(date(now()), interval 10 day) 
		) s1 left join 
		tmp_selfreport_nut_sat1 s2
		on s1.clientid = s2.clientid and s2.eventdate = s1.date 
		group by s1.clientid, s1.date
	) s1 left join  
	(
		select s1.clientid, s1.date, avg(value) as value from 
		(
			select clientid, date
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status ='active' 
			where date >= date_sub(date(now()), interval 15 day) 
		) s1 left join tmp_selfreport_nut_sat2 s2
		on s1.clientid = s2.clientid and s2.eventdate = s1.date 
		group by s1.clientid, s1.date
	) s2 on s1.clientid = s2.clientid and s2.date between date_sub(s1.date, interval 4 day) and s1.date 
	group by s1.clientid, s1.date;
    
create index idx_tmp_nut_sat_5d on tmp_nut_sat_5d(clientid asc, measure_event_date asc, measure_name, nut_sat_5d); 

insert into bi_patient_monitor_measures (clientid, measure_event_date, measure_source, measure_name, measure)
select a.clientid, a.measure_event_date, a.measure_source, a.measure_name, nut_sat_5d
from tmp_nut_sat_5d a
where not exists (select 1 from bi_patient_monitor_measures b where a.clientid = b.clientid and a.measure_event_date = b.measure_event_date and a.measure_name = b.measure_name)
;

update bi_patient_monitor_measures a inner join tmp_nut_sat_5d b on (a.clientid = b.clientid and a.measure_event_date = b.measure_event_date and a.measure_name = b.measure_name)
set a.measure = b.nut_sat_5d
;

drop temporary table if exists tmp_selfreport_nut_sat1; 
drop temporary table if exists tmp_selfreport_nut_sat2;
drop temporary table if exists tmp_nut_sat_5d;



update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_bi_patient_monitor_measures'
;

END