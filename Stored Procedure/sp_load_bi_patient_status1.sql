CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_bi_patient_status1`()
BEGIN
/*   
		2019-10-18 - Aravindan - Added logic for maintaining visitScheduledDate.      
		2020-04-08 - Aravindan - replaced v_client_tmp by v_allClient_list to bring in the PROSPECT status into the table.      
        2020-06-24 - Aravindan - Modified the process not to be Truncate and load. This helps in lot of lock issues getting resolved.  
        2020-07-14 - Aravindan - Included pha maintenance. 
        2021-06-08 - Aravindan - Brought in the changes made in India (like extending days after patient is dischaged/inactive and clean up of records if patient has multiple rows for the same status with same start date, etc).
*/  
            
/* update process log */ 
 update dailyProcessLog 
 set startDate = now() 
 where processName = 'sp_load_bi_patient_status' ;

 drop temporary table if exists temp_bi_patient_status;  
 
 create temporary table temp_bi_patient_status 
 ( 
 clientid int not null, 
 doctor_name varchar(50) null, 
 doctorName_short varchar(30) null, 
 coachName varchar(50) null, 
 coachName_short varchar(30) null, 
 pha varchar(50) null,
 enrollmentdate datetime null, 
 visitdate datetime null, 
 homeVisitDate datetime null, 
 treatmentSetupDate datetime null, 
 status varchar(15) null, 
 status_start_date date null, 
 status_end_date date null 
 )  
 ;  
 
 insert into temp_bi_patient_status(clientid, doctor_name, enrollmentdate, status, status_start_date, status_end_date)  
 
 select distinct s.clientid, doctorname, enrollmentdate, s.status, status_start_date, status_end_date 
 from  
 ( 
 select s1.clientid, s1.status, date(s1.laststatuschange) as status_start_date,  -- date(if((ifnull(date_sub(s2.laststatuschange, interval 1 day), now()) < s1.laststatuschange) or (s1.status not in ('active','pending_active') and s2.laststatuschange is null), s1.laststatuschange, ifnull(date_sub(s2.laststatuschange, interval 1 day), now()))) as status_end_date 
  date(	
		if((ifnull(date_sub(s2.laststatuschange, interval 1 day), now()) < s1.laststatuschange) -- or (s1.status not in ('active','pending_active') and s2.laststatuschange is null)
		, s1.laststatuschange, 
		ifnull(date_sub(s2.laststatuschange, interval 1 day), now()))
     ) as status_end_date 
 from  
 (   
		select @row_num := case when @id = id then @row_num + 1 else 1 end as row_num,  
		@id := id as clientid, laststatuschange, status  
		from   
		(   
			select distinct id, laststatuschange, status 
			from    
			(    
				select distinct id, laststatuschange as laststatuschange, status from clients_aud    
			) s    
			order by id, laststatuschange     
		) s1   cross join (select @row_num := 0, @id := 0 ) s2  
 ) s1 
left join  
 (   
	select @s_num := case when @id = id then @s_num + 1 else 1 end as row_num,  
	@id := id as clientid, laststatuschange, status  
	from   
	(   
			select distinct id, laststatuschange, status 
			from    
			(    
				select distinct id, laststatuschange as laststatuschange, status from clients_aud     
			) s   
			order by id, laststatuschange     
	) s1   cross join (select @s_num := 0, @id := 0 ) s2  
 ) s2  on s1.clientid = s2.clientid and s2.row_num = s1.row_num + 1 ) 
 s inner join v_allClient_list c on s.clientid = c.clientid -- using the v_allclient_list view here only because it has the PROSPECT patients to bring them into BI_PATIENT_STATUS 
 order by clientid, status_start_date 
;  

create index idx_tmp_patient_status on temp_bi_patient_status (clientid asc);   

/*maintain short names of coach, doctor, patient*/ 

update temp_bi_patient_status a inner join v_allClient_list b  on a.clientid = b.clientid 
set a.doctorName_short = b.doctornameShort,     
	a.coachName = b.coachName,     
    a.coachName_short = b.coachNameShort,
	a.pha = b.pha,
    a.enrollmentdate = b.enrollmentdate;  
    
/*maintain the home visitdate in the status tracker table*/ 
update temp_bi_patient_status a inner join  
(select distinct clientid, max(convert_tz(visitdate, @@time_zone, timezoneId)) as homeVisitDate from homevisits a inner join clients b on a.clientid = b.id group by clientid) b  
on a.clientid = b.clientid  
set a.homeVisitDate = b.homeVisitDate ;  


/*maintain the visitdate in status tracker table*/ 
update temp_bi_patient_status a inner join  
(select distinct clientid, max(cast(startdate as datetime)) as visitdate from MomentumWeekDetails group by clientid) b  
on a.clientid = b.clientid  
set a.visitdate = b.visitdate;  

drop temporary table if exists tmp_multiple_status_records;  

-- Delete the status records that have the same start date (due to multiple records in clients_aud table) 
create temporary table tmp_multiple_status_records 
select clientid, status, count(*) as cnt 
from temp_bi_patient_status where status in ('active', 'pending_active','prospect','registration' ,'discharged', 'inactive', 'on_hold') 
group by clientid, status 
having count(*) > 1;  

drop temporary table if exists tmp_recs_to_be_deleted_patient_status;  

create temporary table tmp_recs_to_be_deleted_patient_status as  
select a.clientid, a.status, b.status_start_date, b.status_end_date 
from  tmp_multiple_status_records a inner join temp_bi_patient_status b 
on a.clientid = b.clientid and a.status = b.status and b.status_start_date = b.status_end_Date -- (to see if the status start and end date are exactly same and there is one more record already with the correct end date).   
;  

delete temp_bi_patient_status from temp_bi_patient_status inner join tmp_recs_to_be_deleted_patient_status 
on  (temp_bi_patient_status.clientid = tmp_recs_to_be_deleted_patient_status.clientid  
		and temp_bi_patient_status.status = tmp_recs_to_be_deleted_patient_status.status  
        and temp_bi_patient_status.status_start_date = tmp_recs_to_be_deleted_patient_status.status_start_date  
        and temp_bi_patient_status.status_end_date = tmp_recs_to_be_deleted_patient_status.status_end_date) 
;  

drop temporary table if exists tmp_multiple_status_records; 
drop temporary table if exists tmp_recs_to_be_deleted_patient_status;  

		-- now delete patient status record which might have the same start date but different end date (looks like a bug in product when profile is moved back to a state through some manual work, the lastStatuschange column gets corrupted).
		create temporary table tmp_multiple_status_records
		select clientid, status, count(*) as cnt 
		from temp_bi_patient_status where status in ('active', 'pending_active','prospect','registration' ,'discharged', 'inactive', 'on_hold') 
		group by clientid, status 
		having count(*) > 1
		;  

		drop temporary table if exists tmp_recs_to_be_checked_patient_list;  

		create temporary table tmp_recs_to_be_checked_patient_list as  
		select a.clientid, a.status, b.status_start_date, b.status_end_date 
		from  tmp_multiple_status_records a inner join temp_bi_patient_status b 
		on a.clientid = b.clientid and a.status = b.status
		;

		drop temporary table if exists tmp_recs_to_be_deleted_patient_status;  

		create temporary table tmp_recs_to_be_deleted_patient_status as 
		select a.clientid, a.status, a.status_start_date, a.status_end_date 
		from  tmp_recs_to_be_checked_patient_list a inner join temp_bi_patient_status b 
		on a.clientid = b.clientid and a.status = b.status and a.status_start_date = b.status_start_date and a.status_end_date < b.status_end_Date -- (to see if the status start is exactly same for 2 records with different enddate).   
		;  

		delete temp_bi_patient_status from temp_bi_patient_status inner join tmp_recs_to_be_deleted_patient_status 
		on  (temp_bi_patient_status.clientid = tmp_recs_to_be_deleted_patient_status.clientid  
				and temp_bi_patient_status.status = tmp_recs_to_be_deleted_patient_status.status  
				and temp_bi_patient_status.status_start_date = tmp_recs_to_be_deleted_patient_status.status_start_date  
				and temp_bi_patient_status.status_end_date = tmp_recs_to_be_deleted_patient_status.status_end_date) 
		;  

		drop temporary table if exists tmp_multiple_status_records; 
		drop temporary table if exists tmp_recs_to_be_checked_patient;  
		drop temporary table if exists tmp_recs_to_be_deleted_patient_status;  
        
-- Lets do insert update 
-- Insert  
insert into bi_patient_status (clientid, doctor_name, doctorName_short, coachName, coachName_short, pha, enrollmentdate, visitdate, 
homeVisitDate, treatmentSetupDate, status, status_start_date, status_end_date) 
select clientid, doctor_name, doctorName_short, coachName, coachName_short, pha, enrollmentdate, visitdate, 
homeVisitDate, treatmentSetupDate, status, status_start_date, status_end_date 
from temp_bi_patient_status a 
where not exists (select 1 from bi_patient_status b where a.clientid = b.clientid and a.status = b.status and a.status_start_date = b.status_start_date)  
; 

update bi_patient_status a inner join temp_bi_patient_status b on (a.clientid = b.clientid and a.status = b.status and a.status_start_date = b.status_start_date) 
 set  a.status_end_date = b.status_end_date  
;


update bi_patient_status a inner join temp_bi_patient_status b on (a.clientid = b.clientid)
 set  a.visitdate = b.visitdate,      
      a.homeVisitDate = b.homeVisitDate,      
      a.treatmentSetupDate = b.treatmentSetupDate,      
      a.enrollmentdate = b.enrollmentdate,
      a.doctor_name = b.doctor_name,      
      a.doctorname_short = b.doctorname_short,      
      a.coachName = b.coachName,      
      a.coachName_short = b.coachName_short,
      a.pha = b.pha;  

/* update process log */ 

update dailyProcessLog 
set updateDate = now() 
where processName = 'sp_load_bi_patient_status' ;
END