CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_bi_rct_patient_measures_x_preactivation`()
begin
	
/*
 * 2021-10-22 - Initial version to load the patient measures including the preactivation dates for RCT members. v_allclients_RCT views control the members data to get loaded into the target table in this procedure.
 * 
 */
	
update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_rct_patient_measures_x_preactivation'
;

drop temporary table if exists tmp_RCT_meaures_1d; 

create temporary table tmp_RCT_meaures_1d (
clientid int not null,
date date not null, 
netCarb_1d decimal(10,2) null,
protein_1d decimal(10,2) null,
fat_1d decimal(10,2) null,
fiber_1d decimal(10,2) null,
calories_1d decimal(10,2) null,
macro decimal(6,2) null,
micro decimal(6,2) null,
biota decimal(6,2) null,
cgm_1d decimal(10,2) null,
cgm_5d decimal(10,2) null,
cgm_14d decimal(10,2) null, 

eA1C_1d decimal(10,2) null,
eA1C_5d decimal(10,2) null,

GMI decimal(6,1) null,
medicine_drugs varchar(1000) null,
medicine varchar(1000) null,

Systolic_BP_1d decimal(10,2) null,
Diastolic_BP_1d decimal(10,2) null,

Ketone_1d decimal(10,2) null,

visceralFat_1d decimal(10,2) null,
bmi_1d decimal(10,2) null,
weight_1d decimal(10,2) null
); 

insert into tmp_RCT_meaures_1d (clientid, date, netcarb_1d, protein_1d, fat_1d, fiber_1d, calories_1d)
select s.clientid, s.date, cast(s1.net_carb as decimal(10,2)) as net_carb, 
	cast(s1.protein as decimal(10,2)) as protein , cast(s1.fat as decimal(10,2)) as fat, cast(s1.fibre as decimal(10,2)) as fibre,
    cast(s1.calories as decimal(10,2)) as calories
from 
(
	select a.date, b.clientid 
	from v_date_to_date a inner join v_allclients_RCT b on a.date >= b.enrollmentdate 
) s left join bi_foodlog_nutrition_target_summary_RCT s1 on s.clientid = s1.clientid and s.date = s1.mealdate and s1.category = '1d-Nutrient'
;

-- medicine_drugs ,medicine 
drop temporary table if exists tmp_medicine_RCT
;

create temporary table tmp_medicine_RCT
(
clientid int not null, 
measure_event_date date not null, 
medicine_drugs varchar(1000) null, 
medicine varchar(1000) null
); 

insert into tmp_medicine_RCT (clientid, measure_event_date, medicine_drugs, medicine)
select cd.clientid, cd.date as measure_event_date, ltrim(rtrim(medicine_drug)) as medicine_drugs, ltrim(rtrim(medicine)) as medicine
from 
(
	select a.date, b.clientid 
	from v_date_to_date a inner join v_allclients_rct b on a.date >= b.enrollmentdate  
	
) cd left join
(   
select clientid, eventdate, group_concat(distinct med_drug) as medicine_drug, group_concat(medicine) as medicine
from 
(
	
	select clientid, eventdate, group_concat(distinct med_drug) as med_drug, group_concat(concat(' ', medicine, '-', qty,unit,'x',amt) order by medicine) as medicine
    from 
    (
		select a.clientid, date(scheduledLocalTime) as eventdate, 
			case when b.medicinename = 'GLICLAZIDE' then 'Gc'
				  when b.medicinename = 'GLIMEPIRIDE' then 'Gm'
				  when b.medicinename = 'JANUVIA' then 'Jv'
				  when b.medicinename like 'JANUMET%' then 'Jm'
				  when b.medicinename = 'METFORMIN' then 'M'
				  else b.medicineName end as medicine, floor(targetvalue) as qty, b.unit, count(medicineName) as amt, group_concat(distinct b5.epc,' ','(',b5.category ,')') as med_drug
		from clienttodoitems a left join medicines b on a.medicineid = b.medicineid 
					           
                               left join medicinepharmclasses b4 on b.medicineId = b4.medicineID
                               left join pharmclasses b5 on b4.pharmClassId = b5.pharmClassId
							   inner join v_allclients_rct c on a.clientid = c.clientid
		where a.category = 'medicine' and a.type='medicine' and a.status in ('FINISHED','UNFINISHED')
        
		group by clientid, date(scheduledLocalTime), medicinename
    ) s group by clientid, eventdate

	union all 

	
    select clientId, eventDate, group_concat(distinct med_drug) as med_drug, group_concat(concat(' ',med,'-', unit, 'mgx', amt) order by med) as medicine
	from 
	(
			select a.clientId, date(scheduledLocalTime) as eventDate, 
			case when ifnull(b.medicinename, type) = 'GLIMEPIRIDE' then 'Gm'
				  when ifnull(b.medicinename, type) = 'JANUVIA' then 'Jv'
				  when ifnull(b.medicinename, type) like 'JANUMET%' then 'Jm'
				  when ifnull(b.medicinename, type) = 'METFORMIN' then 'M'
				  else ifnull(b.medicinename, type) end as med, if(targetValue < 1, left(targetValue,3),cast(floor(targetValue) as char(5)))  as unit,   count(type)  as amt , 
                  ifnull(group_concat(distinct b5.epc,' ','(',b5.category ,')'), type) as med_drug 
			from clienttodoitems a left join medicines b on a.medicineid = b.medicineid 
								   left join medicinepharmclasses b4 on b.medicineId = b4.medicineID
								   left join pharmclasses b5 on b4.pharmClassId = b5.pharmClassId
								   inner join v_allclients_rct c on a.clientid = c.clientid 
			where a.category = 'medicine' and type not in ('medicine') and a.status in ('FINISHED','UNFINISHED')
            
			group by clientId, date(scheduledLocalTime), type
	 ) s  
	group by clientId, eventDate
) s 
group by clientid, eventdate
) s on cd.clientid = s.clientid and cd.date = s.eventdate
;

create index idx_tmp_medicine_RCT on tmp_medicine_RCT (clientid asc, measure_event_date asc, medicine_drugs, medicine); 

update tmp_RCT_meaures_1d a inner join tmp_medicine_RCT b on (a.clientid = b.clientid and a.date = b.measure_event_date)
set a.medicine_drugs = b.medicine_drugs,
	a.medicine = b.medicine
;


drop temporary table if exists tmp_medicine_RCT
;

-- cgm_1d , eA1C_1d
update tmp_RCT_meaures_1d a inner join 
(
		select s.clientid, s.date, 'CGM' as measure_name, '1d' as measure_type, cast(cgm_1d as decimal(10,2)) as cgm_1d
		from 
		(
			select a.date, b.clientid 
			from v_date_to_date a inner join v_allclients_rct b on a.date >= b.enrollmentdate 
			
		) s left join (
							select a.clientid, date(convert_tz(eventtime, 'UTC', b.timezoneId)) as eventdate, avg(value) as cgm_1d
							from stagingglucosevalues a inner join v_allclients_rct b on a.clientid = b.clientid 
					        group by a.clientid, date(convert_tz(eventtime, 'UTC', b.timezoneId))
                      ) s1 on s.clientid = s1.clientid and s.date = s1.eventdate
) b on a.clientid = b.clientid and a.date = b.date 
set a.cgm_1d = b.cgm_1d,
	a.eA1C_1d = cast((b.cgm_1d + 46.7)/28.1 as decimal(10,2))
;

-- Systolic_BP_1d , Diastolic_BP_1d
update tmp_RCT_meaures_1d a inner join 
(
			select s.clientid, s.date, s1.Sys_BP, s1.Dia_BP
			from 
			(
				select a.date, b.clientid 
				from v_date_to_date a inner join v_allclients_rct b on a.date >= b.enrollmentdate
				
			) s left join (
								select a.clientid, date(convert_tz(eventtime, 'UTC', b.timezoneId)) as eventdate, floor(avg(systolic)) as Sys_BP, floor(avg(diastolic)) as Dia_BP
								from bloodpressurevalues a inner join v_allclients_rct b on a.clientid = b.clientid 
								
								group by a.clientid, date(convert_tz(eventtime, 'UTC', b.timezoneId))
						  ) s1 on s.clientid = s1.clientid and s.date = s1.eventdate 
) b on a.clientid = b.clientid and a.date = b.date
set a.Systolic_BP_1d = b.Sys_BP,
	a.Diastolic_BP_1d = b.Dia_BP
; 

-- Ketone_1d
update tmp_RCT_meaures_1d a inner join 
(
		select s.clientid, s.date, s1.Ketone_1d
		from 
		(
			select a.date, b.clientid 
			from v_date_to_date a inner join v_allclients_rct b on a.date >= b.enrollmentdate 
			
		) s left join (
							select a.clientid, date(convert_tz(eventtime, 'UTC', b.timezoneId)) as eventdate, convert(avg(ketone), decimal(10,2)) as Ketone_1d
							from glucoseketones a inner join v_allclients_rct b on a.clientid = b.clientid 
							
                            group by a.clientid, date(convert_tz(eventtime, 'UTC', b.timezoneId))
                      ) s1 on s.clientid = s1.clientid and s.date = s1.eventdate 
) b on a.clientid = b.clientid and a.date = b.date
set a.Ketone_1d = b.Ketone_1d
;

-- visceralFat_1d , bmi_1d, weight_1d
update tmp_RCT_meaures_1d a inner join 
(
		select s.clientid, s.date, s1.visceralFat_1d, s1.bmi_1d, s1.weight_1d
		from 
		(
			select a.date, b.clientid 
			from v_date_to_date a inner join v_allclients_rct b on a.date >= b.enrollmentdate 
			
		) s left join (
							select a.clientid, date(convert_tz(eventtime, 'UTC', b.timezoneId)) as eventdate, convert(avg(visceralFat), decimal(10,2)) as visceralFat_1d, convert(avg(bmi), decimal(10,2)) as bmi_1d,
                            convert(avg(weight), decimal(10,2)) as weight_1d
							from weights a inner join v_allclients_rct b on a.clientid = b.clientid 
							
                            group by a.clientid, date(convert_tz(eventtime, 'UTC', b.timezoneId))
                      ) s1 on s.clientid = s1.clientid and s.date = s1.eventdate 
) b on a.clientid = b.clientid and a.date = b.date
set a.visceralFat_1d = b.visceralFat_1d,
	a.bmi_1d = b.bmi_1d,
    a.weight_1d = b.weight_1d
;

-- cgm_14d , GMI 
drop temporary table if exists tmp_RCT_cgm_1d_1; 
        
		create temporary table tmp_RCT_cgm_1d_1 as 
		select clientid, date, cgm_1d from tmp_RCT_meaures_1d 
		;

		create index idx_tmp_RCT_cgm_1d_1 on tmp_RCT_cgm_1d_1 (clientid asc, date asc, cgm_1d);

		drop temporary table if exists tmp_RCT_cgm_1d_2; 
		create temporary table tmp_RCT_cgm_1d_2 as select * from tmp_RCT_cgm_1d_1;

		create index idx_tmp_RCT_cgm_1d_2 on tmp_RCT_cgm_1d_2 (clientid asc, date asc, cgm_1d);

		drop temporary table if exists tmp_cgm_RCT_14d; 
        
		create temporary table tmp_cgm_RCT_14d as 
		select a.clientid, a.date, cast(avg(b.cgm_1d) as decimal(10,2)) as cgm_14d, 
		cast(3.31 + (0.02392*cast(avg(b.cgm_1d) as decimal(10,2))) as decimal(10,1)) as GMI
		from tmp_RCT_cgm_1d_1 a inner join tmp_RCT_cgm_1d_2 b on a.clientid = b.clientid and b.date between date_sub(a.date, interval 13 day) and a.date
		
		group by a.clientid, a.date
		;

		create index idx_tmp_cgm_RCT_14d on tmp_cgm_RCT_14d (clientid asc, date asc, cgm_14d, GMI); 

		update tmp_RCT_meaures_1d a inner join tmp_cgm_RCT_14d b on (a.clientid = b.clientid and a.date = b.date)
        set a.cgm_14d = b.cgm_14d,
			a.GMI = b.GMI
		; 
        
        drop temporary table if exists tmp_cgm_RCT_14d; 

   -- cgm_5d , eA1C_5d

		drop temporary table if exists tmp_cgm_RCT_5d; 
        
		create temporary table tmp_cgm_RCT_5d as 
		select a.clientid, a.date, cast(avg(b.cgm_1d) as decimal(10,2)) as cgm_5d 
		from tmp_RCT_cgm_1d_1 a inner join tmp_RCT_cgm_1d_2 b on a.clientid = b.clientid and b.date between date_sub(a.date, interval 4 day) and a.date
		group by a.clientid, a.date
		;
        
		create index idx_tmp_cgm_RCT_5d on tmp_cgm_RCT_5d (clientid asc, date asc, cgm_5d); 

		update tmp_RCT_meaures_1d a inner join tmp_cgm_RCT_5d b on (a.clientid = b.clientid and a.date = b.date)
        set a.cgm_5d = b.cgm_5d,
			a.eA1C_5d = cast((b.cgm_5d + 46.7)/28.1 as decimal(10,2))
		; 

		drop temporary table if exists tmp_cgm_RCT_5d; 
		drop temporary table if exists tmp_RCT_cgm_1d_1; 
		drop temporary table if exists tmp_RCT_cgm_1d_2; 
        
        -- Insert/Update calculated measures
        create index idx_tmp_RCT_meaures_1d on tmp_RCT_meaures_1d( clientid asc, date asc, netcarb_1d, fat_1d); 
 -- exec
		insert into bi_rct_patient_measures_x_preactivation (clientid, date, netcarb_1d, protein_1d, fat_1d, fiber_1d, calories_1d, cgm_1d, cgm_5d, cgm_14d, eA1C_1d, eA1C_5d, gmi, medicine_drugs, medicine, Systolic_BP_1d, Diastolic_BP_1d, Ketone_1d, visceralFat_1d, bmi_1d, weight_1d)
		select clientid, date, netcarb_1d, protein_1d, fat_1d, fiber_1d, calories_1d, cgm_1d, cgm_5d, cgm_14d, eA1C_1d, eA1C_5d, gmi, medicine_drugs, medicine, Systolic_BP_1d, Diastolic_BP_1d, Ketone_1d, visceralFat_1d, bmi_1d, weight_1d
		from tmp_RCT_meaures_1d a 
		where not exists 
		(select 1 from bi_rct_patient_measures_x_preactivation b where a.clientid = b.clientid and a.date = b.date) 
		;

		update bi_rct_patient_measures_x_preactivation a inner join tmp_RCT_meaures_1d b on ( a.clientid = b.clientid and a.date = b.date)
		set a.netcarb_1d = b.netcarb_1d,
			a.protein_1d = b.protein_1d,
			a.fat_1d = b.fat_1d,
			a.fiber_1d = b.fiber_1d, 
			a.calories_1d = b.calories_1d, 
			a.cgm_1d = b.cgm_1d, 
			a.cgm_5d = b.cgm_5d, 
			a.cgm_14d = b.cgm_14d, 
			a.eA1C_1d = b.eA1C_1d, 
			a.eA1C_5d = b.eA1C_5d, 
			a.gmi = b.gmi,
			a.medicine_drugs = b.medicine_drugs,
			a.medicine = b.medicine,
			a.Systolic_BP_1d = b.Systolic_BP_1d, 
			a.Diastolic_BP_1d = b.Diastolic_BP_1d, 
			a.Ketone_1d = b.Ketone_1d, 
			a.visceralFat_1d = b.visceralFat_1d, 
			a.bmi_1d = b.bmi_1d,
			a.weight_1d = b.weight_1d
		; 

		drop temporary table if exists tmp_RCT_meaures_1d; 
        
        -- macro,micro,biota
        
        update bi_rct_patient_measures_x_preactivation a inner join nutritionscores b on a.clientid = b.clientid and a.date = b.date 
		set a.macro = b.macro,
			a.micro = b.micro,
			a.biota = b.biota
		;
        
        drop temporary table if exists tmp_lbg_hbg; 

-- rlBG, rhBG

		create temporary table tmp_lbg_hbg as
		select clientid, date(eventtime) as eventdate, 
		cast(avg(case when fBG < 0 then rBG else 0 end) as decimal(10,4)) as rlBG, 
		cast(avg(case when fBG > 0 then rBG else 0 end) as decimal(10,4)) as rhBG
		from 
		(
			select a.clientId, convert_tz(eventtime, 'UTC', b.timezoneId) as eventtime, value, ln(value), 
			power(ln(value), 1.084), 1.509*(power(ln(value), 1.084) - 5.381) as fBG, 
			10*power((1.509*(power(ln(value), 1.084) - 5.381)),2) as rBG 
			from stagingglucosevalues a inner join v_allclients_rct  b on a.clientid = b.clientid 
			
		) s
		group by clientid, date(eventtime)
		;

		create index idx_tmp_lbg_hbg on tmp_lbg_hbg(clientid asc, eventdate asc, rlBG, rhBG); 
        
		-- Insert/Update measure values
        
        drop temporary table if exists tmp_1d_cv; 

		create temporary table tmp_1d_cv as 
		select a.clientid, date(convert_tz(eventtime, 'UTC', b.timezoneId)) as eventdate, 
		cast((stddev(value)/avg(value)) * 100 as decimal(6,1)) as cv from stagingglucosevalues a inner join v_allclients_rct b on a.clientid = b.clientid 
		
		group by clientid, date(convert_tz(eventtime, 'UTC', b.timezoneId))
		;

		create index idx_tmp_cv_1d on tmp_1d_cv(clientid asc, eventdate asc, cv); 
        
        update bi_rct_patient_measures_x_preactivation a inner join tmp_lbg_hbg b on a.clientid = b.clientid and a.date = b.eventdate
		set a.LBGI = b.rlBG,
			a.HBGI = b.rhBG
		-- where a.date >= date_sub(date(now()), interval 20 day) and a.date <= date(now())
		;

		update bi_rct_patient_measures_x_preactivation a inner join tmp_1d_cv b on a.clientid = b.clientid and a.date = b.eventdate
		set a.GV = b.cv
		-- where a.date >= date_sub(date(now()), interval 20 day) and a.date <= date(now())
		;

		drop temporary table if exists tmp_lbg_hbg; 
		drop temporary table if exists tmp_1d_cv; 
        
        -- TIR, TAR_1, TAR_2, TBR_1,TBR_2
        
        drop temporary table if exists tmp_1d_values; 

		create temporary table tmp_1d_values as
		select a.clientid, date(convert_tz(eventtime, 'UTC', b.timezoneId)) as eventdate, 
		count(value) as total, 
		sum(case when value > 250 then 1 else null end) as more_than_250, 
		sum(case when value between 181 and 250 then 1 else null end) as between_181_250,
		sum(case when value between 71 and 180 then 1 else null end) as between_71_180,
		sum(case when value between 57 and 70 then 1 else null end) as between_57_70,
		sum(case when value < 57 then 1 else null end) as below_57
		from 
		stagingglucosevalues a inner join v_allclients_rct b on a.clientid = b.clientid 
		
		group by clientid, date(convert_tz(eventtime, 'UTC', b.timezoneId))
		;

		create index idx_tmp_1d_cgm_percentage on tmp_1d_values(clientid asc, eventdate asc, more_than_250, total); 

		update bi_rct_patient_measures_x_preactivation a inner join tmp_1d_values b on a.clientid = b.clientid and a.date = b.eventdate
		set a.TIR = cast((b.between_71_180/b.total)*100 as decimal(6,1)), 
			a.TAR_1 = cast((b.between_181_250/b.total)*100 as decimal(6,1)), 
			a.TAR_2 = cast((b.more_than_250/b.total)*100 as decimal(6,1)), 
			a.TBR_1 = cast((b.between_57_70/b.total)*100 as decimal(6,1)), 
			a.TBR_2 = cast((b.below_57/b.total)*100 as decimal(6,1))
		-- where a.date >= date_sub(date(now()), interval 20 day) and a.date <= date(now())
		; 

		drop temporary table if exists tmp_1d_values; 

		-- AUC_1d
		update bi_rct_patient_measures_x_preactivation a inner join 
		(
			select clientid, mealdate, cast(avg(valleyToEndAUC) as decimal(10,2)) as AUC_1d
			from 
			(
							select distinct a.clientid, a.mealdate, c.spikeId, valleyToEndAUC
							from foodlogs a inner join foodlogspikes b on a.clientid = b.clientid and a.mealdate = b.mealdate and a.foodlogid = b.foodlogid and a.foodid = b.foodid
											inner join spikes c on a.clientid = c.clientid and b.spikeid = c.spikeid
											inner join foods d on a.foodid = d.foodid 
											inner join v_allClients_RCT rct on a.clientid = rct.clientid 
			) s 
			group by clientid, mealdate
		) b on a.clientid = b.clientid and a.date = b.mealdate
		set a.AUC_1d = b.AUC_1d
		; 
-- 
		drop temporary table if exists tmp_1d_sd_value; 

		create temporary table tmp_1d_sd_value as 
		select a.clientid, date(convert_tz(eventtime, 'UTC', b.timezoneId)) as eventdate, cast(stddev(value) as decimal(6,2)) as sd_value
		from 
		stagingglucosevalues a inner join v_allclients_rct b on a.clientid = b.clientid 
		-- where  date(eventtime) >= date_sub(date(now()), interval 40 day)
		group by a.clientid, date(convert_tz(eventtime, 'UTC', b.timezoneId))
		;

		create index idx_tmp_1d_sd_value on tmp_1d_sd_value (clientid asc, eventdate asc, sd_value); 

		drop temporary table if exists tmp_1d_staged_glucose_points1;
		drop temporary table if exists tmp_1d_staged_glucose_points2; 


		create temporary table tmp_1d_staged_glucose_points1 as 
		select @rnum := case when @clientid = a.clientid and @date = a.eventdate then @rnum + 1 else 1 end as rownumber, @clientid := a.clientid as clientid, @date := a.eventdate as date, a.eventtime, a.value 
		from 
		(
			select a.clientid, date(convert_tz(eventtime, 'UTC', b.timezoneId)) as eventdate, convert_tz(eventtime, 'UTC', b.timezoneId) as eventtime, value
			from 
			stagingglucosevalues a inner join v_allclients_rct b on a.clientid = b.clientid 
			-- where date(eventtime) >= date_sub(date(now()), interval 40 day)
			order by a.clientid, date(convert_tz(eventtime, 'UTC', b.timezoneId)), convert_tz(eventtime, 'UTC', b.timezoneId)
		) a cross join (select @clientid := 0, @date := '1900-01-01', @rnum := 0) s
		;

		create temporary table tmp_1d_staged_glucose_points2 as select * from tmp_1d_staged_glucose_points1; 

		create index idx_tmp_1d_staged_glucose_points1 on tmp_1d_staged_glucose_points1(clientid asc, date asc, rownumber asc); 
		create index idx_tmp_1d_staged_glucose_points2 on tmp_1d_staged_glucose_points2(clientid asc, date asc, rownumber asc); 
        
        -- 
        drop temporary table if exists tmp_1d_staged_glucose_values_points_comparison;

		create temporary table tmp_1d_staged_glucose_values_points_comparison as
		select s1.clientid, s1.date, s1.eventtime, s1.value as s1_value, s2.value as s2_value, ifnull(abs(s1.value - s2.value),0) as points_difference
		from 
		tmp_1d_staged_glucose_points1  s1 left join tmp_1d_staged_glucose_points2  s2 on s1.clientid = s2.clientid and s1.date = s2.date and s1.rownumber = s2.rownumber + 1
		;

		drop temporary table if exists tmp_1d_staged_glucose_points1;
		drop temporary table if exists tmp_1d_staged_glucose_points2; 


		drop temporary table if exists tmp_1d_MAGE_values; 

		create temporary table tmp_1d_MAGE_values as
		select s1.clientid, s1.date, sum(case when points_difference > sd_value then points_difference else 0 end) as sum_of_points_above_SD, count(distinct case when points_difference > sd_value then s1.eventtime else null end) as total,
		sum(case when points_difference > sd_value then points_difference else 0 end)/count(distinct case when points_difference > sd_value then s1.eventtime else null end) as mage
		from
			tmp_1d_staged_glucose_values_points_comparison s1 left join tmp_1d_sd_value s3 on s1.clientid = s3.clientid and s1.date = s3.eventdate
		group by s1.clientid, s1.date 
		;

-- mage
		create index idx_tmp_1d_MAGE_values on tmp_1d_MAGE_values(clientid asc, date asc, mage); 


		update bi_rct_patient_measures_x_preactivation a inner join tmp_1d_MAGE_values b on a.clientid = b.clientid and a.date = b.date
		set a.mage = b.mage
		-- where a.date >= date_sub(date(now()), interval 30 day)
		; 

		drop temporary table if exists tmp_1d_sd_value; 
		drop temporary table if exists tmp_1d_MAGE_values; 

	-- nonDiabetic_medicine_drugs_HTN  , nonDiabetic_medicine_drugs_CHOL ,nonDiabetic_medicine_drugs_HD

		drop temporary table if exists tmp_nondiabetic_drugs_RCT; 

		create temporary table tmp_nondiabetic_drugs_RCT  
		(
				clientid int not null,
				date date not null, 
				HTN_drugs varchar(500) null, 
				CHOL_drugs varchar(500) null,
				HD_drugs varchar(500) null
		); 

			insert into tmp_nondiabetic_drugs_RCT (clientid, date, HTN_drugs, CHOL_drugs, HD_drugs) 
			select s1.clientid, s1.date, 
			group_concat(distinct case when subcategory = 'HYPERTENSION' then drugname else null end) as HTN_drugs,   
			group_concat(distinct case when subcategory = 'CHOLESTEROL' then drugname else null end) as CHOL_drugs,
			group_concat(distinct case when subcategory = 'HEARTDISEASE' then drugname else null end) as HD_drugs
			from 
			(
							select a.date, b.clientid 
							from v_date_to_date a inner join v_allclients_rct b on a.date >= b.enrollmentdate 
							
			) s1 left join 
			(
			   select distinct clientid, subcategory, drugname, min(startdate) as startdate, max(enddate) as enddate 
				from 
				(
				select distinct P.clientId, pm.medicineid, subcategory, c.medicineName, e.drugname, 
				(date(pm.startLocaldate)) as startdate, (date(pm.endLocaldate)) as enddate 
				from prescriptions P join prescriptionmedicines PM on P.prescriptionid = PM.prescriptionid
									left join medicines c on pm.medicineid = c.medicineid
									left join medicinedrugs d on pm.medicineid = d.medicineid 
									left join drugs e on d.drugid = e.drugid
									left join drugclasses f on e.drugclassid = f.drugclassid 
									inner join v_allclients_rct ac on p.clientid = ac.clientid
				
				where  (pm.startLocaldate is not null and pm.endLocalDate is not null)
				) s 
				group by clientid, subcategory, drugname
			) s2 on s1.clientid = s2.clientid and (s1.date between s2.startdate and s2.enddate)
			group by s1.clientid, s1.date;

			create index idx_tmp_nondiabetic_drugs_RCT on tmp_nondiabetic_drugs_RCT (clientid asc, date asc, HTN_drugs, CHOL_drugs, HD_drugs); 

			update bi_rct_patient_measures_x_preactivation a inner join tmp_nondiabetic_drugs_RCT b on (a.clientid = b.clientid and a.date = b.date) 
			set 
			a.nonDiabetic_medicine_drugs_HTN = b.HTN_drugs, 
			a.nonDiabetic_medicine_drugs_CHOL = b.CHOL_drugs,
			a.nonDiabetic_medicine_drugs_HD = b.HD_drugs
			;

			drop temporary table if exists tmp_nondiabetic_drugs_RCT; 

            
            update dailyProcessLog
			set updateDate = now()
			where processName = 'sp_load_bi_rct_patient_measures_x_preactivation'
            ;	
	
END