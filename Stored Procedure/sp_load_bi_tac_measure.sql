CREATE DEFINER=`aravindan`@`%` PROCEDURE `twins`.`sp_load_bi_tac_measure`()
BEGIN
/*
2019-04-28 Aravindan - Baseline version that calculates facts for TAC scorecard dashboard
2019-05-09 Aravindan - Modified the number of voicelogs logic.
					 - Extended the timeframe as 12AM - 7AM from 3AM - 7AM for fasting spike calculation
2019-05-16 Aravindan - bi_measures table TAC information will be maintained by this process. 
2019-05-17 Aravindan - 1. Modifed the spike food count logic to include the foodlabel instead of foodname because some cases foodname can be blank.
					   2. Modifed logtime_apart_indicator logic to show as 1 even if there is 1 mealtype logged properly within the timeframe. Show as 0 only if nothing is logged in a day.
                       3. Modifed logged_itme_quantity logic to consider fasting information. if a mealtype is missing data and if it was due to fasting consider that positive case. Show such cases with 'F' symbol. 
2019-05-23 Aravindan - Modified the TAC score using a new logic
2019-05-20 Aravindan - Added new column voicelog_review_time       
2019-06-14 Aravindan - Added logic for creating indicators using new Curation logic implemented in the platform.                
2019-07-22 Aravindan - Updated the RedfoodInd logic to use redspike_cur <= redfoodcount from respike_cur = redfoodcount
2020-05-28 Aravindan - Introduced code maintenance for table bi_foodlogs_x_voicelogs_summary - for the speed up of dashboard views.
2020-08-18 Aravindan - Have taken out the 5d measure calculation from this process. 
2020-11-10 Aravindan - food quantity logging section - modified to consider at least 1 food logged per mealtype (to give full score) instead of 2.  
2020-11-18 Aravindan - MealComplete and MealTime Indicators - modified the code to give full score if the patient has skipped all meals in a given day with a supporting fasting record. 
 */
 
/*log the starttime*/
update dailyprocesslog
set startDate = now()
where processName = 'sp_load_bi_tac_measure'
;

/* Time apart and logtime by mealtype section */
/*This is working based on if a food is logged by a client and it is falling in the range, then the indicator will be 1 or 0*/
create temporary table temp_logtime_reference
as
select cd.clientid, cd.date as measure_date, ifnull(logtime_mealtype,'') as logtime_by_mealtype, 
-- ifnull(logtime_apart_indicator,0) as logtime_apart_indicator, -- OLD logic commented - consider the patient as good if the meal is skipped for the whole day
if(s3.b_fasting = 1 and s3.l_fasting = 1 and s3.d_fasting = 1, 1, 0) as full_day_fasting_ind,
if(s3.b_fasting = 1 and s3.l_fasting = 1 and s3.d_fasting = 1, 1, ifnull(logtime_apart_indicator,0)) as logtime_apart_indicator
from
		(
		select distinct d.date, c.clientId
		from twins.v_date_to_date d
		left join bi_patient_status c on d.date between ifnull(date_add(date(c.visitdate), interval 1 day), c.status_start_date) and c.status_end_date and c.status = 'active'
		where d.date >= date_sub(date(now()), interval 50 day)
        ) cd 
left outer join 
(
select s1.clientid, s1.mealdate, 
concat(B_mealtype,if(B_mealtype = 'B: ' and s2.b_fasting = 1,'F',''),', ',
	   L_mealtype,if(L_mealtype = 'L: ' and s2.l_fasting = 1,'F',''), ', ',
       D_mealtype,if(D_mealtype = 'D: ' and s2.d_fasting = 1,'F','')) as logtime_mealtype,
       if((B_timeapart + L_timeapart + D_timeapart) = 0, 1, 0) as logtime_apart_indicator
from 
(
		select clientid, mealdate, 
        ifnull(max(B_mealtype),'B: ') as b_mealtype, ifnull(max(B_timeapart),0) as B_timeapart,  /* this is required to write the entries by mealtype in the same order (breakfast, lunch, dinner). */
		ifnull(max(L_mealtype),'L: ') as l_mealtype, ifnull(max(L_timeapart),0) as L_timeapart, 
		ifnull(max(D_mealtype),'D: ') as d_mealtype, ifnull(max(d_timeapart),0) as D_timeapart
		from 
		(
				select clientid, mealdate, 
				case when mealtype = 'breakfast' then hrs_per_mealtype end as B_mealtype, 
				case when mealtype = 'breakfast' then timeapart end as B_timeapart,
				case when mealtype = 'lunch' then hrs_per_mealtype end as L_mealtype, 
				case when mealtype = 'lunch' then timeapart end as L_timeapart,
				case when mealtype = 'dinner' then hrs_per_mealtype end as d_mealtype, 
				case when mealtype = 'dinner' then timeapart end as d_timeapart
				from 
				(	
					select  clientId, mealDate,  mealtype, -- hour(minTime), hour(maxTime),
					hrs_per_mealtype,
					case when mealType = 'BREAKFAST' and (( (minTime) >= 3 and (maxTime) < 12 )) then 0
						when mealType = 'LUNCH' and (((minTime) >= 12 and (maxTime) < 17 )) then 0
						when mealType = 'DINNER' and (((minTime) >= 18 and (maxTime) < 24)) then 0
						else 1 end as timeApart
					from 
					(    
						select clientId, eventdate as mealDate, categoryType as mealtype, 
                        concat(case when categoryType='BREAKFAST' then 'B' when categoryType = 'LUNCH' then 'L' when categoryType = 'DINNER' then 'D' end, ':', group_concat(distinct hour(eventTime))) as hrs_per_mealtype,
						HOUR(min(eventTime)) as minTime, HOUR(max(eventTime)) as maxTime
						from bi_food_supplement_log_detail
                        where category = 'foodlog'
						-- where mealdate >= date_sub(date(now()), interval 50 day)
						group by clientId, eventdate, categoryType
					) m
				) x
		)y
		group by clientid, mealdate
) s1 
left join 
(
    select clientid, mealdate, 
    max(ifnull(b_fasting,0)) as b_fasting, 
    max(ifnull(l_fasting,0)) as l_fasting, 
    max(ifnull(d_fasting,0)) as d_fasting
    from
    (
		select clientid, mealdate, case when mealtype = 'breakfast' then fasting end as b_fasting,
		case when mealtype = 'lunch' then fasting end as l_fasting,
		case when mealtype = 'dinner' then fasting end as d_fasting
		from mealjourneys 
		-- where mealdate >= date_sub(date(now()), interval 55 day)
		order by clientid, mealdate
    ) x group by clientid, mealdate
) s2 on s1.clientid = s2.clientid and s1.mealdate = s2.mealdate
) s on cd.date = s.mealdate  and cd.clientid = s.clientid
left join 
(
    select clientid, mealdate, 
    max(ifnull(b_fasting,0)) as b_fasting, 
    max(ifnull(l_fasting,0)) as l_fasting, 
    max(ifnull(d_fasting,0)) as d_fasting
    from
    (
		select clientid, mealdate, case when mealtype = 'breakfast' then fasting end as b_fasting,
		case when mealtype = 'lunch' then fasting end as l_fasting,
		case when mealtype = 'dinner' then fasting end as d_fasting
		from mealjourneys 
		-- where mealdate >= date_sub(date(now()), interval 55 day)
		order by clientid, mealdate
    ) x group by clientid, mealdate
) s3 on cd.clientid = s3.clientid and cd.date = s3.mealdate
;

insert into bi_tac_measure (clientid, measure_date, logtime_by_mealtype, logtime_apart_indicator)
select clientid, measure_date, logtime_by_mealtype, logtime_apart_indicator
from temp_logtime_reference a
where not exists (select 1 from bi_tac_measure b where a.clientid = b.clientid and a.measure_date = b.measure_date)
;

update bi_tac_measure a
inner join temp_logtime_reference b on  (a.clientid = b.clientid and a.measure_date = b.measure_date)
set a.logtime_by_mealtype = b.logtime_by_mealtype, 
	a.logtime_apart_indicator = b.logtime_apart_indicator
;

drop temporary table temp_logtime_reference
;


/*food quantity logging section*/
create temporary table temp_foodquantity_reference
as 
select cd.clientid, cd.date as measure_date, 
ifnull(item_quantity_logged,'') as logged_item_quantity, 
-- ifnull(items_mealtype_indicator,0) as items_mealtype_indicator, -- OLD logic commented - consider the patient as good if the meal is skipped for the whole day
if(s3.b_fasting = 1 and s3.l_fasting = 1 and s3.d_fasting = 1, 1, 0) as full_day_fasting_ind,
if(s3.b_fasting = 1 and s3.l_fasting = 1 and s3.d_fasting = 1, 1, ifnull(items_mealtype_indicator,0)) as items_mealtype_indicator
from
		(
		select distinct d.date, c.clientId
		from twins.v_date_to_date d
		left join bi_patient_status c on d.date between ifnull(date_add(date(c.visitdate), interval 1 day), c.status_start_date) and c.status_end_date and c.status = 'active'
		where d.date >= date_sub(date(now()), interval 50 day)
        ) cd 
left outer join 
(
select clientid, mealdate,
concat('B',':',x.breakfast_qty,if(x.breakfast_qty = 0 and x.b_fasting=1,'F',''),
	 ', L',':',x.lunch_qty,if(x.lunch_qty = 0 and x.l_fasting=1,'F',''),
     ', D',':',x.dinner_qty,if(x.dinner_qty = 0 and x.d_fasting=1,'F','')) as item_quantity_logged, -- Lets add 'F' as fasting to see if quantity is 0 due to fasting patient 
case when (x.breakfast_qty >= 1 or x.breakfast_qty = 0 and x.b_fasting = 1) 
		and (x.lunch_qty >= 1 or x.lunch_qty = 0 and x.l_fasting = 1) 
        and (x.dinner_qty >= 1 or x.dinner_qty = 0 and x.d_fasting = 1) 
        then 1 else 0 end as items_mealtype_indicator
from 
(
select s1.clientid, s1.mealdate, s1.breakfast_qty, s2.b_fasting, s1.lunch_qty, s2.l_fasting, s1.dinner_qty, s2.d_Fasting
from 
(	
	select clientid, mealdate, 
	max(ifnull(breakfast_qty,0)) as breakfast_qty,max(ifnull(lunch_qty,0)) as lunch_qty, max(ifnull(dinner_qty,0)) as dinner_qty
	from 
	(
		select clientid, mealdate, 
		case when mealtype= 'BREAKFAST' then cnt_of_qty end as breakfast_qty,
		case when mealtype= 'LUNCH' then cnt_of_qty end as lunch_qty,
		case when mealtype= 'DINNER' then cnt_of_qty end as dinner_qty
		from 
		(
                 select clientId, eventdate as mealDate, categoryType as mealtype, count(quantity) as cnt_of_qty
				 from bi_food_supplement_log_detail
				 where category = 'foodlog'
                 -- where mealdateITZ >= date_sub(date(now()), interval 50 day)
				 group by clientId, eventdate, categoryType
		) a
	) x
	group by clientid, mealdate
) s1
left join 
(	
	select clientid, mealdate, max(ifnull(b_fasting,0)) as b_fasting, max(ifnull(l_fasting,0)) as l_fasting, max(ifnull(d_fasting,0)) as d_fasting
    from
    (
		select clientid, mealdate, case when mealtype = 'breakfast' then fasting end as b_fasting,
		case when mealtype = 'lunch' then fasting end as l_fasting,
		case when mealtype = 'dinner' then fasting end as d_fasting
		from mealjourneys 
        -- where mealdate >= date_sub(date(now()), interval 55 day)
		order by clientid, mealdate
    ) x group by clientid, mealdate
) s2 on s1.clientid = s2.clientid and s1.mealdate = s2.mealdate
) x
) s on cd.clientid = s.clientid and cd.date = s.mealdate
left join 
(
    select clientid, mealdate, 
    max(ifnull(b_fasting,0)) as b_fasting, 
    max(ifnull(l_fasting,0)) as l_fasting, 
    max(ifnull(d_fasting,0)) as d_fasting
    from
    (
		select clientid, mealdate, case when mealtype = 'breakfast' then fasting end as b_fasting,
		case when mealtype = 'lunch' then fasting end as l_fasting,
		case when mealtype = 'dinner' then fasting end as d_fasting
		from mealjourneys 
		-- where mealdate >= date_sub(date(now()), interval 55 day)
		order by clientid, mealdate
    ) x group by clientid, mealdate
) s3 on cd.clientid = s3.clientid and cd.date = s3.mealdate
;

insert into bi_tac_measure (clientid, measure_date, logged_item_quantity, items_mealtype_indicator)
select clientid, measure_date, logged_item_quantity, items_mealtype_indicator
from temp_foodquantity_reference a
where not exists (Select 1 from bi_tac_measure b where a.clientid = b.clientid and a.measure_date = b.measure_date)
;

update bi_tac_measure a
inner join temp_foodquantity_reference b on (a.clientid = b.clientid and a.measure_date = b.measure_date)
set a.logged_item_quantity = b.logged_item_quantity, 
	a.items_mealtype_indicator = b.items_mealtype_indicator
;

drop temporary table temp_foodquantity_reference
;


/*Spike vs Redfood section*/
create temporary table temp_spike_redfood_reference
as
select 	cd.clientid, 
		cd.date as measure_date,
		if(so.cgm_count > 0, 'Y','N') as is_cgm_available,
        is_predictionON,
		ifnull(so.final_spike_cnt,0) as spikes_count,
		ifnull(so.redfood_cnt,0) as redfood_count,
        ifnull(so.pstar_cnt,0) as pstar_food_count,
        ifnull(so.redfood_cnt,0) - ifnull(so.pstar_cnt,0) as adjusted_redfood_count,
		ifnull(so.sr_ind,0) as spike_redfood_indicator
from 
(
		select distinct d.date, c.clientId
		from twins.v_date_to_date d
		left join bi_patient_status c on d.date between ifnull(date_add(date(c.visitdate), interval 1 day), c.status_start_date) and c.status_end_date and c.status = 'active'
		where d.date >= date_sub(date(now()), interval 30 day) 
) cd 
left outer join 
(
select s.clientid, 
	   s.eventdate, 
	   s.cgm_count, 
       s.is_predictionON,
	   case when ((s.cgm_count > 0 and final_spikes_count is null) or (s.is_predictionON = 'Y' and x.mealdate is not null and final_spikes_count is null)) then 0 else final_spikes_count end as final_spike_cnt, -- If a patient is on prediction then see if foodlog is there to give full score
	   case when redfood_cnt is null then 0 else redfood_cnt end as redfood_Cnt,
       case when pstar_cnt is null then 0 else pstar_cnt end as pstar_cnt,
	   case when ((s.cgm_count > 0 and final_spikes_count is null and redfood_cnt is null) or (s.is_predictionON = 'Y' and x.mealdate is not null and final_spikes_count is null)) then 1 else spike_redfood_indicator end as SR_IND -- If a patient is on prediction then see if foodlog is there to give full score
from 
(
	select a.clientid, measure_event_date as eventdate, if(cgm_1d is null,0,1) as cgm_count, if(b.clientid is not null, 'Y', 'N') as is_predictionON
    from bi_patient_measures a left join predictedCGMCycles b on a.clientid = b.clientid and a.measure_event_date between b.startDate and b.endDate
	where measure_event_date >= date_sub(date(now()), interval 40 day) 
) s
left join 
(
		select 
		f.clientid, 
        f.mealdate,
		s.final_spikes_count,
		f.redfood_cnt,
		f.pstar_cnt,
		IF(ifnull(final_spikes_count,0) <= redfood_cnt, 1, 0) as spike_redfood_indicator
		from 
		(
							select f.clientid, f.eventdate mealdate, 
                            count(case when f.category = 'foodlog' and E5grading = 'Red' then f.itemid else null end) as redfood_cnt,
							count(distinct case when f.category = 'foodlog' and E5grading = 'Red' then pf.foodid else null end) as pstar_cnt
							from bi_food_supplement_log_detail f
							left join personalizedfoods pf on f.clientid = pf.clientid and f.itemid = pf.foodid and pf.recommendationrating is not null
							where f.eventdate >= date_sub(date(now()), interval 40 day)
                            group by f.clientid, f.eventdate
		) f left join  
        (
				select a.clientid, a.mealdate, count(distinct c.spikeId) as final_spikes_count
				from foodlogs a inner join foodlogspikes b on a.clientid = b.clientid and a.mealdate = b.mealdate and a.foodlogid = b.foodlogid and a.foodid = b.foodid
								inner join spikes c on a.clientid = c.clientid and b.spikeid = c.spikeid
								inner join foods d on a.foodid = d.foodid 
				where c.spikeamount > 50
				and hour(c.startTime) not between 0 and 8 -- To ignore dawn spikes
				and d.recommendationrating <= 1 -- To get the spikes caused by red foods only
                and a.mealdate >= date_sub(date(now()), interval 35 day)
				group by a.clientid, a.mealdate
		) s  on (s.clientId = f.clientId and f.mealDate = s.mealdate)
) x on s.clientid = x.clientid and s.eventdate = x.mealdate
) so on cd.clientid = so.clientid and cd.date = so.eventdate
group by cd.clientid, cd.date
;


insert into bi_tac_measure (clientid, measure_date, is_cgm_available, spikes_count, redfood_count, pstar_food_count, spike_redfood_indicator)
select clientid, measure_date, is_cgm_available, spikes_count, redfood_count, pstar_food_count, spike_redfood_indicator
from temp_spike_redfood_reference a
where not exists (select 1 from bi_tac_measure b where a.clientid = b.clientid and a.measure_date = b.measure_date)
;

update bi_tac_measure a
inner join temp_spike_redfood_reference b on (a.clientid = b.clientid and a.measure_date = b.measure_date)
set a.is_cgm_available = b.is_cgm_available,
	a.spikes_count = b.spikes_count,
    a.redfood_count = b.redfood_count,
    a.pstar_food_count = b.pstar_food_count,
    a.spike_redfood_indicator = b.spike_redfood_indicator
;

drop temporary table temp_spike_redfood_reference
;

/* Actual vs Predict section */
-- Underreporting table is not available in the twinsml US schema so commenting this part. 
/*
create temporary table temp_actual_predict_reference
as
select clientid, mealdate as measure_date, actual_predict_variance_under_reported, actual_predict_indicator
from 
(
select a.clientid, a.date as mealdate, 
		convert(avgUnderReporting, decimal(7,2)) as actual_predict_variance_under_reported,
		case when b.avgunderreporting is not null then 0 else 1 end as actual_predict_indicator
from 
		(
		select distinct d.date, c.clientId
		from twins.v_date_to_date d
		left join bi_patient_status c on d.date between date_add(date(c.visitdate), interval 1 day) and c.status_end_date and c.status = 'active'
        where visitdate is not null
		) a
left join 
(
			SELECT 
					clientid, mealdate as mealdate, AVG(glucoseMax - pred) avgUnderReporting
			FROM
					twinsml.underreporting
			WHERE
					dateScored = (SELECT MAX(datescored) FROM twinsml.underreporting)
			AND IFNULL(glucoseMax, 0) > 0
			AND glucoseMax - pred > 60
            group by clientid, mealdate
) b
on a.clientid = b.clientid and a.date = b.mealdate
) a
;


insert into bi_tac_measure (clientid, measure_date, actual_predict_variance_under_reported, actual_predict_indicator)
select clientid, measure_date, actual_predict_variance_under_reported, actual_predict_indicator
from temp_actual_predict_reference a
where not exists (select 1 from bi_tac_measure b where a.clientid = b.clientid and a.measure_date = b.measure_date)
;

update bi_tac_measure a
inner join temp_actual_predict_reference b on (a.clientid = b.clientid and a.measure_date = b.measure_date)
set a.actual_predict_variance_under_reported = b.actual_predict_variance_under_reported,
	a.actual_predict_indicator = b.actual_predict_indicator
;

drop temporary table temp_actual_predict_reference
;

*/

/*number of voice and manual logs*/
update bi_tac_measure a 
inner join
( 
select a.clientid, mealdate, 
sum(Case when voiceFoodLogId is null then 1 else 0 end) as num_of_manuallogs, 
sum(Case when voicefoodlogid is not null then 1 else 0 end) as num_of_voicelogs 
from 
v_active_clients a left join foodlogs b on a.clientid = b.clientid 
group by a.clientid, mealdate
) b on a.clientid = b.clientid and a.measure_date = b.mealdate
set a.num_of_manuallogs = b.num_of_manuallogs,
	a.num_of_voicelogs = b.num_of_voicelogs
;

/*total voicelogs load logic modified*/
update bi_tac_measure a
inner join 
(
		select distinct a.clientid, mealdate, b.total_voicelogs
		from foodlogs a 
		inner join 
		(
			select clientid,date(b.eventtime) as eventtimeDate, count(*) as total_voicelogs
			from voicefoodlogs b
			group by clientid,date(b.eventtime)
		) b  on a.clientid = b.clientid and date(a.mealtime) = b.eventtimeDate
) x on a.clientid = x.clientid and a.measure_date = x.mealdate
set a.total_voicelogs = x.total_voicelogs
;

/*number of logs corrected by coach*/
update bi_tac_measure a 
inner join
( 
select a.clientid, mealdate, 
sum(case when a.username <> b.createdby then 1 else 0 end) as num_logs_corrected_by_coach
from v_Active_clients a left join foodlogs b on a.clientid = b.clientid
group by a.clientid, mealdate
) b on a.clientid = b.clientid and a.measure_date = b.mealdate
set a.num_logs_corrected_by_coach = b.num_logs_corrected_by_coach
;


/*number of VLs reviewed by coach*/
update bi_tac_measure a 
inner join
( 
select clientid, mealdate, sum(case when humanoutput is not null then 1 else 0 end) as num_voicelogs_reviewed_by_coach
from voicefoodlogs
where mealdate is not null
group by clientid, mealdate
) b on a.clientid = b.clientid and a.measure_date = b.mealdate
set a.num_voicelogs_reviewed_by_coach = b.num_voicelogs_reviewed_by_coach
;

/*VLs review time*/
update bi_tac_measure a 
inner join
( 
select clientid, date(submittedTime) as submitdate, cast(sum(total_review_time) as decimal(5,2)) as voicelog_review_time
from
(
	select a.voicefoodlogid, a.clientid, a.submittedTime, b.reviewedTime, timestampdiff(minute, submittedtime, reviewedTime)/60 as total_review_time
	from
	(select voiceFoodLogId, clientId, eventTime as submittedTime from voicefoodlogs) a inner join
	(select voiceFoodLogId, clientId, dateModified as reviewedTime from voicefoodlogs where humanOutput is not null) b
	on a.clientid = b.clientid and a.voicefoodlogid = b.voicefoodlogid
	where date(a.submittedtime) >= '2019-05-20'
) x
group by clientid, date(submittedTime) 
) b on a.clientid = b.clientid and a.measure_date = b.submitdate
set a.voicelog_review_time = b.voicelog_review_time
;

/*Curation logic is added below so new set of indicators are available in the table and maintained by this portion of code. */
create temporary table tmp_indicators_correction_by_curation
as
select a.clientid, measure_date, 
redfood_count, pstar_food_count, spikes_count, 
spike_redfood_indicator as current_spike_redfood_ind,
-- if(redfood_count =  b.redspike_curate, 1, spike_redfood_indicator) as new_spike_redfood_ind,
if((b.redspike_curate <= (redfood_count - ifnull(pstar_food_count,0))), 1, spike_redfood_indicator) as new_spike_redfood_ind, -- Lets exclude personalized redfood from total redfood count
B.redspike_curate,

logtime_by_mealtype, 
logtime_apart_indicator as current_mealtime_ind,
if(logtime_apart_indicator = 0 and b.MealTime_curate = 'YES' and logtime_by_mealtype <> '', 1, logtime_apart_indicator) as new_mealtime_ind,
b.MealTime_curate,

logged_item_quantity, 
items_mealtype_indicator as current_mealcomplete_ind,
if(items_mealtype_indicator = 0 and b.MealComplete_curate = 'YES' and logged_item_quantity <> '', 
	if(logged_item_quantity like 'B:0,%' or logged_item_quantity like '%L:0,%' or logged_item_quantity like '%D:0', items_mealtype_indicator, 1),
        items_mealtype_indicator) as new_mealcomplete_ind,
B.Mealcomplete_Curate

from (select * from bi_tac_measure where measure_date >=  date_sub(date(now()), interval 50 day)) a 
left join 
(
	select clientid, eventdate, max(MealTime_curate) as MealTime_curate, 
    max(MealComplete_curate) as MealComplete_curate,
    max(redspike_curate) as redspike_curate
	from 
    (
		select a.clientid, date(a.eventtime) as eventdate,
		case when calllogfieldid = 1402 then s.value else null end as MealTime_curate,
		case when calllogfieldid = 1602 then s.value else null end as MealComplete_curate,
		case when calllogfieldid = 1002 then s.value else null end as redspike_curate
		from calllogs a inner join calllogdetails s 
		on a.calllogid = s.calllogid
		-- where date(a.dateadded) >= '2019-06-10' -- date chosen to exclude lot of test entries before
		where date(a.eventtime) >=  date_sub(date(now()), interval 60 day)
	) s
    group by clientid, eventdate
) b on a.clientid = b.clientid and a.measure_date = b.eventdate
;

update bi_tac_measure a inner join tmp_indicators_correction_by_curation b
on a.clientid = b.clientid and a.measure_date = b.measure_date
set a.curate_logtime_apart_indicator = b.new_mealtime_ind,
	a.curate_items_mealtype_indicator = b.new_mealcomplete_ind,
    a.curate_spike_redfood_indicator = b.new_spike_redfood_ind,
    a.redspike_curate = b.redspike_curate,
    a.mealtime_curate = b.MealTime_curate,
    a.mealcomplete_curate = b.Mealcomplete_Curate
;

drop temporary table tmp_indicators_correction_by_curation
;

/*TAC */
update bi_tac_measure a
inner join bi_tac_measure b
on a.clientid = b.clientid and a.measure_date = b.measure_date
-- set a.tac_score = ifnull(b.spike_redfood_indicator,0) + ifnull(b.logtime_apart_indicator,0) + ifnull(b.items_mealtype_indicator,0)
set a.tac_score = (case when ifnull(b.curate_items_mealtype_indicator,0) = 0 then 0 
					   when ifnull(b.curate_items_mealtype_indicator,0) = 1 and ifnull(b.curate_logtime_apart_indicator,0) = 0 and ifnull(b.curate_spike_redfood_indicator,0) = 0 then 1
					   when ifnull(b.curate_items_mealtype_indicator,0) = 1 and ifnull(b.curate_logtime_apart_indicator,0) = 1 and ifnull(b.curate_spike_redfood_indicator,0) = 0 then 2
					   when ifnull(b.curate_items_mealtype_indicator,0) = 1 and ifnull(b.curate_logtime_apart_indicator,0) = 0 and ifnull(b.curate_spike_redfood_indicator,0) = 1 then 2
					   when ifnull(b.curate_items_mealtype_indicator,0) = 1 and ifnull(b.curate_logtime_apart_indicator,0) = 1 and ifnull(b.curate_spike_redfood_indicator,0) = 1 then 3
				   else 0 end)
;

-- Section to maintain the Summary of foodlogs and voicelogs entered by Client and entered/corrected by Coach to use in the TAC detail and summary related views
-- This helps to speed up the view run time. 
drop temporary table if exists tmp_voicelogs; 

create temporary table tmp_voicelogs as 
select a.clientid, mealdate as mealdate, count(*) as total_logged, 
		sum(case when voicefoodlogid is null and a.username = coalesce(b.createdby, a.username) and a.username = coalesce(b.modifiedby, a.username) then 1 else 0 end) as mlogs_created_client,
		sum(case when voicefoodlogid is null and a.username = b.createdby and modifiedby <> createdby then 1 else 0 end) as mlogs_corrected_coach,
		sum(case when voicefoodlogid is null and a.username <> b.createdby then 1 else 0 end) as mlogs_entered_coach,
		sum(case when voicefoodlogid is not null and a.username = b.createdby then 1 else 0 end) as vlogs_created_client,
		sum(case when voicefoodlogid is not null and a.username <> b.createdby then 1 else 0 end) as vlogs_added_coach,
        sum(case when voicefoodlogid is not null and a.username = b.createdby and modifiedby <> createdby then 1 else 0 end) as vlogs_corrected_coach
from v_Active_clients a inner join foodlogs b on a.clientid = b.clientid
where mealdate >= date_sub(date(itz(now())), interval 35 day)  
group by a.clientid, date(itz(mealdate))
;

create index idx_tmp_voicelogs on tmp_voicelogs(clientid asc, mealdate asc, total_logged, vlogs_created_client, vlogs_corrected_coach, mlogs_entered_coach);

-- we dont want to keep more data in this table because this is designed only to solve the Summary calculation and Detail query to run faster in the TAC realted views. 
-- v_bi_tac_measure_details, v_bi_tact_measure_summary
delete from bi_foodlogs_x_voicelogs_summary where mealdate >= date_sub(date(now()), interval 35 day); 

insert into bi_foodlogs_x_voicelogs_summary 
select clientid, mealdate, total_logged, mlogs_created_client, mlogs_corrected_coach, mlogs_entered_coach,
vlogs_created_client, vlogs_added_coach, vlogs_corrected_coach
from tmp_voicelogs a 
where not exists 
(select 1 from bi_foodlogs_x_voicelogs_summary b where a.clientid = b.clientid and a.mealdate = b.mealdate)
;

update bi_foodlogs_x_voicelogs_summary a 
inner join tmp_voicelogs b on (a.clientid = b.clientid and a.mealdate = b.mealdate)
set 
a.total_logged = b.total_logged,
a.mlogs_created_client = b.mlogs_created_client,
a.mlogs_corrected_coach = b.mlogs_corrected_coach,
a.mlogs_entered_coach = b.mlogs_entered_coach,
a.vlogs_created_client = b.vlogs_created_client,
a.vlogs_added_coach = b.vlogs_added_coach,
a.vlogs_corrected_coach = b.vlogs_corrected_coach
; 

drop temporary table if exists tmp_voicelogs; 


/*Capture the completion time */
update dailyprocesslog
set updatedate = now()
where processName = 'sp_load_bi_tac_measure'
;
END