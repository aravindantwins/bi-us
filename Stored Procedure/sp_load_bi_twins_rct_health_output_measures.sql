CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_bi_twins_rct_health_output_measures`()
begin
	
update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_twins_rct_health_output_measures'
;

drop temporary table if exists tmp_rct_patient_lab_dates;
	
create temporary table tmp_rct_patient_lab_dates
as
select distinct d1.clientid ,  d1.cohort,
d1.day0,
d1.day10,
d1.day26,
d1.day55,
d1.day56,
d1.day85,
d1.day86,
d1.day115,
d1.day116,
d1.day145,
d1.day146,
d1.day175,
d1.day176,
d1.day205,
d1.day206,
d1.day235,
d1.day236,
d1.day265,
d1.day266,
d1.day295,
d1.day296,
d1.day325,
d1.day326,
d1.day355,
d1.day356,
d1.day385,
d1.firstBloodWorkDate as D0_labdate, d2.D30_labdate, 
-- d3.D60_labdate, 
d4.D90_labdate,
-- d5.D120_labdate, d6.D150_labdate, 
d7.D180_labdate, 
-- d8.D210_labdate, d9.D240_labdate,
d10.D270_labdate, 
-- d11.D300_labdate, d12.D330_labdate, 
d13.D360_labdate,
d1.sensor_day1,
d1.sensor_day4,
d1.sensor_day5,
d1.sensor_day6,
d1.sensor_day7,
d1.sensor_day10,
d1.sensor_day24,
d1.sensor_day26,
d1.sensor_day30,
d1.sensor_day84,
d1.sensor_day86,
d1.sensor_day90,
d1.sensor_day114,
d1.sensor_day116,
d1.sensor_day120,
d1.sensor_day144,
d1.sensor_day146,
d1.sensor_day150,
d1.sensor_day174,
d1.sensor_day176,
d1.sensor_day180,
d1.sensor_day264,
d1.sensor_day266,
d1.sensor_day270,
d1.sensor_day354,
d1.sensor_day356,
d1.sensor_day360
  from
	(	
	select distinct a.clientid, a.cohort, firstBloodWorkDate, -- b.clientid,
    firstBloodWorkDate as day0,
	date_add(sensor_day1, interval 9 day) as day10,
    date_add(sensor_day1, interval 25 day) as day26,
	date_add(sensor_day1, interval 54 day) as day55,
    date_add(sensor_day1, interval 55 day) as day56,
    date_add(sensor_day1, interval 84 day) as day85,
    date_add(sensor_day1, interval 85 day) as day86,
	date_add(sensor_day1, interval 114 day) as day115,
    date_add(sensor_day1, interval 115 day) as day116,
	date_add(sensor_day1, interval 144 day) as day145,
    date_add(sensor_day1, interval 145 day) as day146,
	date_add(sensor_day1, interval 174 day) as day175,
    date_add(sensor_day1, interval 175 day) as day176,
	date_add(sensor_day1, interval 204 day) as day205,
    date_add(sensor_day1, interval 205 day) as day206,
	date_add(sensor_day1, interval 234 day) as day235,
    date_add(sensor_day1, interval 235 day) as day236,
	date_add(sensor_day1, interval 264 day) as day265,
	date_add(sensor_day1, interval 265 day) as day266,
	date_add(sensor_day1, interval 294 day) as day295,
    date_add(sensor_day1, interval 295 day) as day296,
	date_add(sensor_day1, interval 324 day) as day325,
    date_add(sensor_day1, interval 325 day) as day326,
	date_add(sensor_day1, interval 354 day) as day355,
    date_add(sensor_day1, interval 355 day) as day356,
	date_add(sensor_day1, interval 384 day) as day385,
	a.sensor_day1,
	date_add(sensor_day1, interval 3 day) as sensor_day4,
    date_add(sensor_day1, interval 4 day) as sensor_day5,
	date_add(sensor_day1, interval 5 day) as sensor_day6,
    date_add(sensor_day1, interval 6 day) as sensor_day7,
	date_add(sensor_day1, interval 9 day) as sensor_day10,
	date_add(sensor_day1, interval 22 day) as sensor_day24,
	date_add(sensor_day1, interval 25 day) as sensor_day26,
	date_add(sensor_day1, interval 29 day) as sensor_day30,
	date_add(sensor_day1, interval 82 day) as sensor_day84,
	date_add(sensor_day1, interval 85 day) as sensor_day86,
	date_add(sensor_day1, interval 89 day) as sensor_day90,
	date_add(sensor_day1, interval 112 day) as sensor_day114,
	date_add(sensor_day1, interval 115 day) as sensor_day116,
	date_add(sensor_day1, interval 119 day) as sensor_day120,
	date_add(sensor_day1, interval 142 day) as sensor_day144,
	date_add(sensor_day1, interval 145 day) as sensor_day146,
	date_add(sensor_day1, interval 149 day) as sensor_day150,
	date_add(sensor_day1, interval 172 day) as sensor_day174,
	date_add(sensor_day1, interval 175 day) as sensor_day176,
	date_add(sensor_day1, interval 179 day) as sensor_day180,
	date_add(sensor_day1, interval 262 day) as sensor_day264,
	date_add(sensor_day1, interval 265 day) as sensor_day266,
	date_add(sensor_day1, interval 269 day) as sensor_day270,
	date_add(sensor_day1, interval 352 day) as sensor_day354,
	date_add(sensor_day1, interval 355 day) as sensor_day356,
	date_add(sensor_day1, interval 359 day) as sensor_day360
	from

          (
		   select a.clientid, a.status, treatmentDays,  "TWIN_PRIME" as cohort,
			min(date(firstBloodWorkDate)) as firstBloodWorkDate, min(programStartDate_analytics) as sensor_day1
			from dim_client a 
				 inner join v_allclients_rct b on a.clientid=b.clientid
            where upper(a.status) in ('ACTIVE')
            and  treatmentDays >= 1
           group by a.clientid having min(date(firstBloodWorkDate)) is not null
           
            union all
	
            select a.clientid, a.status, treatmentDays,  "TWIN_PRIME" as cohort,
			min(date(firstBloodWorkDate)) as firstBloodWorkDate,  min(programStartDate_analytics) as sensor_day1
			from dim_client a inner join v_allclients_rct b on a.clientid=b.clientid
            where upper(a.status) in ('DISCHARGED','INACTIVE')
            and  datediff(date(latestBloodWorkDate), date(firstBloodWorkDate)) > 25
            group by clientid having min(date(firstBloodWorkDate)) is not null
        	)a 
            ) d1
	 left join (select distinct a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as D30_labdate from clinictestresults a inner join v_allclients_rct b on a.clientid=b.clientid  where a.deleted = 0) d2 on d1.clientid = d2.clientid and (d2.D30_labdate between d1.day26 and d1.day55)
	 -- left join (select distinct a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as D60_labdate from clinictestresults a inner join v_allclients_rct b on a.clientid=b.clientid where deleted = 0) d3 on d1.clientid = d3.clientid and (d3.D60_labdate between d1.day56 and d1.day85)
     left join (select distinct a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as D90_labdate from clinictestresults a inner join v_allclients_rct b on a.clientid=b.clientid where deleted = 0) d4 on d1.clientid = d4.clientid and (d4.D90_labdate between d1.day86 and d1.day115)
	 -- left join (select distinct a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as D120_labdate from clinictestresults a inner join v_allclients_rct b on a.clientid=b.clientid where deleted = 0) d5 on d1.clientid = d5.clientid and (d5.D120_labdate between d1.day116 and d1.day145)
	 -- left join (select distinct a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as D150_labdate from clinictestresults a inner join v_allclients_rct b on a.clientid=b.clientid where deleted = 0) d6 on d1.clientid = d6.clientid and (d6.D150_labdate between d1.day146 and d1.day175)
	 left join (select distinct a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as D180_labdate from clinictestresults a inner join v_allclients_rct b on a.clientid=b.clientid where deleted = 0) d7 on d1.clientid = d7.clientid and (d7.D180_labdate between d1.day176 and d1.day205)
	 -- left join (select distinct a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as D210_labdate from clinictestresults a inner join v_allclients_rct b on a.clientid=b.clientid where deleted = 0) d8 on d1.clientid = d8.clientid and (d8.D210_labdate between d1.day206 and d1.day235)
     -- left join (select distinct a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as D240_labdate from clinictestresults a inner join v_allclients_rct b on a.clientid=b.clientid where deleted = 0) d9 on d1.clientid = d9.clientid and (d9.D240_labdate between d1.day236 and d1.day265)
	 left join (select distinct a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as D270_labdate from clinictestresults a inner join v_allclients_rct b on a.clientid=b.clientid where deleted = 0) d10 on d1.clientid = d10.clientid and (d10.D270_labdate between d1.day266 and d1.day295)
	 -- left join (select distinct a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as D300_labdate from clinictestresults a inner join v_allclients_rct b on a.clientid=b.clientid where deleted = 0) d11 on d1.clientid = d11.clientid and (d11.D300_labdate between d1.day296 and d1.day325)
	 -- left join (select distinct a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as D330_labdate from clinictestresults a inner join v_allclients_rct b on a.clientid=b.clientid where deleted = 0) d12 on d1.clientid = d12.clientid and (d12.D330_labdate between d1.day326 and d1.day355)
	 left join (select distinct a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as D360_labdate from clinictestresults a inner join v_allclients_rct b on a.clientid=b.clientid where deleted = 0) d13 on d1.clientid = d13.clientid and (d13.D360_labdate between d1.day356 and d1.day385)
	 ;
     
create index idx_tmp_rct_patient_lab_dates on tmp_rct_patient_lab_dates(clientid asc, D0_labdate, day0); 

drop temporary table if exists tmp_rct_lab_test_output;
create temporary table tmp_rct_lab_test_output as 
select b.clientid, b.bloodworkdate, b.category, cast(b.measure as decimal(10,4)) as measure from 
(select clientid from tmp_rct_patient_lab_dates) a, 
(
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'HbA1c ' as category, avg(veinhba1c) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all  
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'HDL' as category, avg(hdlCholesterol) as  measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all 
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'LDL' as category, avg(ldlCholesterol) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all 
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'Triglycerides(TG)' as category, avg(triglycerides) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'TG/HDL' as category, cast(avg(triglycerides/hdlCholesterol) as decimal(10,2)) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all 
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'LDL/apoB' as category, cast(avg(ldlCholesterol/apoB) as decimal(10,2)) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all 
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'apoB' as category, avg(apoB) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'apoA1' as category, avg(apoA1) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'cholesterol' as category, avg(cholesterol) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'lipoproteinA' as category, avg(lipoproteinA) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'LpPla2' as category, avg(lpPla2) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'VLDL' as category, avg(vldlCholesterol) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'Non-HDL_cholesterol' as category, cast(avg(cholesterol-hdlCholesterol) as decimal(10,2))  as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'Calculated_LDL' as category, cast(avg(cholesterol-hdlCholesterol-(triglycerides/5)) as decimal(10,2))  as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'sdLDL' as category, cast(avg(0.580*(cholesterol-hdlCholesterol)+0.407*ldlCholesterol-0.719*(cholesterol-hdlCholesterol-(triglycerides/5))-12.05) as decimal(10,2))  as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'TyG' as category, cast(avg((Ln(triglycerides*glucose))/2) as decimal(10,2))  as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'AIP' as category, cast(Avg(Log((triglycerides*0.0113)/(hdlCholesterol*0.0259))) as decimal(10,2))  as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'ALT' as category, avg(alt) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'GGT' as category, avg(gammaGlutamylTransferase) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'ast' as category, avg(ast) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'ast/Alt' as category, avg(astAltRatio) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'TSH' as category, avg(tsh) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'Testosterone' as category, avg(testosterone) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'cortisol' as category, avg(cortisol) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'Homa2B' as category, avg(homa2B) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'Homa2IR' as category, avg(homa2IR) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId))union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'HomaIR' as category, avg(homaIR) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId))union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'cPeptide' as category, avg(cPeptide) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'homa2S' as category, avg(homa2S) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'cystatinC' as category, avg(cystatinC) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'ntProBnp' as category, avg(ntProBnp) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'UricAcid' as category, avg(uricAcid) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'hsCRP' as category, avg(hsCReactive) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'Urine Microalbumin' as category, avg(urineMicroalbumin) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'eGFR' as category, avg(glomerularFiltrationRate) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'serumCreatine' as category, avg(serumCreatine) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'ferritin' as category, avg(ferritin) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'esr' as category, avg(esr) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'whiteBloodCellCount' as category, avg(whiteBloodCells) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'neutrophilsCount' as category, avg(neutrophils) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'Amylase' as category, avg(amylase) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'Lipase' as category, avg(lipase) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'glucose' as category, avg(glucose) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'ketones' as category, avg(ketones) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'fasting_insulin' as category, avg(insulin) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'waist Circumference' as category, avg(waistCircumference) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'platelets' as category, avg(platelets) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'albumin' as category, avg(albumin) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'redCellDistributionWidth' as category, avg(redCellDistributionWidth) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'smoking' as category, avg(useSmoking) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'Alcohol' as category, avg(useAlcohol) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'Potassium' as category, avg(potassium) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'Phosphorous' as category, avg(phosphorous) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'Sodium' as category, avg(sodium) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'Magnesium' as category, avg(magnesium) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'Calcium' as category, avg(calcium) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'SerumIron' as category, avg(serumIron) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'SerumCopper' as category, avg(serumCopper) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'VitaminA' as category, avg(vitaminA) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'VitaminDTotal' as category, avg(vitaminDTotal) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'VitaminE' as category, avg(vitaminE) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'VitaminK' as category, avg(vitaminK) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'VitaminB9' as category, avg(vitaminB9) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) union all
select a.clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId)) as bloodworkdate, 'VitaminB12' as category, avg(vitaminB12) as measure from clinictestresults a inner join v_allclients_rct b on a.clientid = b.clientid where  a.deleted = 0 group by clientid, date(convert_tz(ifnull (a.bloodworktime, a.eventtime ),'UTC',b.timezoneId))
) b where bloodworkdate is not null and a.clientid=b.clientid;

create index idx_tmp_rct_lab_test_output on tmp_rct_lab_test_output(clientid asc, bloodworkdate asc, category, measure); 

drop temporary table if exists tmp_rct_output_score_data;
create temporary table tmp_rct_output_score_data
(
	clientid int null,
	date date not null,
	date_category varchar(7) null,
	category varchar(100) null
);

insert into tmp_rct_output_score_data(clientid, date, date_category, category)
select a.clientid, case when a.bloodworkdate = b.D0_labdate then a.bloodworkdate
						when a.bloodworkdate = b.D30_labdate then a.bloodworkdate
                        -- when a.bloodworkdate = b.D60_labdate then a.bloodworkdate
						when a.bloodworkdate = b.D90_labdate then a.bloodworkdate
                        -- when a.bloodworkdate = b.D120_labdate then a.bloodworkdate
                        -- when a.bloodworkdate = b.D150_labdate then a.bloodworkdate
						when a.bloodworkdate = b.D180_labdate then a.bloodworkdate
                        -- when a.bloodworkdate = b.D210_labdate then a.bloodworkdate
                        -- when a.bloodworkdate = b.D240_labdate then a.bloodworkdate
                        when a.bloodworkdate = b.D270_labdate then a.bloodworkdate
                        -- when a.bloodworkdate = b.D300_labdate then a.bloodworkdate
                        -- when a.bloodworkdate = b.D330_labdate then a.bloodworkdate
                        when a.bloodworkdate = b.D360_labdate then a.bloodworkdate end as date,
case when a.bloodworkdate = b.D0_labdate then 'D0'
	when a.bloodworkdate = b.D30_labdate then 'D30'
    -- when a.bloodworkdate = b.D60_labdate then 'D60'
	when a.bloodworkdate = b.D90_labdate then 'D90'
    -- when a.bloodworkdate = b.D120_labdate then 'D120'
    -- when a.bloodworkdate = b.D150_labdate then 'D150'
	when a.bloodworkdate = b.D180_labdate then 'D180'
    -- when a.bloodworkdate = b.D210_labdate then 'D210'
    -- when a.bloodworkdate = b.D240_labdate then 'D240'
    when a.bloodworkdate = b.D270_labdate then 'D270'
    -- when a.bloodworkdate = b.D300_labdate then 'D300'
    -- when a.bloodworkdate = b.D330_labdate then 'D330'
    when a.bloodworkdate = b.D360_labdate then 'D360' end as date_category,
a.category
from
tmp_rct_lab_test_output a inner join tmp_rct_patient_lab_dates b on (a.clientid = b.clientid and (a.bloodworkdate = b.D0_labdate or
																												 a.bloodworkdate = b.D30_labdate or
                                                                                                                 -- a.bloodworkdate = b.D60_labdate or
                                                                                                                 a.bloodworkdate = b.D90_labdate or
                                                                                                                 -- a.bloodworkdate = b.D120_labdate or
                                                                                                                 -- a.bloodworkdate = b.D150_labdate or
                                                                                                                 a.bloodworkdate = b.D180_labdate or
                                                                                                                 -- a.bloodworkdate = b.D210_labdate or
                                                                                                                 -- a.bloodworkdate = b.D240_labdate or
                                                                                                                 a.bloodworkdate = b.D270_labdate or
                                                                                                                 -- a.bloodworkdate = b.D300_labdate or
                                                                                                                 -- a.bloodworkdate = b.D330_labdate or
                                                                                                                 a.bloodworkdate = b.D360_labdate)
                                                                                                                 ) 										
;



drop temporary table if exists tmp_twin_rct_health_measures_final;

create temporary table tmp_twin_rct_health_measures_final
(
	clientid int not null,
	category varchar(100) not null,
	date date not null,
	date_category varchar(10) not null,
	measure decimal(10,2) null
); 

insert into tmp_twin_rct_health_measures_final (clientid, category, date, date_category, measure)
select a.clientid, a.category, a.date, a.date_category, cast(avg(b.measure) as decimal(10,2)) as measure
from tmp_rct_output_score_data a inner join tmp_rct_lab_test_output b on a.clientid = b.clientid and 
a.date = b.bloodworkdate and a.category = b.category 
group by a.clientid, a.category, a.date, a.date_category having avg(b.measure) is not null
;

-- ----------------------RHR (5day average)
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.eventdate,"D0" as date_category,"RHR" as category,convert(avg(b.avg_rest_hr), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.eventdate) as eventdate
from 
tmp_rct_patient_lab_dates s1 left join  bi_patient_daily_heartrate_summary s2 on (s1.clientid = s2.clientid 
																		and (s2.eventdate between s1.sensor_day1 and s1.sensor_day5) 
                                                                       -- and s2.measure_name = 'Rest-HR' 
                                                                      --  and s2.measure_type = '1d'                        
                                                                        and s2.avg_rest_hr is not null)
where eventdate is not null
group by s1.clientid) as a, bi_patient_daily_heartrate_summary  b
 where 
 a.clientid=b.clientid
 and b.eventdate between a.eventdate and date_add(a.eventdate, INTERVAL 4 DAY)
 -- and b.measure_name = 'Rest-HR' and b.measure_type = '1d' 
 and b.avg_rest_hr is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.eventdate,"D30" as date_category,"RHR" as category, convert(avg(b.avg_rest_hr), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.eventdate) as eventdate
from 
tmp_rct_patient_lab_dates s1 left join bi_patient_daily_heartrate_summary s2 on (s1.clientid = s2.clientid 
																		and (s2.eventdate between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.avg_rest_hr is not null)
where eventdate is not null
group by s1.clientid) as a, bi_patient_daily_heartrate_summary b
 where 
 a.clientid=b.clientid
 and b.eventdate between date_sub(a.eventdate, INTERVAL 4 DAY) and a.eventdate
 and b.avg_rest_hr is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.eventdate,"D90" as date_category,"RHR" as category, convert(avg(b.avg_rest_hr), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.eventdate) as eventdate
from 
tmp_rct_patient_lab_dates s1 left join bi_patient_daily_heartrate_summary s2 on (s1.clientid = s2.clientid 
																		and (s2.eventdate between s1.sensor_day86 and s1.sensor_day90) 
                                                                                           
                                                                        and s2.avg_rest_hr is not null)
where eventdate is not null
group by s1.clientid) as a, bi_patient_daily_heartrate_summary b
 where 
 a.clientid=b.clientid
 and b.eventdate between date_sub(a.eventdate, INTERVAL 4 DAY) and a.eventdate
 and b.avg_rest_hr is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.eventdate,"D180" as date_category,"RHR" as category, convert(avg(b.avg_rest_hr), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.eventdate) as eventdate
from 
tmp_rct_patient_lab_dates s1 left join bi_patient_daily_heartrate_summary s2 on (s1.clientid = s2.clientid 
																		and (s2.eventdate between s1.sensor_day176 and s1.sensor_day180) 
                                                                                            
                                                                        and s2.avg_rest_hr is not null)
where eventdate is not null
group by s1.clientid) as a, bi_patient_daily_heartrate_summary b
 where 
 a.clientid=b.clientid
 and b.eventdate between date_sub(a.eventdate, INTERVAL 4 DAY) and a.eventdate
 and b.avg_rest_hr is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.eventdate,"D270" as date_category,"RHR" as category, convert(avg(b.avg_rest_hr), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.eventdate) as eventdate
from 
tmp_rct_patient_lab_dates s1 left join bi_patient_daily_heartrate_summary s2 on (s1.clientid = s2.clientid 
																		and (s2.eventdate between s1.sensor_day266 and s1.sensor_day270) 
                                                                                            
                                                                        and s2.avg_rest_hr is not null)
where eventdate is not null
group by s1.clientid) as a, bi_patient_daily_heartrate_summary b
 where 
 a.clientid=b.clientid
 and b.eventdate between date_sub(a.eventdate, INTERVAL 4 DAY) and a.eventdate
 and b.avg_rest_hr is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.eventdate,"D360" as date_category,"RHR" as category, convert(avg(b.avg_rest_hr), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.eventdate) as eventdate
from 
tmp_rct_patient_lab_dates s1 left join bi_patient_daily_heartrate_summary s2 on (s1.clientid = s2.clientid 
																		and (s2.eventdate between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.avg_rest_hr is not null)
where eventdate is not null
group by s1.clientid) as a, bi_patient_daily_heartrate_summary b
 where 
 a.clientid=b.clientid
 and b.eventdate between date_sub(a.eventdate, INTERVAL 4 DAY) and a.eventdate
 and b.avg_rest_hr is not null
 group by clientid
 ;
 
 -- -- ----------------------Sys BP (5day average)

 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null 
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
 and b.systolic_BP_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D30" as date_category,"Sys BP" as category,convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D90" as date_category,"Sys BP" as category,convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D180" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D270" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D360" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
-- -- ----------------------Dia BP (5day average)

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
and b.diastolic_BP_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D30" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D90" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D180" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D270" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D360" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 -- -- ----------------------weight (5day average)


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, min(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
 and b.weight_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;
 
-- -- ----------------------BMI (5day average)


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
 and b.bmi_1d is not null
 group by clientid
 ;



insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D30" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D90" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D180" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D270" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D360" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
 -- -- ----------------------visceral Fat (5day average)


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
where a.clientid=b.clientid
and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
and b.visceralFat_1d is not null
group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D30" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D90" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D180" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D270" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D360" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 
 -- -- ----------------------eA1C (5day average)
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "eA1C" as category, convert(avg(b.eA1C_60d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_patient_measures s2 on (s1.clientid = s2.clientid 
																		and s2.measure_event_date = s1.sensor_day1
                                                                        )
where s2.measure_event_date is not null
group by s1.clientid) as a, bi_patient_measures b
 where a.clientid=b.clientid
 and b.measure_event_date = a.measure_event_date
 group by clientid
 ;
 
 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"eA1C" as category, convert(avg(b.eA1C_60d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_patient_measures s2 on (s1.clientid = s2.clientid 
																		and s2.measure_event_date = s1.sensor_day30
                                                                        )
where s2.measure_event_date is not null
group by s1.clientid) as a, bi_patient_measures b
 where a.clientid=b.clientid
 and b.measure_event_date = a.measure_event_date
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"eA1C" as category, convert(avg(b.eA1C_60d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_patient_measures s2 on (s1.clientid = s2.clientid 
																		and s2.measure_event_date = s1.sensor_day90
                                                                        )
where s2.measure_event_date is not null
group by s1.clientid) as a, bi_patient_measures b
 where a.clientid=b.clientid
 and b.measure_event_date = a.measure_event_date
 group by clientid
 ;
 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"eA1C" as category, convert(avg(b.eA1C_60d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_patient_measures s2 on (s1.clientid = s2.clientid 
																		and s2.measure_event_date = s1.sensor_day180
                                                                        )
where s2.measure_event_date is not null
group by s1.clientid) as a, bi_patient_measures b
 where a.clientid=b.clientid
 and b.measure_event_date = a.measure_event_date
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"eA1C" as category, convert(avg(b.eA1C_60d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_patient_measures s2 on (s1.clientid = s2.clientid 
																		and s2.measure_event_date = s1.sensor_day270
                                                                        )
where s2.measure_event_date is not null
group by s1.clientid) as a, bi_patient_measures b
 where a.clientid=b.clientid
 and b.measure_event_date = a.measure_event_date
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"eA1C" as category, convert(avg(b.eA1C_60d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_patient_measures s2 on (s1.clientid = s2.clientid 
																		and s2.measure_event_date = s1.sensor_day360
                                                                        )
where s2.measure_event_date is not null
group by s1.clientid) as a, bi_patient_measures b
 where a.clientid=b.clientid
 and b.measure_event_date = a.measure_event_date
 group by clientid
 ;
 
 -- -- ----------------------GV (5day average)

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5)
                                                                        and s2.GV is not null)	
where s2.date is not null
and s1.cohort='TWIN_PRIME'                                                                     
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
 and b.GV is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 
 --  -- -- ----------------------GMI (5day average)

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5)
                                                                        and s2.GMI is not null)	
where s2.date is not null
and s1.cohort='TWIN_PRIME'                                                                     
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
 and b.GMI is not null
 group by clientid
 ;
 
 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 
 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;

 -- -- ----------------------TIR (7day average)

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day7) 
                                                                        )	
where s2.date is not null
and s1.cohort='TWIN_PRIME'                                                                        
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 6 DAY)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"TIR" as category,
 (case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day24 and s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day84 and s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day174 and s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day264 and s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day354 and s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
  -- -- ----------------------TAR_1 (7day average)

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "TAR_1" as category, 
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day7) 
                                                                        )	
where s2.date is not null
and s1.cohort='TWIN_PRIME'                                                                           
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 6 DAY)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day24 and s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day84 and s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day174 and s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day264 and s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day354 and s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


 -- -- ----------------------TAR_2 (7day average)

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "TAR_2" as category, 
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day7) 
                                                                        )	
where s2.date is not null
and s1.cohort='TWIN_PRIME'                                                                           
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 6 DAY)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day24 and s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day84 and s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day174 and s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"TAR_2" as category,(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day264 and s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day354 and s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 -- -- ----------------------TBR_1 (7day average)

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "TBR_1" as category, 
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day7) 
                                                                        )	
where s2.date is not null
and s1.cohort='TWIN_PRIME'                                                                           
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 6 DAY)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day24 and s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day84 and s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day174 and s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day264 and s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day354 and s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 -- -- ----------------------TBR_2 (7day average)

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "TBR_2" as category, 
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day7) 
                                                                        )	
where s2.date is not null
and s1.cohort='TWIN_PRIME'                                                                           
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 6 DAY)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day24 and s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day84 and s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day174 and s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day264 and s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day354 and s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 -- -- ----------------------diabetic_drugs_count 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.start_medicine_diabetes_drugs) - length(replace(b.start_medicine_diabetes_drugs,',','')) + 1) > 0,  max(length(b.start_medicine_diabetes_drugs) - length(replace(b.start_medicine_diabetes_drugs,',','')) + 1), 0) as measure 
from 
(select s1.clientid, min(s1.sensor_day1 ) as measure_event_date
from tmp_rct_patient_lab_dates s1 left join dim_client s2 on s1.clientid = s2.clientid 
group by s1.clientid
) as a, dim_client b
 where a.clientid=b.clientid 
 and b.is_row_current="Y"
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 -- -- ----------------------HTN_drugs_count 

 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.start_HTN_medicine_drugs) - length(replace(b.start_HTN_medicine_drugs,',','')) + 1)>0, max(length(b.start_HTN_medicine_drugs) - length(replace(b.start_HTN_medicine_drugs,',','')) + 1),0) as measure 
from 
(select s1.clientid, min(s1.sensor_day1 ) as measure_event_date
from tmp_rct_patient_lab_dates s1 left join dim_client s2 on s1.clientid = s2.clientid	
group by s1.clientid
) as a, dim_client b
 where a.clientid=b.clientid
and b.is_row_current="Y"
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ; 
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 -- -- ----------------------is_on_metformin_only
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.start_medicine_diabetes_drugs)) = 'METFORMIN' or ltrim(rtrim(b.start_medicine_diabetes_drugs)) = 'Biguanide(DIABETES)' then 1 else 0 end) as measure
from (
select s1.clientid, min(s1.sensor_day1 ) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join dim_client s2 on s1.clientid = s2.clientid 															
group by s1.clientid
) as a, dim_client b
 where  a.clientid=b.clientid
 and b.is_row_current="Y"
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;

 -- -- ----------------------is_on_insulin


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "is_on_insulin" as category, 
(case when ltrim(rtrim(b.start_medicine_diabetes_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, min(s1.sensor_day1 ) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join dim_client s2 on s1.clientid = s2.clientid 															
group by s1.clientid
) as a, dim_client b
 where a.clientid=b.clientid
 and b.is_row_current="Y"
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day30) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day90) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day180) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day270) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;



insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day360) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


create index idx_tmp_twin_rct_health_measures_final on tmp_twin_rct_health_measures_final(clientid asc, category, date asc, date_category); 


insert into bi_twins_rct_health_output_measures
(clientid, category, date, date_category, measure)
select clientid, category, date, date_category, measure
from tmp_twin_rct_health_measures_final a 
where not exists (select 1 from bi_twins_rct_health_output_measures b where a.clientid = b.clientid and a.date = b.date and a.date_category = b.date_category and a.category = b.category)
;

update bi_twins_rct_health_output_measures a inner join tmp_twin_rct_health_measures_final b on (a.clientid = b.clientid and a.date = b.date and a.date_category = b.date_category and a.category = b.category)
set a.measure = b.measure
;

drop temporary table if exists tmp_twin_rct_health_measures_final
;

drop temporary table if exists tmp_rct_output_score_data;


update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_bi_twins_rct_health_output_measures'
;


END