CREATE TABLE `bi_doctor_measure_x_scorecard` (
  `report_date` date NOT NULL,
  `doctorId` varchar(50) NOT NULL,
  `measure_name` varchar(50) NOT NULL,
  `measure_group` varchar(50) NOT NULL,
  `measure` decimal(7,2) DEFAULT NULL,
  PRIMARY KEY (`report_date`,`doctorId`,`measure_name`,`measure_group`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
