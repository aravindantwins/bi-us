CREATE TABLE `bi_doctor_measure_x_scorecard_30d` (
  `report_date` date NOT NULL,
  `doctorId` varchar(25) NOT NULL,
  `measure_name` varchar(50) NOT NULL,
  `measure_group` varchar(50) NOT NULL,
  `measure` decimal(7,2) DEFAULT NULL,
  PRIMARY KEY (`report_date`,`doctorId`,`measure_name`,`measure_group`),
  KEY `idx_bi_doctor_measure_x_scorecard_30d` (`doctorId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
