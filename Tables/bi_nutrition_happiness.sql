drop table bi_nutrition_happiness; 
create table bi_nutrition_happiness
(
	clientid int not null,
	date date not null,
	foodRating_avg decimal(6,1) null,
	foodRating_flag char(5) null,
	mealRating_avg decimal(6,1) null,
    mealRating_goodOkay_percentage decimal(6,2) null,
	mealRating_flag char(5) null,
	nutritionFriction_flag char(5) null,
	nut_happy char(5) null,
	primary key pk_nut_happiness (clientid asc, date asc)
);

create index idx_bi_nutrition_happiness on bi_nutrition_happiness(date asc);