CREATE TABLE bi_twins_rct_health_output_measures (
  clientid int NOT NULL,
  date date NOT NULL,
  date_category varchar(7) NOT NULL,
  category varchar(100) NOT NULL,
  measure decimal(10,4) DEFAULT NULL,
  PRIMARY KEY (clientid,date,date_category,category)
) 