-- set collation_connection = 'latin1_swedish_ci'

alter view v_active_clients as 
SELECT 
        c.ID AS clientId,
        UPPER(CONCAT(cp.firstName, ' ', cp.lastName)) AS patientName,
        UPPER(CONCAT(cp.firstName, ' ', LEFT(cp.lastName, 1))) AS patientNameShort,
		ap.city AS city,
		d.ID as doctorID,
        UPPER(CONCAT(d.firstName, ' ', d.lastName)) AS doctorName,
        UPPER(CONCAT(d.firstName, ' ', LEFT(d.lastName, 1))) AS doctorNameShort,
        h.id as coachID, 
        UPPER(CONCAT(h.firstName, ' ', h.lastName)) AS coachName,
        UPPER(CONCAT(h.firstName, ' ', LEFT(h.lastName, 1))) AS coachNameShort,
        c.gender,
        cp.userName AS userName,
        ((YEAR(CURDATE()) - YEAR(cp.dateOfBirth)) - (DATE_FORMAT(CURDATE(), '%m%d') < DATE_FORMAT(cp.dateOfBirth, '%m%d'))) AS age,
        c.enrollmentDate AS enrollmentDate,
        c.paidType AS paidType,
        t.name AS cohortGroup,
		c.enrollmentDate AS enrollmentDateITZ,
        (vs.VisitDate) AS visitDateITZ,
        v.homeVisitDate as homeVisitDateITZ,
        v.dateAdded AS visitDateAddedITZ,
        c.renewalVisitScheduled AS renewalVisitScheduledDate,
        c.dischargeReason AS dischargeReason,
		CASE
            WHEN (c.status = 'Active') THEN (TO_DAYS(ITZ(NOW())) - TO_DAYS(c.enrollmentDate))
            WHEN (c.status <> 'Active') THEN (TO_DAYS(c.lastStatusChange) - TO_DAYS(c.enrollmentDate))
        END AS enrollmentDates, 
        null as renewalCount,
        c.firstDiabetesInReversalTime, 
        c.termEndDate AS termEndDate,
        c.planCode AS planCode,
        c.labels,
        c.vegetarian,
        c.timeZoneID,
        c.notes
        
    FROM
       twins.clients c
       inner join twinspii.clientspii cp on c.id = cp.id
       LEFT JOIN twinspii.addressespii ap on c.addressId = ap.ID
        JOIN twins.doctors d ON c.doctorId = d.ID
        INNER JOIN twins.coaches h ON c.coachId = h.ID
        LEFT JOIN (
				select a.clientid, a.date_of_visit, a.homevisitdate, a.dateadded from 
                (
						SELECT 	clientId,
								DATE(visitDate) as date_of_visit,
								MAX(visitDate) AS homeVisitDate,
								MAX(DateAdded) AS dateAdded
						FROM homevisits
						GROUP BY clientId, date(visitdate) -- there are multiple entries for the same home schedule date so pick the one needed (most recent)
                ) a
                inner join (select clientid, MAX(DATE(visitdate)) as date_of_visit from homevisits group by clientid) b on a.clientid = b.clientid and a.date_of_visit = b.date_of_visit -- (pick the most recent schedule date)
        ) v ON (c.ID = v.clientId)
        LEFT JOIN cohorts t ON (c.cohortId = t.cohortId)
        LEFT JOIN  (
				SELECT 
					clientId AS CLIENTID,
					TIMESTAMP(MAX(startDate)) AS VisitDate
				FROM
					twins.momentumweekdetails
				GROUP BY clientId
        ) vs on c.id = vs.clientid 
        LEFT JOIN dim_client dc on c.id = dc.clientid and dc.is_row_current = 'y'
    WHERE
        ((c.status IN ('ACTIVE'))
            AND (c.deleted = 0)
            AND (d.test = FALSE))