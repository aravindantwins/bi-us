-- set collation_connection = 'latin1_swedish_ci';

    create view v_allClient_list as 
    SELECT 
        `c`.`ID` AS `clientId`,
        up.id as userId, 
        date(now()) as date,
        UPPER(CONCAT(`cp`.`firstName`, ' ', `cp`.`lastName`)) AS `patientName`,
        UPPER(CONCAT(`cp`.`firstName`,
                        ' ',
                        LEFT(`cp`.`lastName`, 1))) AS `patientNameShort`,
        `cp`.`dateOfBirth`,                
        ((YEAR(CURDATE()) - YEAR(`cp`.`dateOfBirth`)) - (DATE_FORMAT(CURDATE(), '%m%d') < DATE_FORMAT(`cp`.`dateOfBirth`, '%m%d'))) AS `age`,
        d.id as doctorId, 
        UPPER(CONCAT(`d`.`firstName`, ' ', `d`.`lastName`)) AS `doctorName`,
        UPPER(CONCAT(`d`.`firstName`,
                        ' ',
                        LEFT(`d`.`lastName`, 1))) AS `doctorNameShort`,
		d.clinicName,
        UPPER(CONCAT(`h`.`firstName`, ' ', `h`.`lastName`)) AS `coachName`,
        UPPER(CONCAT(`h`.`firstName`,
                        ' ',
                        LEFT(`h`.`lastName`, 1))) AS `coachNameShort`,
		UPPER(CONCAT(up1.firstname, ' ', up1.lastname)) as pha, 
        c.gender,
        `c`.`dateAdded` AS `dateAdded`,
        `c`.`dateModified` AS `dateModified`,
        `c`.`status` AS `status`,
        `t`.`name` AS `cohortGroup`,
        `c`.`enrollmentDate` AS `enrollmentDate`,
        `c`.`lastStatusChange` AS `lastStatusChange`,
        `c`.`renewalVisitScheduled` AS `renewalVisitScheduledDate`,
        `c`.`dischargeReason` AS `dischargeReason`,
        (CASE
            WHEN (`c`.`status` = 'Active') THEN (TO_DAYS(NOW()) - TO_DAYS(`c`.`enrollmentDate`))
            WHEN (`c`.`status` <> 'Active') THEN (TO_DAYS(`c`.`lastStatusChange`) - TO_DAYS(`c`.`enrollmentDate`))
        END) AS `daysEnrolled`,
        (CASE
            WHEN (`c`.`status` = 'Active') THEN (FLOOR(((TO_DAYS(`c`.`termEndDate`) - TO_DAYS(`c`.`enrollmentDate`)) / 84)) - 1)
            WHEN (`c`.`status` = 'INACTIVE') THEN FLOOR(((TO_DAYS(`c`.`lastStatusChange`) - TO_DAYS(`c`.`enrollmentDate`)) / 84))
        END) AS `renewalCount`,
        `c`.`firstDiabetesInReversalTime` AS `firstDiabetesInReversalTime`,
        `c`.`termEndDate` AS `termEndDate`,
        `c`.`planCode` AS `planCode`,
        c.labels,
        c.sourceType as source,
        c.timezoneId,
		c1.customerType,
        c1.name as customerName,
        c.notes
    FROM
        ((((`twins`.`clients` `c`
        JOIN `twinspii`.`clientspii` `cp` ON ((`c`.`ID` = `cp`.`ID`)))
        left JOIN twinspii.userspii up on c.id = up.clientid 
        JOIN `twins`.`doctors` `d` ON ((`c`.`doctorId` = `d`.`ID`)))
        LEFT JOIN `twins`.`coaches` `h` ON ((`c`.`coachId` = `h`.`ID`)))
        LEFT JOIN `twins`.`cohorts` `t` ON ((`c`.`cohortId` = `t`.`cohortId`))
        LEFT JOIN twins.clientauxfields ca on c.id = ca.id
        left join twinspii.userspii up1 on ca.counselorUserId = up1.id
		left join twins.customers c1 on c.customerID = c1.ID
        )
    WHERE
        ((`c`.`status` IN ('ACTIVE' , 'REGISTRATION', 'INACTIVE', 'DISCHARGED', 'PENDING_ACTIVE', 'PROSPECT'))
            AND (`c`.`deleted` = 0)
            AND (`d`.`test` = FALSE)
            AND c.test = FALSE
            AND (NOT ((`cp`.`firstName` LIKE '%TEST%')))
            AND (NOT ((`cp`.`lastName` LIKE '%TEST%'))))
    ORDER BY `c`.`enrollmentDate`