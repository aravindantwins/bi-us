-- set collation_connection = 'latin1_swedish_ci';

alter  view v_allClients_RCT as 
select c.id as clientid, 
        UPPER(CONCAT(`cp`.`firstName`, ' ', `cp`.`lastName`)) AS `patientName`,
        UPPER(CONCAT(`cp`.`firstName`,
                        ' ',
                        LEFT(`cp`.`lastName`, 1))) AS `patientNameShort`,
        `cp`.`dateOfBirth`,                
        ((YEAR(CURDATE()) - YEAR(`cp`.`dateOfBirth`)) - (DATE_FORMAT(CURDATE(), '%m%d') < DATE_FORMAT(`cp`.`dateOfBirth`, '%m%d'))) AS `age`,
        d.id as doctorId, 
        LTRIM(RTRIM(UPPER(CONCAT(`d`.`firstName`, ' ', `d`.`lastName`)))) AS `doctorName`,
        LTRIM(RTRIM(UPPER(CONCAT(`d`.`firstName`,
                        ' ',
                        LEFT(`d`.`lastName`, 1))))) AS `doctorNameShort`,
		d.clinicName,
        UPPER(CONCAT(`h`.`firstName`, ' ', `h`.`lastName`)) AS `coachName`,
        UPPER(CONCAT(`h`.`firstName`,
                        ' ',
		LEFT(`h`.`lastName`, 1))) AS `coachNameShort`,
        TO_DAYS(NOW()) - TO_DAYS(`c`.`enrollmentDate`) as daysEnrolled,
		c.labels, 
        c.status, 
        c.gender,
        date(convert_tz(c.enrollmentdate, 'UTC', c.timeZoneId)) as enrollmentdate,
        c.sourceType,
		cu.name as customerName,
        dc.durationYears_diabetes,
        c.timeZoneID
from clients c 
			JOIN `twinspii`.`clientspii` `cp` ON ((`c`.`ID` = `cp`.`ID`))
			JOIN `twins`.`doctors` `d` ON ((`c`.`doctorId` = `d`.`ID`))
			LEFT JOIN `twins`.`coaches` `h` ON ((`c`.`coachId` = `h`.`ID`))
            left join dim_client dc on c.id = dc.clientid 
            left join customers cu on c.customerId = cu.ID 
WHERE (
			`c`.`deleted` = 0
			and dc.is_row_current = 'y'
			and cu.name like '%cleveland%'
			and c.status in ('Active', 'PENDING_ACTIVE', 'DISCHARGED', 'INACTIVE')
	   );
       
       
	  
	  
	  
	  SELECT 