set collation_connection = 'latin1_swedish_ci';

alter view v_bi_Aetna_BHI_performance_member_detail as 

select s.clientid, s.customername, s.patientname, s.doctorname, s.coachname, s.member_status, s.durationYears_diabetes, s.gender, s.postalCode, s.dateOfBirth, s.TA_date, s.treatmentdays, 
s.start_medicines_without_Metformin, s.start_medicine_diabetes, s.current_medicines_without_Metformin, s.medicine_drugs, s.medicine,
s.start_LabA1c, s.start_BMI, s.d90_LabA1c, s.d180_LabA1c, s.d270_LabA1c, s.d360_LabA1c,
s.starRating_28d, s.starRating_28d_count,
s.totalMedicationCards_Completion_28d, 
s.totalMedicationCards_28d_count,
s.cgm_60d,
s.eA1C_60d,
s.start_symptoms,
s.symptoms,
s.isInReversal,
s.isMetforminOnlyReversal,

TA90_starRating_avg,
TA90_starRating_cnt,
TA180_starRating_avg,
TA180_starRating_cnt,
TA270_starRating_avg,
TA270_starRating_cnt,
TA365_starRating_avg,
TA365_starRating_cnt,
total_med_adhering_days,
total_med_available_days,
total_med_adhering_days_D1_180,
total_med_available_days_D1_180,
total_med_adhering_days_D181_365,
total_med_available_days_D181_365,

d180_medicine_excluding_metformin,
d365_medicine_excluding_metformin,

length(d180_medicine_excluding_metformin) - length(replace(d180_medicine_excluding_metformin,',','')) + 1 as D180_medicines_without_Metformin, 
length(d365_medicine_excluding_metformin) - length(replace(d365_medicine_excluding_metformin,',','')) + 1 as D365_medicines_without_Metformin


from 

(
		select a.clientid, customername, patientname, doctorname, coachname, a.status as member_status, durationYears_diabetes, a.gender, a.postalCode, a.dateOfBirth, programStartDate_analytics as TA_date, a.treatmentdays, 
		a.start_meds_wo_metformin_unformatted_count as start_medicines_without_Metformin,
		a.start_meds_unformatted as start_medicine_diabetes,
		/*
		start_medicines_without_Metformin,
		start_medicine_diabetes,  
		start_medicine_diabetes_drugs, 
		if(bm.medicine_drugs like '%Biguanide(DIABETES)%', 
			(length(bm.medicine_drugs) - length(replace(bm.medicine_drugs,',','')) + 1) - 1, 
		    length(bm.medicine_drugs) - length(replace(bm.medicine_drugs,',','')) + 1) as current_medicines_without_Metformin, 
		*/
		
		length(bm.medicine_excl_metformin) - length(replace(bm.medicine_excl_metformin,',','')) + 1 as current_medicines_without_Metformin, 
		bm.medicine_drugs, 
		bm.medicine,
		start_LabA1c,
		start_BMI,
		max(case when a.d90_date_lab = date(ifnull(bloodworktime, eventtime)) then veinHba1c else null end) as d90_LabA1c,
		max(case when a.d180_date_lab = date(ifnull(bloodworktime, eventtime)) then veinHba1c else null end) as d180_LabA1c,
		max(case when a.d270_date_lab = date(ifnull(bloodworktime, eventtime)) then veinHba1c else null end) as d270_LabA1c,
		max(case when a.d360_date_lab = date(ifnull(bloodworktime, eventtime)) then veinHba1c else null end) as d360_LabA1c,
		bm.starRating_28d,
		bm.starRating_28d_cnt as starRating_28d_count,
		bm.totalMedicationCards_Completion_28d, 
		bm.totalMedicationCards_28d as totalMedicationCards_28d_count,
		bm.cgm_60d,
		cast((bm.cgm_60d+ 46.7)/28.7 as decimal(6,2)) as eA1C_60d,
		a.start_symptoms,
		bm.symptoms,
		bm.is_InReversal as isInReversal,
		bm.is_Metonly_by_ops AS isMetforminOnlyReversal
		from v_client_tmp a left join bi_patient_measures bm on a.clientid = bm.clientid and bm.measure_event_date = date_sub(date(now()), interval 1 day)
							left join clinictestresults ct on a.clientid = ct.clientid and (a.d90_date_lab = date(ifnull(bloodworktime, eventtime)) or a.d180_date_lab = date(ifnull(bloodworktime, eventtime)) or a.d270_date_lab = date(ifnull(bloodworktime, eventtime)) or a.d360_date_lab = date(ifnull(bloodworktime, eventtime)))
		where a.customerName = 'Aetna Better Health Illinois'
		and a.status in ('Active', 'Discharged')
		group by a.clientid 
) s left join 
		(
				select a.clientid, 
				avg(case when a.treatmentdays between 1 and 90 then starRating_1d else null end) as TA90_starRating_avg, 
				count(case when a.treatmentdays between 1 and 90 then starRating_1d else null end) as TA90_starRating_cnt, 
				
				avg(case when a.treatmentdays between 91 and 180 then starRating_1d else null end) as TA180_starRating_avg, 
				count(case when a.treatmentdays between 91 and 180 then starRating_1d else null end) as TA180_starRating_cnt, 
				
				avg(case when a.treatmentdays between 181 and 270 then starRating_1d else null end) as TA270_starRating_avg, 
				count(case when a.treatmentdays between 181 and 270 then starRating_1d else null end) as TA270_starRating_cnt, 
				
				avg(case when a.treatmentdays between 271 and 365 then starRating_1d else null end) as TA365_starRating_avg,
				count(case when a.treatmentdays between 271 and 365 then starRating_1d else null end) as TA365_starRating_cnt,
				
				count(distinct case when medicine_drugs is not null and med_adh = 'yes' then measure_event_date else null end) as total_med_adhering_days,
				count(distinct case when medicine_drugs is not null then measure_event_date else null end) as total_med_available_days,
				
				
				count(distinct case when a.treatmentdays BETWEEN 1 AND 180 AND medicine_drugs is not null and med_adh = 'yes' then measure_event_date else null end) as total_med_adhering_days_D1_180,
				count(distinct case when a.treatmentdays BETWEEN 1 AND 180 and medicine_drugs is not null then measure_event_date else null end) as total_med_available_days_D1_180,
				
				count(distinct case when a.treatmentdays BETWEEN 181 AND 365 AND medicine_drugs is not null and med_adh = 'yes' then measure_event_date else null end) as total_med_adhering_days_D181_365,
				count(distinct case when a.treatmentdays BETWEEN 181 AND 365 and medicine_drugs is not null then measure_event_date else null end) as total_med_available_days_D181_365,
				
				max(case when a.treatmentdays = 180 then a.medicine_excl_metformin else null end) as d180_medicine_excluding_metformin,
				max(case when a.treatmentdays = 365 then a.medicine_excl_metformin else null end) as d365_medicine_excluding_metformin
				

				from bi_patient_measures a inner join v_client_tmp b on a.clientid = b.clientid 
				where b.customerName = 'Aetna Better Health Illinois'
				and b.status in ('Active', 'Discharged')
				
				-- where clientid IN (SELECT id FROM clients WHERE customerId = (SELECT customerID FROM customers WHERE name LIKE '%Aetna%') and status in ('Active', 'Discharged') and test is false)
				group by a.clientid
		) s1 on s.clientid = s1.clientid 
		;
		
	
