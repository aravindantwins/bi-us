alter view v_bi_CBG_eligible_list as 
select * from
(select C.clientId, C.source, C.source_2, c.customerName, c.patientname, c.coachnameShort as coach, c.treatmentdays, 
     -- (select score from scores S where scoreType='CGM' and S.clientid = C.id order by eventTime desc limit 1) latest1DG,
     (select count(distinct FL.mealdate)
     from foodlogs FL where FL.clientid = C.clientid and mealType IN ('BREAKFAST', 'LUNCH', 'DINNER') and FL.mealtime > DATE_SUB(CURDATE(), INTERVAL 14 DAY)) foodLogDays
from v_client_tmp C join clientauxfields CA on C.clientid = CA.id 
where C.status = 'ACTIVE' and (C.currentDiabetesInReversal = true OR CA.currentDiabetesMetforminReversal = true) 
and (C.latestDiabetesInReversalTime < DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL 7 DAY) OR CA.latestDiabetesMetforminReversalTime < DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL 7 DAY))
and C.activationDate <= DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL 35 DAY)) X
where X.foodLogDays >= 10;

