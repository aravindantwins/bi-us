-- set collation_connection = 'latin1_swedish_ci'

alter view v_bi_active_patient_lab_due_alert as 
select a.date,a.clientid, a.patientname, city, timezoneID, a.labels, a.firstbloodworkDate, a.Labdue, a.LabDueDate, a.isDisenrollOpen, 
a.bloodwork_Scheduled_date, a.bloodwork_Scheduled_Status, 
if(b.clientid is not null, 'Yes', '') as isBloodReport_present,
if(b.clientid is not null, convert_tz(bloodworkTime,'UTC', timezoneId), '') as bloodWorkTime, 
planCode,
startDate, programStartDate_analytics
from
(
    select s.date, s.clientid, s.patientname, city, timezoneID, s.labels, s.firstbloodworkDate, s.LabDue, s.LabDueDate, s.isDisenrollOpen, planCode, startDate, programStartDate_analytics, 
	case when s.labDue = 'D90' then D90_BloodWork_Scheduled_Date
		 when s.labDue = 'D180' then D180_BloodWork_Scheduled_Date
		 when s.labDue = 'D270' then D270_BloodWork_Scheduled_Date
		 when s.labDue = 'D360' then D360_BloodWork_Scheduled_Date else null end as bloodwork_Scheduled_date,
	case when s.labDue = 'D90' then D90_bloodwork_status
		 when s.labDue = 'D180' then D180_bloodwork_status
		 when s.labDue = 'D270' then D270_bloodwork_status
		 when s.labDue = 'D360' then D360_bloodwork_status else null end as bloodwork_Scheduled_Status
	from 
	(
		select date, clientid, patientname, city, timezoneID, labels, firstbloodworkDate,planCode, startDate, programStartDate_analytics, 
        case when a.date = Day89_labdate then Day89_labdate
			 when a.date = Day179_labdate then Day179_labdate
			 when a.date = Day269_labdate then Day269_labdate
			 when a.date = Day359_labdate then Day359_labdate
			 when a.date = Day539_labdate then Day539_labdate
			 when a.date = Day719_labdate then Day719_labdate end as LabDueDate,
		case when a.date = Day89_labdate then 'D90' 
			 when a.date = Day179_labdate then 'D180'
			 when a.date = Day269_labdate then 'D270'
			 when a.date = Day359_labdate then 'D360'
			 when a.date = Day539_labdate then 'D540'
			 when a.date = Day719_labdate then 'D720' end as LabDue, isDisenrollOpen
		from v_date a left join 
		(
				select clientid, patientname, labels, timezoneID, city, firstBloodWorkDate, planCode, startDate, programStartDate_analytics, 
				date_add(startDate, interval 89 day) as Day89_labdate,
				date_add(startDate, interval 90 day) as Day90_labdate,
				date_add(startDate, interval 179 day) as Day179_labdate,
				date_add(startDate, interval 180 day) as Day180_labdate,
				date_add(startDate, interval 269 day) as Day269_labdate,
				date_add(startDate, interval 270 day) as Day270_labdate,
				date_add(startDate, interval 359 day) as Day359_labdate,
				date_add(startDate, interval 360 day) as Day360_labdate,
				date_add(startDate, interval 539 day) as Day539_labdate,
				date_add(startDate, interval 540 day) as Day540_labdate,
				date_add(startDate, interval 719 day) as Day719_labdate,
				date_add(startDate, interval 720 day) as Day720_labdate,
				isDisenrollOpen
				from 
				(
					select clientid, patientname, timeZoneID, labels, firstbloodworkDate, city, planCode, programStartDate_analytics, 
					programStartDate_analytics as startDate, isDisenrollOpen
					from 
					(
							select a.clientid, patientname, timeZoneID, labels, city, 
							date(firstbloodworkDate) as firstBloodWorkDate, a.planCode, programStartDate_analytics, 
							if(c.clientid is not null, 'Yes', 'No') as isDisenrollOpen
							from v_active_clients a inner join dim_Client b on a.clientid = b.clientid 
													left join 
													(
															select distinct clientid, max(date(itz(report_time))) as openDate, max(date(ifnull(resolved_time, now()))) as resolvedDate 
															from bi_incident_mgmt where category = 'Disenrollment' 
															group by clientid
													) c on a.clientid = c.clientid and date(now()) between opendate and ifnull(resolvedDate,date(now()))
							where b.is_row_current = 'y'
					) s 
				) s 
		) s on a.date = Day89_labdate or a.date = Day179_labdate or a.date = Day269_labdate or a.date = Day359_labdate or a.date = Day539_labdate or a.date = Day719_labdate
		where date >= date_sub(date(now()), interval 1 month) and date <= date_add(date(now()), interval 14 day) 
	) s left join 
	(
				select a.clientid, 
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D90' then a.eventLocalTime else null end) as D90_BloodWork_Scheduled_Date, 
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D90' then a.status else null end) as D90_bloodwork_status,                        

				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D180' then a.eventLocalTime else null end) as D180_BloodWork_Scheduled_Date, 
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D180' then a.status else null end) as D180_bloodwork_status,
							
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D270' then a.eventLocalTime else null end) as D270_BloodWork_Scheduled_Date, 
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D270' then a.status else null end) as D270_bloodwork_status,
							
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D360' then a.eventLocalTime else null end) as D360_BloodWork_Scheduled_Date, 
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D360' then a.status else null end) as D360_bloodwork_status
						   
				from clientappointments a inner join bloodworkschedules c on a.clientid = c.clientid and a.clientAppointmentId = c.clientAppointmentId
				where a.type = 'BLOOD_WORK'
				group by a.clientid 
	) s1 on s.clientid = s1.clientid 
) a
left join 
(			
	select clientid, bloodworktime, eventtime from clinictestresults where deleted = 0 
) b on a.clientid = b.clientid and date(ifnull(b.bloodWorkTime, b.eventTime)) between date_add(date(a.bloodwork_Scheduled_date), interval -1 day) and date_add(date(a.bloodwork_Scheduled_date), interval 10 day)
;

