set collation_connection = 'latin1_swedish_ci';

alter view v_bi_all_active_members_coach_ops_detail as 
select a.clientid, customername, patientname, coachname, doctorname, a.status as member_status, durationYears_diabetes, 
a.gender, a.postalCode, a.dateOfBirth, programStartDate_analytics as TA_date, a.treatmentdays, 
a.start_meds_unformatted as start_medicine_diabetes,
length(a.start_meds_unformatted) - length(replace(a.start_meds_unformatted,',','')) + 1 as start_medicines_cnt, 
bm.medicine as current_medicine,
length(bm.medicine) - length(replace(bm.medicine,',','')) + 1 as current_medicines_cnt, 
bm.medicine_drugs, 
start_LabA1c,
bm.cgm_5d,
bm.cgm_60d,
if(bm.is_InReversal = 'Yes' or bm.is_MetOnly_by_OPS = 'Yes', 'Yes', 'No') as isReversal, 
cast((bm.cgm_60d+ 46.7)/28.7 as decimal(6,2)) as eA1C_60d,
case when bm.nut_adh_syntax_5d >= 0.8 then 'Yes' else 'No' end as nut_adh_5d,
bm.TAC_5d,
bm.med_adh, 
bm.recentLabDate,
bm.recentLabA1C
/*
case when cgm_5d is null then null 
	 when (a.firstDiabetesInReversalTime is not null or latestDiabetesInReversalTime is not null or firstDiabetesMetforminReversalTime is not null or latestDiabetesMetforminReversalTime is not null) and (bm.is_InReversal = 'yes' or bm.is_MetOnly_by_OPS = 'yes') and cgm_5d >= 135 and cgm_5d <= 139.9 then 'Yes' else null end as isRiskRelapse,
	 
case when (a.firstDiabetesInReversalTime is not null or latestDiabetesInReversalTime is not null or firstDiabetesMetforminReversalTime is not null or latestDiabetesMetforminReversalTime is not null) and bm.medicine_drugs is null and cgm_5d > 140 then 'Yes' else null end as isRelapse
*/

from v_client_tmp a left join bi_patient_measures bm on a.clientid = bm.clientid and bm.measure_event_date = date_sub(date(now()), interval 1 day)
					left join clientauxfields c on a.clientid = c.id 
where a.status in ('Active')
;