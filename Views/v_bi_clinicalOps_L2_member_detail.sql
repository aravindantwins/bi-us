create view v_bi_clinicalOps_L2_member_detail as 
select a.clientid,a.patientName,a.doctorNameShort,a.age,a.durationYears_diabetes, 
a.programStartDate_analytics as TA_Date, a.treatmentdays, a.source_2 as client_type, a.customerName, a.coachNameShort as Coach,
a.start_conditions,
IFNULL(a.start_medicines, 0) AS start_medicine, 
c.current_medicine,c.Baseline_NUT_TAC,c.Current_NUT_TAC,
b.Steps_per as Steps_Reporting_Percentage,
b.Sleep_per as Sleep_Reporting_Percentage,
c.Activity_steps_5d,c.Sleeping_Reporting_5d,c.Coatch_Rating_5d,c.Energy_Rating_5d,c.Mood_Rating_5d,c.Meal_Rating_5d,c.Nut_Happy_5d 
from v_client_tmp a
left join 
(
		select clientid,  count(measure_event_date) as Days_in_Program,
        cast(count(IF((steps > 0), measure_event_date,null))/count(measure_event_date) as decimal(5,2)) AS Steps_per,
		cast(count(IF((sleep_duration_minutes > 0),measure_event_date,null))/count(measure_event_date) as decimal(5,2)) AS Sleep_per
		from bi_patient_measures  -- where clientid=57602 
		group by clientid
) b on a.clientid=b.clientid 
left join 
(
		select a.clientid,
		max(case when a.end_date=b.measure_event_date then ((LENGTH(b.medicine_drugs) - LENGTH(REPLACE(b.medicine_drugs, ',', ''))) + 1) else null end )AS current_medicine,
		max(case when a.start_date=b.measure_event_date then b.TAC_1d else null end) as Baseline_NUT_TAC,
		max(case when a.end_date=b.measure_event_date then b.TAC_1d else null end ) as Current_NUT_TAC,
		max(case when a.end_date=b.measure_event_date then b.steps_5d else null end) as Activity_steps_5d,
		max(case when a.end_date=b.measure_event_date then b.sleep_duration_minutes_5d else null end) as Sleeping_Reporting_5d, 
		max(case when a.end_date=b.measure_event_date then  b.starRating_5d else null end) as Coatch_Rating_5d,
		max(case when a.end_date=b.measure_event_date then b.energy_5d else null end) as Energy_Rating_5d,
		max(case when a.end_date=b.measure_event_date then b.mood_5d else null end )as Mood_Rating_5d,
		max(case when a.end_date=c.date then c.mealRating_avg_5d else null end) as Meal_Rating_5d,
		max(case when a.end_date=c.date then c.nut_happy_5d else null end) as Nut_Happy_5d
		from
		(
			select clientid, min(measure_event_date) as start_date, max(measure_event_date) as end_date from bi_patient_measures
			group by clientid
		) a
		inner join bi_patient_measures b on a.clientid=b.clientid and (a.start_date=b.measure_event_date or a.end_date = b.measure_event_date)
		left join bi_nutrition_happiness c on a.clientid=c.clientid and (a.end_date = c.date)
		group by a.clientid
) c on a.clientid=c.clientid

where a.status='active'
;

