alter view v_bi_clinicalOps_L2_metrics as 
select b.clientid, a.date, c.patientname, c.doctornameShort as doctor, c.coachnameShort as coach, c.age, c.durationYears_diabetes, c.source_2 as client_partner, c.customerName, d.treatmentdays, d.cgm_5d, 
ifnull(start_medicines,0) as start_medicine, 
(length(d.medicine_drugs) - length(replace(d.medicine_drugs,',',''))) + 1 as current_medicine,
d.is_InReversal, d.is_MetOnly_by_OPS, 
tac_1d, 
starRating_1d as coachRating, 
nut_sat, symptoms, energy, mood, if(steps > 0, 'Yes', 'No') as isStepsLogged, if(sleep_duration_minutes > 0, 'Yes', 'No') as isSleepLogged,
e.mealRating_avg as mealRating,
e.nut_happy
from v_date_to_date a inner join (
									select clientid, max(status_start_date) as status_start_date, max(status_end_date) as status_end_date 
									from bi_patient_status b 
									where b.status = 'active'
                                    group by clientid
                                  ) b on a.date between b.status_start_date and b.status_end_date 
					  inner join v_Client_tmp c on b.clientid = c.clientid
                      left join bi_patient_measures d on b.clientid = d.clientid and a.date = d.measure_event_date
                      left join bi_nutrition_happiness e on b.clientid = e.clientid and a.date = e.date
where patientname not like '%obsolete%'
;

