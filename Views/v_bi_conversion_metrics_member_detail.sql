drop view v_bi_conversion_metrics_member_detail;

set collation_connection = 'latin1_swedish_ci'; 


create view v_bi_conversion_metrics_member_detail as 
select a.clientid, b.patientnameShort as patient, b.status as current_status, date(b.dateadded) as memberAddedDate, b.dateOfBirth as dob, b.coachnameshort as coach, b.doctornameShort as doctor, b.customerName,
				   max(case when type = 'INTRO_TO_TWIN' and a.status = 'SCHEDULED' then convert_tz(a.datemodified, 'UTC', b.timezoneId) else null end) as itt_scheduled_time,
				   max(case when type = 'INTRO_TO_TWIN' and a.status = 'CANCELLED' then convert_tz(a.datemodified, 'UTC', b.timezoneId) else null end) as itt_cancelled_time,
                   max(case when type = 'INTRO_TO_TWIN' and a.status = 'COMPLETED' then convert_tz(a.datemodified, 'UTC', b.timezoneId) else null end) as itt_completed_time,
                   max(case when type = 'INTRO_TO_TWIN' then eventLocalTime else null end) as itt_appointment_time,
                   
                   max(case when type = 'CARE_TEAM_ONBOARDING_VISIT' and a.status = 'SCHEDULED' then convert_tz(a.datemodified, 'UTC', b.timezoneId) else null end) as ctov_scheduled_time,
				   max(case when type = 'CARE_TEAM_ONBOARDING_VISIT' and a.status = 'CANCELLED' then convert_tz(a.datemodified, 'UTC', b.timezoneId) else null end) as ctov_cancelled_time,
                   max(case when type = 'CARE_TEAM_ONBOARDING_VISIT' and a.status = 'COMPLETED' then convert_tz(a.datemodified, 'UTC', b.timezoneId) else null end) as ctov_completed_time,
                   max(case when type = 'CARE_TEAM_ONBOARDING_VISIT' then eventLocalTime else null end) as ctov_appointment_time,

                   max(case when type = 'SENSOR_ACTIVATION' and a.status = 'SCHEDULED' then convert_tz(a.datemodified, 'UTC', b.timezoneId) else null end) as SA_scheduled_time,
				   max(case when type = 'SENSOR_ACTIVATION' and a.status = 'CANCELLED' then convert_tz(a.datemodified, 'UTC', b.timezoneId) else null end) as SA_cancelled_time,
                   max(case when type = 'SENSOR_ACTIVATION' and a.status = 'COMPLETED' then convert_tz(a.datemodified, 'UTC', b.timezoneId) else null end) as SA_completed_time,
                   max(case when type = 'SENSOR_ACTIVATION' then eventLocalTime else null end) as SA_appointment_time,
                                                                               
                   max(case when type = 'HOME_VISIT' and a.status = 'SCHEDULED' then convert_tz(a.datemodified, 'UTC', b.timezoneId) else null end) as TA_scheduled_time,
				   max(case when type = 'HOME_VISIT' and a.status = 'CANCELLED' then convert_tz(a.datemodified, 'UTC', b.timezoneId) else null end) as TA_cancelled_time,
                   max(case when type = 'HOME_VISIT' and a.status = 'COMPLETED' then convert_tz(a.datemodified, 'UTC', b.timezoneId) else null end) as TA_completed_time,
                   max(case when type = 'HOME_VISIT' then eventLocalTime else null end) as TA_appointment_time,

                   max(case when type = 'BLOOD_WORK' and a.status = 'SCHEDULED' and c.clientid is not null then convert_tz(a.datemodified, 'UTC', b.timezoneId) else null end) as D0_BW_scheduled_time,
				   max(case when type = 'BLOOD_WORK' and a.status = 'CANCELLED' and c.clientid is not null then convert_tz(a.datemodified, 'UTC', b.timezoneId) else null end) as D0_BW_cancelled_time,
                   max(case when type = 'BLOOD_WORK' and a.status = 'COMPLETED' and c.clientid is not null then convert_tz(a.datemodified, 'UTC', b.timezoneId) else null end) as D0_BW_completed_time,
                   max(case when type = 'BLOOD_WORK' and c.clientid is not null then eventLocalTime else null end) as D0_BW_appointment_time
                   
from clientappointments_aud a inner join v_allClient_list b on a.clientid = b.clientid
                              left join bloodworkschedules c on a.clientid = c.clientid and c.bloodworkquarter = 'Q1' and c.bloodworkday = 'D1'
group by a.clientid 
;

