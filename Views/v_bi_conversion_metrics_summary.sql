alter view v_bi_conversion_metrics_summary as 
select a.date, a.customerName, a.coach, a.total_pending_active, a.total_registration, a.total_newly_registered, total_discharged, total_inactive, total_onHold, total_active, 
b.introTwin_SCH, b.introTwin_CAN, b.introTwin_COM, 
b.ctov_SCH, b.ctov_CAN, b.ctov_COM,
b.sensorActivation_SCH, b.sensorActivation_CAN, b.sensorActivation_COM, 
b.treatmentActivation_SCH, b.treatmentActivation_CAN, b.treatmentActivation_COM, 
b.D0_bloodWork_SCH, b.D0_bloodWork_CAN, b.D0_bloodWork_COM
from 
(
	select a.date, c.customerName, c.coachnameShort as coach, 
	count(distinct case when b.status = 'active' and b.status not in ('registration', 'pending_active') then b.clientid else null end) as total_active, 
	count(distinct case when b.status = 'pending_active' and b.status not in ('registration', 'active') then b.clientid else null end) as total_pending_active, 
	count(distinct case when b.status = 'registration' and b.status not in ('pending_active', 'active') then b.clientid else null end) as total_registration,
	count(distinct case when b.status = 'registration' and a.date = b.status_start_date then b.clientid else null end) as total_newly_registered,
	count(distinct case when b.status = 'discharged' and b.status not in ('pending_active', 'active', 'registration', 'inactive') then b.clientid else null end) as total_discharged,
	count(distinct case when b.status = 'inactive' and b.status not in ('pending_active', 'active', 'registration', 'discharged') then b.clientid else null end) as total_inactive,
    count(distinct case when b.status = 'on_hold' and b.status not in ('pending_active', 'active', 'registration', 'discharged', 'inactive') then b.clientid else null end) as total_onHold
   
	from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date
						  inner join v_allClient_list c on b.clientid = c.clientid 
	where a.date >= date_sub(date(now()), interval 1 month)
	group by a.date, c.customerName, c.coachnameShort 
) a left join 
(
	select a.date, customerName, coach, 
	sum(case when type = 'INTRO_TO_TWIN' and status = 'SCHEDULED' then 1 else null end) as introTwin_SCH,
	sum(case when type = 'INTRO_TO_TWIN' and status = 'CANCELLED' then 1 else null end) as introTwin_CAN,
	sum(case when type = 'INTRO_TO_TWIN' and status = 'COMPLETED' then 1 else null end) as introTwin_COM,

	sum(case when type = 'CARE_TEAM_ONBOARDING_VISIT' and status = 'SCHEDULED' and (bw_flag is null or bw_flag = 'Y') then 1 else null end) as ctov_SCH,
	sum(case when type = 'CARE_TEAM_ONBOARDING_VISIT' and status = 'CANCELLED' then 1 else null end) as ctov_CAN,
	sum(case when type = 'CARE_TEAM_ONBOARDING_VISIT' and status = 'COMPLETED' then 1 else null end) as ctov_COM,

	sum(case when type = 'SENSOR_ACTIVATION' and status = 'SCHEDULED' and (ctov_flag is null or ctov_flag = 'Y') then 1 else null end) as sensorActivation_SCH,
	sum(case when type = 'SENSOR_ACTIVATION' and status = 'CANCELLED' then 1 else null end) as sensorActivation_CAN,
	sum(case when type = 'SENSOR_ACTIVATION' and status = 'COMPLETED' then 1 else null end) as sensorActivation_COM,

	sum(case when type = 'HOME_VISIT' and status = 'SCHEDULED' and (sa_flag is null or sa_flag = 'Y') then 1 else null end) as treatmentActivation_SCH,
	sum(case when type = 'HOME_VISIT' and status = 'CANCELLED' then 1 else null end) as treatmentActivation_CAN,
	sum(case when type = 'HOME_VISIT' and status = 'COMPLETED' then 1 else null end) as treatmentActivation_COM,

	sum(case when type = 'BLOOD_WORK' and status = 'SCHEDULED' and isD0_bloodWork = 'Yes' and (itt_flag is null or itt_flag = 'Y') then 1 else null end) as D0_bloodWork_SCH,
	sum(case when type = 'BLOOD_WORK' and status = 'CANCELLED' and isD0_bloodWork = 'Yes' then 1 else null end) as D0_bloodWork_CAN,
	sum(case when type = 'BLOOD_WORK' and status = 'COMPLETED' and isD0_bloodWork = 'Yes' then 1 else null end) as D0_bloodWork_COM
	from v_date_to_date a left join 
	(
		select a.clientid, eventLocalTime, a.status, type, if(c.clientid is not null and a.type = 'blood_work', 'Yes', 'No') as isD0_bloodWork, 
		itt_flag, ctov_flag, sa_flag, ta_flag, bw_flag, b.customerName, b.coachnameShort as coach
		from clientappointments a inner join v_allClient_list b on a.clientid = b.clientid
								  left join bloodworkschedules c on a.clientid = c.clientid and c.bloodworkquarter = 'Q1' and c.bloodworkday = 'D1' 
								  left join (
													select a.clientid, max(case when type = 'INTRO_TO_TWIN' and status = 'COMPLETED' then 'Y' else null end) as itt_flag, 
																	   max(case when type = 'CARE_TEAM_ONBOARDING_VISIT' and status = 'COMPLETED' then 'Y' else null end) as ctov_flag, 
																	   max(case when type = 'SENSOR_ACTIVATION' and status = 'COMPLETED' then 'Y' else null end) as sa_flag,
																	   max(case when type = 'HOME_VISIT' and status = 'COMPLETED' then 'Y' else null end) as ta_flag,
																	   max(case when type = 'BLOOD_WORK' and status = 'COMPLETED' then 'Y' else null end) as bw_flag
													from clientappointments a left join bloodworkschedules b on a.clientid = b.clientid and b.bloodworkquarter = 'Q1' and b.bloodworkday = 'D1'
													group by clientid 
											  ) d on a.clientid = d.clientid 
	) b on a.date = date(b.eventLocalTime)	
    where date >= date_sub(date(now()), interval 1 month)
	group by a.date, customerName, coach
) b on a.date = b.date and a.customerName = b.customerName and ifnull(a.coach,'') = ifnull(b.coach,'')
;



