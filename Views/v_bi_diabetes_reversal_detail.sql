set collation_connection = 'latin1_swedish_ci';


alter view v_bi_diabetes_reversal_detail as 
select 
	cast((now() + interval -(28) hour) as date) AS reportdate,
    c.clientid AS clientid,c.patientname AS patientname,
    c.status AS status,c.customername AS customername,
    c.isPreDiabetic_Labelled AS isPreDiabetic_Labelled,c.source AS source,
    c.source_2 AS source_2,c.start_labA1c AS start_labA1c,c.coachname AS coachname,
    c.doctorname AS Doctor_Provider,
    c.registerednurse AS Registered_Nurse_RN,
    c.intakeRN,
	c.medicalassistant,
    c.Manual_MRT AS Manual_MRT,cohortreason.latest_cohortReason AS Manual_MRT_latest_cohortReason,
    measureslastweek.treatmentday_current AS treatmentday_current,
    (case 
		when ((measureslastweek.is_inreversal_current_minus_1 = 'Yes') and (measureslastweek.is_metonly_by_OPS_current = 'Yes')) then 'Yellow' 
        when (((measureslastweek.is_inreversal_current_minus_1 = 'Yes') and (measureslastweek.is_inreversal_current is null) and (measureslastweek.is_metonly_by_OPS_current is null)) or ((measureslastweek.is_metonly_by_OPS_current_minus_1 = 'Yes') and (measureslastweek.is_metonly_by_OPS_current is null))) then 'Red' 
        when ((measureslastweek.is_inreversal_current_minus_1 is null) and (measureslastweek.is_metonly_by_OPS_current_minus_1 is null) and ((measureslastweek.is_metonly_by_OPS_current = 'Yes') or (measureslastweek.is_inreversal_current = 'Yes'))) then 'Green' 
        when ((measureslastweek.is_metonly_by_OPS_current_minus_1 = 'Yes') and (measureslastweek.is_inreversal_current = 'Yes')) then 'Blue' else 'Grey' 
        end) AS reversal_status_change_color,
	measureslastweek.reversal_status_with_or_without_med_current AS reversal_status_with_or_without_med_current,
    measureslastweek.eA1C_60d_current AS eA1C_60d_current,
    measureslastweek.cgm_1d_current AS cgm_1d_current,
    measureslastweek.cgm_1d_current_minus_1 AS cgm_1d_current_minus_1,
    measureslastweek.cgm_1d_current_minus_2 AS cgm_1d_current_minus_2,measureslastweek.
    cgm_1d_current_minus_3 AS cgm_1d_current_minus_3,measureslastweek.cgm_5d_current AS cgm_5d_current,
    measureslastweek.cgm_5d_current_minus_1 AS cgm_5d_current_minus_1,measureslastweek.cgm_5d_current_minus_2 AS cgm_5d_current_minus_2,
    measureslastweek.cgm_5d_current_minus_3 AS cgm_5d_current_minus_3,measureslastweek.medicine_current AS medicine_current,
    measureslastweek.med_adh_current AS med_adh_current,measureslastweek.nut_syntax_current AS nut_syntax_current,
    measureslastweek.nut_adh_syntax_current AS nut_adh_syntax_current,measureslastweek.nut_adh_syntax_5d_current AS nut_adh_syntax_5d_current,
    measureslastweek.nut_tac_1d_current AS nut_tac_1d_current,measureslastweek.steps_last_week_avg_excl_0 AS steps_last_week_avg_excl_0,
    measureslastweek.sleepScore_last_3_days_avg_excl_0 AS sleepScore_last_3_days_avg_excl_0 
from 
	(
		(
			(
				(
					select 
						v_client_tmp.clientId AS clientid,
                        v_client_tmp.patientName AS patientname,
                        v_client_tmp.status AS status,
                        v_client_tmp.customerName AS customername,
                        v_client_tmp.isPreDiabetic_Labelled AS isPreDiabetic_Labelled,
                        v_client_tmp.source AS source,v_client_tmp.source_2 AS source_2,
                        v_client_tmp.start_LabA1c AS start_labA1c,v_client_tmp.coachName AS coachname,
                        v_client_tmp.doctorName AS doctorname,
                        if((v_client_tmp.cohortType like '%Manual%'),'yes','no') AS Manual_MRT ,
                        intakeRN,
                        medicalassistant,
                        registerednurse
					from v_client_tmp
				) c 
                left join 
					(
						select 
							measures.clientid AS clientid,
							max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),
                            measures.treatmentDays,NULL)) AS treatmentday_current,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),
                            measures.reversal_with_or_without_med,NULL)) AS reversal_status_with_or_without_med_current,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),
                            measures.reversal_with_or_without_med_prev_1,NULL)) AS reversal_status_with_or_without_med_current_minus_1,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),
                            measures.is_inreversal,NULL)) AS is_inreversal_current,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.is_inreversal_prev_1,NULL)) AS is_inreversal_current_minus_1,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.is_metonly_by_OPS,NULL)) AS is_metonly_by_OPS_current,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.is_metonly_by_OPS_prev_1,NULL)) AS is_metonly_by_OPS_current_minus_1,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.cgm_1d,NULL)) AS cgm_1d_current,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.cgm_1d_prev_1,NULL)) AS cgm_1d_current_minus_1,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.cgm_1d_prev_2,NULL)) AS cgm_1d_current_minus_2,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.cgm_1d_prev_3,NULL)) AS cgm_1d_current_minus_3,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.cgm_5d,NULL)) AS cgm_5d_current,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.cgm_5d_prev_1,NULL)) AS cgm_5d_current_minus_1,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.cgm_5d_prev_2,NULL)) AS cgm_5d_current_minus_2,max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),
                            measures.cgm_5d_prev_3,NULL)) AS cgm_5d_current_minus_3,max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.eA1C_60d,NULL)) AS eA1C_60d_current,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.medicine,NULL)) AS medicine_current,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.med_adh,NULL)) AS med_adh_current,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.nut_syntax,NULL)) AS nut_syntax_current,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.nut_adh_syntax,NULL)) AS nut_adh_syntax_current,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.nut_adh_syntax_5d,NULL)) AS nut_adh_syntax_5d_current,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.tac_1d,NULL)) AS nut_tac_1d_current,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.tac_5d,NULL)) AS nut_tac_5d_current,
                            max(if((measures.measure_event_date = cast((now() + interval -(28) hour) as date)),measures.steps,NULL)) AS steps_current,
                            avg(if(((measures.steps = 0) or (0 <> NULL)),NULL,measures.steps)) AS steps_last_week_avg_excl_0,
                            sum(if(((measures.steps = 0) or (0 <> NULL)),1,0)) AS days_with_no_steps_last_week,
                            avg(if(((measures.measure_event_date >= (cast((now() + interval -(28) hour) as date) + interval -(2) day)) and (measures.measure_event_date <= cast((now() + interval -(28) hour) as date)) and (measures.sleepScore is not null) and (measures.sleepScore <> 0)),measures.sleepScore,NULL)) AS sleepScore_last_3_days_avg_excl_0 
						from 
							(
								select 
									bi_patient_measures.clientid AS clientid,
									bi_patient_measures.measure_event_date AS measure_event_date,
									bi_patient_measures.treatmentDays AS treatmentDays,
									bi_patient_measures.is_InReversal AS is_inreversal,
									lag(bi_patient_measures.is_InReversal,1) OVER (PARTITION BY bi_patient_measures.clientid ORDER BY bi_patient_measures.measure_event_date )  AS is_inreversal_prev_1,
									bi_patient_measures.is_MetOnly_by_OPS AS is_metonly_by_OPS,lag(bi_patient_measures.is_MetOnly_by_OPS,1) OVER (PARTITION BY bi_patient_measures.clientid ORDER BY bi_patient_measures.measure_event_date )  AS is_metonly_by_OPS_prev_1,
									if(((bi_patient_measures.is_InReversal = 'yes') or (bi_patient_measures.is_MetOnly_by_OPS = 'yes')),'yes','no') AS reversal_with_or_without_med,
									lag((select reversal_with_or_without_med),1) OVER (PARTITION BY bi_patient_measures.clientid ORDER BY bi_patient_measures.measure_event_date )  AS reversal_with_or_without_med_prev_1,bi_patient_measures.cgm_1d AS cgm_1d,
									lag(bi_patient_measures.cgm_1d,1) OVER (PARTITION BY bi_patient_measures.clientid ORDER BY bi_patient_measures.measure_event_date )  AS cgm_1d_prev_1,
									lag(bi_patient_measures.cgm_1d,2) OVER (PARTITION BY bi_patient_measures.clientid ORDER BY bi_patient_measures.measure_event_date )  AS cgm_1d_prev_2,
									lag(bi_patient_measures.cgm_1d,3) OVER (PARTITION BY bi_patient_measures.clientid ORDER BY bi_patient_measures.measure_event_date )  AS cgm_1d_prev_3,
									bi_patient_measures.cgm_5d AS cgm_5d,lag(bi_patient_measures.cgm_5d,1) OVER (PARTITION BY bi_patient_measures.clientid ORDER BY bi_patient_measures.measure_event_date )  AS cgm_5d_prev_1,
									lag(bi_patient_measures.cgm_5d,2) OVER (PARTITION BY bi_patient_measures.clientid ORDER BY bi_patient_measures.measure_event_date )  AS cgm_5d_prev_2,
									lag(bi_patient_measures.cgm_5d,3) OVER (PARTITION BY bi_patient_measures.clientid ORDER BY bi_patient_measures.measure_event_date )  AS cgm_5d_prev_3,
									bi_patient_measures.eA1C_60d AS eA1C_60d,bi_patient_measures.medicine AS medicine,bi_patient_measures.medicine_drugs AS medicine_drugs,
									bi_patient_measures.med_adh AS med_adh,bi_patient_measures.nut_syntax AS nut_syntax,bi_patient_measures.nut_syntax_color AS nut_syntax_color,
									bi_patient_measures.nut_adh_syntax AS nut_adh_syntax,bi_patient_measures.nut_adh_syntax_5d AS nut_adh_syntax_5d,
									bi_patient_measures.TAC_1d AS tac_1d,
									bi_patient_measures.TAC_5d AS tac_5d,bi_patient_measures.steps AS steps,
									bi_patient_measures.sleepScore AS sleepScore 
								from bi_patient_measures 
								where 
									(
										(
											bi_patient_measures.measure_event_date >= (cast((now() + interval -(28) hour) as date) + interval -(6) day)
										) 
										and (bi_patient_measures.measure_event_date <= cast((now() + interval -(28) hour) as date))
									) 
								order by bi_patient_measures.clientid,bi_patient_measures.measure_event_date
							) measures 
						group by measures.clientid
					) measureslastweek on((c.clientid = measureslastweek.clientid))
				) 
                
			) 
            left join 
				(
					select 
						ch.clientid AS clientid,
                        max(if((ch.startTime = ch.latest_startTime),ch.cohortReason,NULL)) AS latest_cohortReason 
					from 
						(
							select 
								cohorthistories.clientId AS clientid,
                                cohorthistories.cohortId AS cohortID,
                                cohorthistories.startTime AS startTime,cohorthistories.
                                cohortReason AS cohortReason,
                                max(cohorthistories.startTime) OVER (PARTITION BY cohorthistories.clientId )  AS latest_startTime 
							from cohorthistories
						) ch 
					group by ch.clientid
				) cohortreason on((c.clientid = cohortreason.clientid))
	)
	;