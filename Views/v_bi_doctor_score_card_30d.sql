create view v_bi_doctor_score_card_30d as 
select report_date, a.doctorId, doctorname, measure_name, measure_group, measure
from bi_doctor_measure_x_scorecard_30d a inner join v_alldoctors_list d on a.doctorId = d.doctorId
;