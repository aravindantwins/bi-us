SET collation_connection = 'latin1_swedish_ci'; 

-- drop view v_bi_medicine_adoption_detail; 

alter view v_bi_medicine_adoption_detail
as
select 
date(convert_tz(h.dateAdded,'UTC',p.timezoneID)) as medchangedate, 
p.doctornameshort as doctor, 
p.doctorId, 
p.clientid,
p.patientname as patient, 
c.cohortType, 
h.changeType, 
convert_tz(h.dateAdded,'UTC',p.timezoneID) as medChangeTime, 
case when h.changeReason is null and h.overrideReasons is null then null
	 when h.changeReason is null then h.overrideReasons
	 when h.overrideReasons is null then h.changeReason
     else concat(trim(h.changeReason), '; ', trim(h.overrideReasons)) end as doctorComments,  
r.rationale as rationale, 
h.recommendedMedication, 
h.actualMedication,
h.medicationtierid,
h.overrideReasons 
from medicationhistories h, 
medicationRecommendations r, 
v_client_tmp p, 
cohorts c
where h.clientId=p.clientid 
and h.medicationRecommendationId=r.medicationRecommendationId 
and date(convert_tz(h.dateAdded,'UTC',p.timezoneID)) >= date_sub(date(now()), interval 3 month)
and h.cohortId=c.cohortId
;
