-- select * from v_bi_medicine_adoption_detail;
create view v_bi_medicine_adoption_summary_by_doctor
as 
-- select report_date, 'Overall' as doctor, 0 as doctorid, measure from bi_overall_measure where report_date >= '2019-06-20' and measure_group like '%adopt%'
-- union all 
select sor1.date, sor1.doctor, sor1.doctorid, avg(sor2.adopt_percentage) as measure
from 
(
	select date, doctor, doctorId, 
	sum(case when changetype = 'Automated' then 1 else 0 end) as automated_cnt, 
	sum(case when b.clientid is not null then 1 else 0 end) as total_cnt,
	sum(case when changetype = 'Automated' then 1 else 0 end)/sum(case when b.clientid is not null then 1 else 0 end) as adopt_percentage
	from 
	v_date_to_date a left join v_bi_medicine_adoption_detail b on a.date = b.medchangedate
	where b.cohorttype not in ('B3','Manual')
	-- and a.date >= '2019-06-20'
	group by date, doctor, doctorId
) sor1 inner join 
(
	select date, doctor, doctorId,
	sum(case when changetype = 'Automated' then 1 else 0 end) as automated_cnt, 
	sum(case when b.clientid is not null then 1 else 0 end) as total_cnt,
	sum(case when changetype = 'Automated' then 1 else 0 end)/sum(case when b.clientid is not null then 1 else 0 end) as adopt_percentage
	from 
	v_date_to_date a left join v_bi_medicine_adoption_detail b on a.date = b.medchangedate
	where b.cohorttype not in ('B3','Manual')
	-- and a.date >= '2019-06-20'
	group by date, doctor, doctorId
) sor2 on sor1.doctor = sor2.doctor and sor2.date between date_sub(sor1.date, interval 4 day) and sor1.date
group by sor1.date, sor1.doctor, sor1.doctorid
;