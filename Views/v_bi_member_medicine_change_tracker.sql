-- SET COLLATION_connection = 'latin1_swedish_ci';

alter view v_bi_member_medicine_change_tracker as
select a.clientid, a.patientname, a.doctorname, a.source_2 as clientType, customerName, a.notes, a.treatmentdays, b.status_start_date as treatmentStartDate, 											
group_concat(distinct pTwin.name, ' (', pTwin.dosage, ') ') as preTwin_Meds, 											
group_concat(distinct cs.medName,' ', cs.dayDosage,' ', cs.Timing separator ' ,') as D1_Meds, 											
medChange.dateActioned as dateActionedMedChange, 											
if(medChange.dateActioned is null, '', ifnull(medChange.medicine, 'Off all diabetes medications')) as changedMed,											
datediff(medChange.dateActioned, b.status_start_date) as dayNo											
from 											
(											
	select clientid, patientname, doctorname, source_2, treatmentdays, customerName, notes from v_client_tmp where status = 'Active'										
) a left join (											
					select b.clientid, 'active' as status, max(status_start_date) as status_start_date, max(status_end_date) as status_end_date 						
					from bi_patient_status b						
					where b.status = 'active'						
					and not exists (select 1 from bi_patient_Status d where b.clientid = d.clientid and d.status = 'pending_active' and d.status_end_date > b.status_end_date)						
					group by b.clientid						
			   ) b on a.clientid = b.clientid 								
	left join (										
				select clientid, a.name, cast(concat(truncate(a.breakfastDosage,0), '-', truncate(a.lunchDosage,0), '-', truncate(a.dinnerDosage,0)) as char(20)) as dosage
				from clinicreportmedications a inner join medicines b on a.medicationID = b.medicineID							
											  left join medicinepharmclasses mpc on b.medicineId = mpc.medicineId
                                              left join pharmclasses pc on mpc.pharmclassId = pc.pharmclassId											
				where hasmedicine = 1							
				and category in ('INSULIN', 'DIABETES')							
               ) pTwin on a.clientid = pTwin.clientid											
											
    left join (			
    
					select s.clientid, s.startdate, s.enddate, m.medicineName, m.unit, s.type, s.timeslot, s.frequency, s.status, 						
					case when s.type = 'Medicine' then concat(m.medicineName,' ', unit)						
						 when s.type <> 'Medicine' then concat(m.medicineName,' ',targetValue, ' ', unit) end medName, 					
					s.bDosage, s.lDosage, s.dDosage, concat('(',s.bDosage,'-',s.lDosage,'-',s.dDosage,')') as dayDosage, 						
					cast(case when s.timeslot like '%BEFORE%' then 'Before Food'						
						 when s.timeslot like '%AFTER%' then 'After Food' end as char(15)) as Timing					
					from 						
					(						
						 select clientid, startdate, enddate, medicineId, type, floor(d.targetValue) as targetValue, group_concat(distinct timeslots) as timeslot, frequency, status, 					
						 sum(case when d.timeslots like '%BREAKFAST%' and d.type = 'Medicine' then floor(d.targetValue)					
							 when d.timeslots like '%BREAKFAST%' and d.type <> 'Medicine' then 1 else 0 end) as bDosage,      				
						 sum(case when d.timeslots like '%LUNCH%' and d.type = 'Medicine' then floor(d.targetValue)					
							 when d.timeslots like '%LUNCH%' and d.type <> 'Medicine' then 1 else 0 end) as lDosage,				
						 sum( case when d.timeslots like '%DINNER%' and d.type = 'Medicine' then floor(d.targetValue)					
							 when d.timeslots like '%DINNER%' and d.type <> 'Medicine' then 1 else 0 end) as dDosage 				
						from clienttodoschedules d where category = 'medicine'					
						group by clientid, startdate, enddate, medicineId					
					) s left join medicines m on s.medicineID = m.medicineID						
                    											
				) cs on b.clientid = cs.clientid and b.status_start_date between cs.startDate and cs.endDate							
    left join 											
	(					
    
				select a.clientid, a.changeType, a.dateActioned, group_concat(distinct medName,' ', dayDosage,' ', Timing separator ' ,') as medicine							
				from 							
				(							
					select a.clientid, changeType, date(a.dateadded) as dateActioned, a.medicationrecommendationId, a.medicationTierID, b.eventtime, b.status, b.transitionId						
					from medicationhistories a inner join medicationrecommendations b on a.medicationRecommendationID = b.medicationRecommendationId						
					and changeType not in ('SKIPPED')						
				) a 							
				left join 							
				(							
					select s.clientid, s.startdate, s.enddate, m.medicineName, m.unit, s.type, s.timeslot, s.frequency, s.status, 						
					case when s.type = 'Medicine' then concat(m.medicineName,' ', unit)						
						 when s.type <> 'Medicine' then concat(m.medicineName,' ',targetValue, ' ', unit) end medName, 					
					s.bDosage, s.lDosage, s.dDosage, concat('(',s.bDosage,'-',s.lDosage,'-',s.dDosage,')') as dayDosage, 						
					cast(case when s.timeslot like '%BEFORE%' then 'Before Food'						
						 when s.timeslot like '%AFTER%' then 'After Food' end as char(15)) as Timing					
					from 						
					(						
						 select clientid, startdate, enddate, medicineId, type, floor(d.targetValue) as targetValue, group_concat(distinct timeslots) as timeslot, frequency, status, 					
						 sum(case when d.timeslots like '%BREAKFAST%' and d.type = 'Medicine' then floor(d.targetValue)					
							 when d.timeslots like '%BREAKFAST%' and d.type <> 'Medicine' then 1 else 0 end) as bDosage,      				
						 sum(case when d.timeslots like '%LUNCH%' and d.type = 'Medicine' then floor(d.targetValue)					
							 when d.timeslots like '%LUNCH%' and d.type <> 'Medicine' then 1 else 0 end) as lDosage,				
						 sum( case when d.timeslots like '%DINNER%' and d.type = 'Medicine' then floor(d.targetValue)					
							 when d.timeslots like '%DINNER%' and d.type <> 'Medicine' then 1 else 0 end) as dDosage 				
						from clienttodoschedules d where category = 'medicine'					
						group by clientid, startdate, enddate, medicineId					
					) s left join medicines m on s.medicineID = m.medicineID						
				) b on a.clientid = b.clientid and a.dateActioned between b.startDate and b.enddate 							
				group by a.clientid, a.dateActioned							
		) medChange on a.clientid = medChange.clientid and medChange.dateActioned > b.status_start_date									
 group by a.clientid, medChange.dateActioned											
;											