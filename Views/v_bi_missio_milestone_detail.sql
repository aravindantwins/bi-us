set collation_connection = 'latin1_swedish_ci';

alter view v_bi_missio_milestone_detail as 
select a.clientid, a.customerName, a.patientname, a.doctorname, a.coachname, enrollmentDate, activationDate, status, if(a.status = 'active', null, a.dischargeDate) as dischargeDate, M1a_date, M1b_date, M3_date, a.start_labA1c,
max(case when a.M1a_date = b.measure_event_date then b.eA1c_60d else null end) as eA1c60d_M1a_date, 
max(case when a.M1a_date = b.measure_event_date then b.eA1c60d_difference else null end) as eA1c60dDifference_M1a_date, 

max(case when a.M1b_date = b.measure_event_date then b.eA1c_60d else null end) as eA1c60d_M1b_date, 

max(case when a.M3_date = b.measure_event_date then b.eA1c_60d else null end) as eA1c60d_M3_date,
max(case when a.M3_date = b.measure_event_date then if(b.medicine_drugs is null, 'No MED', if(b.medicine_drugs in ('Biguanide (DIABETES)', 'METFORMIN', 'METFORMIN (DIAB)'),'Metformin Only', '')) else null end) as medicineDrugs_M3_date

from 

(
	select 
	a.clientid, a.customerName, patientname, a.doctorname, a.coachname, a.enrollmentDate, a.activationDate, a.status, d.dischargeDate, start_LabA1c, 
	min(case when b.m1a_counter = 30 then b.measure_event_date else null end) as M1a_date,
	min(case when b.m1b_counter = 30 then b.measure_event_date else null end) as M1b_date,
	min(case when b.m3_counter = 30 then b.measure_event_date else null end) as M3_date
	
	
	from v_client_tmp a left join bi_patient_measures b on a.clientid = b.clientid 
						left join (select clientid, min(status_Start_date) as dischargeDate from bi_patient_status where status = 'discharged' group by clientid) d on a.clientid = d.clientid 
	where status in ('Active', 'Discharged', 'Inactive')
	-- and customername like '%Missio%'
	group by a.clientid
) a left join bi_patient_measures b on a.clientid = b.clientid and (a.M1a_date = b.measure_event_date or a.M1b_date = b.measure_event_date or a.M3_date = b.measure_event_date)
group by a.clientid
;



