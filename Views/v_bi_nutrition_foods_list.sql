create view v_bi_nutrition_foods_list as 
select a.foodid, foodlabel, category, primaryCategory, draftStatus, healthLabels, 
if(rec.foodid is not null, 'Yes', 'No') as isRecipeInstruct_present, 
if(foodIng.foodid is not null, 'Yes', 'No') as isfoodIngredient_present

from foods a left join (select foodid, count(*) as total_steps from recipeinstructions group by foodId) rec on a.foodId = rec.foodid
			 left join (select foodid, count(*) as total_steps from foodingredientmappings group by foodId) foodIng on a.foodid = foodIng.foodid
where a.draftStatus = 'published'
;