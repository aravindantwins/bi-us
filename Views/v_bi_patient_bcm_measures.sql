create view v_bi_patient_bcm_measures as 
select a.date, a.clientid, 
max(case when a.date = s.eventdate then bodyFat else null end) as bodyFat, 
max(case when a.date = s.eventdate then bmr else null end) as bmr, 
max(case when a.date = s.eventdate then bodyWater else null end) as bodyWater, 
max(case when a.date = s.eventdate then subcutaneousFat else null end) as subcutaneousFat, 
max(case when a.date = s.eventdate then visceralFat else null end) as visceralFat, 
max(case when a.date = s.eventdate then boneMass else null end) as boneMass, 
max(case when a.date = s.eventdate then leanBodyWeight else null end) as leanBodyWeight, 
max(case when a.date = s.eventdate then protein_percentage else null end) as protein_percentage, 

cast(avg(bodyFat) as decimal(6,2)) as bodyFat_7d,
cast(avg(bmr) as decimal(6,2)) as bmr_7d,
cast(avg(bodyWater) as decimal(6,2)) as bodyWater_7d,
cast(avg(subcutaneousFat) as decimal(6,2)) as subcutaneousFat_7d,
cast(avg(visceralFat) as decimal(6,2)) as visceralFat_7d,
cast(avg(boneMass) as decimal(6,2)) as boneMass_7d,
cast(avg(leanBodyWeight) as decimal(6,2)) as leanBodyWeight_7d,
cast(avg(protein_percentage) as decimal(6,2)) as protein_percentage_7d
from (select date, clientid 
	  from v_date_to_date a inner join bi_patient_status b on a.date between status_start_date and status_end_date and status = 'active') a left join 
(
	select a.clientid, date(convert_tz(eventtime, 'UTC', b.timezoneID)) as eventDate, cast(avg(fat) as decimal(6,2)) as bodyFat, cast(avg(bmr) as decimal(6,2)) as bmr, 
    cast(avg(bodyWaterRate) as decimal(6,2)) as bodyWater, cast(avg(subcutaneousFat) as decimal(6,2)) as subcutaneousFat,
	cast(avg(visceralFat) as decimal(6,2)) as visceralFat, cast(avg(bonemass) as decimal(6,2)) as boneMass, 
    cast(avg(leanBodyWeight) as decimal(6,2)) as leanBodyWeight, cast(avg(protein) as decimal(6,2)) as protein_percentage
	from weights a inner join v_allClient_list b on a.clientid = b.clientid 
	group by a.clientid, date(convert_tz(eventtime, 'UTC', b.timezoneID))
) s on a.clientid = s.clientid and (a.date = s.eventdate or s.eventdate between date_sub(a.date, interval 6 day) and a.date)
where date >= date_sub(date(now()), interval 2 month)
group by a.date, a.clientid
;


