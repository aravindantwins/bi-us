alter view v_bi_patient_coach_detail as 
select 
s.clientid, s.date, ct.patientname, ct.coachnameShort as coach, ct.treatmentdays, 
start_medicines, length(medicine_drugs) - length(replace(medicine_drugs, ',','')) + 1 as current_med_count,
ct.durationYears_diabetes, s.status_start_date as activeStatusStartDate, 
s1.nut_adh_syntax as nut_adh_1d, 
case when s1.nut_adh_syntax_5d >= .80 then 'Yes' else 'No' end as nut_adh_5d, 
actionScore_5d, TAC_5d, cgm_5d, s1.starRating_5d,
ifnull(nut_sat,'') as nut_happiness_rating,
ifnull(s1.is_InReversal, '') as is_InReversal_NoMed, 
ifnull(if(s1.is_InReversal is null, s1.is_MetOnly_by_ops, ''), '') as is_InReversal_Metformin_Only,
d10_success, d35_success,
if(imt.clientid is not null, 'Yes', 'No') as isDisenrollRisk, 
case when s1.treatmentdays >= 35 and (s1.actionScore_5d < 85 or s1.nut_adh_syntax_5d < .80 or cgm_5d > 140) then 'Yes'
     when s1.treatmentdays >= 35 and (s1.is_InReversal is null and s1.is_MetOnly_by_ops = 'No') then 'Yes' 
     when imt.clientid is not null then 'Yes' else 'No'
     end as isPatientRed
from 

(
	select clientid, date, status_start_date
    from v_date_to_date a inner join 
    (
		select clientid, max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
		from bi_patient_status 
		where status = 'Active'
		group by clientid
    ) b on a.date between b.status_start_date and b.status_end_date 
) s left join bi_patient_measures s1 on s.clientid = s1.clientid and s.date = s1.measure_event_date 
	left join v_client_tmp ct on s.clientid = ct.clientid 
    left join (select clientid from bi_incident_mgmt where category = 'DISENROLLMENT' and status = 'OPEN') imt on s.clientid = imt.clientid
;

