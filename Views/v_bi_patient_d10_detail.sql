-- altered the view by adding this new comment to test the versioning in bitbucket
alter view v_bi_patient_d10_detail as 
select clientid, patientname, coach, day10, cohort, is_MetOnly, cgm_5d, eRating_5d as coach_rating_5d, nut_adh_5d, tac_5d, as_5d, success_ind
from 
(
	select clientid, patientname, coach, day10, cgm_5d, tac_5d, AS_5d, eRating_5d,																						
	total_incidents, cohort, nut_adh_5d, is_MetOnly,																						
	if ( (
			nut_adh_5d = 'YES'																						
			and ifnull(tac_5d,0) >= 2.5																				
			and (if(AS_5d >= 85, 'Yes', if(AS_5d < 85 and total_incidents >= 1, 'Yes','No')) = 'Yes')																				
			and ifnull(eRating_5d,4) >= 4																				
		  ) or cohort = 'In Reversal' or is_MetOnly = 'Yes', 'Yes','No') as success_ind																			
	from																							
	(																						
			select a.clientid, ct.patientname, ct.coachNameShort as coach, day10,																						
			re.starRating_5d as eRating_5d, 																					
			re.cgm_5d,																						
			re.actionScore_5d as as_5d,																						
			re.tac_5d,																						
			re.cohort,																						
			case when re.nut_adh_syntax_5d >= 0.8 then 'YES' else 'NO' end as nut_adh_5d, 
            re.is_MetOnly_by_OPS as is_MetOnly,																						
			count(distinct imt.incidentid) as total_incidents																						
			from																						
				(																					
						select distinct b.clientid, date(date_add(status_start_date, interval 10 day)) as day10																			
						from																			
						(																			
							select clientid, status,																		
							max(status_start_date) as status_start_date, max(status_end_date) as status_end_date																		
							from bi_patient_status b																		
							group by clientid, status																		
						) b																			
						where b.status = 'active'																			
						and datediff(status_end_date, status_start_date) >= 10																			
						/*
                        and date(date_add(status_start_date, interval 10 day)) >= date_sub(date(itz(now())), interval 2 month)
						and date(date_add(status_start_date, interval 10 day)) <= date(itz(now()))	
					    */
				) a			
				left join bi_patient_measures re on a.clientid = re.clientid and a.day10 = re.measure_event_date																					
				left join (select distinct incidentid, clientid, status, date(report_time) as report_date from bi_incident_mgmt where category = 'sensors') imt on a.clientid = imt.clientid and report_date <= a.day10																					
				left join v_client_tmp ct on a.clientid = ct.clientid																					
			group by a.clientid																							
		) s																						
) s 
;