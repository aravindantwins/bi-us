alter view v_bi_patient_d35_detail as 
select clientid, patientname, coach, cohortname, day35, cgm_5dg, tac_5d, AS_5d, coach_rating_5d, nut_adh_5d, is_MetOnly, success_ind
from 
(														
			select clientid, patientname, coach, day35, cgm_5dg, tac_5d, round(AS_5d) as AS_5d, coach_rating_5d,													
			nut_adh_5d, total_incidents, cohortname, is_MetOnly,
			if (( nut_adh_5d = 'YES'													
					and ifnull(tac_5d,0) >= 2.5												
					and (if(AS_5d >= 85, 'Yes', if(AS_5d < 85 and total_incidents >= 1, 'Yes','No')) = 'Yes')												
					and ifnull(coach_rating_5d,4) >= 4												
				) or cohortname = 'In Reversal' or is_MetOnly = 'Yes', 'Yes','No') as success_ind												
			from														
			(														
			select a.clientid, ct.patientname, ct.coachNameShort as coach,  day35, 													
			re.cgm_5d as cgm_5dg, re.tac_5d as tac_5d, re.actionScore_5d as AS_5d,														
			re.starRating_5d as coach_rating_5d, 										
			case when re.nut_adh_syntax_5d >= 0.8 then 'YES' else 'NO' end as nut_adh_5d, -- bh.nut_happy														
			re.cohort as cohortname, re.is_MetOnly_by_OPS as is_MetOnly													
			,count(distinct imt.incidentid) as total_incidents
			from														
				(													
						select clientid, day35 -- , case when total_cgm_synced_last_14_days > 0 and total_foodlog_synced_last_14_days > 0 then 'Yes' else 'No' end as toIncludeOrNot											
						from											
						(		select distinct b.clientid, date(date_add(status_start_date, interval 35 day)) as day35									
								from	
                                (								
												select clientid, status,					
												max(status_start_date) as status_start_date, max(status_end_date) as status_end_date					
												from bi_patient_status b	
                                                where status = 'active'
												group by clientid, status					
								) b							
								where datediff(status_end_date, status_start_date) >= 35									
								/*
                                and date(date_add(status_start_date, interval 35 day)) >= date_sub(date(now()), interval 2 month)									
								and date(date_add(status_start_date, interval 35 day)) <= date(now())		
                                */
						) s											
				) a		
				left join bi_patient_measures re on a.clientid = re.clientid and a.day35 = re.measure_event_date													
				left join (select distinct incidentid, clientid, status, date(report_time) as report_date from bi_incident_mgmt where category = 'sensors') imt on a.clientid = imt.clientid and report_date <= a.day35													
				left join v_client_tmp ct on a.clientid = ct.clientid													
				group by a.clientid													
			) s													
) s
;														

							
