-- set collation_connection = 'latin1_swedish_ci';

create view v_bi_patient_daily_heartrate_summary as 
select clientid, eventdate as measure_event_date, avg_rest_hr, cardio_hr_percentage, fitness_hr_percentage 
from bi_patient_daily_heartrate_summary
; 
