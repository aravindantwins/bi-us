create view v_bi_patient_exclusion_detail_all_patients as 
select doctorname, clientid, patientname, coachname, age, isServiceActivated, current_status, dateEnrolled,
enrolledDays, ifnull(treatmentDays,0) as treatmentDays, totalEscalationDays, durationYears_diabetes, escalationCohort, med_count, start_insulin_units, start_medicine_diabetes_drugs,
is_GADA, is_Type1Like, First_HBA1C, First_BMI, First_Albumin, First_AST, First_ALT, First_Creatinine, ifnull(First_GFR, start_eGFR) as First_GFR, First_cPeptide, First_Homa2b, 
is_Exclusion, Exclusion_Cohort, 
Latest_HBA1C, Latest_BMI, Latest_Albumin, Latest_ast, Latest_alt, Latest_Creatinine, Latest_GFR, Latest_cPeptide, Latest_Homa2b
from 
(
	select 																
	a.doctornameShort as doctorname, a.clientid, a.patientname, a.coachnameShort as coachname, 																
    if(c.programStartDate_analytics is not null, 'Yes', 'No') as isServiceActivated,
	a.status as current_status,																
	a.enrollmentdate as dateEnrolled, 	
	a.age, 															
	c.enrolledDays, 																
	c.treatmentDays, 																
	d.totalEscalationDays, 																
	case when b.cohort = 'In Reversal' then 'In Reversal' 																
		 when b.cohort is null then '' 															
		 else 'Escalation' end as escalationCohort,																
	length(medicine_drugs) - length(replace(medicine_drugs,',','')) + 1 as med_count,																
	c.is_type1 as is_GADA, 																
	c.durationYears_diabetes, 
	c.start_insulin_units,
	c.start_medicine_diabetes_drugs,
	-- if ((f.cPeptide <= 0.5 and ifnull(f.homa2b,0) <= 51 and c.start_insulin_units > 0) or (f.cPeptide < 1.1 and ifnull(f.homa2b,0) <= 51 and c.start_insulin_units > 0 and c.durationYears_diabetes >= 25) or (b1.cgm_5d > 175 and b1.ketone_5d > 0.4), 'Yes','No') as is_Type1Like, 
	c.is_type1Like,
	cast(f.labA1C as decimal(10,2)) as First_HBA1C, cast(c.start_BMI as decimal(10,2)) as First_BMI, cast(f.Albumin as decimal(10,2)) as First_Albumin, cast(f.cPeptide as decimal(10,2)) as First_cPeptide,															
	cast(f.ast as decimal(10,2)) as First_AST, cast(f.alt as decimal(10,2)) as First_ALT, cast(f.creatinine as decimal(10,2)) as First_Creatinine, cast(f.gfr as decimal(10,2)) as First_GFR, cast(f.homa2b as decimal(10,2)) as First_Homa2b, 																
	c.start_eGFR,
	/* OLD
	case when ((f.laba1c < 5.7 and c.start_medicine_diabetes_drugs is not null) or f.BMI < 18.5 or f.Albumin < 2.5 or f.ast > 102 or f.alt > 147 or (f.creatinine > 2 or f.gfr < 60) or f.cPeptide < 0.35 or c.is_type1 = 'Yes' or 
		((f.cPeptide <= 0.5 and ifnull(f.homa2b,0) <= 51 and c.start_insulin_units > 0) or (f.cPeptide < 1.1 and ifnull(f.homa2b,0) <= 51 and c.start_insulin_units > 0 and c.durationYears_diabetes >= 25))
	) then 'Yes' else 'No' end is_Exclusion, 
	*/ 

	case when (a.age > 70 or (f.laba1c < 5.5 and c.start_medicine_diabetes_drugs is null) or c.start_BMI < 18.5 or f.ast > 102 or f.alt > 147 or ifnull(f.GFR, c.start_eGFR) < 45 or f.cPeptide < 0.3 or c.is_type1 = 'Yes' or is_type1Like = 'Yes') then 'Yes' else 'No' end is_Exclusion, 
																	
	case when a.age > 70 then 'Age'
		 when c.is_type1 = 'Yes' then 'GADA'
		 when f.cPeptide < 0.3 then 'cPeptide'																
		 -- when (f.cPeptide <= 0.5 and ifnull(f.homa2b,0) <= 51 and c.start_insulin_units > 0) or (f.cPeptide < 1.1 and ifnull(f.homa2b,0) <= 51 and c.start_insulin_units > 0 and c.durationYears_diabetes >= 25) then 'Type1Like'															
		 when ifnull(f.GFR, c.start_eGFR) < 45 then 'GFR' 															
		 -- when f.creatinine > 2 then 'Creatinine' 															
		 when c.is_type1Like = 'Yes' then 'Type1-Like'
         when c.start_BMI < 18.5 then 'BMI'						-- take BMI from dim_client because sometimes the first blood work takes time to reach clinictestresults but vitals are getting collected earlier. 										
		 -- when f.Albumin < 2.5 then 'Albumin'		
		 when f.ast > 102 then 'AST'															
		 when f.alt > 147 then 'ALT'																
		 when (f.laba1c < 5.7 and c.start_medicine_diabetes_drugs is null) then 'HBA1C'		
		 end as Exclusion_Cohort, 																
																	
																	
	cast(g.labA1C as decimal(10,2)) as Latest_HBA1C, cast(g.BMI as decimal(10,2)) as Latest_BMI, cast(g.Albumin as decimal(10,2)) as Latest_Albumin, 																
	cast(g.ast as decimal(10,2)) as Latest_ast, cast(g.alt as decimal(10,2)) as Latest_alt, cast(g.creatinine as decimal(10,2)) as Latest_Creatinine, cast(g.gfr as decimal(10,2)) as Latest_GFR,
	cast(g.cPeptide as decimal(10,2)) as Latest_cPeptide, cast(g.homa2b as decimal(10,2)) as Latest_Homa2b
																	
	from 																
	v_client_tmp a left join bi_patient_measures b on a.clientid = b.clientid and b.measure_event_date = date_sub(date(now()), interval 1 day)
				   left join dim_client c on a.clientid = c.clientid and c.is_row_current = 'y'													
				   left join (																
									select clientid, count(distinct case when cohort = 'In Reversal' then measure_event_date else null end) as totalInReversalDays, 								
									count(distinct case when cohort not in ('Default','In Reversal') then measure_event_date else null end) as totalEscalationDays								
									from bi_patient_measures 								
									group by clientid							
							  ) d on a.clientid = d.clientid 																
					left join (																
											select s.clientid, s.durationYears_diabetes, LabA1c, gfr, albumin, ast, alt, creatinine, GADA, cPeptide, homa2B, BMI							
											from dim_client s inner join (
															select s.clientid, cast(avg(veinhba1c) as decimal(10,2)) as LabA1c, avg(glomerularFiltrationRate) as gfr, avg(albumin) as albumin, 							
															avg(ast) as ast, avg(alt) as alt, avg(Creatinine) as creatinine, 							
															avg(GADA) as GADA, avg(cPeptide) as cPeptide, avg(homa2B) as homa2B,							
															avg(Weight)/(avg(Height)*avg(Height)) as BMI	
															from 
															(
																select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork
																from clinictestresults 
																where deleted = 0
																group by clientid 
															) s inner join clinictestresults s1 on s.clientid = s1.clientid and s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime)
															group by clientid		
                                            ) c on s.clientid = c.clientid 
                                            where s.is_row_current = 'y'
								) f on a.clientid = f.clientid 									
					left join (																
											select s.clientid, s.durationYears_diabetes, LabA1c, gfr, albumin, ast, alt, creatinine, GADA, cPeptide, homa2B, BMI							
											from dim_client s inner join (
															select s.clientid, cast(avg(veinhba1c) as decimal(10,2)) as LabA1c, avg(glomerularFiltrationRate) as gfr, avg(albumin) as albumin, 							
															avg(ast) as ast, avg(alt) as alt, avg(Creatinine) as creatinine, 							
															avg(GADA) as GADA, avg(cPeptide) as cPeptide, avg(homa2B) as homa2B,							
															avg(Weight)/(avg(Height)*avg(Height)) as BMI	
															from 
															(
																select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork, max(ifnull(bloodworktime, eventtime)) as lastbloodwork
																from clinictestresults 
																where deleted = 0
																group by clientid 
															) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.lastbloodwork)
															group by clientid		
                                            ) c on s.clientid = c.clientid 
                                            where s.is_row_current = 'y'		
							 ) g on a.clientid = g.clientid  										
	where a.status in ('Active','pending_active', 'discharged','inactive')
) s 
-- where is_Exclusion = 'Yes'												
;	