-- SET collation_connection = 'latin1_swedish_ci';

create view v_bi_patient_foodlog_x_spike as 
select a.clientid, ct.patientname, ct.timezoneId, b.spikeid, a.categorytype as mealtype, 
a.eventdate as mealdate, a.eventtime mealtime, a.itemid as foodid, a.E5grading as health_score, a.quantity, a.measurementType,
b.multifood, c.spikeamount, c.valleyToPeakAUC, c.valleyToPeakSpread, c.valleyToEndAUC, c.valleyToEndSpread
from bi_food_supplement_log_detail a left join foodlogspikes b on a.clientid = b.clientid and a.eventdate = b.mealdate and a.itemid = b.foodid and a.categorytype = b.mealtype -- and time(eventtime) = b.mealDateTimeOnly
									 left join spikes c on b.spikeid = c.spikeid and b.clientid = c.clientid and c.deleted = 0
									 inner join v_client_tmp ct on a.clientid = ct.clientid
where a.category = 'foodlog'
;