alter view v_bi_patient_monitor_measures as 
select a.clientid, b.patientname, b.doctornameShort, b.coachnameShort, b.cohortGroup, b.status, b.treatmentdays, b.start_symptoms, b.start_medicines, 
ifnull(b.d10_success,'') as d10_success, ifnull(b.d35_success,'') as d35_success, 
measure_event_date, 
cgm_1d, cgm_5d, cgm_14d, GMI, ketone_1d, ketone_5d, eA1C, medicine,medicine_drugs,	
actionScore_1d, actionScore_5d, starRating_1d, starRating_5d, systolic_1d, diastolic_1d, systolic_5d, diastolic_5d, 
tac_1d, tac_5d, 
weight, BMI, steps, 
sleep_duration_minutes, deepSleep_mins, remSleep_mins, lightSleep_mins, awakeSleep_mins, 
energy, mood, symptoms, nut_sat as nut_happy, 
avg_rest_hr, cardio_hr_percentage, fitness_hr_percentage,
a.cohort, a.med_adh, a.is_InReversal, a.is_MetOnly_by_OPS, 
a.nut_syntax, a.nut_syntax_color, a.nut_adh_syntax, a.nut_adh_syntax_5d,
d.total_frictions
from bi_patient_measures a inner join v_client_tmp b on a.clientid = b.clientid
						   left join bi_patient_daily_heartrate_summary c on a.clientid = c.clientid and a.measure_event_date = c.eventdate
                           left join (
											select clientid, date(report_time) as report_date, count(distinct incidentid) total_frictions 
											from bi_incident_mgmt where category not in ('Approval') 
											group by clientid, date(report_time)
                                      ) d on a.clientid = d.clientid and a.measure_event_date = d.report_date
; 