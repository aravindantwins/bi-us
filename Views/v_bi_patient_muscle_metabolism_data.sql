alter view v_bi_patient_muscle_metabolism_data as 
select a.clientid, a.patientname, a.coachname, a.doctorname, a.age, a.cohortType, a.treatmentdays, a.source, a.source_2, a.customerName, a.start_labA1c, a.latest_labA1c, b.start_eGFR, b.latest_eGFR, a.start_BMI, a.latest_BMI
from v_client_tmp a left join 
(
		select s.clientid, s.first_bloodworktime, s.latest_bloodworktime, s.start_labA1c, s.latest_labA1c, s.start_creatinine, s.latest_creatinine,
		cast((141 * 
			power(if((start_creatinine/eGFR_Constant_K) < 1, (start_creatinine/eGFR_Constant_K), 1), eGFR_Constant_Alpha) * 
			power(if((start_creatinine/eGFR_Constant_K) > 1, (start_creatinine/eGFR_Constant_K), 1), -1.209) * 
			power(0.993, age) * 
			eGFR_Constant_Gender) as decimal(7,2)) as start_eGFR,
		cast((141 * 
			power(if((latest_creatinine/eGFR_Constant_K) < 1, (latest_creatinine/eGFR_Constant_K), 1), eGFR_Constant_Alpha) * 
			power(if((latest_creatinine/eGFR_Constant_K) > 1, (latest_creatinine/eGFR_Constant_K), 1), -1.209) * 
			power(0.993, age) * 
			eGFR_Constant_Gender) as decimal(7,2)) as latest_eGFR
		from 
		(
				select s1.clientid, s3.gender, s3.age, 
				if(s3.gender = 'FEMALE', 0.7, 0.9) as eGFR_Constant_K, 
				if(s3.gender = 'FEMALE', -0.329, -0.411) as eGFR_Constant_Alpha, 
				if(s3.gender = 'FEMALE', 1.018, 1) as eGFR_Constant_Gender, 
				s1.first_bloodworktime, s1.latest_bloodworktime,
				max(case when s1.first_bloodworkTime = ifnull(s2.bloodworktime, s2.eventtime) then s2.veinHba1c else null end) as start_labA1c, 
				max(case when s1.latest_bloodworktime = ifnull(s2.bloodworktime, s2.eventtime) then s2.veinHba1c else null end) as latest_labA1c, 
				max(case when s1.first_bloodworkTime = ifnull(s2.bloodworktime, s2.eventtime) then s2.creatinine else null end) as start_creatinine, 
				max(case when s1.latest_bloodworktime = ifnull(s2.bloodworktime, s2.eventtime) then s2.creatinine else null end) as latest_creatinine
				from 

				(
					select clientid, min(investigationDate) as first_bloodworkTime, max(investigationDate) as latest_bloodworktime -- this is because Bloodworktime is not coming for some of the investigations. Fix is being implemented in the product end.
					from 
					(
						select id, clientid, eventtime, bloodworktime, creatinine, veinHba1c, height, weight, if(bloodworktime is null, eventtime, bloodworktime) as investigationDate
						from clinictestresults
						where deleted = 0
					) s 
					group by clientid
				) s1 inner join clinictestresults s2 on s1.clientid = s2.clientid and (s1.first_bloodworkTime = ifnull(s2.bloodworktime, s2.eventtime) or s1.latest_bloodworktime = ifnull(s2.bloodworktime, s2.eventtime))
					 inner join v_Client_tmp s3 on s1.clientid = s3.clientid 
				group by s1.clientid
		) s 
) b on a.clientid = b.clientid
;

