set collation_connection = 'latin1_swedish_ci'; 

drop view v_bi_patient_nut_adherence; 

create view v_bi_patient_nut_adherence as 
select a.clientid, b.patientname, a.measure_event_date, round(macro) as macro_score, round(micro) as micro_score, round(biota) as biota_score, round(taste) as taste_score,
tac_1d, tac_5d, d.nut_syntax as allowed_syntax, a.nut_syntax, a.nut_syntax_color, a.nut_adh_syntax, a.nut_adh_syntax_5d
from bi_patient_measures a inner join v_client_tmp b on a.clientid = b.clientid 
						   left join nutritionscores c on a.clientid = c.clientid and a.measure_event_date = c.date
                           left join dim_client d on b.clientid = d.clientid and d.is_row_current = 'y'
; 
