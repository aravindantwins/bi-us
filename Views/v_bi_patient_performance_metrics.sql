alter view v_bi_patient_performance_metrics as 
select clientid, patientname, source_2, customerName, status, treatmentdays, TA_date, start_med_cnt, current_med_count, is_MetOnly as current_met_only,
baseline_eA1c, current_ea1c, 

d90_date_lab, d90_labA1c, 
case when d90_labA1c < 7 or baseline_eA1c-d90_labA1c > 2 then 'Yes' else '' end as d90_labA1c_flag_1, 
case when d90_labA1c < 7 then '< 7%' 
	 when baseline_eA1c-d90_labA1c > 2 then '2% decrease from baseline' else '' end as d90_labA1c_flag_reason_1, 

case when d90_labA1c < 8 or baseline_eA1c-d90_labA1c > 2 then 'Yes' else '' end as d90_labA1c_flag_2, 
case when d90_labA1c < 8 then '< 8%' 
	 when baseline_eA1c-d90_labA1c > 2 then '2% decrease from baseline' else '' end as d90_labA1c_flag_reason_2, 
     
d180_date_lab, d180_labA1c, 
case when d180_labA1c < 7 or baseline_eA1c-d180_labA1c > 2 then 'Yes' else '' end as d180_labA1c_flag_1, 
case when d180_labA1c < 7 then '< 7%' 
	 when baseline_eA1c - d180_labA1c > 2 then '2% decrease from baseline' else '' end as d180_labA1c_flag_reason_1, 
case when d180_labA1c < 8 or baseline_eA1c-d180_labA1c > 2 then 'Yes' else '' end as d180_labA1c_flag_2, 
case when d180_labA1c < 8 then '< 8%' 
	 when baseline_eA1c - d180_labA1c > 2 then '2% decrease from baseline' else '' end as d180_labA1c_flag_reason_2, 
     
d270_date_lab, d270_labA1c,      
case when d270_labA1c < 7 or baseline_eA1c-d270_labA1c > 2 then 'Yes' else '' end as d270_labA1c_flag_1, 
case when d270_labA1c < 7 then '< 7%' 
	 when baseline_eA1c - d270_labA1c > 2 then '2% decrease from baseline' else '' end as d270_labA1c_flag_reason_1, 
case when d270_labA1c < 8 or baseline_eA1c-d270_labA1c > 2 then 'Yes' else '' end as d270_labA1c_flag_2, 
case when d270_labA1c < 8 then '< 8%' 
	 when baseline_eA1c - d270_labA1c > 2 then '2% decrease from baseline' else '' end as d270_labA1c_flag_reason_2, 
     
d360_date_lab, d360_labA1c,      
case when d360_labA1c < 7 or baseline_eA1c-d360_labA1c > 2 then 'Yes' else '' end as d360_labA1c_flag_1, 
case when d360_labA1c < 7 then '< 7%' 
	 when baseline_eA1c - d360_labA1c > 2 then '2% decrease from baseline' else '' end as d360_labA1c_flag_reason_1,
case when d360_labA1c < 8 or baseline_eA1c-d360_labA1c > 2 then 'Yes' else '' end as d360_labA1c_flag_2, 
case when d360_labA1c < 8 then '< 8%' 
	 when baseline_eA1c - d360_labA1c > 2 then '2% decrease from baseline' else '' end as d360_labA1c_flag_reason_2,

case when start_med_cnt = 0 then 'NA'
	 when start_med_cnt > 0 and current_med_count = 0 then 'Yes'
	 when start_med_cnt > 0 and current_med_count = 1 and is_MetOnly = 'Yes' then 'Yes' 
     else 'No' end as MedFlag, 
     
case when start_med_cnt = 0 then 'No PreTwin Med'
	 when start_med_cnt > 0 and current_med_count = 0 then 'No Medications'
	 when start_med_cnt > 0 and current_med_count = 1 and is_MetOnly = 'Yes' then 'Metformin Only' 
     else 'Measure Not Met' end as MedFlag_reason 
from 
(
	select s.clientid, s.patientname, s.source_2, customerName, s.status, s.treatmentdays, s.TA_date,
	s.start_labA1c as baseline_eA1c, s.start_medicine_diabetes_drugs, ifnull(s.start_medicines,0) as start_med_cnt, 
	ea1c as current_ea1c, s1.medicine_drugs as current_medicine_drugs, ifnull(length(s1.medicine_drugs) - length(replace(s1.medicine_drugs,',','')) + 1, 0) as current_med_count, s1.is_MetOnly, 
	s.d90_date_lab,
    max(case when s.d90_date_lab = s2.investigationDate then s2.veinHba1c else null end) as d90_labA1c,
	s.d180_date_lab,
    max(case when s.d180_date_lab = s2.investigationDate then s2.veinHba1c else null end) as d180_labA1c,
    s.d270_date_lab, 
	max(case when s.d270_date_lab = s2.investigationDate then s2.veinHba1c else null end) as d270_labA1c,
    s.d360_date_lab,
	max(case when s.d360_date_lab = s2.investigationDate then s2.veinHba1c else null end) as d360_labA1c
	from 
	(
		select a.clientid, a.patientname, status, source_2, a.customerName, programStartDate_analytics as TA_date, treatmentdays, start_labA1c, start_medicine_diabetes_drugs, start_medicines, 
		d90_date_lab, d180_date_lab, d270_date_lab, d360_date_lab, durationYears_diabetes, dayBeforeDischaged
		from v_client_tmp a left join (	
											select clientid, max(status_start_date) as discharge_date, date_sub(max(status_start_date), interval 1 day) dayBeforeDischaged 
											from bi_patient_status where status = 'discharged' group by clientid
									  ) dis on a.clientid = dis.clientid
		where status in ('Active', 'Discharged', 'Inactive')
	) s left join bi_patient_measures s1 on s.clientid = s1.clientid and s1.measure_event_date = if(s.status = 'Active', date_sub(date(now()), interval 1 day), dayBeforeDischaged)
		left join (	
						select clientid, date(ifnull(bloodworktime, eventtime)) as investigationDate, veinHba1c
						from clinictestresults 
						where deleted = 0
				  ) s2 on s.clientid = s2.clientid and (s.d90_date_lab = s2.investigationDate or 
														s.d180_date_lab = s2.investigationDate or 
														s.d270_date_lab = s2.investigationDate or 
														s.d360_date_lab = s2.investigationDate)
	group by s.clientid 
) s 