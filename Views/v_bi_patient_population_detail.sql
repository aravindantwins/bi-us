set collation_connection = 'latin1_swedish_ci';

alter view v_bi_patient_population_detail as 
select b.clientid, a.date, c.patientname, c.doctorname, c.coachname, source_2 as client_partner, c.customerName, d.treatmentdays, d.cgm_5d, d.eA1C, 
cast((d.cgm_5d + 46.7)/28.7 as decimal(10,2)) as eA1C_5d, cast((d.cgm_1d + 46.7)/28.7 as decimal(10,2)) as eA1C_1d, 
start_labA1C, 
ifnull(start_medicines,0) as start_medicine, 
(length(d.medicine_drugs) - length(replace(d.medicine_drugs,',',''))) + 1 as current_medicine,
d.is_InReversal, d.is_MetOnly_by_OPS, d.cohort,
labA1c_d90, labA1c_d180, labA1c_d270, labA1c_d360,
is_reversal_eA1C60d_with_noMed, is_reversal_eA1C60d_with_only_met, eA1C_60d,
if(d.nonAdh_brittle_category is not null, 'Yes', null) as isNABrittle,
c.is_type1Like,
c.isPreDiabetic_Labelled 
from v_date_to_date a inner join (
									select clientid, max(status_start_date) as status_start_date, max(status_end_date) as status_end_date 
									from bi_patient_status b 
									where b.status = 'active'
                                    group by clientid
                                  ) b on a.date between b.status_start_date and b.status_end_date 
					  inner join v_Client_tmp c on b.clientid = c.clientid
                      left join bi_patient_measures d on b.clientid = d.clientid and a.date = d.measure_event_date
                      left join (
                                    select a.clientid, max(Case when bloodworkdate = d90_date_lab then veinHba1c else null end) as labA1c_d90, 
                                    max(Case when bloodworkdate = d180_date_lab then veinHba1c else null end) as labA1c_d180, 
                                    max(Case when bloodworkdate = d270_date_lab then veinHba1c else null end) as labA1c_d270, 
                                    max(Case when bloodworkdate = d360_date_lab then veinHba1c else null end) as labA1c_d360
                                    from 
                                    (
										select clientid, date(ifnull(bloodworktime, eventtime)) as bloodworkdate, veinHba1c from clinictestresults where deleted = 0 
                                    ) a inner join v_Client_tmp b on a.clientid = b.clientid
                                    group by a.clientid                                    
								) lab on c.clientid = lab.clientid
where patientname not like '%obsolete%'
and start_labA1C is not null
;
