-- SET collation_connection = 'latin1_swedish_ci';

alter view v_bi_patient_reversal_management as 
select date, coach, leadCoach, customerName, 
count(distinct case when treatmentdays > 35 then a.clientid else null end) as total_35D,
count(distinct case when treatmentdays > 35 and isUnqualified = 'Yes' then a.clientid else null end) as total_35D_Unqualified,
count(distinct case when treatmentdays > 35 and isNABrittle = 'Yes' then a.clientid else null end) as total_35D_NABrittle,

count(distinct case when treatmentdays > 35 and is_type1like = 'Yes' then a.clientid else null end) as total_35D_type1Like,
count(distinct case when treatmentdays > 35 and (is_medical_exclusion = 'Yes' or isExcluded = 'Yes') and isMedExclusion_AST_ALT_BMI = 'No' and isMedExclusion_Age = 'No' then a.clientid else null end) as total_35D_medExclusion,
count(distinct case when treatmentdays > 35 and (isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') then a.clientid else null end) as total_35D_medExclusion_to_Include,


-- count(distinct case when treatmentdays > 35 and daysFromLast5d > 98 then a.clientid else null end) as total_35D_NoCGM_98days, 
-- count(distinct case when treatmentdays > 35 and daysFromLast5d > 120 then a.clientid else null end) as total_35D_NoCGM_120days,
count(distinct case when treatmentdays > 35 and daysFromLast5d > 150 then a.clientid else null end) as total_35D_NoCGM_150days,

-- Commercial numbers
count(distinct case when treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and daysFromLast5d <= 150 and isExcluded = 'No' then a.clientid else null end) as total_35D_base_commercial,
						
count(distinct case when (treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and daysFromLast5d <= 150 and isExcluded = 'No' and ((is_InReversal = 'Yes' and is_MetOnly_by_OPS = 'No') or is_CBGtoCGM_to_include = 'Yes')) then a.clientid  else null end) as total_35D_base_InReversal_commercial, 

count(distinct case when treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and daysFromLast5d <= 150 and isExcluded = 'No' and (is_InReversal is null and is_MetOnly_by_OPS = 'Yes') and is_CBGtoCGM_to_include = 'No' then a.clientid   else null end) as total_35D_base_InReversal_WMet_commercial

from  
(
	select distinct a.clientid, coach, ct.leadCoachNameShort as leadCoach, ct.customerName, a.treatmentDays, a.date, datediff(a.date, b.last_available_cgm_5d_date) as daysFromLast5d, 
	b.is_InReversal, 
	b.is_MetOnly_by_OPS,
	if(ifnull(ct.start_medicines,0) > 0 or ct.start_labA1c >= 6.5, 'Yes', 'No') isDiabeticMet,
    if(ct.labels like '%unqualified%', 'Yes', 'No') as isUnqualified,
 	if(ct.labels like '%exclude%', 'Yes', 'No') as isExcluded,
	if(b.nonAdh_brittle_category is not null, 'Yes', 'No') as isNABrittle,
    b.nonAdh_brittle_category,
	ifnull(dc.is_type1like, 'No') as is_type1like,
    dc.is_medical_exclusion,
    case when dc.is_medical_exclusion = 'Yes' and (is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes') then 'Yes' 
		 when dc.is_medical_exclusion = 'Yes' and (is_InReversal is null and is_MetOnly_by_OPS = 'No') then 'No' 
         else null end as isMedExclusion_to_include,
         
    case when dc.is_medical_exclusion = 'Yes' and medical_exclusion_cohort in ('AST', 'ALT', 'BMI') then 'Yes' 
         else 'No' end as isMedExclusion_AST_ALT_BMI,
         
    case when dc.is_medical_exclusion = 'Yes' and medical_exclusion_cohort in ('Age') then 'Yes' 
         else 'No' end as isMedExclusion_Age,
	-- if(at.clientid is not null, 'Yes', 'No') as is_app_suspended,
	if(cb.clientid is not null, 'Yes', 'No') as is_CBGtoCGM_to_include      
	from 
	(
		select a.date, b.clientId, coach, b.startdate, (datediff(a.date, b.startDate) + 1) as treatmentDays --  (datediff(case when a.date between f.startdate and f.enddate then date_sub(b.startdate, interval 1 day) else a.date end, b.startDate) + 1) as treatmentDays
		from twins.v_date_to_date a
		inner join
		(
			select clientid, coach, startDate, endDate
			from
			(
				select clientid, coachname_short as coach, max(status_start_date) as startDate, max(Status_end_date) as endDate
				from bi_patient_status
				where status = 'active'
				group by clientid
			) s
		) b on a.date between b.startDate and b.endDate
		where a.date >= date_sub(date(itz(now())), interval 45 day) and a.date <= date(now())
	) a left join bi_patient_measures b on a.clientid = b.clientid and a.date = b.measure_event_date 
        left join v_client_tmp ct on a.clientid = ct.clientid 
        left join dim_client dc on a.clientid = dc.clientid and dc.is_row_current = 'y'
        left join (
					select s.clientid, s.cbgEndDate, date_add(s.cbgenddate, interval 1 day) as day1_after_cbg_is_over, date_add(s.cbgenddate, interval 7 day) as day7_after_cbg_is_over, 
                    s.dayFirst5dg_available, datediff(dayFirst5dg_available, cbgEndDate ) as dayFirst5dg_available_after_CBGisOver, 							
					s1.cgm_5d, -- s1.is_inReversal, s1.is_metonly_by_ops, 							
					case when is_InReversal is null and is_MetOnly_by_OPS = 'No' then 'Yes'							
						 when is_InReversal is null and is_MetOnly_by_OPS is null then 'NoCGM5d_yet'						
						 when is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes' then 'inReversal'
						 else 'No' end is_Escalation_finder						
					from 							
					(							
						select a.clientid, a.date as cbgEndDate, min(case when cgm_5d is not null then b.measure_event_date else null end) as dayFirst5dg_available						
						from 						
						(						
							select distinct b.clientid, a.date 					
							from v_date_to_date a inner join predictedcgmcycles b on a.date = b.enddate					
												  inner join v_client_tmp c on b.clientid = c.clientid
							where a.date >= date_sub(date(itz(now())), interval 45 day)
                            and datediff(b.enddate, b.startdate) >= 50
						) a  						
						 left join bi_patient_measures b on a.clientid = b.clientid and b.measure_event_date > a.date						
						 group by a.clientid, a.date						
					 ) s left join bi_patient_measures s1 on s.clientid = s1.clientid and s.dayFirst5dg_available = s1.measure_event_date 			
					) cb on a.clientid = cb.clientid and a.date between cb.day1_after_cbg_is_over and cb.day7_after_cbg_is_over        
        where ct.patientname not like '%obsolete%'
) a 
group by date, coach, leadCoach, customerName
;



