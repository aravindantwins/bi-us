-- SET collation_connection = 'latin1_swedish_ci';

alter view v_bi_patient_reversal_management_detail as 
select clientid, patient, coach, customerName, leadCoach, date, treatmentDays,  
medCount, medicine_drugs, medicine, 
as_5d, nut_adh_5d, cohortname, 
isPatientIncludedCommercial as isPatientIncluded,
cgm_5d,
is_InReversal,
is_MetOnly_by_OPS,
is_Escalation,
is_type1like,
is_medical_exclusion,
isMedExclusion_to_include,
isUnqualified,
isDiabeticMet,
daysFromLast5d,
isExcluded,
isNABrittle,
nonAdh_brittle_category,
totalEscalationDays,
isInReversal_previous_day
from 
(
	select clientid, patient, coach, customerName, leadCoach, date, treatmentDays, isDiabeticMet, daysFromLast5d,  
	medCount, ifnull(medicine_drugs,'') as medicine_drugs, ifnull(medicine,'') as medicine,
	as_5d,
	nut_adh_syntax_5d as nut_adh_5d,
    cgm_5d,
	cohortname,
   
	case when treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and daysFromLast5d <= 150 and isExcluded = 'No' then 'Yes' else 'No' end as isPatientIncludedCommercial,
    ifnull(is_InReversal, 'No') as is_InReversal,
	is_MetOnly_by_OPS,
	if(ifnull(is_InReversal,'No') = 'No' and ifnull(is_MetOnly_by_OPS,'No') = 'No', 'Yes', 'No') as is_Escalation,
    is_type1like,
    is_medical_exclusion,
    isMedExclusion_to_include,
    isUnqualified,
    isNABrittle,
    nonAdh_brittle_category,
    isExcluded,
    totalEscalationDays
    ,isInReversal_previous_day
	from 
	(
		select distinct a.clientid, ct.patientnameShort as patient, ct.doctornameShort as doctor, ct.customerName, coach, ct.leadCoachNameShort as leadCoach, a.date, datediff(a.date, b.last_available_cgm_5d_date) as daysFromLast5d, a.treatmentDays, 
		b.cohort as cohortname,
		b.is_InReversal, 
		b.is_MetOnly_by_OPS,
		b.isNoCGM_14days,
		b.cgm_5d,
		length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1 as medCount, 
		b.medicine, 
		b.medicine_drugs,
		b.actionScore_5d as as_5d,
		b.nut_adh_syntax_5d, 
		if(ifnull(ct.start_medicines,0) > 0 or ct.start_labA1c >= 6.5, 'Yes', 'No') isDiabeticMet,
		if(ct.labels like '%unqualified%', 'Yes', 'No') as isUnqualified,
		if(ct.labels like '%exclude%', 'Yes', 'No') as isExcluded,
        b.nonAdh_brittle_category, 
		if(b.nonAdh_brittle_category is not null, 'Yes', 'No') as isNABrittle,
		ifnull(dc.is_type1like, 'No') as is_type1like,
		dc.is_medical_exclusion,
		case when dc.is_medical_exclusion = 'Yes' and (b.is_InReversal = 'Yes' or b.is_MetOnly_by_OPS = 'Yes') then 'Yes' 
			when dc.is_medical_exclusion = 'Yes' and (b.is_InReversal is null and b.is_MetOnly_by_OPS = 'No') then 'No' 
			else null end as isMedExclusion_to_include,
		
        case when dc.is_medical_exclusion = 'Yes' and medical_exclusion_cohort in ('AST', 'ALT', 'BMI') then 'Yes' 
         else 'No' end as isMedExclusion_AST_ALT_BMI,
         
		case when dc.is_medical_exclusion = 'Yes' and medical_exclusion_cohort in ('Age') then 'Yes' 
         else 'No' end as isMedExclusion_Age,
        totalEscalationDays
		,if(b1.is_InReversal = 'Yes' or b1.is_MetOnly_by_OPS = 'Yes', 'Yes', 'No') as isInReversal_previous_day
		from 
		(
			select a.date, b.clientId, coach, b.startdate, (datediff(a.date, b.startDate) + 1) as treatmentDays --  (datediff(case when a.date between f.startdate and f.enddate then date_sub(b.startdate, interval 1 day) else a.date end, b.startDate) + 1) as treatmentDays
			from twins.v_date_to_date a
			inner join
			(
				select clientid, coach, startDate, endDate
				from
				(
					select clientid, coachname_short as coach, max(status_start_date) as startDate, max(Status_end_date) as endDate
					from bi_patient_status
					where status = 'active'
					group by clientid
				) s
			) b on a.date between b.startDate and b.endDate
			where a.date >= date_sub(date(now()), interval 14 day)   -- >  if(hour(itz(now())) < 8, date_sub(date(itz(now())), interval 1 day), date(itz(now())))
		) a left join bi_patient_measures b on a.clientid = b.clientid and a.date = b.measure_event_date 
			left join v_client_tmp ct on a.clientid = ct.clientid 
			left join dim_client dc on a.clientid = dc.clientid and dc.is_row_current = 'y'
            left join (
						select clientid, count(distinct case when is_InReversal is null and is_MetOnly_by_OPS = 'No' then measure_event_date else null end) totalEscalationDays, 
                        count(distinct case when is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes' then measure_event_date else null end) totalReversalDays
						from bi_patient_measures
						group by clientid 
					  )rd on a.clientid = rd.clientid 
			left join bi_patient_measures b1 on a.clientid = b1.clientid and a.date = date_sub(b1.measure_event_date, interval 1 day)
            where ct.patientname not like '%obsolete%'

	) s 
) s 
-- where isPatientIncludedCommercial = 'Yes' 
;

