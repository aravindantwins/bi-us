-- set collation_connection = 'latin1_swedish_ci';

create view v_bi_patient_sleep_measures as 
select clientid, measure_event_date, sleep_duration_minutes, deepSleep_mins, remSleep_mins, lightSleep_mins, awakeSleep_mins
from bi_patient_measures; 
