create view v_bi_patient_twin10_disease_reversal_dashboard as 
select date, 

count(distinct case when isPatientIncluded = 'Yes' then clientid else null end) as total_Diabetes_included, 
count(distinct case when isPatientIncluded = 'Yes' and (actual_medCount <= start_medCount) and last_available_eA1c < start_labA1c then clientid else null end) as total_diabetes_improved,
count(distinct case when isPatientIncluded = 'Yes' and (is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes') then clientid else null end) as total_diabetes_reversal, 

count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' then clientid else null end) as total_otherDisease_included,
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and first_available_BMI > 25 then clientid else null end) as total_Obesity_included, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and first_available_BMI > 25 and last_available_BMI is not null and daysFromLast1dWeight < 30 then clientid else null end) as total_Obesity_reversal_base, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and first_available_BMI > 25 and last_available_BMI is not null and daysFromLast1dWeight < 30  and last_available_BMI < first_available_BMI then clientid else null end) as total_Obesity_base_improved, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and first_available_BMI > 25 and last_available_BMI is not null and daysFromLast1dWeight < 30 and last_available_BMI < 25 then clientid else null end) as total_Obesity_reversed,


count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and (start_tg_hdl_ratio >= 4 or start_conditions like '%Dyslipidemia%' or start_CHOL_medicine_drugs > 0) then clientid else null end) as total_Dysl_included, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and (start_tg_hdl_ratio >= 4 or start_conditions like '%Dyslipidemia%' or start_CHOL_medicine_drugs > 0) and last_tg_hdl_ratio is not null then clientid else null end) as total_Dysl_reversal_base, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and (start_tg_hdl_ratio >= 4 or start_conditions like '%Dyslipidemia%' or start_CHOL_medicine_drugs > 0) and last_tg_hdl_ratio is not null and last_tg_hdl_ratio < start_tg_hdl_ratio and ifnull(current_CHOL_medicine_drugs,0) <= ifnull(start_CHOL_medicine_drugs,0) then clientid else null end) as total_Dysl_base_improved, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and (start_tg_hdl_ratio >= 4 or start_conditions like '%Dyslipidemia%' or start_CHOL_medicine_drugs > 0) and last_tg_hdl_ratio is not null and last_tg_hdl_ratio < 4 and ifnull(current_CHOL_medicine_drugs,0) = 0 then clientid else null end) as total_Dysl_reversed,


count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_eGFR < 90 then clientid else null end) as total_KidneyHealth_included, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_eGFR < 90 and latest_eGFR is not null then clientid else null end) as total_KidneyHealth_reversal_base, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_eGFR < 90 and latest_eGFR is not null and latest_eGFR > start_eGFR then clientid else null end) as total_KidneyHealth_base_improved, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_eGFR < 90 and latest_eGFR is not null and latest_eGFR >= 90 then clientid else null end) as total_KidneyHealth_reversed, 
 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_homa2IR >= 2.72 then clientid else null end) as total_InsulinResistance_included, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_homa2IR >= 2.72 and last_homa2IR is not null then clientid else null end) as total_InsulinResistance_reversal_base,  
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_homa2IR >= 2.72 and last_homa2IR is not null and last_homa2IR < start_homa2IR then clientid else null end) as total_InsulinResistance_base_improved,  
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_homa2IR >= 2.72 and last_homa2IR is not null and last_homa2IR  < 2.72 then clientid else null end) as total_InsulinResistance_reversed,

count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_homa2b <= 54.2 then clientid else null end) as total_PancreasHealth_included, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_homa2b <= 54.2 and last_homa2b is not null then clientid else null end) as total_PancreasHealth_reversal_base, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_homa2b <= 54.2 and last_homa2b is not null and last_homa2b > start_homa2b then clientid else null end) as total_PancreasHealth_base_improved, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_homa2b <= 54.2 and last_homa2b is not null and last_homa2b > 54.2 then clientid else null end) as total_PancreasHealth_reversed,

count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and ((start_systolicBP >= 140 or start_diastolicBP >= 90) or start_HTN_medicine_drugs > 0 or start_conditions like '%Hypertension%') then clientid else null end) as total_HTN_included, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and ((start_systolicBP >= 140 or start_diastolicBP >= 90) or start_HTN_medicine_drugs > 0 or start_conditions like '%Hypertension%') 
					and (last_systolicBP is not null and last_diastolicBP is not null) then clientid else null end) as total_HTN_reversal_base, 
                    
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and ((start_systolicBP >= 140 or start_diastolicBP >= 90) or start_HTN_medicine_drugs > 0 or start_conditions like '%Hypertension%') 
					and (last_systolicBP is not null and last_diastolicBP is not null) 
                    and (last_systolicBP < start_systolicBP or last_diastolicBP < start_diastolicBP) 
                    and ifnull(current_HTN_medicine_drugs,0) <= ifnull(start_HTN_medicine_drugs,0) then clientid else null end) as total_HTN_base_improved, 
                    
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and ((start_systolicBP >= 140 or start_diastolicBP >= 90) or start_HTN_medicine_drugs > 0 or start_conditions like '%Hypertension%') 
					and (last_systolicBP is not null and last_diastolicBP is not null) 
                    and (last_systolicBP < 140 and last_diastolicBP < 90 and ifnull(current_HTN_medicine_drugs,0) = 0) then clientid else null end) as total_HTN_reversed,

count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_hsCRP >= 3 then clientid else null end) as total_Inflammation_included, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_hsCRP >= 3 and last_hsCRP is not null then clientid else null end) as total_Inflammation_reversal_base, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_hsCRP >= 3 and last_hsCRP is not null and last_hsCRP < start_hsCRP then clientid else null end) as total_Inflammation_base_improved, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_hsCRP >= 3 and last_hsCRP is not null and last_hsCRP < 3 then clientid else null end) as total_Inflammation_reversed,

count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_NAFLD_LFS >= -0.64 then clientid else null end) as total_LiverHealth_included, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_NAFLD_LFS >= -0.64 and last_NAFLD_LFS is not null then clientid else null end) as total_LiverHealth_reversal_base, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_NAFLD_LFS >= -0.64 and last_NAFLD_LFS is not null and last_NAFLD_LFS < start_NAFLD_LFS then clientid else null end) as total_LiverHealth_base_improved, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_NAFLD_LFS >= -0.64 and last_NAFLD_LFS is not null and last_NAFLD_LFS < -0.64 then clientid else null end) as total_LiverHealth_reversed, 


count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_FRS >= 20 then clientid else null end) as total_HeartHealth_included, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_FRS >= 20 and last_FRS is not null then clientid else null end) as total_HeartHealth_reversal_base, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_FRS >= 20 and last_FRS is not null and last_FRS < start_FRS then clientid else null end) as total_HeartHealth_base_improved, 
count(distinct case when isPatientIncludedForOtherDiseases = 'Yes' and start_FRS >= 20 and last_FRS is not null and last_FRS < 20 then clientid else null end) as total_HeartHealth_reversed


from 

(
	select date, clientid, coach, treatmentdays, daysFromLast5d, 
	isExcluded,
	isNABrittle, 
    is_medical_exclusion,
	isMedExclusion_AST_ALT_BMI,
	isMedExclusion_Age,
	is_app_suspended,
    is_InReversal,
    is_MetOnly_by_OPS,
	first_available_BMI, last_available_BMI, 
	start_tg_hdl_ratio, last_tg_hdl_ratio, 
	start_eGFR, latest_eGFR, 
	start_conditions, 
	start_hsCRP, last_hsCRP,
	start_homa2IR, last_homa2IR,
	start_homa2b, last_homa2b,
    start_NAFLD_LFS, last_NAFLD_LFS,
    start_FRS, last_FRS,
    start_CHOL_medicine_drugs, current_CHOL_medicine_drugs,
    start_HTN_medicine_drugs, current_HTN_medicine_drugs, 
    start_labA1c, last_available_eA1c, start_medCount, actual_medCount, 
    start_systolicBP, start_diastolicBP,
    last_systolicBP, last_diastolicBP,
    daysFromLast1dWeight,
    case when treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and daysFromLast5d <= 150 and isExcluded = 'No' then 'Yes' else null end as isPatientIncluded,
    case when treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isExcluded = 'No' then 'Yes' else null end as isPatientIncludedForOtherDiseases

	from 
	(
		select distinct a.clientid, coach, a.treatmentDays, a.date, last_available_cgm_5d, 
        datediff(a.date, last_available_cgm_5d_date) as daysFromLast5d, 
		b.is_InReversal, 
		b.is_MetOnly_by_OPS,
		-- cx.suspended,
		if(ifnull((length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0) > 0 or dc.start_labA1c >= 6.5, 'Yes', 'No') isDiabeticMet,
		if(b.nonAdh_brittle_category is not null, 'Yes', 'No') as isNABrittle,
		ifnull(dc.is_type1like, 'No') as is_type1like,
		dc.is_medical_exclusion,
		case when dc.is_medical_exclusion = 'Yes' and (is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes') then 'Yes' 
			 when dc.is_medical_exclusion = 'Yes' and (is_InReversal is null and is_MetOnly_by_OPS = 'No') then 'No' 
			 else null end as isMedExclusion_to_include,
			 
		case when dc.is_medical_exclusion = 'Yes' and medical_exclusion_cohort in ('AST', 'ALT', 'BMI') then 'Yes' 
			 else 'No' end as isMedExclusion_AST_ALT_BMI,
			 
		case when dc.is_medical_exclusion = 'Yes' and medical_exclusion_cohort in ('Age') then 'Yes' 
			 else 'No' end as isMedExclusion_Age,
		if(cx.id is not null, 'Yes', 'No') as is_app_suspended,
        if(ct.labels like '%EXCLUDE%', 'Yes', 'No') as isExcluded,
		-- ifnull(dc.start_BMI, cast(dc.start_weight/(dc.height*dc.height) as decimal(10,2))) as first_available_BMI, 
        dc.start_BMI_bcm as first_available_BMI, 
		-- ifnull(cast(dc.last_available_weight/(dc.height*dc.height) as decimal(10,2)), dc.last_available_BMI) as last_available_BMI,
        dc.last_available_BMI as last_available_BMI,
		start_tg_hdl_ratio, last_tg_hdl_ratio, 
		start_eGFR, latest_eGFR, 
		start_conditions, 
		start_hsCRP, last_hsCRP,
		start_homa2IR, last_homa2IR,
		start_homa2b, last_homa2b,
        start_NAFLD_LFS, last_NAFLD_LFS,
        start_FRS, last_FRS,
        length(start_CHOL_medicine_drugs) - length(replace(start_CHOL_medicine_drugs,',','')) + 1 as start_CHOL_medicine_drugs, 
        length(current_CHOL_medicine_drugs) - length(replace(current_CHOL_medicine_drugs,',','')) + 1 as current_CHOL_medicine_drugs,
        length(start_HTN_medicine_drugs) - length(replace(start_HTN_medicine_drugs,',','')) + 1 as start_HTN_medicine_drugs, 
        length(current_HTN_medicine_drugs) - length(replace(current_HTN_medicine_drugs,',','')) + 1 as current_HTN_medicine_drugs,
        dc.start_labA1c,  cast((46.7 + last_available_cgm_5d) / 28.7 as decimal(10,2)) as last_available_eA1c,
        ifnull((length(dc.start_medicine_diabetes_drugs) - length(replace(dc.start_medicine_diabetes_drugs,',','')) + 1),0) as start_medCount, ifnull((length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0) as actual_medCount,
		dc.start_systolicBP, dc.start_diastolicBP,
		dc.last_systolicBP, dc.last_diastolicBP,
        b.last_available_1d_weight_date,
        datediff(a.date, last_available_1d_weight_date) as daysFromLast1dWeight
		from 
		(
			select a.date, b.clientId, coach, b.startdate, (datediff(a.date, b.startDate) + 1) as treatmentDays --  (datediff(case when a.date between f.startdate and f.enddate then date_sub(b.startdate, interval 1 day) else a.date end, b.startDate) + 1) as treatmentDays
			from twins.v_date_to_date a
			inner join
			(
				select clientid, coach, startDate, endDate
				from
				(
					select clientid, coachname_short as coach, max(status_start_date) as startDate, max(Status_end_date) as endDate
					from bi_patient_status
					where status = 'active'
					group by clientid
				) s
			) b on a.date between b.startDate and b.endDate
			where a.date >= date_sub(date(itz(now())), interval 45 day) and a.date <= date(itz(now()))
            -- where a.date = '2021-06-25'
		) a left join bi_patient_measures b on a.clientid = b.clientid and a.date = b.measure_event_date 
			left join v_client_tmp ct on a.clientid = ct.clientid 
			left join dim_client dc on a.clientid = dc.clientid and dc.is_row_current = 'y'
            left join clientauxfields cx on a.clientid = cx.id and cx.suspended = 1
			where ct.patientname not like '%obsolete%'
	) a 
) s 
group by date -- , coach, leadCoach
;


