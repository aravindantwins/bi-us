set collation_connection = 'latin1_swedish_ci'; 

CREATE 
VIEW `v_bi_rct_patient_measures_daily_x_lab` AS
    SELECT 
        `f1`.`clientid` AS `clientid`,
        `f2`.`patientName` AS `patientName`,
        'TWIN_PRIME' AS `labels`,
        `f2`.`status` AS `status`,
        `f2`.`doctorName` AS `doctorName`,
        `f2`.`coachName` AS `coachName`,
        `f3`.`treatmentDays` AS `treatmentDays`,
        `f2`.`gender` AS `gender`,
        `f2`.`age` AS `age`,
        `f2`.`durationYears_diabetes` AS `durationYears_diabetes`,
        `f1`.`category` AS `measure_category`,
        AVG(`f1`.`D0`) AS `D0_measure`,
        AVG(`f1`.`D30`) AS `D30_measure`,
        AVG(`f1`.`D90`) AS `D90_measure`,
        AVG(`f1`.`D180`) AS `D180_measure`,
        AVG(`f1`.`D270`) AS `D270_measure`,
        AVG(`f1`.`D360`) AS `D360_measure`
    FROM
        (((SELECT 
            `f`.`clientid` AS `clientid`,
                `f`.`date` AS `date`,
                `f`.`category` AS `category`,
                AVG((CASE
                    WHEN (`f`.`date_category` = 'D0') THEN CAST(`f`.`measure` AS DECIMAL (10 , 2 ))
                END)) AS `D0`,
                AVG((CASE
                    WHEN (`f`.`date_category` = 'D30') THEN CAST(`f`.`measure` AS DECIMAL (10 , 2 ))
                END)) AS `D30`,
                AVG((CASE
                    WHEN (`f`.`date_category` = 'D90') THEN CAST(`f`.`measure` AS DECIMAL (10 , 2 ))
                END)) AS `D90`,
                AVG((CASE
                    WHEN (`f`.`date_category` = 'D180') THEN CAST(`f`.`measure` AS DECIMAL (10 , 2 ))
                END)) AS `D180`,
                AVG((CASE
                    WHEN (`f`.`date_category` = 'D270') THEN CAST(`f`.`measure` AS DECIMAL (10 , 2 ))
                END)) AS `D270`,
                AVG((CASE
                    WHEN (`f`.`date_category` = 'D360') THEN CAST(`f`.`measure` AS DECIMAL (10 , 2 ))
                END)) AS `D360`
        FROM
            `bi_twins_rct_health_output_measures` `f`
        GROUP BY `f`.`clientid` , `f`.`date` , `f`.`category`) `f1`
        LEFT JOIN (SELECT 
            `v_allclients_rct`.`clientid` AS `clientid`,
                `v_allclients_rct`.`patientName` AS `patientName`,
                `v_allclients_rct`.`age` AS `age`,
                `v_allclients_rct`.`labels` AS `labels`,
                `v_allclients_rct`.`status` AS `status`,
                `v_allclients_rct`.`doctorName` AS `doctorName`,
                `v_allclients_rct`.`coachName` AS `coachName`,
                `v_allclients_rct`.`gender` AS `gender`,
                CAST(`v_allclients_rct`.`durationYears_diabetes`
                    AS DECIMAL (10 , 2 )) AS `durationYears_diabetes`
        FROM
            `v_allclients_rct`) `f2` ON ((`f1`.`clientid` = `f2`.`clientid`)))
        LEFT JOIN (SELECT 
            `dim_client`.`clientid` AS `clientid`,
                `dim_client`.`treatmentDays` AS `treatmentDays`
        FROM
            `dim_client`
        WHERE
            (`dim_client`.`is_row_current` = 'y')) `f3` ON ((`f1`.`clientid` = `f3`.`clientid`)))
    GROUP BY `f1`.`clientid` , `f1`.`category`