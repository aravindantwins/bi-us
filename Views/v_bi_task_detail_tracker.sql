-- set collation_connection = 'latin1_swedish_ci';

-- drop view v_bi_task_detail_tracker;

create view v_bi_task_detail_tracker as 
select a.notificationID, a.clientid, ct.coachnameShort as coach, a.targetUserId, concat(c.firstname,' ',c.lastname) as userName, r.roleType, 
convert_tz(a.eventtime, 'UTC', u.timeZoneId) as eventtime, 
convert_tz(a.datemodified, 'UTC', u.timeZoneId) as datemodified, 
date_add(convert_tz(a.eventtime, 'UTC', u.timeZoneId), interval slaInMinutes minute) as SLA_End_time, 
actionStatus, slaInMinutes, b.message, b.sourceName, a.category,
nc.comment, 
nc.isDeleted,
nc.dateAdded 
from notifications a left join twinspii.notificationspii b on a.notificationID = b.notificationID
					 left join twinspii.userspii c on a.targetUserID = c.id
                     left join users u on c.id = u.id
                     left join roles r on u.roleID = r.id
                     LEFT JOIN notificationcomments nc ON a.notificationId = nc.notificationId 
                     inner join v_client_tmp ct on a.clientid = ct.clientid 
where a.status not in ('cancelled') and date(eventtime) >= '2022-06-01' and date(eventtime) <= date(now())
and b.sourceName = 'TASK_SCHEDULER'
;
