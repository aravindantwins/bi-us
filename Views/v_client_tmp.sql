-- 	set collation_connection = 'latin1_swedish_ci';

	alter view v_client_tmp as 
    SELECT 
        `c`.`ID` AS `clientId`,
        date(now()) as date, 
        dc.treatmentdays, 
        UPPER(CONCAT(`cp`.`firstName`, ' ', `cp`.`lastName`)) AS `patientName`,
        UPPER(CONCAT(`cp`.`firstName`,
                        ' ',
                        LEFT(`cp`.`lastName`, 1))) AS `patientNameShort`,
        ((YEAR(CURDATE()) - YEAR(`cp`.`dateOfBirth`)) - (DATE_FORMAT(CURDATE(), '%m%d') < DATE_FORMAT(`cp`.`dateOfBirth`, '%m%d'))) AS `age`,
        c.doctorId,
        UPPER(CONCAT(`d`.`firstName`, ' ', `d`.`lastName`)) AS `doctorName`,
        UPPER(CONCAT(`d`.`firstName`,
                        ' ',
                        LEFT(`d`.`lastName`, 1))) AS `doctorNameShort`,
		d.clinicName,
        UPPER(CONCAT(`h`.`firstName`, ' ', `h`.`lastName`)) AS `coachName`,
        UPPER(CONCAT(`h`.`firstName`,
                        ' ',
                        LEFT(`h`.`lastName`, 1))) AS `coachNameShort`,
		upper(concat(up.firstName ,' ' ,left(up.lastName, 1))) as leadCoachNameShort,
		upper(concat(up1.firstName ,' ' ,up1.lastName)) as intakeRN,
		upper(concat(up2.firstName ,' ' ,up2.lastname)) as medicalAssistant,
		upper(concat(up3.firstName ,' ' ,up3.lastname)) as registeredNurse,
				
		cp.dateOfBirth,
		c.gender, 
		c.race,
		c.ethnicity,
		ap.city,
		ap.postalCode,
        `c`.`dateAdded` AS `dateAdded`,
        `c`.`dateModified` AS `dateModified`,
        `c`.`status` AS `status`,
        `t`.`name` AS `cohortGroup`,
        `c`.`enrollmentDate` AS `enrollmentDate`,
        `c`.`lastStatusChange` AS `lastStatusChange`,
        `c`.`renewalVisitScheduled` AS `renewalVisitScheduledDate`,
        `c`.`dischargeReason` AS `dischargeReason`,
        upper(dc.start_symptoms) as start_symptoms_list,
        (length(dc.start_symptoms) - length(replace(dc.start_symptoms,',',''))) + 1 as start_symptoms, 
		(length(dc.start_conditions) - length(replace(dc.start_conditions,',',''))) + 1 as start_conditions, 
        (length(dc.start_medicine_diabetes_drugs) - length(replace(dc.start_medicine_diabetes_drugs,',',''))) + 1 as start_medicines, 
        if(start_medicine_diabetes_drugs like '%Biguanide(DIABETES)%', 
				(length(start_medicine_diabetes_drugs) - length(replace(start_medicine_diabetes_drugs,',','')) + 1) - 1, 
				length(start_medicine_diabetes_drugs) - length(replace(start_medicine_diabetes_drugs,',','')) + 1) as start_medicines_without_Metformin, 
        start_medicine_diabetes,
        start_medicine_diabetes_drugs,
        dc.d10_success,
        dc.d35_success,
        (CASE
            WHEN (`c`.`status` = 'Active') THEN (TO_DAYS(NOW()) - TO_DAYS(`c`.`enrollmentDate`))
            WHEN (`c`.`status` <> 'Active') THEN (TO_DAYS(`c`.`lastStatusChange`) - TO_DAYS(`c`.`enrollmentDate`))
        END) AS `daysEnrolled`,
        (CASE
            WHEN (`c`.`status` = 'Active') THEN (FLOOR(((TO_DAYS(`c`.`termEndDate`) - TO_DAYS(`c`.`enrollmentDate`)) / 84)) - 1)
            WHEN (`c`.`status` = 'INACTIVE') THEN FLOOR(((TO_DAYS(`c`.`lastStatusChange`) - TO_DAYS(`c`.`enrollmentDate`)) / 84))
        END) AS `renewalCount`,
        `c`.`firstDiabetesInReversalTime` AS `firstDiabetesInReversalTime`,
		c.latestDiabetesInReversalTime, 
        c.currentDiabetesInReversal,
        c.activationDate,
       -- `c`.`termEndDate` AS `termEndDate`,
       -- `c`.`planCode` AS `planCode`,
        c.labels,
        c.sourceType as source,
        c.source as source_2,
        c1.customerType,
        c1.name as customerName, 
        c.timezoneId,
        dc.durationYears_diabetes,
        dc.programStartDate_analytics,
        dc.start_LabA1c,
        dc.start_height,
        dc.start_weight,
        cast(dc.start_weight/(dc.start_height*dc.start_height) as decimal(10,2)) as start_BMI,
        -- tr.hotButtonIssues as motivations,
        dc.motivations as motivations,
        dc.latest_LabA1C,
        dc.last_available_weight,
        cast(dc.last_available_weight/(dc.start_height*dc.start_height) as decimal(10,2)) as latest_BMI,
        t.cohortType,
        dc.d90_date_lab,
        dc.d180_date_lab,
        dc.d270_date_lab,
        dc.d360_date_lab,
        dc.is_type1Like,
        ptwin.ptwin_meds as start_meds_unformatted,
		(length(ptwin.ptwin_meds) - length(replace(ptwin.ptwin_meds,',',''))) + 1 as start_meds_unformatted_count, 
		(length(ptwin.ptwin_meds_wo_metformin) - length(replace(ptwin.ptwin_meds_wo_metformin,',',''))) + 1 as start_meds_wo_metformin_unformatted_count,
		c.notes,
		if(c.labels like '%PREDIABETES%', 'Yes', 'No') as isPreDiabetic_Labelled,
        if(t.cohortType LIKE '%MANUAL%', 'Yes','No') AS isMemberOnManualCohort

    FROM
        `twins`.`clients` `c`
        JOIN `twinspii`.`clientspii` `cp` ON ((`c`.`ID` = `cp`.`ID`))
        LEFT JOIN twinspii.addressespii ap on c.addressId = ap.ID
        JOIN `twins`.`doctors` `d` ON ((`c`.`doctorId` = `d`.`ID`))
        -- left join twins.cohorts ch on c.cohortId = ch.cohortId
        LEFT JOIN `twins`.`coaches` `h` ON ((`c`.`coachId` = `h`.`ID`))
        LEFT JOIN `twins`.`cohorts` `t` ON ((`c`.`cohortId` = `t`.`cohortId`))
        LEFT JOIN twins.dim_client dc on c.id = dc.clientid and dc.is_row_current='Y'
        left join customers c1 on c.customerID = c1.ID
    	LEFT JOIN twinspii.userspii up on h.leadCoachID = up.id
        LEFT JOIN (
							select s.id as clientid, 
							ltrim(rtrim(group_concat(distinct concat('  ',s.allMeds, ' ', s.allMedsDosage)))) as ptwin_meds, 
							ltrim(rtrim(group_concat(distinct concat('  ',s.allMedWoMet, ' ', s.allMedWoMetDosage)))) as ptwin_meds_wo_metformin
							from 
							(
									select distinct id, a1.name as allMeds, a1.dosage as allMedsDosage, b1.name as allMedWoMet, b1.dosage as allMedWoMetDosage
									
									from clients a left join  
									(
												select clientid, a.name, a.medicationID, cast(concat('(',truncate(a.breakfastDosage,0), '-', truncate(a.lunchDosage,0), '-', truncate(a.dinnerDosage,0), ')') as char(20)) as dosage
												from clinicreportmedications a inner join medicines b on a.medicationID = b.medicineID							
																			  left join medicinepharmclasses mpc on b.medicineId = mpc.medicineId
																			  left join pharmclasses pc on mpc.pharmclassId = pc.pharmclassId											
												where hasmedicine = 1							
												and a.medicalcondition like '%Diabetes%'	
									) a1 on a.id = a1.clientid 
									left join 
									(
												select clientid, a.name, a.medicationID, cast(concat('(',truncate(a.breakfastDosage,0), '-', truncate(a.lunchDosage,0), '-', truncate(a.dinnerDosage,0), ')') as char(20)) as dosage
												from clinicreportmedications a inner join medicines b on a.medicationID = b.medicineID							
																			  left join medicinepharmclasses mpc on b.medicineId = mpc.medicineId
																			  left join pharmclasses pc on mpc.pharmclassId = pc.pharmclassId											
												where hasmedicine = 1							
												and a.medicalcondition like '%Diabetes%'	
												and pc.epc not like '%Biguanide%'
									) b1 on a.id = b1.clientid 
							) s 
							group by s.id 
				)ptwin on c.id = ptwin.clientid
		LEFT JOIN twins.clientauxfields cx ON c.id = cx.id 
		LEFT JOIN twinspii.userspii up1 ON cx.intakernuserid = up1.ID
		left join twinspii.userspii up2 on cx.medicalassistantuserid = up2.ID
		left join twinspii.userspii up3 on cx.rnTwinOperatedUserId  = up3.ID
		
    WHERE
        ((`c`.`status` IN ('ACTIVE' , 'REGISTRATION', 'INACTIVE', 'DISCHARGED', 'PENDING_ACTIVE'))
            AND (`c`.`deleted` = 0)
            AND (`d`.`test` = FALSE)
            AND (c.test = FALSE)
            AND (NOT ((`cp`.`firstName` LIKE '%TEST%')))
            AND (NOT ((`cp`.`lastName` LIKE '%TEST%'))))
    ORDER BY `c`.`enrollmentDate`
    ;
    
