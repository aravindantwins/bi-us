-- set collation_connection='latin1_swedish_ci';
alter view v_daily_mealplan
as 
select a.planname, measuregroup, measurecategory, concat('Day ',a.day) as day
, concat(ifnull(a.foodlabel,''),':', floor(d.quantity),' ',a.measure) as measurevalue
from
(
	select a.planName, 'mealtype' as measureGroup, mealtype as measureCategory, b.day, c.foodid, c.measure, a.vegetarian, 
	foodlabel
	from dailyplantemplates a,
	   dailyplantemplatemappings b, 
	   foods c
	where a.dailyplantemplateid = b.dailyplantemplateid
	and b.foodid = c.foodid
) a
left outer join defaultfoodplans d on a.foodid = d.foodid and a.measurecategory = d.mealtype and a.vegetarian = d.vegetarian

union all

select a.planname, a.measuregroup, a.measurecategory, concat('Day ',a.day) as day, convert(sum(a.netcarb_measurevalue * d.quantity), decimal(7,2)) as measurevalue
from
(
	select a.planName, 'L1' as measureGroup,'Netcarb' as measureCategory, b.day, a.vegetarian, c.foodid, b.mealtype, carb - fibre as netcarb_measureValue
	from dailyplantemplates a,
		dailyplantemplatemappings b,
		foods c 
	where a.dailyplantemplateid = b.dailyplantemplateid
	and b.foodid = c.foodid
) a
left outer join defaultfoodplans d on a.foodid = d.foodid and a.mealtype = d.mealtype and a.vegetarian = d.vegetarian
group by a.planname, a.day

union all

select a.planname, a.measuregroup, a.measurecategory, concat('Day ',a.day) as day, convert((sum(a.calories) * d.quantity), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Calories' as measureCategory, b.day, a.vegetarian, c.foodid, b.mealtype, calories
from dailyplantemplates a,
    dailyplantemplatemappings b,
    foods c 
where a.dailyplantemplateid = b.dailyplantemplateid
and b.foodid = c.foodid
) a
left outer join defaultfoodplans d on a.foodid = d.foodid and a.mealtype = d.mealtype and a.vegetarian = d.vegetarian
group by a.planname, a.day

union all

select a.planname, a.measuregroup, a.measurecategory, concat('Day ',a.day) as day, convert((sum(a.ngc_measureValue) * d.quantity), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'NGC' as measureCategory, b.day, a.vegetarian, c.foodid, b.mealtype, (IFNULL((((carb - fibre) * glycemicIndex) / 55), 0)) as ngc_measureValue
from dailyplantemplates a,
    dailyplantemplatemappings b,
    foods c 
where a.dailyplantemplateid = b.dailyplantemplateid
and b.foodid = c.foodid
) a
left outer join defaultfoodplans d on a.foodid = d.foodid and a.mealtype = d.mealtype and a.vegetarian = d.vegetarian
group by a.planname, a.day

union all

select a.planname, a.measuregroup, a.measurecategory, concat('Day ',a.day) as day, convert((sum(a.fibre) * d.quantity), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Fibre' as measureCategory, b.day, a.vegetarian, c.foodid, b.mealtype, fibre
from dailyplantemplates a,
     dailyplantemplatemappings b,
     foods c 
where a.dailyplantemplateid = b.dailyplantemplateid
and b.foodid = c.foodid
) a
left outer join defaultfoodplans d on a.foodid = d.foodid and a.mealtype = d.mealtype and a.vegetarian = d.vegetarian
group by a.planname, a.day

union all

select a.planname, a.measuregroup, a.measurecategory, concat('Day ',a.day) as day, convert((sum(a.fat) * d.quantity), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Fat' as measureCategory, b.day, a.vegetarian, c.foodid, b.mealtype, fat
from dailyplantemplates a,
    dailyplantemplatemappings b,
    foods c 
where a.dailyplantemplateid = b.dailyplantemplateid
and b.foodid = c.foodid
) a
left outer join defaultfoodplans d on a.foodid = d.foodid and a.mealtype = d.mealtype and a.vegetarian = d.vegetarian
group by a.planname, a.day

union all

select a.planname, a.measuregroup, a.measurecategory, concat('Day ',a.day) as day, convert((sum(a.protein) * d.quantity), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Protein' as measureCategory, b.day, a.vegetarian, c.foodid, b.mealtype, protein
from dailyplantemplates a,
    dailyplantemplatemappings b,
    foods c 
where a.dailyplantemplateid = b.dailyplantemplateid
and b.foodid = c.foodid
) a
left outer join defaultfoodplans d on a.foodid = d.foodid and a.mealtype = d.mealtype and a.vegetarian = d.vegetarian
group by a.planname, a.day

union all

select a.planname, a.measuregroup, a.measurecategory, concat('Day ',a.day) as day, convert((sum(a.carb) * d.quantity), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Total carb' as measureCategory, b.day, a.vegetarian, c.foodid, b.mealtype, carb
from dailyplantemplates a,
    dailyplantemplatemappings b,
    foods c 
where a.dailyplantemplateid = b.dailyplantemplateid
and b.foodid = c.foodid
) a
left outer join defaultfoodplans d on a.foodid = d.foodid and a.mealtype = d.mealtype and a.vegetarian = d.vegetarian
group by a.planname, a.day

union all

select a.planname, a.measuregroup, a.measurecategory, concat('Day ',a.day) as day, null as measurevalue-- convert(count(IF(E5foodGrading in ('Green','Green*'),1,null))/count(E5foodGrading), decimal(7,2)) as measurevalue
from
(
	select a.planName, 'L2' as measureGroup,'E5' as measureCategory, b.day, a.vegetarian, c.foodid, b.mealtype-- , E5foodGrading
	from dailyplantemplates a,
		dailyplantemplatemappings b,
        foods c
		-- v_food_recorded_orig c 
	where a.dailyplantemplateid = b.dailyplantemplateid
	-- and b.foodid = c.foodid
) a
left outer join defaultfoodplans d on a.foodid = d.foodid and a.mealtype = d.mealtype and a.vegetarian = d.vegetarian
group by a.planname, a.day