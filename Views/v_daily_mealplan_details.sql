CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `aravindan`@`%` 
    SQL SECURITY DEFINER
VIEW `v_daily_mealplan_details` AS
    SELECT 
        `bi_daily_mealplan_detail`.`planname` AS `planname`,
        `bi_daily_mealplan_detail`.`mealtype` AS `mealtype`,
        `bi_daily_mealplan_detail`.`measuregroup` AS `measuregroup`,
        `bi_daily_mealplan_detail`.`measurecategory` AS `measurecategory`,
        `bi_daily_mealplan_detail`.`foodlabel` AS `foodlabel`,
        `bi_daily_mealplan_detail`.`E5foodGrading` AS `E5foodGrading`,
        `bi_daily_mealplan_detail`.`food_measure` AS `food_measure`,
        `bi_daily_mealplan_detail`.`quantity` AS `quantity`,
        `bi_daily_mealplan_detail`.`day` AS `day`,
        `bi_daily_mealplan_detail`.`measurevalue` AS `measurevalue`
    FROM
        `bi_daily_mealplan_detail`