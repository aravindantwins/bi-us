-- set collation_connection = 'latin1_swedish_ci';

alter view `v_muscle_metabolism_analysis` as
select
    `s1`.`clientid` as `clientid`,
    `s1`.`patientName` as `patientName`,
    `s1`.`clientType` as `clientType`,
    s1.customerName,
    s1.TA_start_date,
    `s1`.`treatmentDays` as `treatmentDays`,
    `s1`.`day0` as `day0`,
    `s1`.`day30` as `day30`,
    `s1`.`currentDate` as `currentDate`,
    `s1`.`endDate` as `endDate`,
    (case
        when (`s1`.`endDate` is null) then 'Yes'
        else 'No'
    end) as `Active_Member`,
    `w`.`rct_bloodwork_date` as `rct_bloodwork_date`,
    `s1`.`activity_cur_compliance` as `activity_cur_compliance`,
    `s1`.`activity_total_days` as `activity_total_days`,
    `s1`.`activity_d30_compliance` as `activity_d30_compliance`,
    `s1`.`activity_d30_days` as `activity_d30_days`,
    max((case when (`s1`.`day0` = `s2`.`measure_event_date`) then `s2`.`cgm_5d` else null end)) as `d0_cgm_5d`,
    max((case when (`s1`.`day30` = `s2`.`measure_event_date`) then `s2`.`cgm_5d` else null end)) as `d30_cgm_5d`,
    max((case when (`s1`.`currentDate` = `s2`.`measure_event_date`) then `s2`.`cgm_5d` else null end)) as `current_cgm_5d`,
    max((case when (`s1`.`day0` = `s2`.`measure_event_date`) then (`s2`.`sleep_duration_minutes_5d` / 60) else null end)) as `d0_sleep_5d`,
    max((case when (`s1`.`day30` = `s2`.`measure_event_date`) then (`s2`.`sleep_duration_minutes_5d` / 60) else null end)) as `d30_sleep_5d`,
    max((case when (`s1`.`currentDate` = `s2`.`measure_event_date`) then (`s2`.`sleep_duration_minutes_5d` / 60) else null end)) as `current_sleep_5d`,
    max((case when (`s1`.`day0` = `s2`.`measure_event_date`) then `s2`.`weight_5d` else null end)) as `d0_weight_5d`,
    max((case when (`s1`.`day30` = `s2`.`measure_event_date`) then `s2`.`weight_5d` else null end)) as `d30_weight_5d`,
    max((case when (`s1`.`currentDate` = `s2`.`measure_event_date`) then `s2`.`weight_5d` else null end)) as `current_weight_5d`,
    max((case when (`s1`.`day0` = `h`.`eventdate`) then `h`.`avg_rest_hr_5d` else null end)) as `d0_rhr_5d`,
    max((case when (`s1`.`day30` = `h`.`eventdate`) then `h`.`avg_rest_hr_5d` else null end)) as `d30_rhr_5d`,
    max((case when (`s1`.`currentDate` = `h`.`eventdate`) then `h`.`avg_rest_hr_5d` else null end)) as `current_rhr_5d`,
    max((case when (`s1`.`currentDate` = `s2`.`measure_event_date`) then ((46.7 + `s2`.`cgm_5d`) / 28.7) else null end)) as `current_cgm_5d_EA1C`,
    max((case when (`s1`.`currentDate` = `s2`.`measure_event_date`) then ((46.7 + `s2`.`cgm_60d`) / 28.7) else null end)) as `current_cgm_60d_EA1C`,
    max((case when (`s1`.`day0` = `s2`.`measure_event_date`) then `s2`.`is_MetOnly_by_OPS` else null end)) as `d0_Reversal_Status_Met_Only`,
    max((case when (`s1`.`day30` = `s2`.`measure_event_date`) then `s2`.`is_MetOnly_by_OPS` else null end)) as `d30_Reversal_Status_Met_Only`,
    max((case when (`s1`.`currentDate` = `s2`.`measure_event_date`) then `s2`.`is_MetOnly_by_OPS` else null end)) as `Current_Reversal_Status_Met_Only`,
    max((case when (`s1`.`day0` = `s2`.`measure_event_date`) then `s2`.`is_InReversal` else null end)) as `d0_Reversal_Status_No_Med`,
    max((case when (`s1`.`day30` = `s2`.`measure_event_date`) then `s2`.`is_InReversal` else null end)) as `d30_Reversal_Status_No_Med`,
    max((case when (`s1`.`currentDate` = `s2`.`measure_event_date`) then `s2`.`is_InReversal` else null end)) as `Current_Reversal_Status_No_Med`,
    max((case when (`s1`.`day0` = `s2`.`measure_event_date`) then `s2`.`BMI` else null end)) as `d0_BMI`,
    max((case when (`s1`.`day30` = `s2`.`measure_event_date`) then `s2`.`BMI` else null end)) as `d30_BMI`,
    max((case when (`s1`.`currentDate` = `s2`.`measure_event_date`) then `s2`.`BMI` else null end)) as `Current_BMI`,
    `w`.`rct_bloodwork_HBA1C` as `rct_bloodwork_HBA1C`
from
    ((((
    select
        `a`.`clientId` as `clientid`,
        `a`.`patientName` as `patientName`,
        `a`.`source_2` as `clientType`,
        a.customerName,
        a.programStartDate_analytics as TA_start_date,
        `dc`.`treatmentDays` as `treatmentDays`,
        `ct`.`startDate` as `day0`,
        `ct`.`endDate` as `endDate`,
        (`ct`.`startDate` + interval 30 day) as `day30`,
        cast(now() as date) as `currentDate`,
        sum(if((`t`.`status` = 'Finished'), 1, 0)) as `activity_cur_compliance`,
        count(distinct `t`.`scheduledTime`) as `activity_total_days`,
        sum(if(((`t`.`status` = 'Finished') and (cast(`t`.`scheduledTime` as date) <= (`ct`.`startDate` + interval 30 day))), 1, 0)) as `activity_d30_compliance`,
        sum(if((cast(`t`.`scheduledTime` as date) <= (`ct`.`startDate` + interval 30 day)), 1, 0)) as `activity_d30_days`
    from
        (((`v_client_tmp` `a`
    join `dim_client` `dc` on
        (((`a`.`clientId` = `dc`.`clientid`)
            and (`dc`.`is_row_current` = 'y'))))
    join `clienttodoschedules` `ct` on
        (((`a`.`clientId` = `ct`.`clientId`)
            and (`ct`.`category` = 'activity')
                and (`ct`.`type` = 'MUSCLE_EXERCISE')
                    and (`ct`.`status` = 'active'))))
    left join `clienttodoitems` `t` on
        (((`a`.`clientId` = `t`.`clientId`)
            and (`t`.`scheduledTime` >= `ct`.`startDate`)
                and (`t`.`scheduledTime` <= now())
                    and (`t`.`category` = 'ACTIVITY')
                        and (`t`.`type` = 'MUSCLE_EXERCISE'))))
    where
        (`a`.`status` = 'Active')
    group by
        `a`.`clientId`,
        `a`.`patientName`,
        `a`.`source_2`,
        `dc`.`treatmentDays`,
        `ct`.`startDate`,
        `ct`.`endDate`) `s1`
left join `bi_patient_measures` `s2` on
    (((`s1`.`clientid` = `s2`.`clientid`)
        and ((`s1`.`day0` = `s2`.`measure_event_date`)
            or (`s1`.`day30` = `s2`.`measure_event_date`)
                or (`s1`.`currentDate` = `s2`.`measure_event_date`)))))
left join `bi_patient_daily_heartrate_summary` `h` on
    (((`s1`.`clientid` = `h`.`clientid`)
        and ((`s1`.`day0` = `h`.`eventdate`)
            or (`s1`.`day30` = `h`.`eventdate`)
                or (`s1`.`currentDate` = `h`.`eventdate`)))))
left join (
    select
        `a`.`clientid` as `clientId`,
        cast(`a`.`eventtime` as date) as `rct_bloodwork_date`,
        `a`.`veinHba1c` as `rct_bloodwork_HBA1C`
    from
        (`clinictestresults` `a`
    join (
        select
            `clinictestresults`.`clientid` as `clientId`,
            max(`clinictestresults`.`eventtime`) as `eventTime`
        from
            `clinictestresults`
        group by
            `clinictestresults`.`clientid`) `b` on
        (((`a`.`clientid` = `b`.`clientId`)
            and (`a`.`eventtime` = `b`.`eventTime`))))) `w` on
    ((`s1`.`clientid` = `w`.`clientId`)))
group by
    `s1`.`clientid`,
    `s1`.`patientName`,
    `s1`.`clientType`,
    `s1`.`treatmentDays`,
    `s1`.`day0`,
    `s1`.`day30`,
    `s1`.`currentDate`,
    `s1`.`endDate`,
    `s1`.`activity_cur_compliance`,
    `s1`.`activity_total_days`,
    `s1`.`activity_d30_compliance`,
    `s1`.`activity_d30_days`,
    `w`.`rct_bloodwork_date`